<?php
class PollsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'Polls';
	var $uses = array('Poll');
	var $layout = 'default';
	
	
	function index($id = null,$odpoved=null){
		$this->set('fastlinks',array());
		$this->set('poll_id',$id);
		
		$fields = array(
			'Poll.id',
			'Poll.name',
			'Poll.type',
			'Poll.data',
		);
		$conditions = array(
			'status'=>1,
			'kos'=>0,
			'id'=>$id,
		);
		
		$poll = $this->Poll->find('first',array('conditions'=>$conditions,'fields'=>$fields));
		
		$this->loadModel('PollVote');
		$delete_votes = $this->PollVote->deleteAll(array('created <'=>date('Y-m-d')));
		
		$vote = $this->PollVote->find('first',array('conditions'=>array('ip'=>$_SERVER['REMOTE_ADDR'],'poll_id'=>$id,'created'=>date('Y-m-d'))));
		if ($vote){
			$novote = array();
			$this->set('novote',$novote[$id] = true);
		}
			
		if ($odpoved != null){
			
			if (!isset($novote)){
			
			list($answer,$hlasu) = explode('|',$poll['Poll']['data'][$odpoved]);
			$value = array();
			$hlasu = $hlasu+1;
			$poll['Poll']['data'][$odpoved] = $answer.'|'.$hlasu;
			
			$save_poll = array(
				'id'=>$id,
				'data'=>$poll['Poll']['data'],
			);
			$this->Poll->save($save_poll);
			$this->Poll->id = null;
			
			$vote = array(
				'ip'=>$_SERVER['REMOTE_ADDR'],
				'poll_id'=>$id,
			);
			$this->PollVote->save($vote);
			
			$this->set('novote',$novote[$id] = true);
			
			
			
			} else {
				die('jiz hlasovano');
			}
		}
		
		
		
		$this->set('poll',$poll);
		$this->render('../elements/polls/index');
		
		
	}
	/*
	function vote($id,$odpoved){
		$con
		$poll = $this->Poll->find('first',array('conditions'=>$conditions,'fields'=>$fields));
		
	}
	*/
	
}	
?>