<?php
class GuestbooksController extends AppController {
	var $components  = array('RequestHandler','Email');
	var $name = 'Guestbooks';
	var $uses = array('Guestbook');
	var $layout = 'default';
	
	
	
	function index($id=null){
			
		$this->set('form_id',$id);	
		if (empty($this->data)){	
			/*
			 * Podminka a fields
			 */	
			$condition=array( 
				'Guestbook.kos'=>0,
				'Guestbook.status'=>1,
				'Guestbook.guestbook_group_id'=>$id,
			);
			$field=array(
				'Guestbook.name',
				'Guestbook.text',
				'Guestbook.created',
			);
			
			/*
			 * Nastaveni pagination
			 */
			$this->Pagination->ajaxDivUpdate = 'items';
			$this->Pagination->show = 10;
			$this->Pagination->sortBy = 'created';
			$this->Pagination->direction = 'DESC';
			$this->Pagination->url = '/'.$this->params['url']['url'].'/';
			list($order,$limit,$page) = $this->Pagination->init($condition,null,array('modelClass'=>'Guestbook'));	
				
			/*
			 * search in DB
			 */

			$guestbook_all = $this->Guestbook->find('all',array('conditions'=>$condition,'fields'=>$field,'order'=>$order,'limit'=>$limit,'page'=>$page));
			$this->set('guestbook_all',$guestbook_all); 
			
			$this->set('page_caption','Seznamka');
			$this->set('fastlinks',array('Seznamka'=>'#'));
			
		
		} else {
			if ($this->data['Guestbook']['spam'] != 123) die('Jste SPAM');
			$this->Guestbook->save($this->data);
			$this->Guestbook->id = null;
			
			$condition=array( 
				'Guestbook.kos'=>0,
				'Guestbook.status'=>1,
				'Guestbook.guestbook_group_id'=>$this->data['Guestbook']['guestbook_group_id'],
			);
			$field=array(
				'Guestbook.name',
				'Guestbook.text',
				'Guestbook.created',
			);
			
			/*
			 * Nastaveni pagination
			 */
			$this->Pagination->ajaxDivUpdate = 'items';
			$this->Pagination->show = 10;
			$this->Pagination->sortBy = 'created';
			$this->Pagination->direction = 'DESC';
			$this->Pagination->url = '/'.$this->params['url']['url'].'/';
			list($order,$limit,$page) = $this->Pagination->init($condition,null,array('modelClass'=>'Guestbook'));	
				
			/*
			 * search in DB
			 */

			$guestbook_all = $this->Guestbook->find('all',array('conditions'=>$condition,'fields'=>$field,'order'=>$order,'limit'=>$limit,'page'=>$page));
			$this->set('guestbook_all',$guestbook_all);
		
	
		}
		if ($this->RequestHandler->isAjax()){
			$this->render('../guestbooks/items');	
		} else{
			$this->render('../guestbooks/index');
		}	
	
	
	}
	
	
}	
?>