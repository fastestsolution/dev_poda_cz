<?php
class ArticlesController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'Articles';
	var $uses = array('Article');
	var $layout = 'default';
	var $menu_item_list = array();
	var $article_field = array(
		'Article.id',
		'Article.javascript',
		'Article.name',
		'Article.text',
		'Article.keywords',
		'Article.alias_'
	);

	/* find menu */
	function findMenu($data,$parent = ''){
		foreach ($data as $key=>$val){
			$this->menu_item_list[$val['MenuItem']['id']] = $parent.'/'.$val['MenuItem']['alias_'];
			if (!empty($val['MenuItem']['name']))
				$this->menu_item_list[$val['MenuItem']['id']] = empty($val['MenuItem']['spec_url'])?$parent.'/'.$val['MenuItem']['alias_']:$val['MenuItem']['spec_url'];
			if(isset($val['children'][0])){
				$this->findMenu($val['children'],$parent.'/'.$val['MenuItem']['alias_']);
				
			} 
		}	
	}
	
	function view($alias_typu=null,$id_typu=null){
		$arguments = array();	
		for ($i=0;$i<func_num_args();$i++) $arguments[] = func_get_arg($i);
		/*
		$this->menu_item = am($this->menu_item,$this->menu_item2);
		$this->menu_item = am($this->menu_item,$this->menu_item3);
		*/
		$this->findMenu($this->menu_item);
		
		$menu_array = array_flip($this->menu_item_list);
		
		
		$url = '/'.implode('/',$arguments);
		//pr($menu_array);die();
		
		
		if (isset($menu_array[$url])){
			$id = $menu_array[$url];
		} else {
			$this->error_404();
			die();
		}
		$this->loadModel('ConnectionMenuArticle');
		$cm = $this->ConnectionMenuArticle->find('list',array('conditions'=>array('ConnectionMenuArticle.menu_item_id'=>$id), 'fields'=>array('id','article_id')));
		$article_ids = array_values($cm);
		
		if (isset($this->params['url']['data']['search_fulltext'])){
			$search_fulltext = $this->params['url']['data']['search_fulltext'];
			$condition_find = array(
				'or'=>array(
					'Article.name LIKE'=>'%'.$search_fulltext.'%',
					'Article.text LIKE'=>'%'.$search_fulltext.'%',
				),
				'Article.status'=>1,'Article.kos'=>0);
		
		} else {
			$condition_find = array('Article.id'=>$article_ids,'Article.status'=>1,'Article.kos'=>0);
		}
		
		
		
		// overeni zda neni status 0
		$article_tmp= $this->Article->find('list',array('conditions'=>$condition_find, 'fields'=>array('id')));
		$article_ids = array_values($article_tmp);
		//pr($article_ids);
		
		// pokud je search nenalezeno
		if (isset($search_fulltext) && count($article_ids)==0){
			$this->no_search_result($search_fulltext);return;
		// pokud je jeden clanek
		} elseif (count($article_ids) == 1){
			$this->detail('from_menu',$article_ids[0],$id);
		
		// pokud je vice clanku
		} else {		
			
	
			
			$condition = array(
				'Article.id'=>$article_ids,
				'Article.status'=>	1,
				'Article.kos'	=>	0,
				//'1=1 GROUP BY Article.id'
			);
				
			$this->Pagination->ajaxDivUpdate = 'items';
			$this->Pagination->show = 20;
			$this->Pagination->sortBy = 'updated';
			$this->Pagination->sortByClass = 'Article';
			$this->Pagination->direction = 'DESC';
			$this->Pagination->url = '/'.$this->params['url']['url'];
			
			list($order,$limit,$page) = $this->Pagination->init($condition,$this->article_field,array('modelClass'=>'Article'));			
			
			$articles_list_all = $this->Article->find('all',array('conditions'=>$condition,'fields'=>$this->article_field,'order'=>$order,'limit'=>$limit,'page'=>$page));
			
			if ($articles_list_all==null){$this->error_404();return;}
			
			$this->set('articles_list_all',$articles_list_all);
			
			if ($this->RequestHandler->isAjax()){
				$this->autoLayout = false;
				$this->set('isAjax',true);
				$this->render('../articles/index_items');	
			} else {
				// START load kategorie info
				$menu = $this->MenuItem->read(array('MenuItem.name'),$id);
				
				if (isset($search_fulltext))
					$this->set('page_caption',lang_vysledky_vyhledavani_vyrazu.': '.$search_fulltext);
				else	
					$this->set('page_caption',ucfirst($menu['MenuItem']['name']));
				
				
				$path = $this->MenuItem->getpath($id,array('MenuItem.name','MenuItem.alias_','MenuItem.title'));
				
				$c = array(); $p = '';
				
				foreach($path as $ar){
					if (!empty($ar['MenuItem']['title']))
						$c[$ar['MenuItem']['title']] = $p.'/'.$ar['MenuItem']['alias_'];
					else
						$c[$ar['MenuItem']['name']] = $p.'/'.$ar['MenuItem']['alias_'];
					$p = '/'.$ar['MenuItem']['alias_'];
				}
				end($c);
				$c[key($c)] = '#';
				
				if (isset($search_fulltext))
					$this->set('fastlinks', array(lang_vyhledavani=>'#'));
				else {
					$this->set('fastlinks', $c);
				}
						
				// END load kategorie info
				
				$this->render('../articles/index');
			}	
		}
	}
	/**
	* no search result
	*/
	function no_search_result($search){
		
		$this->set('page_caption',lang_vysledky_vyhledavani_vyrazu.': '.$search)	;
		$this->set('fastlinks', array(lang_vyhledavani=>'#'));
		$this->render('no_search_result');
	}
	
	function search_article() {
		$this->loadModel('ConnectionMenuArticle');
		$cm = $this->ConnectionMenuArticle->find('list',array('conditions'=>array('ConnectionMenuArticle.menu_item_id'=>$id), 'fields'=>array('id','article_id')));
		$article_ids = array_values($cm);
		
		if (isset($this->params['url']['data']['search_fulltext'])){
			$search_fulltext = $this->params['url']['data']['search_fulltext'];
			$condition_find = array(
				'or'=>array(
					'Article.name LIKE'=>'%'.$search_fulltext.'%',
					'Article.text LIKE'=>'%'.$search_fulltext.'%',
				),
				'Article.status'=>1,'Article.kos'=>0);
		
		} else {
			$condition_find = array('Article.id'=>$article_ids,'Article.status'=>1,'Article.kos'=>0);
		}
	}
	
	/*
	* DETAIL CLANKU
	*/
	function detail($alias=null,$id=null, $kategorie_id=null){
		$this->cacheAction = true;
		switch ($alias){
			case 'special_prohlaseni':
				$article = $this->Article->find('first',
				array(
					'conditions'=>array('Article.spec_type'=>1),
					'fields'=>array(
							'Article.id',
							'Article.name',
							'Article.alias_',
							'Article.text',
							'Article.description',
							'Article.keywords',
							'Article.created',
							'Article.updated',
					)
				));
			break;
		
			default:
				$article = $this->Article->find(
					'first',
					array(
						'fields'=>array(
							'Article.name',
							'Article.text',
							'Article.keywords',
							'Article.description',
							'Article.alias_',
							'Article.fotogaleries',
							'Article.addons',
						),
						'conditions'=>array('Article.id'=>$id,'Article.status'=>1,'Article.kos'=>0),
					)
				);
			break;
		}
		// pokud neni article error
		if (!$article) $this->error_404();
		
		// meta tags
		App::import('Helper', 'Fastest');
		$fastest = new FastestHelper(); 
		$this->keywords_data = array(
			'text'=>$fastest->orez($article['Article']['text'],100),
			'keywords'=>$article['Article']['keywords'],
			'description'=>$article['Article']['description'],
		);		
		$this->set('article',$article);
		
		// pokud je kategorie
		if ($kategorie_id != null){
			$path = $this->MenuItem->getpath($kategorie_id,array('MenuItem.name','MenuItem.alias_','MenuItem.title'));
			//pr($path);
			$c = array(); $p = '';
			foreach($path as $key=>$ar){ //pr($ar);
				if($key == 0) {
					if (!empty($ar['MenuItem']['title']))
						$c[ucfirst($ar['MenuItem']['title'])] = $ar['MenuItem']['alias_'];
					else
						$c[ucfirst($article['Article']['name'])] = $ar['MenuItem']['alias_'];
				} else {
					if (!empty($ar['MenuItem']['title']))
						$c[ucfirst($ar['MenuItem']['title'])] = $p.'/'.$ar['MenuItem']['alias_'];
					else
						$c[ucfirst($article['Article']['name'])] = $p.'/'.$ar['MenuItem']['alias_'];

				}
					
				$last = ucfirst($ar['MenuItem']['title']);			
				$p = $ar['MenuItem']['alias_'];
				//pr($c);
			}
			//end($c);
			$c[$last] = '#';
			
			$this->set('fastlinks', $c);
            $this->set('page_caption',ucfirst($article['Article']['name']));
			
		
		} else {
			$this->set('page_caption',ucfirst($article['Article']['name']));
			$this->set('fastlinks', array(ucfirst($article['Article']['name'])=>'#'));
		}
		
		
		$this->render('detail');
		
	}
	
	
	
	
	
	function sitemap(){
		$this->set('page_caption',lang_mapa_stranek);
		$this->set('fastlinks', array(lang_mapa_stranek=>'#'));
	
	}
	
	function article_fotogalerie($id){
		$this->loadModel('Fotogalerie');
		$galery = $this->Fotogalerie->find('first',array('conditions'=>array('id'=>$id,'status'=>1,'kos'=>0)));
		$galery['imgs'] = $galery['Fotogalerie']['imgs'];
		$galery['name'] = $galery['Fotogalerie']['name'];
		$this->set('galery',$galery);
		$this->render('elements/fotogalerie');
	}
	
	
}	
?>