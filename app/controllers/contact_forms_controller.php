<?php
class ContactFormsController extends AppController {
	var $components  = array('RequestHandler','Email');
	var $name = 'ContactForms';
	var $uses = array('ContactForm');
	var $layout = 'default';
	
	
	
	function index($id=null){
		$this->set('form_id',$id);
		$this->loadModel('ContactFormGroup');
		$form_data = $this->ContactFormGroup->find('first',array('conditions'=>array('ContactFormGroup.id'=>$id,'kos'=>0,'status'=>1)));
		//pr($this->data);
		if (empty($this->data)){	
			$this->set('form_data',$form_data);
			$this->set('page_title','');
			$this->set('fastlinks',array());
			
		
		} else {
			//if ($this->data['ContactForm']['spam'] != 123) die('Jste spam');
			$admin_email = $this->setting['contact_forms']['email_from'];
			$admin_name = $this->setting['contact_forms']['email_name'];
			$email_podpis = $this->setting['email_podpis'];
			
			if(isset($this->data['ContactForm']['name']) && !empty($this->data['ContactForm']['name'])) {
				$from_email = $this->data['ContactForm']['name'];
				$from_name = $this->data['ContactForm']['name'];
			} else {
				$from_email = 'info@poda.cz';
				$from_name = 'PODA';
			}
			
			
			
			$email_data = '<br /><table width="60%">';
				$email_data .='<tr><td>Email:</td><td>'.(!empty($this->data['ContactForm']['name'])?$this->data['ContactForm']['name']:'Nevyplněno').'</td></tr>';
				// vypis  variabilnich hodnot
				foreach ($this->data['ContactForm']['text'] AS $key=>$fd){
					if ($fd['type']==4)
						$email_data .='<tr><td>'.$fd['name'].':</td><td>'.(($fd['value']==1)?'Ano':'Ne').'</td></tr>';
					else
						$email_data .='<tr><td>'.$fd['name'].':</td><td>'.(!empty($fd['value'])?$fd['value']:'Nevyplněno').'</td></tr>';
				}
				
			$email_data .= '<table>';
			
			$this->data['ContactForm']['domena'] = '<a href="http://'.$_SERVER['HTTP_HOST'].'/">http://'.$_SERVER['HTTP_HOST'].'/</a>';
			$this->data['ContactForm']['text'] = $email_data;
			$this->data['ContactForm']['podpis'] = $email_podpis;
			
			
			
			$save_form = array(
				'name'=>(isset($this->data['ContactForm']['name']) && !empty($this->data['ContactForm']['name'])?$this->data['ContactForm']['name']:'PODA'),
				'text'=>$email_data,
				'contact_form_group_id'=>$form_data['ContactFormGroup']['id'],
			);
			$this->ContactForm->save($save_form);
			$this->ContactForm->id = null;
			
			
			if (!$this->Email->send_from_template($form_data['ContactFormGroup']['template'],$from_name,$from_email,null,array($admin_email)))
				die(json_encode(array('r'=>false,'m'=>lang_chyba_odeslani_formulare)));
				
		
		
			
		if (!empty($this->data['ContactForm']['name']) && !empty($form_data['ContactFormGroup']['autoreply'])){
			$email_list = array($this->data['ContactForm']['name']);
			$this->Email->send_from_template($form_data['ContactFormGroup']['autoreply'],$admin_name,$admin_email,null,$email_list);
		}
		 
		
		die(json_encode(array('r'=>true,'m'=>lang_formular_byl_odeslan,'clear'=>true)));
	
	}
			
	
	
	}
	
	
}	
?>