<?php
class GoogleMapsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'GoogleMaps';
	var $uses = array('GoogleMap');
	var $layout = 'default';
	
	
	function index($id = null){
		$this->set('fastlinks',array());
		$this->set('map_id',$id);
		
		$fields = array(
			'GoogleMap.id',
			'GoogleMap.name',
			'GoogleMap.ulice',
			'GoogleMap.mesto',
			'GoogleMap.psc',
			'GoogleMap.lat',
			'GoogleMap.lng',
		);
		$conditions = array(
			'status'=>1,
			'kos'=>0,
			'google_map_group_id'=>$id,
		);
		$limit = 100;
		
		$maps = $this->GoogleMap->find('all',array('conditions'=>$conditions,'fields'=>$fields,'order'=>'name ASC'));
		
		$maps_data = json_encode($maps);
		$this->set('maps_data',$maps_data);
		
		
		if (isset($this->params['url']['load'])){
			//$maps = $this->GoogleMap->find('all',array('conditions'=>$conditions,'fields'=>$fields,'limit'=>$limit,'order'=>'name ASC'));
			//die(json_encode(array('result'=>true,'data'=>$maps)));
		}
		
	}
	
}	
?>