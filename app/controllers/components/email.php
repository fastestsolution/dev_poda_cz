<?php 
App::import('Vendor', 'phpmailer', array('file' => 'phpmailer'.DS.'class.phpmailer.php')); 

class BBCode {
	function replace_text($text_original, $data= array(), $from = 'data'){
		if ($from == 'data')
			preg_match_all('/##(.*)##/ismU',$text_original,$text2);
		else {
			preg_match_all('/##'.$from.'.(.*)##/ismU',$text_original,$text2);
		}
		foreach($text2[1] as $key=>$item){
			if (strpos($item,'.')){
				list($class, $col,) = explode('.',$item);
				if (isset($data[$from][$class][$col])){
					//pr($data[$from][$class][$col]);
					if (empty($data[$from][$class][$col])){
						$text = 'Nevyplněno';
					} else {
						$text = $data[$from][$class][$col];
					}
					//pr($text);
					//$text2[1][$key]	= ((!empty($data[$from][$class][$col]) || $data[$from][$class][$col]==0 || $data[$from][$class][$col]=='')?$data[$from][$class][$col]:'Nevyplněno');
					$text2[1][$key]	= $text;
				} else {
					$text2[1][$key] = 'Nevyplněno';
				}
			}
		}
		//pr($text2);
		return str_replace($text2[0], $text2[1], $text_original);
	}
	
	function get_text($text, $data){
		preg_match_all('/##FOREACH(.*)ENDFOREACH##/ismU',$text,$text2);
		//pr($data['replace_list'] );
		foreach($text2[1] as &$foreach_item):
			$position_close_zav = strpos($foreach_item,'}');
			if ($position_close_zav){
				$text_to_foreach = substr($foreach_item, $position_close_zav + 1);
				list($variable, $sub)  = explode('|',trim(substr($foreach_item,1,$position_close_zav-1)));
				if (isset($data['replace_list'] [$variable])):
					$foreach_item = '';
					foreach($data['replace_list'][$variable] as $ssub){
						$foreach_item .= $this->replace_text($text_to_foreach,array($sub=>$ssub),$sub);
					}
				endif;
			}
		endforeach;
		$template = str_replace($text2[0], $text2[1], $text);
		return  $this->replace_text($template, array('data'=>$data),'data');
	}
}

class EmailComponent{
    	var $from         = 'noreply@fastest.cz';
    	var $fromName     = 'Fastest Solution';
    	var $smtpUserName = 'noreply@fastest.cz';  	// SMTP username
    	var $smtpPassword = '9YueiNRr'; 		// SMTP password
    	var $smtpHostNames=  'smtp.fastest.cz';  // specify main and backup server
    	var $text_body = null;
    	var $html_body = null;
    	var $to = null;
    	var $toName = null;
    	var $subject = null;
    	var $cc = null;
    	var $bcc = null;
    	var $template = null;
    	var $attachments = null;
    	var $layout = 'email';

    	var $controller;

    	function startup( &$controller ) {
			$this->controller = &$controller;
			$this->mail = new PHPMailer();
			$this->bbcode = new BBCode();		
    	}

    	function bodyHTML() {
    		ob_start();
			$temp_layout 				= 	$this->controller->layout;
			$this->controller->layout 	= 	$this->layout; 
			$this->controller->render($this->template); 
			$mail 					= 	ob_get_clean();
			$this->controller->layout 	= 	$temp_layout; 
			return $mail;
    	}

    	function AddStringAttachment($string, $filename, $encoding = "base64", $type = "application/octet-stream") {
    		$this->mail->AddStringAttachment($string, $filename, $encoding, $type);
    	}
	
		function addAttachFromFunc($filename= 'attach.txt', $fn = null, $layout = '', $encoding = "base64", $type = "application/octet-stream"){
			$ex = explode('/',ltrim($fn,'/'));
			$action = $ex[1];
			$params = $ex[2];
			
			ob_start();
			$temp_layout = $this->controller->layout;
			$this->controller->layout = $layout;
			$this_data = $this->controller->data;
			unset($this->controller->data);
			
			$this->controller->priloha = true;
			echo $this->controller->setAction($action,array($params));
			$attach = ob_get_contents();
			ob_end_clean();
			
			$this->controller->layout = $temp_layout; 
			$this->controller->data = $this_data;
			unset($this_data);
			$this->AddStringAttachment($attach,$filename,$encoding,$type);
		}
    	
    	function addAttachFromView($filename = 'attach.txt',$view = null, $layout = 'email',$encoding = "base64", $type = "application/octet-stream"){
    		ob_start();
			$temp_layout = $this->controller->layout;
			$this->controller->layout = $layout;  
			$this->controller->render($view); 
			$attach = ob_get_clean();
			$this->controller->layout = $temp_layout; 
			$this->AddStringAttachment($attach,$filename,$encoding,$type);
    	}
    	
    	function attach($filename, $asfile = '') {
			if (empty($this->attachments)) {
        		$this->attachments = array();
        		$this->attachments[0]['filename'] = $filename;
        		$this->attachments[0]['asfile'] = $asfile;
			} else {
        		$count = count($this->attachments);
        		$this->attachments[$count+1]['filename'] = $filename;
        		$this->attachments[$count+1]['asfile'] = $asfile;
			}
    	}
		
		
		
		function addAttachFromFtp($filename){
			$filename = ltrim($filename,'/');
			$ex = explode('/',$filename);
			$filename = $ex[count($ex)-1];
			unset($ex[count($ex)-1]);
			$path = implode('/',$ex);
			$path = strtr($path,array('uploaded'=>''));
			
			$conn_id = ftp_connect($this->ftp_host) or die("Couldn't connect to $this->ftp_host"); 
			if (@ftp_login($conn_id, $this->ftp_user, $this->ftp_heslo)) {
				$temp_file = tmpfile();
				ftp_chdir($conn_id, $path);				
				if (ftp_fget($conn_id, $temp_file, $filename, FTP_BINARY, 0)) {
					fseek($temp_file, 0);
					$fs = ftp_size($conn_id, $filename);
					$string =  fread($temp_file,$fs);
					$this->AddStringAttachment($string, $filename);
				}
			} 

			ftp_close($conn_id);  
		}

			
	function send_from_template($template_id = null,$from_name=null,$from_email=null,$email_podpis=null, $recipient_list = array(), $replace_list = array(), $attachment_list = array()){
		$this->mail->IsSMTP();            // set mailer to use SMTP
		$this->mail->SMTPAuth 	= true;     // turn on SMTP authentication
		$this->mail->Host   	= $this->smtpHostNames;
		$this->mail->Username 	= $this->smtpUserName;
		$this->mail->Password 	= $this->smtpPassword;
    	$this->mail->From     	= $from_email;
    	$this->mail->FromName 	= $from_name;
		
		foreach ($recipient_list as $to):
			$this->mail->AddAddress($to);
    	endforeach;
		
		$this->mail->CharSet  = 'UTF-8';
    	$this->mail->WordWrap = 50;  // set word wrap to 50 characters

    	if (!empty($attachment_list)) {
			foreach ($attachment_list as $attachment) {
				if (empty($attachment['asfile'])) {
					$this->mail->AddAttachment($attachment['filename']);
				} else {
					$this->mail->AddAttachment($attachment['filename'], $attachment['asfile']);
				}
			}
    	}

    	$this->mail->IsHTML(true); 
		
		$this->mail->Body    = $this->loadTemplate($template_id);
		
		// komu to teda pujde
		$this->mail->to = array();
    	foreach ($recipient_list as $receiper):
			$this->mail->AddAddress($receiper);
			$result = $this->mail->Send();
    		$this->mail->attachment = array();
    		$this->mail->to = array();
    	endforeach;
    	if($result === false)
    		return array('result'=>false,'message'=>$this->mail->ErrorInfo);
    	else 
    		return array('result'=>true);
						
	}
	/**
	 * 
	 * @param object $template_id [optional]
	 * @return 
	 */
	function loadTemplate($template_id = null){
  		if ($template_id != null){
  			$this->controller->loadModel('EmailTemplate');
  			$template = $this->controller->EmailTemplate->read(null, $template_id);
  			if ($template){
  				$this->load_APPData();
  				$this->mail->Subject = $this->bbcode->get_text($template['EmailTemplate']['name'], $this->controller->data);
  				return $this->load_style($this->bbcode->get_text($template['EmailTemplate']['text'], $this->controller->data));
  			} else 
  				return false;	
  		}
  		return false; 
  	}	
	/**
  	 * nadefinovani ##APP.neco## pro emaily pro BBCode
  	 */
  	function load_APPData(){
		// load basic APP data for template
		//if (isset($this->controller->shop_setting)){
			$settingModel = ClassRegistry::init('Setting', 'Model');
			// zakladni setting
			$setting = $settingModel->read(array('value'),1);
			$APP = array('APP'=>array(
				'name' 			=> $setting['Setting']['value']['name'],
				'server' 		=> 'http://'.$_SERVER['HTTP_HOST'],
				'podpis' 			=> nl2br($setting['Setting']['value']['email_podpis']),
				'user_sign'		=> '',
				'date'			=> date("Y-m-d"),
				'datetime'		=> date("Y-m-d H:i:s")
			));
			
			// shop setting
			$setting = $settingModel->read(array('value'),'shop');
			if ($setting){
				$APP['APP']['banka'] = $setting['Setting']['value']['ucet_banka'];
				$APP['APP']['cislo_uctu'] = $setting['Setting']['value']['ucet'];
			}
			
			$this->controller->data = am($this->controller->data, $APP); 
		//}
  	}
	/**
	 * 
	 * @param object $html
	 * @param object $out [optional]
	 * @return 
	 */
	function load_style($html, $out = ''){
  		$out .= '<html>';
  		$out .= '<head>';
  		$out .= '<style>';
		$out .= 'body {font-family:Tahoma, Arial; font-size:13px; width:700px;}';  	
		$out .= 'p {font-size:13px}';  	
		$out .= 'h2 {font-size:16px}';  	
		$out .= 'h3 {font-size:14px}';  	
		$out .= 'table {border: 1px solid #CCC; width:700px}';  	
		$out .= 'table th {background:#AAAAAA; text-align:left;vertical-align:top;color:#FFFFFF;}';  	
		$out .= 'table td,table th {font-size:13px;}';  	
		$out .= '</style>';
		$out .= '</head>';
		$out .= '<body>';
		$out .= $html;
		$out .= '</body>';
		$out .= '</html>';
		return $out;
  	}
	
	function send(){
		 $this->mail->IsSMTP();            // set mailer to use SMTP
		 $this->mail->SMTPAuth = true;     // turn on SMTP authentication
		 $this->mail->Host   = $this->smtpHostNames;
		 $this->mail->Username = $this->smtpUserName;
		 $this->mail->Password = $this->smtpPassword;
    	 $this->mail->From     = $this->from;
    	 $this->mail->FromName = $this->fromName;
			
			foreach ($this->to as $to):
				$this->mail->AddAddress($to);
    		endforeach;
    		
			$this->mail->AddReplyTo($this->from, $this->fromName );

    		$this->mail->CharSet  = 'UTF-8';
    		$this->mail->WordWrap = 50;  // set word wrap to 50 characters

    		if (!empty($this->attachments)) {
				foreach ($this->attachments as $attachment) {
						if (empty($attachment['asfile'])) {
						$this->mail->AddAttachment($attachment['filename']);
						} else {
						$this->mail->AddAttachment($attachment['filename'], $attachment['asfile']);
						}
				}
    		}

    		$this->mail->IsHTML(true);  // set email format to HTML

    		$this->mail->Subject = $this->subject;
    		$this->mail->Body = '<<style>body {font-size:13px;text-align:left;} table {font-size:13px;text-align:left;}</style><p>vdfs</p><hr />';
			//$this->mail->Body    = $this->bodyHTML;
    		//$this->mail->AltBody = $this->bodyText();

    		$result = $this->mail->Send();
    		$this->mail->attachment = array();

    		if($result == false ) 
    			$result = $this->mail->ErrorInfo;
    		return $result;
  	}
}
?>