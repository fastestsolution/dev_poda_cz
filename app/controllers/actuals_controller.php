<?php
class ActualsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'Actuals';
	var $uses = array('Actual');
	var $layout = 'default';
	var $test = '';
	
	
	function index($year = null){
		/*
		 * Podminka a fields
		 */	
		if($year == null)
			$year = date('Y');
		 
		 if($year <= 1997) {
			$condition=array(
				'Actual.kos'=>0,
				'Actual.status'=>1,
				'Actual.actual_group_id'=>1,
				'YEAR(Actual.created) <=' => date('Y') - 5
			); 
		 } else {
			$condition=array(
				'Actual.kos'=>0,
				'Actual.status'=>1,
				'Actual.actual_group_id'=>1,
				'YEAR(Actual.created)' => $year
			); 
		 }
		 
		$field=array(
			'Actual.name',
			'Actual.alias_',
			'Actual.text',
			'Actual.perex',
			'Actual.id',
			'Actual.imgs',
			'Actual.created',
			'ActualGroup.name',
		);
		
		/*
		 * Nastaveni pagination
		 */
		$this->Pagination->ajaxDivUpdate = 'items';
		$this->Pagination->show = 100;
		$this->Pagination->sortBy = 'created';
		$this->Pagination->direction = 'DESC';
		$this->Pagination->url = '/'.$this->params['url']['url'].'/';
		list($order,$limit,$page) = $this->Pagination->init($condition,null,array('modelClass'=>'Actual'));	
			
		/*
		 * search in DB
		 */

		$this->Actual->bindModel(array(
        	'belongsTo'=>array(
				'ActualGroup'=>array(),
			)
        ));
		$actuals_list_all = $this->Actual->find('all',array('conditions'=>$condition,'fields'=>$field,'order'=>$order,'limit'=>$limit,'page'=>$page));
		$this->set('actuals_list_all',$actuals_list_all);

		$this->set('page_caption','Novinky');			
		$this->set('fastlinks',array(
			'Novinky' => '#'
			//$actuals_list_all[0]['ActualGroup']['name'] =>	'#',
			)
		);
		
		$this->set('scripts',array('http://scripts.fastesthost.cz/js/clearbox/clearbox.js'));
		
		if ($this->RequestHandler->isAjax()){
			$this->render('../actuals/index_items');	
		} else{
			$this->render('../actuals/index');
		}
	}
	
	function detail($alias=null,$id=null){
		include 'select_config.php';
		$actual_field = array();
		
		$this->Actual->bindModel(array(
        	'belongsTo'=>array(
				'ActualGroup'=>array(),
			)
        ));
		$actual = $this->Actual->find(
			'first',
			array(
				'conditions' => array('Actual.id'=>$id,'Actual.status'=>1,'Actual.kos'=>0),
				'fields' => array(
					'Actual.name',
					'Actual.alias_',
					'Actual.text',
					'Actual.actual_group_id',
					'Actual.id',
					'Actual.created',
					'Actual.updated',
					'Actual.keywords',
					'Actual.imgs',
					'Actual.description',
					'ActualGroup.name',
					'ActualGroup.id',
					'ActualGroup.alias_',
					'Actual.fotogaleries',
				) 
			)
		);
		if (!$actual) $this->error_404();
		
		
		$this->set('scripts',array('http://scripts.fastesthost.cz/js/clearbox/clearbox.js'));
		
		
		$this->set('page_caption',ucfirst($actual['Actual']['name']));
		$this->set('fastlinks',array(
			'Novinky' =>	actuals_link.'/'.date('Y', strtotime($actual['Actual']['created'])), 
			ucfirst($actual['Actual']['name']) =>	'#', 
			)
		);
		
		
		
		// meta tags
		App::import('Helper', 'Fastest');
		$fastest = new FastestHelper(); 
		$this->keywords_data = array(
			'text'=>$fastest->orez($actual['Actual']['text'],100),
			'keywords'=>$actual['Actual']['keywords'],
			'description'=>$actual['Actual']['description'],
		);		

		$this->set('actual',$actual);
		
		
		
		
	}
	
	
	function actuals_box($id){
		$condition=array(
			'Actual.kos'=>0,
			'Actual.status'=>1,
			'Actual.name !='=>'',
			'Actual.actual_group_id'=>$id
		);
		$field=array(
			'Actual.name',
			'Actual.alias_',
			'Actual.text',
			'Actual.id',
			'Actual.created',
			'ActualGroup.id',
			'ActualGroup.name',
		);
		

		$this->Actual->bindModel(array(
        	'belongsTo'=>array(
				'ActualGroup'=>array(),
			)
        ));
		$order = 'Actual.updated DESC';
		$limit = 6;
		$actuals_box = $this->Actual->find('all',array('conditions'=>$condition,'fields'=>$field,'order'=>$order,'limit'=>$limit));
		$this->set('actuals_box',$actuals_box);
		$this->set('page_caption','');
		$this->set('fastlinks',array());
		
		
	}
	
	function rss($id=null){
		Header("Content-Type: text/xml; charset=UTF-8");
		$this->layout = 'rss/rss';
			$actual_field = array(
				'Actual.name',
				'Actual.text',
				'Actual.alias_',
				'Actual.imgs',
				'Actual.created',
				'Actual.updated',
				'Actual.id',
				'ActualGroup.name',
			);
		$this->Actual->bindModel(array(
        	'belongsTo'=>array(
				'ActualGroup'=>array(),
			)
        ));
		$conditions = array('Actual.status'=>1,'Actual.kos'=>0,'ActualGroup.id'=>$id);
		$this->set('rss',$rss = $this->Actual->find('all',array('conditions'=>$conditions,'fields'=>$actual_field,'group'=>'Actual.updated DESC','limit'=>10)));
		
		if (!$rss) $this->redirect('/');
	}
	
	
	
}	
?>