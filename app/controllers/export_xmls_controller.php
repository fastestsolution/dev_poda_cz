<?php
class ExportXmlsController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'ExportXmls';
	var $uses = array('Article');
	var $layout = 'xml/xml';
	

	/* find menu */
	function findMenu($data,$parent = ''){
		foreach ($data as $key=>$val){
			$this->menu_item_list[$val['MenuItem']['id']] = $parent.'/'.$val['MenuItem']['alias_'];
			if (!empty($val['MenuItem']['name']))
				$this->menu_item_list[$val['MenuItem']['id']] = empty($val['MenuItem']['spec_url'])?$parent.'/'.$val['MenuItem']['alias_']:$val['MenuItem']['spec_url'];
			if(isset($val['children'][0])){
				$this->findMenu($val['children'],$parent.'/'.$val['MenuItem']['alias_']);
				
			} 
		}	
	}
	
	
	function google_sitemap(){
		
		$article_field = array(
			'Article.id',
			'Article.javascript',
			'Article.name',
			'Article.text',
			'Article.keywords',
			'Article.alias_'
		);

		//Header("Content-Type: text/xml; charset=UTF-8");
		$article_field = array('Article.updated','Article.alias_','Article.id');
		$menu_field = array('MenuItem.updated','MenuItem.id');
		$this->loadModel('MenuItem');
		$this->findMenu($this->menu_item_sitemap);
		$men = $this->MenuItem->find('list',array('conditions'=>array('MenuItem.id'=>array_keys($this->menu_item_list)),'fields'=>array('MenuItem.id','MenuItem.updated')));
		$menu = array();
		foreach($this->menu_item_list as $key => $url){
			$menu[$key] = array('updated'=>$men[$key], 'url'=>$url);
		}
		$this->set('menu',$menu);
	
	}
	
	
	
	
}	
?>