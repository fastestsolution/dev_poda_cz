<?php
class FotogaleriesController extends AppController {
	var $components  = array('RequestHandler');
	var $name = 'Fotogaleries';
	var $uses = array('Fotogalerie');
	var $layout = 'default';
	
	
	
	function index($alias=null,$id=null){
		/*
		 * Podminka a fields
		 */	
		 
		$condition=array(
			'Fotogalerie.kos'=>0,
			'Fotogalerie.status'=>1,
		);
		$field=array(
			'Fotogalerie.name',
			'Fotogalerie.alias_',
			'Fotogalerie.id',
			'Fotogalerie.imgs',
			'Fotogalerie.fotogalerie_group_id',
		);
		$order = 'Fotogalerie.poradi ASC';
		$this->loadModel('FotogalerieGroup');
		$group_list = $this->FotogalerieGroup->find('list',array('conditions'=>array('status'=>1,'kos'=>0),'fields'=>array('id','name'),'order'=>'poradi ASC'));
		$this->set('group_list',$group_list);
		
		$foto_list_all = $this->Fotogalerie->find('all',array('conditions'=>$condition,'fields'=>$field,'order'=>$order));
		$foto_list = array();
		foreach($foto_list_all AS $f){
			$foto_list[$f['Fotogalerie']['fotogalerie_group_id']][] = $f;
		}
		$this->set('foto_list',$foto_list);
		
		
			

		$this->set('page_caption',lang_fotogalerie);			
		$this->set('fastlinks',array(
			lang_fotogalerie =>	'#',
			)
		);
		
		$this->set('scripts',array(
			'http://scripts.fastesthost.cz/js/slimbox/slimbox.js',
			'/js/photo_stack/c_photo_stack.js',
		));
		
		
	}
	
	function detail($alias=null,$id=null){
		include 'select_config.php';
		$actual_field = array();
		
		
		$foto_list = $this->Fotogalerie->find(
			'first',
			array(
				'conditions' => array('Fotogalerie.id'=>$id,'Fotogalerie.status'=>1,'Fotogalerie.kos'=>0),
				'fields' => array(
					'Fotogalerie.name',
					'Fotogalerie.id',
					'Fotogalerie.imgs',
				) 
			)
		);
		if (!$foto_list) $this->error_404();
		
		$this->set('scripts',array('http://scripts.fastesthost.cz/js/slimbox/slimbox.js'));

		
		
		$this->set('page_caption',ucfirst($foto_list['Fotogalerie']['name']));
		$this->set('fastlinks',array(
			lang_fotogalerie =>	fotogalerie_link.'/', 
			ucfirst($foto_list['Fotogalerie']['name']) =>	'#', 
			)
		);
		
		
		
		
		$this->set('foto_list',$foto_list);
		
		
		
		
	}
	
	
	function actuals_box($id){
		$condition=array(
			'Fotogalerie.kos'=>0,
			'Fotogalerie.status'=>1,
			'Fotogalerie.actual_group_id'=>$id
		);
		$field=array(
			'Fotogalerie.name',
			'Fotogalerie.alias_',
			'Fotogalerie.text',
			'Fotogalerie.id',
			'Fotogalerie.created',
			'FotogalerieGroup.id',
			'FotogalerieGroup.name',
		);
		

		$this->Fotogalerie->bindModel(array(
        	'belongsTo'=>array(
				'FotogalerieGroup'=>array(),
			)
        ));
		$order = 'Fotogalerie.updated DESC';
		$limit = 2;
		$actuals_box = $this->Fotogalerie->find('all',array('conditions'=>$condition,'fields'=>$field,'order'=>$order,'limit'=>$limit));
		$this->set('actuals_box',$actuals_box);
		$this->set('page_caption','');
		$this->set('fastlinks',array());
		
		
	}
	
	function rss($id=null){
		Header("Content-Type: text/xml; charset=UTF-8");
		$this->layout = 'rss/rss';
			$actual_field = array(
				'Fotogalerie.name',
				'Fotogalerie.text',
				'Fotogalerie.alias_',
				'Fotogalerie.imgs',
				'Fotogalerie.created',
				'Fotogalerie.updated',
				'Fotogalerie.id',
				'FotogalerieGroup.name',
			);
		$this->Fotogalerie->bindModel(array(
        	'belongsTo'=>array(
				'FotogalerieGroup'=>array(),
			)
        ));
		$conditions = array('Fotogalerie.status'=>1,'Fotogalerie.kos'=>0,'FotogalerieGroup.id'=>$id);
		$this->set('rss',$rss = $this->Fotogalerie->find('all',array('conditions'=>$conditions,'fields'=>$actual_field,'group'=>'Fotogalerie.updated DESC','limit'=>10)));
		
		if (!$rss) $this->redirect('/');
	}
	
	
	
}	
?>