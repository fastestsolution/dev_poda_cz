<div id="header" class="el_adopt" data-el="header_adopt">
	<a href="/" id="logo" title="Poda"></a>
	<?php echo $this->renderElement('layout/menu'); ?>
	
	<?php if(isset($on_main_page)) { ?>
		<div id="blur-box" class="blur-box">
			<div class="line">
				<div class="show">
					<strong data-strong="Jste již naším zákazníkem?" data-regular="Vyberte si další z našich služeb">Mám</strong> Podu
				</div>
				
				<div class="findServices hidden">
					
					<?php /*
					<div class="buttons">
						<a href="#" class="domacnost button blue highway"><small>Potřebuji vylepšit svou</small>Domácnost</a>
						<a href="/kontakty/firemni-sluzby/" class="button blue"><small>Potřebuji vylepšit svou</small>Firmu</a>
					</div>
					*/ ?>
					
					<div id="highway" class="buttons">
						<a href="#" class="domacnost button blue"><small>Chci vylepšit</small>Internet</a>
						<a href="#" class="button domacnost blue"><small>Chci lepší</small>TV</a>
				 		<a href="/podpora/" class="button blue"><small>Potřebuji pomoc</small>Podpora</a>
					</div>
					
					<div class="searchPoda hide">
						<?php echo $this->renderElement('layout/chci_podu',array('type'=>1)); ?>
					</div>
				</div>
			</div> 
			<div class="line"> 
				<div class="show">
					<strong data-strong="Ještě nejste naším zákazníkem?" data-regular="Prohlédněte si naši nabídku">Chci</strong> Podu
				</div>
				
				<div class="findServices hidden">
					
					<div class="buttons">
						<a href="#" class="domacnost button blue"><small>Potřebuji propojit svou</small>Domácnost</a>
						<a href="/kontakty/firemni-sluzby/" class="button blue"><small>Potřebuji propojit svou</small>Firmu</a>
					</div>
					
					<div class="searchPoda hide">
						<?php echo $this->renderElement('layout/chci_podu',array('type'=>2)); ?>
					</div>
				</div>
			</div>
			<?php /*<a href="/vsechny-sluzby/" class="line">Zobrazit všechny služby</a>*/ ?>
		</div>
	<?php } else { ?>
		<?php echo $this->renderElement('layout/drobeckova',array('fastlinks'=>$fastlinks)); ?>
	<?php } ?>
	
	<div id="login-section" class="none">
		<form action="https://klient.poda.cz/moduly/uzivatel/login.php" method="post">
			<input class="input" type="text" name="login" placeholder="Uživatelské jméno" />
			<input class="input" type="password" name="password" placeholder="Heslo" />
			<input type="submit" value="Přihlásit se" class="button red" />
		</form>
		
		<a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank">Zapomněl/a jsem své heslo</a>
	</div>
</div> 

<script>
	header();
</script>
 