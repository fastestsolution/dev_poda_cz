<ul>
	<li class="main">Poda</li>
	<li><a href="/o-nas/" class='ajax_href'>O nás</a></li>
	<li><a href="/novinky/" class='ajax_href'>Novinky</a></li>
	<li><a href="/proc-prave-poda/" class='ajax_href'>Proč právě Poda?</a></li>
	<li><a href="/jak-prejit-k-pode/" class='ajax_href'>Jak přejít k Podě</a></li>
	<li><a href="/dokumenty-ke-stazeni/" class='ajax_href'>Dokumenty ke stažení</a></li>
	<li><a href="https://vp.poda.cz/" class='ajax_href' target="_blank">Věrnostní program</a></li>
	<li><a href="/projekt-eu/" class='ajax_href'>Projekt EU</a></li>
</ul>

<ul>
	<li class="main">Co nejčastěji hledáte</li>
	<li><a href="/internet/" class='ajax_href'>Internet</a></li>
	<li><a href="/tv-sluzby/" class='ajax_href'>Televize</a></li>
	<li><a href="/volani/" class='ajax_href'>Volání</a></li>
	<li><a href="/podpora/" class='ajax_href'>Podpora</a></li>
	<li><a href="/klienti-residomo/" class='ajax_href'>Klienti Residomo</a></li>
</ul> 

<ul>
	<li class="main">Co potřebujete</li>
	<li><a href="/kontakty/klientska-centra/" class='ajax_href'>Kontakty na klientská centra</a></li>
	<li><a href="/pro-media/" class='ajax_href'>Pro média</a></li>
	<li><a href="/cenik/" class='ajax_href'>Ceník</a></li>
	<li><a href="/ochrana-soukromi/" class='ajax_href'>Ochrana soukromí</a></li>
	<li><a href="/reklamace/" class='ajax_href'>Reklamace</a></li>
</ul>

<ul>
	<li class="main">Co se vám může hodit</li>
	<li><a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank" class='ajax_href'>Klientská zóna</a></li>
	<li><a href="https://www.google.cz/maps/place/28.+%C5%99%C3%ADjna+1168%2F102,+702+00+Moravsk%C3%A1+Ostrava+a+P%C5%99%C3%ADvoz/@49.8313726,18.2753946,17z/data=!3m1!4b1!4m5!3m4!1s0x4713e338d49b33d1:0x518e1b993f2645b1!8m2!3d49.8313726!4d18.2775833" target="_blank" class='ajax_href'>Mapa pokrytí</a></li>
	<li><a href="/dokumenty-ke-stazeni/" class='ajax_href'>Podmínky a dokumentace</a></li>
	<li><a href="/vyuctovani-a-platby/" class='ajax_href'>Vyúčtování a platby</a></li>
	<li><a href="/kontakty/#zakaznicka-linka" class='ajax_href'>Kontakt na zákaznickou linku</a></li>
</ul> 