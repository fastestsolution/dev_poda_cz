<div class="wrapper">
	<div class="mobileTarifs">
		<div class="mobileT">
			<div class="title">Mobil<br />základ plus</div>
			
			<div class="row">
				<label>Minuta volání v síti PODA (min.)</label> <strong>0.59,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS v síti PODA</label> <strong>0.79,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání do jiné mobilní sítě</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS do jiné mobilní sítě</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání na pevné linky</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Volné jednotky*</label> <img src="/css/layout/none.png" alt="none" />
			</div>
			<div class="row">
				<label>Data</label> <img src="/css/layout/none.png" alt="none" />
			</div>
			<div class="row bottomLine">
				<label>Tarifikace</label> <strong>60+1</strong>
			</div>
			
			<div class="price spanTop"><span>Měsíční paušál</span> 0,-</div>
			
			<a href="/volani/chci-tarif/?tarif=základ plus" class="button">Chci tento tarif</a>
		</div>
		<div class="mobileT">
			<div class="title">Mobil<br />volně plus</div>
			
			<div class="row">
				<label>Minuta volání v síti PODA (min.)</label> <strong>0,-</strong>
			</div> 
			<div class="row">
				<label>Cena SMS v síti PODA</label> <strong>0,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání do jiné mobilní sítě</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS do jiné mobilní sítě</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání na pevné linky</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Volné jednotky*</label> <img src="/css/layout/none.png" alt="none" />
			</div>
			<div class="row">
				<label>Data</label> <img src="/css/layout/none.png" alt="none" />
			</div>
			<div class="row bottomLine">
				<label>Tarifikace</label> <strong>60+1</strong>
			</div>
			
			<div class="price spanTop"><span>Měsíční paušál</span> 100,-</div>
			
			<a href="/volani/chci-tarif/?tarif=volně plus" class="button">Chci tento tarif</a>
		</div>
		<div class="mobileT">
			<div class="title">Mobil<br />volně 100 plus</div>
			
			<div class="row">
				<label>Minuta volání v síti PODA (min.)</label> <strong>0,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS v síti PODA</label> <strong>0,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání do jiné mobilní sítě</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS do jiné mobilní sítě</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání na pevné linky</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Volné jednotky*</label> <strong>100</strong>
			</div>
			<div class="row">
				<label>Data</label> <strong>150MB</strong>
			</div>
			<div class="row bottomLine">
				<label>Tarifikace</label> <strong>60+1</strong>
			</div>
			
			<div class="price spanTop"><span>Měsíční paušál</span> 200,-</div>
			
			<a href="/volani/chci-tarif/?tarif=volně 100 plus" class="button">Chci tento tarif</a>
		</div>
		<div class="mobileT">
			<div class="title">Mobil<br />základ online</div>
			
			<div class="row">
				<label>Minuta volání v síti PODA (min.)</label> <strong>0.59,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS v síti PODA</label> <strong>0.79,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání do jiné mobilní sítě</label> <strong>3.49,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS do jiné mobilní sítě</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání na pevné linky</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Volné jednotky*</label> <img src="/css/layout/none.png" alt="none" />
			</div>
			<div class="row">
				<label>Data</label> <strong>3000MB</strong>
			</div>
			<div class="row bottomLine">
				<label>Tarifikace</label> <strong>60+1</strong>
			</div>
			
			<div class="price spanTop"><span>Měsíční paušál</span> 400,-</div>
			
			<a href="/volani/chci-tarif/?tarif=základ online" class="button">Chci tento tarif</a>
		</div>
		<div class="mobileT">
			<div class="title">Mobil<br />volně všude</div>
			
			<div class="row">
				<label>Minuta volání v síti PODA (min.)</label> <strong>0,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS v síti PODA</label> <strong>0,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání do jiné mobilní sítě</label> <strong>0,-</strong>
			</div>
			<div class="row">
				<label>Cena SMS do jiné mobilní sítě</label> <strong>0,-</strong>
			</div>
			<div class="row">
				<label>Minuta volání na pevné linky</label> <strong>1.49,-</strong>
			</div>
			<div class="row">
				<label>Volné jednotky*</label> <img src="/css/layout/none.png" alt="none" />
			</div>
			<div class="row">
				<label>Data</label> <strong>1500MB</strong>
			</div>
			<div class="row bottomLine">
				<label>Tarifikace</label> <img src="/css/layout/none.png" alt="none" />
			</div>
			
			<div class="price spanTop"><span>Měsíční paušál</span> 750,-</div>
			
			<a href="/volani/chci-tarif/?tarif=volně všude" class="button">Chci tento tarif</a>
		</div>
	</div>
	
	<br class="clear" />
</div>

<div class="smaller-wrapper">
	<p class="txt-center">* jednotka = 1 volná minuta volání nebo 1 volná SMS do jakékoliv mobilní sítě či na pevnou linku</p>
	<p class="txt-center">Volání je doplňková služba pro naše klienty! Zaujala Vás naše nabídka? <a href="/kontakty/" class="blue">Kontaktovat</a> nás můžete písemně nebo využijte zákaznickou linku</p>
	<p class="txt-center"><strong class="bigger blue">844 844 033</strong> nebo <strong class="bigger blue">730 430 430</strong></p>
</div>

<br class="clear" />

<div class="blok cover_img more-text" data-src="/css/layout/banner/blokMobil1.jpg">
	<h2>Doplňkové služby</h2>
	
	<div class="wrap">
		<ul>
			<li>Možnost zachování stávajících telefonních čísel</li>
			<li>Zdarma on-line okamžitý podrobný výpis hovorů</li>
			<li>Služba CLIP - identifikace volajícího</li>
			<li>Automatické přesměrování, podmíněné přesměrování</li>
			<li>Přesměrování na více čísel</li>
			<li>Sériové linky</li>
			<li>Možnost předávání/přepojování hovorů</li>
			<li>Více hovorů současně</li>
			<li>Identifikace zmeškaných hovorů přímo na telefonu</li>
		</ul>
	</div>
</div>

<div class="blok cover_img more-text h570" data-src="/css/layout/banner/blokMobil2.jpg">
	<h2>Poda mobil - ideální (nejen) pro rodiny</h2>
	
	<div class="wrap"> 
		<ul>
			<li>Volné jednotky a <strong>bezplatné</strong> volání v síti PODA</li>
			<li><strong>ZDARMA</strong> aktivace SIM karet v hodnotě 99,- dle jednotlivých tarifů</li>
			<li>Minimální ceny mobilních služeb <strong>v EU</strong> (viz. <a href="/uploaded/dokumenty/roaming_cerven2017.pd" target="_blank">WORLD ROAMING</a>)</li>
			<li>S našimi tarity už nemusíte hlídat výdaje. Díky volným jednotkách se PODA Mobil zcela přizpůsobí Vašim zvyklostem nebo momentálním potřebám.</li>
		</ul>
	</div>

	<div class="borderTopBoxes">
		<div class="borderTopBox">
			MMS do všech sítí
			
			<strong>4,-</strong>
			<small>(1 MMS)</small>
		</div>
		
		<div class="borderTopBox">
			Data 150 MB, GPRS/EDGE/3G
			
			<strong>65,-</strong>
			<small>(1 kB)</small>
		</div>
		
		<div class="borderTopBox">
			Data 300 MB, GPRS/EDGE/3G
			
			<strong>120,-</strong>
			<small>(1 kB)</small>
		</div>
		
		<div class="borderTopBox">
			Data 600 MB, GPRS/EDGE/3G
			
			<strong>220,-</strong>
			<small>(1 kB)</small>
		</div>
		
		<div class="borderTopBox">
			Data 1200 MB, GPRS/EDGE/3G
			
			<strong>330,-</strong>
			<small>(1 kB)</small>
		</div>
		
		<div class="borderTopBox">
			Data 1500 MB, GPRS/EDGE/3G
			
			<strong>375,-</strong>
			<small>(1 kB)</small>
		</div>
		
		<div class="borderTopBox">
			Data 3000 MB, GPRS/EDGE/3G
			
			<strong>400,-</strong>
			<small>(1 kB)</small>
		</div>
	
	</div>
		
	<div class="wrap">	
		<ul>
			<li>Přenos čísla <strong>ZDARMA</strong></li>
			<li>99,- poplatek za vystavení další či nové SIM karty v případě ztráty nebo odcizení</li>
			<li>99,- reaktivační poplatek v přípradě pozastavení SIM karty</li>
			<li>LTE lze kombinovat v <a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank">KLIENTSKÉ ZÓNĚ</a> za 15,- měsíčně, u datových balíčků nad 1200 MB <strong>ZDARMA</strong></li>
			<li>Fakturována je první minuta hovoru, pote se hovorné účtuje po vteřinách</li>
		</ul>
	</div>
</div>