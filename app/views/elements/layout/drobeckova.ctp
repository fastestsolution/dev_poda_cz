<div id="drobeckova" class="drobeckova <?php echo (!isset($fstlinksNoBg)?'':'no-bg'); ?>">
	<a href='<?php echo (($lang != '/')?$lang:'') ?>/' class='ajax_href'><?php echo @$setting['drobeckova'] ?></a>
	
	<?php if(isset($fastlinks) && count($fastlinks) > 0):?>
		<span class="awesome">&#xf105;</span>
		
		<?php foreach($fastlinks as $caption=>$link):?>
			<?php if($link!='#'){?> 
				<a href='<?php echo $lang.rtrim($link,'/').'/';?>' title="<?php echo $caption;?>" class='ajax_href'><?php echo $caption;?></a> <span class="awesome">&#xf105;</span> 
			<?php } else {?> 
				<strong><?php echo $caption;?></strong><?php } ?>
		<?php endforeach;?>
	<?php endif;?>
</div>					 