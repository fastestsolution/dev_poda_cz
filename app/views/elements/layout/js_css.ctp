<?php if (isset($compress)){ ?>
			
		<link rel="stylesheet" type="text/css" href="<?php echo $cssUri;?>"  media="screen" />
		<script src="<?php echo $jsUri;?> "></script>

<?php } else { ?>
		<?php 
		foreach($debug_css AS $key=>$path_list){
			if ($key == 'css'){
				foreach($path_list AS $path){
					echo "<link rel='stylesheet/less' href='".$path."' type='text/css' media='screen' />\n";
				}
			}
			if ($key == 'js'){
				foreach($path_list AS $path){
					echo "<script src='".$path."'></script>\n";
				}
			}
			//echo "<script src='/js/MooSwipe/MooSwipe.js'></script>\n";
		}
		
		?>
    <link rel="stylesheet/less" type="text/css" href="/css/main.less"  media="screen" />
    <script type="text/javascript" src="/js/less.js"></script>

<?php } ?>
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

<?php #<!-- IE CONDITIONS --> ?>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!--[if lte IE 7]><script>window.location = 'http://browsers.fastest.cz/'</script><![endif]-->

<!--[if gte IE 9]>
<link href='/css/fastest/ie9.css' rel='stylesheet' type='text/css' media='screen' />
<![endif]-->
