<div class="wrapper">
	<h2 class="txt-center">Ceník volání do zahraničí naleznete</h2>
	<a href="/uploaded/dokumenty/cenik_mobildozahranici.pdf" target="_blank" class="button mb25">Ceník ke stažení</a>
</div>

<div class="smaller-wrapper">
	<p class="txt-center">Volání je doplňková služba pro naše klienty! Zaujala Vás naše nabídka? Kontaktovat nás můžete písemně přes <a href="" class="blue">poptávkový formulář</a> nebo využijte zákaznickou linku</p>
	<p class="txt-center"><strong class="bigger blue">844 844 033</strong> nebo <strong class="bigger blue">730 430 430</strong></p>
</div>
