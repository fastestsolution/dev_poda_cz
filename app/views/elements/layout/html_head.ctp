<?php #<!-- META --> ?>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name='description' content='<?php echo @$page_caption.' '.$description ?>'/>
<meta name='keywords' content='<?php echo $keywords ?>'/>
<meta name="robots" content="all,index,follow" />
<meta name='author' content='Fastest Solution, 2017 All Rights Reserved url: http://www.fastest.cz' />
<meta name='dcterms.rights' content='Fastest Solution, 2017 All Rights Reserved url: http://www.fastest.cz' />

<?php #<!-- RESPONSIVE SETTING -->	?>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<?php #<!-- FB --> ?>

<meta property="og:title" content="<?php echo (isset($og_graph['title'])?$og_graph['title']:@$page_caption)?>" />
<meta property="og:description" content="<?php echo (isset($og_graph['description'])?$og_graph['description']:$description)?>" />
<?php #<meta property="og:image" content="<?php echo (isset($og_graph['img'])?$og_graph['img']:'http://'.$_SERVER['HTTP_HOST'].'/css/fastest/layout/logo.png')" />?>

<?php if (!empty($setting['ga_superadmin_valid'])): ?>
    <meta name="google-site-verification" content="<?php echo $setting['ga_superadmin_valid'] ?>" />
<?php endif; ?>

<?php #<!-- LINKS --> ?>
<link href='/css/layout/icons/favicon.png' rel='icon' />
<title><?php echo $page_caption;?> <?= ($brand_title ? ' - '. $brand_title: ''); ?></title>
