<div class="wrapper">
	<div class="mobileTarifs internet">

		<div class="mobileT">
			<div class="title">Speed</div>
			
            <div class="row">
                <label>Oblíbený tarif ideální pro malou domácnost. Jako bonus televize v mobilu.</label>
                <label>Aktivace internetu jednorázově 500,-</label>
                <a href="/tv-sluzby/podanet-tv/">2 PODA net. TV zdarma</a>
                <br/>
                <a href="/volani/">1 SIM karta PODA mobil</a>
                <br>
                <span>40 televizních kanálů</span>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>15/1500<br/><span>Mbps</span></div>

		</div>
        <div class="mobileT">
            <div class="title">Rychlý Speed</div>

            <div class="row">
                <label>Internet pro každou domácnost i malé podnikání. K němu přes 50 televizních kanálů do kapsy.</label>
                <label>Aktivace internetu jednorázově 0,-</label>
                <a href="/tv-sluzby/podanet-tv/">4 PODA net. TV zdarma</a>
                <br/>
                <a href="/volani/">3 SIM karta PODA mobil</a>
                <br>
                <span>50+ televizních kanálů</span>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>30/3000<br/><span>Mbps</span></div>

        </div>


	</div>
    <br class="clear" />
    <div class="mobileTarifs internet">

        <div class="mobileT">
            <div class="title">Citylink</div>

            <div class="row">
                <label>Pomocník na prohlížení webu a Facebooku, práci s e-maily nebo vyhledávání v jízdních řádech.</label>
                <label>Aktivace internetu jednorázově 0,-</label>

            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>25<br/><span>Mbps</span></div>

        </div>
        <div class="mobileT">
            <div class="title">Duo</div>

            <div class="row">
                <label>Oblíbený tarif pro malou domácnost. Jako bonus televize v mobilu.</label>
                <label>Aktivace internetu jednorázově 0,-</label>
                <a href="/tv-sluzby/podanet-tv/">1 zařízení PODA net. TV zdarma</a>
                <br/>
                <a href="/volani/">1 SIM karta PODA mobil</a>
                <br>
                <span>20 televizních kanálů</span>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>25<br/><span>Mbps</span></div>

        </div>
        <div class="mobileT">
            <div class="title">Duo s prémií</div>

            <div class="row">
                <label>Oblíbený tarif pro malou domácnost. Jako bonus televize v mobilu.</label>
                <label>Aktivace internetu jednorázově 0,-</label>
                <a href="/tv-sluzby/podanet-tv/">2 zařízení PODA net. TV zdarma</a>
                <br/>
                <a href="/volani/">2 SIM karta PODA mobil</a>
                <br>
                <span>40 televizních kanálů</span>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>30<br/><span>Mbps</span></div>

        </div>
        <div class="mobileT">
            <div class="title">Duo naplno</div>

            <div class="row">
                <label>Oblíbený tarif pro malou domácnost. Jako bonus televize v mobilu.</label>
                <label>Aktivace internetu jednorázově 0,-</label>
                <a href="/tv-sluzby/podanet-tv/">4 zařízení PODA net. TV zdarma</a>
                <br/>
                <a href="/volani/">3 SIM karta PODA mobil</a>
                <br>
                <span>50+ televizních kanálů</span>
            </div>

            <div class="price spanTop"><span>Rychlost internetu</span>30<br/><span>Mbps</span></div>

        </div>


    </div>
	<br class="clear" />
</div>

