<h2 class="txt-center uppercase mt50">Často kladené otázky</h2>
		
<div id="faq" class="faq mt50">
	<div class="mainTitle">Volání</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Kde a jak si mohu objednat Vaše služby?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Kdy a jak službu dostanu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">V jaké síti působí poda mobil?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Kolik zaplatím za SIM kartu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Volám z ČR do zahraničí</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Volám z ČR do zahraničí na zahraniční číslo</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Volám ze zahraničí a mám příchozí hovor z ČR - Kolik to bude stát?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Volám ze zahraničí na zahraniční číslo, jak se bude hovor účtovat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Jsem v zahraničí a přestal mi fungovat roaming, co mám dělat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Jsem v zahraničí a nikomu se nemohu dodolat, co mám dělat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Přešel bych k podě s vlastním telefonním číslem, je to možné?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">V případě, že vypovím služby internet a tv nebo jejich kombinaci, ponechá mi poda Mobilní služby?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Podporují PODA SIM karty bankovnictví (ovládání účtu přes SIM kartu - SIM Toolkit)?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Jak mohu aktikovat/zablokovat hlasovou schránku?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Nechci, aby mi volala cizí čísla, jak je zablokovat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Potřebuji identifikaci volajícího</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Co je konferenční hovor a jak se provádí</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Je možné použít služby přesměrování hovorů?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Co je PIN a PUK a k čemu slouží a jak je zjistím?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Zadal jsem 3x chybně PIN a telefon požaduje PUK, co s tím?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Mohu zablokovat ztracený nebo odcizený telefon?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Jak získám novou SIM kartu po ztrátě nebo odcizení a kdy bude aktivní?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Za jaké období jsou účtovány mobilní služby?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="mainTitle mt50">TV Kanály</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Můžu měnit pořadí TV kanálů?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Může mít každý člen rodiny vlastní řazení kanálů?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Jak zobrazím informace o právě sledovaném pořadu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	
	<div class="mainTitle mt50">Pevná linka</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Jakým způsobem mohu aktivovat pevnou linku?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Hlasová schránka</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle bottomLine">SMS v pevné síti</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="mainTitle mt50">Data</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Jak nastavit připojení k internetu v telefonU? Jak aktivovat mobilní internet?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Co je to Tzv. Fair User Policy (FUP)?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Jak mohu navýšit objem dat/dokoupit nová data?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Jak získám další data ke svému tarifu a jak je zaplatím?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Předádějí se nevyčerpaná data do dalšího měsíce?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Chci se připojovat na internet v zahraničí, co mám dělat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Mám aktivní datový balíček, budu mít přenosy zdarma i v zahraničí?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Mohu si vybrat typ technologie připojení?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Chci používat mobilní internet v tabletu. Co budu potřebovat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Jak se dozvím o vyčerpání mého datového limitu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle">Jak si mohu navýšit datový limit?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow">
		<div class="fRowTitle bottomLine">Kde mohu zjistit aktuální stav mého datového limitu?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	


	<div class="mainTitle mt50">SIM</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Jaké parametry se dají na SIM kartě nastavit?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">Lze pozastavit SIM kartu na dobu určitou a za jakých podmínek - Musí to být osobně? Je to zpoplatněno?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="mainTitle mt50">SMS</div>
	
	<div class="fRow mt25">
		<div class="fRowTitle">Jak mohu aktivovat/zablokovat prémiové služby?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Kolik stojí premium SMS?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Co jsou dárcovské SMS a jak je odesílat?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Dají se zablokovat SMS z konkrétního čísla?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow ">
		<div class="fRowTitle">Nejdou mi odesílat SMS, co mám dělAT?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
	<div class="fRow bottomLine">
		<div class="fRowTitle">kOLIK STOJÍ POSÍLÁNÍ sms ZE ZAHRANIČÍ/DO ZAHRANIČÍ?</div>
		
		<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
	</div>
	
<br class="clear" />

<a href="" class="button blue">Dokumenty ke stažení</a>