<?php if (isset($check_browser)){ ?>
			<p class="alert_browser">Upozornění: používáte neaktuální verzi prohlížeče. Některé funkce mohou být neaktivní, nebo omezeny. Doporučujeme aktualizovat Váš prohlížeč.
			<script type="text/javascript">
			//<![CDATA[
			   setTimeout(function(){
				window.location = 'http://browsers.fastest.cz/';
			   },5000);
			//]]>
			</script>
			</p>
		<?php } ?>
			<noscript>
				<p class="alert_javacript">Upozornění: Nemáte povolený Javascript, můžete mít omezeny některé funkce.</p>
				
			</noscript>
			