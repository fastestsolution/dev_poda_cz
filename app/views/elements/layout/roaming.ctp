<div class="wrapper">
	<h2 class="txt-center">Ceník mobilních služeb PODA v zahraničí</h2>
	<a href="/uploaded/dokumenty/roaming_cerven2017.pdf" target="_blank" class="button mb25">World roaming</a>
</div>

<div class="smaller-wrapper">
	<p class="txt-center">Volání je doplňková služba pro naše klienty! Zaujala Vás naše nabídka? Kontaktovat nás můžete písemně přes <a href="" class="blue">poptávkový formulář</a> nebo využijte zákaznickou linku</p>
	<p class="txt-center"><strong class="bigger blue">844 844 033</strong> nebo <strong class="bigger blue">730 430 430</strong></p>
</div>

<!-- <a href="" class="button huge red">Více o akčních balíčcích TV a internetu!</a> -->