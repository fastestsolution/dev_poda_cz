<?php if(isset($paging)):?>
<div class="pagination">
<?php
    if($pagination->setPaging($paging)):
    $leftArrow = '&nbsp;';
    $rightArrow = '&nbsp;';
    
    $prev = $pagination->prevPage($leftArrow,false);
    $prev = $prev?$prev:$leftArrow;
    $next = $pagination->nextPage($rightArrow,false);
    $next = $next?$next:$rightArrow;

    $pages = $pagination->pageNumbers(" | ");

	if ($paging['pageCount']>1){
	echo '<span class="pag_left">'.$prev.'</span>';
	echo '<span class="pag_right">'.$next.'</span>';
	echo '<span class="pag_center">'.$pages.'</span>';
	echo ' <br class="clear" /> ';
	}
    endif;
	echo '<a href="'.$pagination->_generateUrl().'" id="refresh" class="none">Refresh</a>';
	
	
?>
</div> 

<?php endif;?>