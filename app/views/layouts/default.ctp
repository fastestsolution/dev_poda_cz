<?php
if (isset($setting['name'])) $main_name = $setting['name'];
else $main_name = @$setting['name'];
?>
<!DOCTYPE html>
<html>
	<head>
		<?php echo $this->renderElement('layout/html_head'); ?>
		<?php echo $this->renderElement('layout/js_css'); ?>
		<?php
			if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $javascript->link($link);}} else {echo $javascript->link($scripts);}}
			if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $html->css($style);} } else {echo $html->css($styles);}}
		?>
		<title>
			<?php echo $fastest->page_title(($this->params['url']['url']=='/')?array():(isset($fastlinks)?$fastlinks:''),$main_name);?>
		</title>
	</head>
    <body id="body" class="<?php echo (isset($on_main_page)?'main-page':'other-pages'); ?>">
		<div id="header_adopt" data-src="/css/layout/banner/hp_img<?php echo rand(1,3); ?>.jpg" class="<?php echo (isset($on_main_page)?'banner':''); ?> cover_img"></div>
        
        <?php if(isset($on_main_page)) { ?>
		    <div class="quick-icons on-mobile">
				<div class="quick-box">
					<div class="icon i1"></div>
					
					<span>Zákaznická pevná linka <a href="tel:844844033"><strong>844 844 033</strong></a></span>
				</div>
				<div class="quick-box">
					<div class="icon i2"></div>
					
					<span>Zákaznická mobilní linka <a href="tel:730430430"><strong>730 430 430</strong></a></span>
				</div>
				<div class="quick-box">
					<div class="icon i3"></div>
					
					<span>E-mail pro obchod <a href="mailto:obchod@poda.cz">obchod@poda.cz</a></span>
				</div>
				<div class="quick-box">
					<div class="icon i4"></div>
					
					<span>E-mail pro technickou podporu <a href="mailto:podpora@poda.cz">podpora@poda.cz</a></span>
				</div>
			</div> 
			
			<section>
		        <div class="wrapper">
		            <h1><?php echo $page_caption; ?></h1>
		            <?php echo $content_for_layout; ?>
		            
		            <?php echo $this->renderElement('layout/header'); ?>
		        </div>
	        </section>  
	    <?php } else { ?>   
		    <section>    
				<?php echo $content_for_layout; ?>
		    </section>
		    
		    <?php echo $this->renderElement('layout/header'); ?>
        <?php } ?>
        
        <div id="searchModal" class="none">
        	<?php echo $this->renderElement('layout/search'); ?>
        </div>
        
        <footer>
            <div class="wrapper">
                <?php echo $this->renderElement('layout/footer'); ?>
               
                <br class="clear" />
               
                <?php echo $this->renderElement('layout/footer_statistiky'); ?>
                <?php echo $this->renderElement('layout/footer_copyright'); ?>
            </div>
        </footer>
     </body>
</html>