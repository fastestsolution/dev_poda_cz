
<style>
.fselect {border:0px solid red !important;width:2px !important;height:1px !important;position:absolute;top:0;right:0;visibility:hidden;}
.fselect_obal {float:left;position:relative;width: 61%;margin-right:2px;}
.fselect_input {background: none repeat scroll 0 0 #FFFFFF;cursor:pointer;border: 1px solid #CCCCCC;color: #000000;float: left;margin: 3px 0;padding:0 3px;line-height:20px;width: 97%;height:20px;}
.fselect_point {width:16px;height:16px;border:1px solid #ccc;line-height:16px;position:absolute;top:5px;right:0px;background:transparent url('/css/fastest/layout/fselect_point.gif') no-repeat center center;cursor:pointer;}
.fselect_input.open {border-bottom:0px;}
.fselect_list {position:absolute;background:#fff;overflow:hidden;top:24px;width:100%;border:0px solid #ccc;border-width:0 1px 1px 1px;border-top:1px solid #E5E5E5;}
.fselect_opt {padding:0 1%;width:98%;height:20px;line-height:20px;border-bottom:1px solid #E5E5E5;display:block;cursor:pointer;}
.fselect_opt.factive {background:#ccc;}



.fcheckbox_obal {float:left;}
.fcheckbox_input {border:1px solid #ccc;width:16px;height:16px;position:relative;text-align:center;}
.fcheckbox_input_active {background:#ff0000;position:absolute;top:1px;left:1px;width:14px;height:14px;display:block;}
</style>
<form class="form" id="testForm">
<div class="sll">
	<?php echo $htmlExt->input('a',array('label'=>'text','class'=>'text validate[\'required\']',)).'<br />'; ?>
	<?php echo $htmlExt->selectTag('aa',$telefon_pref,null,array('label'=>'text 2','class'=>'text fselect  validate[\'required\']','title'=>'vyberte tel')).'<br />'; ?>
</div>
<div class="slr"> 
	<?php echo $htmlExt->input('a2',array('label'=>'text','class'=>'text  validate[\'required\']')).'<br />'; ?>
	<?php echo $htmlExt->selectTag('aa2',$stat_list,null,array('label'=>'text 2','class'=>'text fselect  validate[\'required\']','title'=>'Vyberte hodnotu')).'<br />'; ?>
</div>
	<?php echo $htmlExt->submit('send',array('id'=>'testForm_send')).'<br />'; ?>
</form>
<script type="text/javascript" language="javascript">
//<![CDATA[
var valid_form = new FormCheck('testForm');
contact_form_id = 'testForm';
if ($(contact_form_id+'_send')){
			$(contact_form_id+'_send').removeProperty('disabled');
			$(contact_form_id+'_send').addEvent('click',function(e){
				
				if (valid_form.isFormValid(contact_form_id) == true){
					new Event(e).stop();
				
					button_preloader($(contact_form_id+'_send'),true); 
					
					new Request.JSON({
						url:$(contact_form_id).action,	
						onComplete:function(json){
							if (!json || json.result == false){
								alert('Chyba odeslání formuláře');
							} else {
								alert(json.message);
								$$('.text').each(function(item){
									item.value = '';
								});
							}	
							button_preloader($(contact_form_id+'_send'));
						}
					}).post($(contact_form_id));		
				} 
			});
	}

function forgot_password(form_id){
var valid_form = new FormCheck(form_id);
   $('zapomenute_heslo_send').addEvent('click',function(e){
		
		if (valid_form.isFormValid(form_id) == true){
			new Event(e).stop();
			button_preloader($('zapomenute_heslo_send'));
			new Request.JSON({
				url:$('zapomenute_heslo_form').action,		
				onComplete:function(json){
					button_preloader($('zapomenute_heslo_send'));
					if (json.result == true){
						alert(json.message);
						domwin.closeWindow('zapomenute_heslo_domwin');
						
					} else {
						alert(json.message);
					}
				}
			}).post($('zapomenute_heslo_form'));
		}
			
	});
}
function fcheckbox(){
	window.addEvent('domready', function(){
		$$('.fcheckbox').each(function(item){
			var div_obal = new Element('div',{'class':'fcheckbox_obal'}).inject(item,'after');
			var fcheckbox_input = new Element('div',{'class':'fcheckbox_input'}).inject(div_obal);
			fcheckbox_input.addEvent('click',function(e){
				if (this.getElement('div')){
					this.getElement('div').dispose();
					item.removeProperty('checked','checked');
				} else {
					var fcheckbox_input_active = new Element('div',{'class':'fcheckbox_input_active'}).inject(fcheckbox_input);
					item.setProperty('checked','checked');
				}
			});
			if (item.getProperty('checked')){
				fcheckbox_input.fireEvent('click');
			}
			item.setStyles({
				'width':'0px',
				'height':'0px',
				'display':'none'
			});
		});
	
	});
}
function fselect(){
	$$('.fselect').each(function(item){
		//console.log(item);
		//item.addClass('none');
		var div_obal = new Element('div',{'class':'fselect_obal'}).inject(item,'after');
		var fselect_input = new Element('div',{'class':'fselect_input fselect_click text'}).inject(div_obal);
		var fselect_point = new Element('div',{'class':'fselect_point fselect_click'}).inject(div_obal);
		var fselect_list = new Element('div',{'class':'fselect_list'}).inject(div_obal);
		
		div_obal.adopt(item);
		
		
		fselect_input.addClass('open');
		item.getElements('option').each(function(opt){
			//console.log(opt);
			
				fselect_opt = new Element('div',{'class':'fselect_opt','rel':opt.value}).inject(fselect_list).set('text',opt.getProperty('title'));
				
				if (opt.value == ''){
					fselect_opt.addClass('empty');
					fselect_opt.addClass('none');
				}
				
				fselect_opt.addEvent('click',function(e){
					new Event(e).stop();
					fselect_input.set('text',this.get('text'));
					
					item.value = this.getProperty('rel');
					item.fireEvent('change');
					
					if (this.get('text') == ''){
						this.addClass('none');
						fselect_input.set('text',item.getProperty('title'));
						
					}
					//console.log(fselect_opt);
					if (item.value != ''){
						fselect_list.getElements('.fselect_opt').each(function(it){
						if (it.hasClass('empty'))
							it.removeClass('none');
						
						});
					}
					fselect_list.tween('height', 0);
					
				});
				
				fselect_opt.addEvent('mouseover',function(e){
					$$('.fselect_opt').removeClass('factive');
					this.addClass('factive');
				});
				
				
				fselect_input.set('text',item.getProperty('title'));
				if (item.value != ''){
					if (fselect_opt.hasClass('empty'))
						fselect_opt.removeClass('none');
					if (fselect_opt.getProperty('rel') == item.value){
						fselect_opt.addClass('factive');
					}
				}
		});
		
		if (item.value != ''){
			fselect_input.set('text',item.getOptionText());
		}
		
		fselect_list.setStyle('height', 0);
		
		fselect_point.addEvent('click',open_list);
		fselect_input.addEvent('click',open_list);
		
		function open_list(){
			var height_opt = fselect_opt.getSize().y;
			
			if (fselect_list.getSize().y <5){
				count = 0;
				fselect_list.getElements('div').each(function(i){
					if (!i.hasClass('none'))
						count++;
				});
				height = height_opt*count;
				fselect_list.tween('height', height-2);
				fselect_list.setStyles({
					'z-index':1000
				});
			} else {
				fselect_list.setStyles({
					'z-index':1
				});
				fselect_list.tween('height', 0);

			}
		}
		
		
		
		
		
	});
	$(document.body).addEvent('click',function(e){ 
		$$('.fselect_list').setStyle('height',0);
	});
}
fselect();
//]]>
</script>