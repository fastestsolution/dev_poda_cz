<div class="mainImg cover_img" data-src="/css/layout/banner/podanettv-bg.jpg">
	<div class="middleText">
		<h1 class="title">Poda.<span class="lowercase">net</span> TV<small>Využijte svoji televizi naplno! Televizní vysílání můžete mít dostupné všude tam, kde potřebujete!</small></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<br class="clear" />

<div class="smallest-wrapper">
	<p class="txt-center mt50"><strong class="uppercase black">Nechcete už sledovat s babičkou mexickou telenovelu, v níž podesáté Rodriquez vyznává lásku Manuele?</strong></p>
	<p class="txt-center mt25">Už nemusíte dělat kompromisy, pokud jde o televizní vysílání. Díky PODA net.TV mohou u vás doma všichni shlédnout svůj oblíbený pořad nejen v televizi, ale také na notebooku, chytrém telefonu nebo tabletu.</p>
</div>

<br class="clear" />

<div class="wrapper">
	<ul class="kontakt-list podanet">
		<li class="blue">
			<h2>Telefon</h2>
			<p>Podpora na jakémkoliv mobilním telefonu s iOS nebo Android.</p>
		</li> 
		<li class="blue">
			<h2>Tablet</h2>
			<p>Podpora na jakémkoliv tablet s iOS nebo Android.</p>
		</li>
		<li class="blue">
			<h2>Počítač</h2>
			<p>Podpora na jakémkoliv notebooku nebo počítači.</p>
		</li>
	</ul>
</div>

<br class="clear" />

<div class="blok cover_img " data-src="/css/layout/banner/podanet-1.jpg">
	<h2 class="mt200">Přehrávejte a nahrávejte i pořady, které již skončily</h2>
</div>

<br class="clear" />

<a href="/tv-sluzby/seznam-programu/" class="button blue">Seznam TV programů</a>

<br class="clear" />

<div class="smallest-wrapper">
	<p class="txt-center mt50">U tarifů Tandem služba obsahuje i některé kanály z Rozšířené televizní nabídky. Které to jsou je určeno zakoupenými balíčky z této nabídky. V tuto chvíli ještě nejsou zařazeny všechny kanály, může se proto stát, že některý z vámi zvolených ve svém zařízení nenajdete.</p>
	
	<p class="txt-center mt25">Podívejte se z vašeho tabletu, počítače nebo smartphonu na velký přehled TV programů díky naší bezplatné aplikaci PODA net.TV a zároveň využijte i další funkce. Své oblíbené pořady si můžete vychutnat, i když nebudete na Wi-Fi síti, ale třeba na mobilním připojení k internetu. Jediné, co doopravdy potřebujete, je internet od kvalitního poskytovatele.</p>
	
	<p class="txt-center mt25">
		<a href="/tv-sluzby/podanet-tv/operacni-system-android/" class="blue mb10 dblock">CHCI SE DÍVAT NA TELEFONU NEBO TABLETU S OPERAČNÍM SYSTÉMEM ANDROID</a>
		<a href="/tv-sluzby/podanet-tv/zarizeni-s-operacnim-systemem-ios/" class="blue mb10 dblock">CHCI SE DÍVAT NA TELEFONU ČI TABLETU S OPERAČNÍM SYSTÉMEM iOS</a>
		<a href="/tv-sluzby/podanet-tv/notebooky-pocitace/" class="blue mb10 dblock">CHCI SE DÍVAT NA NOTEBOOKU, STOLNÍM POČÍTAČI</a>
	</p>
</div>

<div class="blok cover_img mt25" data-src="/css/layout/banner/podanet-2.jpg">
	<h2 class="mt200">PODA.<span class="lowercase">nET</span> TV - svou televizi Máte stále u sebe!</h2>
</div>

<br class="clear" />

<div class="wrapper">
	<p class="txt-center"><strong class="big black uppercase bold">Máte zájem?</strong></p>
	<p class="txt-center mt25">Zadejte, prosím, adresu svého domova kde budete TV užívat...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>	
</div>