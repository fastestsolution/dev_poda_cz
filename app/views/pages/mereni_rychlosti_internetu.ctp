
<div class="mainImg cover_img bgposbottom h270" data-src="/css/layout/banner/bg-dokumenty.png">
	<div class="middleText">
		<h1 class="title">Dokumenty ke stažení<small>Péče a podpora. Více svobody. Více rodiny. Propojujeme generace...</small></h1>
	</div>
</div>

<br class="clear" />
<div class="wrapper mt50 rychlost-internetu">
    <h2>Doporučený postup pro měření rychlosti internetového připojení</h2>
    <p class="mt25">
        <h3>Nejpřesněji změříte rychlost internetového připojení, pokud splníte následující podmínky</h3>

    </p>
    <ul>
        <li>měřící počítač je připojen přímo k účastnické zásuvce ethernetovým kabelem (nikoli přes router nebo WiFi)</li>
        <li>k měření použijeme počítač s nezavirovaným a správně nastaveným operačním systémem</li>
        <li>v průběhu měření nebude v měřicím počítači komunikovat do internetu žádná jiná aplikace</li>
        <li>v průběhu měření nebude do internetu připojen jiný počítač, televize, smartphone apod.</li>
    </ul>
    <div class="mt25">
        <h3>Orientační měření rychlosti</h3>
        <p>
            Orientační měření rychlosti přenosu dat se provádí pomocí stránek s webovým měřičem rychlosti (např. www.rychlost.cz), které vyhodnotí rychlost přenosu dat a odezvu na základě stažení nebo odeslání malého testovacího souboru. Naměřené výsledky jsou pouze orientační proto, že toto krátkodobé měření probíhá v prohlížeči internetu (html, javascript, flash) a naměřené hodnoty jsou závislé na aktuálním stavu prostředků v měřícím počítači a na momentální celkové propustnosti datové trasy k webovému měřiči rychlosti.
        <br />
            Proto doporučujeme provést Orientační měření rychlosti na naši adrese http://rychlost.poda.cz/.
        </p>
    </div>
    <div class="mt25">
        <h3>Objektivní měření rychlosti</h3>
            <p>
                Objektivní měření rychlosti je vhodné provést následně, jakmile naměřené hodnoty v Orientačním měření signalizují zhoršení parametrů Vašeho internetového připojení a neodpovídají dané službě. Objektivní měření provedeme stažením velkého testovacího souboru (18.6 GB) na adrese <a class="blue" href="http://rychlost.poda.cz/test.bin " target="_blank">http://rychlost.poda.cz/test.bin</a><br />
            </p>
        <ul>
            <li>spusťte si Váš internetový prohlížeč (např. Firefox, Chrome, Explorer … nebo jiný)</li>
            <li>do okna adresa zadejte odkaz <a class="blue" href="http://rychlost.poda.cz/test.bin" target="_blank">http://rychlost.poda.cz/test.bin</a></li>
            <li>stiskněte ENTER</li>
            <li>otevře se Vám okno s nabídkou Otevřít / Uložit soubor, vyberte si jakoukoliv možnost. V případě, že vyberete položku uložit, vymažte po stažení tento soubor z místa uložení, aby Vám nezabíral místo na disku.</li>
            <li>po stažení testovacího souboru, za přibližně 20 min (záleží na rychlosti linky), se po přihlášení do Klientské zóny můžete podívat na graf s naměřenými hodnotami na adrese: <a class="blue" href="https://klient.poda.cz/moduly/sluzba/grafy.php" target="_blank">https://klient.poda.cz/moduly/sluzba/grafy.php</a></li>
            <li>pro přihlášení použijte jako uživatelské jméno Vaše klientské číslo a heslo dané pro Klientskou zónu. Neznáte-li jej, vyžádejte si nové heslo emailem na podpora@poda.cz.</li>
        </ul>

    </div>
    <div class="mt25">
        <img class="img-responsive" src="/css/layout/mereni-rychlosti-internetu/mereni_rychlosti.jpg" alt="Měření rychlosti" />
    </div>
    <div class="mt25">
        <ul>
            <li>Pokud se Vám grafy nezobrazují ihned po stažení, vyčkejte, prosím, přibližně 20 minut a aktualizujte si okno s grafy v internetovém prohlížeči například stisknutím klávesy F5.</li>
            <li>Pokud rychlost naměřená tímto testem, odpovídá objednané službě, je Vaše internetové připojení v pořádku.</li>
        </ul>
    </div>
    <div class="mt25">
        <h3>Řešení potíží</h3>
        <p>Pokud i po objektivním měření, neodpovídají naměřené parametry internetového připojení objednané službě, doporučujeme provést kontrolu těchto nejčastějších poruch:</p>
        <ul>
            <li>Ověřte, zda není mechanicky poškozený propojovací kabel mezi účastnickou zásuvkou a počítačem, případně jiným IP zařízením ( WiFi router, switch apod.)</li>
            <li>Pokud používáte pro připojení počítačů router nebo switch, restartujte jej krátkým odpojením a připojením jeho napájení a opakujte měření rychlosti internetu.</li>
            <li>Pokud Vaše připojení stále nevykazuje odpovídající hodnoty, zkuste zcela vyřadit Váš WiFi router nebo switch a připojte Vaše PC přímo kabelem (jedna strana kabelu je zapojena přímo do účastnické zásuvky internetu a druhá do PC), nastavte si na svém PC správně IP adresy dle předávacího protokolu, jež jste od nás obdržel při předání internetu a opakujte měření rychlosti.</li>
            <li>Hodnoty opět zkontrolujte na Vašich grafech v klientské zóně.</li>
        </ul>
    </div>
    <div class="smallest-wrapper mt50 pt75 idea">

        <p class="txt-center">
            <strong class="uppercase color-red">
                Nestačí Vám odpovědi?
            </strong>
        </p>
        <br />
        <p class="txt-center">
            Informace nepomohly? Nevadí. Využijte, prosím, naší e-mailovou adresu pro technickou podporu níže...
        </p>

        <a href="mailto:podpora@poda.cz" class="mt25 txt-center dblock w100p color-red">
            <strong class="fs40 color-red lowercase">
                podpora@poda.cz
            </strong>
        </a>

        <br class="clear" />
    </div>
</div>
