<div class="mainImg cover_img h270" data-src="/css/layout/banner/firemni-bg.jpg">
	<div class="middleText">
		<h1 class="title">Ekonomické údaje<small> Vývoj naši firmy</small></h1>
	</div>
</div>

<br class="clear" />

<div class="smaller-wrapper">
	<h2 class="txt-center mt50">Vývoj počtu připojených k internetu – firmy a organizace</h2>
	<div id="chart_firmy_celkem" class="googleChart"></div>
	
	<h2 class="txt-center mt50">Vývoj počtu domácností připojených k internetu</h2>
	<div id="chart_domacnosti" class="googleChart"></div>
	
	<h2 class="txt-center mt50">Vývoj obratu za telekomunikační služby (v tis. Kč)</h2>
	<div id="chart_obrat" class="googleChart"></div>
	
	<br class="clear" />
	<h2 class="txt-center mt50">Výroční zprávy</h2>
	<p class="txt-center mt25">Historii výročních zpráv můžete nalézt v dokumentech ke stažení</p>
	<a href="/dokumenty-ke-stazeni/" class="button">Výroční zprávy</a>
</div> 