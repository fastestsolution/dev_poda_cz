<div class="mainImg cover_img h270" data-src="/css/layout/banner/firemni-bg.jpg">
	<div class="middleText">
		<h1 class="title">Firemní služby<small>Jsme Vám plně k dispozici na níže uvedených kontaktech...</small></h1>
	</div>
</div>

<br class="clear" />

<div class="smaller-wrapper">
	<p class="txt-center mt50"><strong class="black bigger uppercase">Chcete objednat nebo rozšířit Vaše služby své firmy?<br />
Rozšíříme či navýšíme Vaše stávající služby nové objednané.</strong></p>

	<p class="txt-center mt25">Objevte služby, které se vám přizpůsobí. Získejte limitovanou nabídku. 
Získejte i vy firemní výhody. Máme pro vás stále nové služby, máme pro vás řešení na míru. 
Připravíme vám ideální řešení přesně podle vašich představ.
Ať jste živnostník nebo velká firma, u nás to nic neznamená – naše služby vždy maximálně přizpůsobíme vašim potřebám.
Soustřeďte se na vaše podnikání, my se za vás postaráme nejen o váš internet. 
Každé podnikání má své požadavky. My vám navrhneme řešení na míru.</p>
</div>

<div class="smaller-wrapper">
	<h2 class="txt-center mt50 mb25 uppercase bigger">Zavoláme vám se skvělou nabídkou, nechte nám na sebe kontakt.</h2>
	
	<div class="load_contact_form" data-type="5"></div> 
</div>
 
<div class="blok cover_img" data-src="/css/layout/banner/blok1.jpg">
	<h2>Naše služby pro ty, kteří chtějí, aby jejich podnikání stále rostlo.</h2>
	<div class="wrap">
		<ul>
			<li>žádné svazující podmínky – my vás úvazky na dobu určitou svazovat nebudeme</li>
			<li>nadstandardní péče – k dispozici máte vlastního konzultanta, který se vám bude věnovat</li>
			<li>žádné složité smlouvy – u nás máte jednu smlouvu na všechny služby, vyúčtování přehledně na jedné faktuře</li>
			<li>výhodné volání ve firmě</li>
		</ul>
	</div>
</div>

<div class="wrapper contacts">
	<ul class="icons-list mt50">  
		<li>
			<img src="/css/layout/icons/penize-icon.png" alt="Neutrácejte zbytečně" />
			
			<strong>Zavolejte nám</strong>
			<p>Jsme Vám k dispozici od pondělí do neděle od 7:00 do 19:30...</p>
			<strong class="fs40 blue mt25">844 844 033<br />730 430 430</strong>
		</li>
		<li>
			<img src="/css/layout/icons/lupa-icon.png" alt="Mějte výdaje pod kontrolou" />
			
			<strong>My zavoláme vám!</strong>
			<p>Nechte nám na Vás kontakt a my Vám zavoláme! Žádný problém...</p>
			
			<div class="load_contact_form" data-type="3"></div>
		</li>
		<li>
			<img src="/css/layout/icons/prasatko-icon.png" alt="Nejvýhodnější cenyt" />
			
			<strong>Nechce se vám volat?</strong>
			<p>Nevadí, napište nám níž uvedený @mail a můžeme vše probrat elektronicky...</p>
			
			<a href="mailto:info@poda.cz"><strong class="fs40 blue mt25 lowercase">info@poda.cz</strong></a>
		</li>
	</ul>
	
	<br class="clear" />
</div>
	
	<div class="shadow-line"></div>
<div class="wrapper">	
	<br class="clear" />
	<h2 class="txt-center uppercase mt50 bigger">Naše reference</h2>

	
	<ul class="kontakt-list pro-media mt0">
		<li class="blue icon-off"><img alt="Agrozet reference" class="img-responsive img-center-center" src="/css/layout/reference/agrozet.png" />
		<div class="about">
		<h2 class="color-blue">Agrozet</h2>
	
		<p>Funkční propojení poboček po celé republice</p>
		</div>
		</li>
		<li class="blue icon-off"><img alt="Nikal reference" class="img-responsive img-center-center" src="/css/layout/reference/nikal.png" />
		<div class="about">
		<h2 class="color-blue">Nikal</h2>
	
		<p>Internetové připojení v kombinaci s hlasovými službami a virtuálním faxem</p>
		</div>
		</li>
		<li class="blue icon-off"><img alt="New travel reference" class="img-responsive img-center-center" src="/css/layout/reference/newTravel.png" />
		<div class="about">
		<h2 class="color-blue">New Travel.cz, s.r.o.</h2>
	
		<p>Komplexní řešení připojení internetu, hlasových služeb a server housingu</p>
		</div>
		</li>
		<br class="clear" />
		<br />
		<br />
		 
		<li> </li>
	</ul>
</div>  