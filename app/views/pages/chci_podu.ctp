<div class="mainImg cover_img bgposbottom" data-src="/css/layout/banner/chci-podu.jpg">
	<div class="middleText">
		<h1 class="title">Našli jsme vhodné služby,...<small>Na vaší adrese je možnost velkých výběrů Internetu, TV i Volání...</small></h1>
	</div>
</div> 

<br class="clear" />
 
<div class="wrapper mt50">
	<div id="tarify" class="tarifs box-3">
		<?php if(isset($balicky) && !empty($balicky)) { ?>
			<?php foreach($balicky as $balicek) { ?>
				<div class="tarifBox"> 
					<div class="title"><?php echo $balicek->tarif_nazev; ?></div>
					
					<div class="price"><span>od</span> <?php echo $balicek->kc_pausal; ?>,-</div>
					 
					<div class="row">
						<label>Programová nabídka</label> <a href="/tv-sluzby/seznam-programu/" target="_blank" class="black"><strong><?php echo mb_substr($balicek->param_tv,0,14,'UTF-8'); ?></strong></a>
					</div>
					<div class="row">
						<label>Internet</label> <strong><?php echo $balicek->param_net; ?></strong>
					</div>
					
					<?php /*
					<div class="row">
						<label>Volání</label> <strong><?php echo $balicek->param_pvr; ?></strong>
					</div>
					*/ ?>
					
					<br class="clear" />
					<a href="/objednat/<?php echo $balicek->tarif_id; ?>" class="button">Nezávazně objednat</a>
					<a href="#" class="link">Další informace k tomuto balíčku</a>
				 	
					<div class="back hide">
						<div class="close"></div>
						<p><?php echo $balicek->popis_net; ?></p>
						<p><?php echo $balicek->popis_nettv; ?></p>
						<p><?php echo $balicek->popis_mobil; ?></p>
						<ul>
							<li>Aktivace: <?php echo $balicek->kc_aktivace; ?>,-</li>
							<li>Instalace: <?php echo $balicek->kc_instalace; ?></li>
							<li>Archiv TV: <?php echo $balicek->param_tvarchiv; ?></li>
							<li>Balíček TV program: <?php echo $balicek->param_tvprogram; ?></li>
							<li>Volání: <?php echo $balicek->param_pvr; ?></li>
						</ul>
						
						<p>Je na Vás těch informací moc? Nevadí. Zavolejte <strong>844 844 033</strong> a rádi Vám pomůžeme!</p>
						
						<a href="/objednat/<?php echo $balicek->tarif_id; ?>" class="button">Nezávazně objednat</a>
					</div>
				</div>
			<?php } ?>
		<?php } ?>
	</div>
</div>

<br class="clear" />

<div class="white-places"> 	
	<div class="title blue uppercase mt25">Poda <strong>služby</strong></div>
	<p class="txt-center">Dle Vámi zadané adresy můžete níže najít služby,<br />které Vám u Vás doma, můžeme nabídnout.</p>
	<?php echo $this->renderElement('layout/chci_podu'); ?>
</div>