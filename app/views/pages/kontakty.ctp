<div class="mainImg cover_img" data-src="/css/layout/banner/kontakty-bg.jpg">
	<div class="middleText">
		<h1 class="title"><?php echo $smallbox_list[11]['Smallbox']['title']; ?></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div> 
</div>

<div id="kontakt-google-map" class="wrapper">
	<ul id="zakaznicka-linka" class="kontakt-list">
		<li class="blue">
			<h2>Zákaznická linka</h2>
			<p>Máte-li jakýkoliv dotaz, zavolejte nám na telefonní čísla každý den od <b>7:00 - 19:30</b></p>
			
			<strong>
			844 844 033 <br />
			730 430 430 
			</strong> 
		</li>
		<li class="blue">
			<h2>Kontaktní e-mail pro obchod</h2>
			<p>Každé podnikání má své požadavky. My vám navrhneme řešení na míru. Kontaktujte nás.</p>
			
			<a href="mailto:info@poda.cz"><strong>info@poda.cz</strong></a>
		</li>
		<li class="red">
			<h2>Kontaktní e-mail pro technickou kontrolu</h2>
			<p>Nepřetržitý přístup zdarma z počítače nebo chytrého telefonu.</p>
			
			<a href="mailto:podpora@poda.cz"><strong>podpora@poda.cz</strong></a>
		</li>
	</ul>
	
	<br class="clear" />
	
	<h2 class="txt-center mt50"><strong class="black uppercase bold">Centrála společnosti PODA</strong></h2>
	<br />
	<p class="txt-center mt10"><strong class="blue uppercase">Ostrava</strong></p>
	<br />
	<p class="txt-center"><strong>28. října 1168/102<br />702 00 Ostrava - Moravská</strong></p>
	<br />
	<a href="https://www.google.cz/maps/place/28.+%C5%99%C3%ADjna+1168%2F102,+702+00+Moravsk%C3%A1+Ostrava+a+P%C5%99%C3%ADvoz/@49.8313726,18.2753946,17z/data=!3m1!4b1!4m5!3m4!1s0x4713e338d49b33d1:0x518e1b993f2645b1!8m2!3d49.8313726!4d18.2775833" target="_blank" class="button">Zobrazit na mapě</a>
	
	<br />
	<p class="txt-center mt25"><strong class="uppercase">Praha</strong></p>
	<br />
	<p class="txt-center">Budova City Tower<br />Hvězdova 1716/2b<br />140 78 Praha 4</p>
	
	<br class="clear" />
	<p class="txt-center mt50">IČ: 25816179, DIČ: CZ25816179, datová schránka PODA a.s.: ID: 597597000</p>
	<p class="txt-center"><small>PODA a.s. je zapsána u Kr. soudu v Ostravě, oddíl B, vl. č. 4020<br />bankovní spojení ČSOB: 217239274/0300</small></p>
	
	<a href="/kontakty/klientska-centra/" class="button wider">Kontakty na klientská centra</a>
	
	<br class="clear" />
	
</div>

<div class="smaller-wrapper">
	<div id="load_google_map" class="mt25 load_google_map">
		<div id="map_canvas"></div>
	</div>
</div>
	
<div class="wrapper">
	<br class="clear" />
	
	<ul id="sluzba-buttons" class="sluzba-buttons">
		<li><a href="#" data-id="sectionDomaciObchod" class="button active">Domácí obchod</a></li>
		<li><a href="#" data-id="sectionFiremniObchod" class="button">Firemní obchod</a></li>
	</ul>
	
	<div id="sectionDomaciObchod" class="sluzba-section mt50">
		<div class="smaller-wrapper">
			<p class="txt-center">Nenašli jste, co jste hledali?<br /> Využijte některý z dalších kontaktů:</p>
		</div>
		
		<ul class="kontakt-list">
			<li class="blue h175">
				<strong> 
					844 844 033 <br />
					730 430 430
				</strong>
			</li>
			<li class="blue h175">
				<a href="mailto:info@poda.cz"><strong>info@poda.cz</strong></a>
			</li>
		</ul>
		
		<br class="clear" />
		<a href="/kontakty/obchodnici/" class="button">Obchodní tým</a>
	</div>
	
	<div id="sectionFiremniObchod" class="sluzba-section mt50 none">
		<p class="txt-center">Máte-li jakýkoliv dotaz související s firemní klientelou,<br /> využijte, prosím, níže uvedené kontakty.</p>
		
		<div class="smaller-wrapper">
			<p class="txt-center mt25"><b>Ostrava</b><br />
	PODA a.s., 28. října 1168/102<br />
	702 00 Ostrava-Moravská Ostrava</p>
	
			<p class="txt-center mt25"><b>Praha</b><br />
				Budova City Tower, Hvězdova 1716/2b<br />
				140 78 Praha 4
			</p>
			
			<p class="txt-center mt25"><b>Brno</b><br />
			PODA a.s., Gajdošova 4392/7<br />
			615 00 Brno-Židenice</p>
			
			
			<p class="txt-center mt25">
				<b>Znojmo</b><br />
				PODA a.s., Pražská 1653/30<br />
				669 02 Znojmo
			</p>
			
			<p class="txt-center mt25">
				<b>Třeboň</b><br />
				PODA a.s., Boženy Němcové 817/II<br />
				379 01 Třeboň</p>
		</div>
		
		<ul class="kontakt-list">
			<li class="blue h175">
				<strong>
					844 844 033 <br />
					730 430 430
				</strong>
			</li>
			<li class="blue h175">
				<a href="mailto:info@poda.cz"><strong>info@poda.cz</strong></a>
			</li>
		</ul>
		
		<br class="clear" />
		<a href="/kontakty/existenci-siti/" class="button wider">Vyjádření o existenci sítí</a>
	</div>
	
</div>

<div class="mainImg cover_img relative50" data-src="/css/layout/banner/sluzby.jpg">
	<div class="bottomText">
		<h2 class="title">TV + Internet za 399,-<small>udělejte rodině radost</small></h2>
		
		<a href="/tv-sluzby/" class="button">Mám zájem o tuto nabídku</a>
	</div>
</div>