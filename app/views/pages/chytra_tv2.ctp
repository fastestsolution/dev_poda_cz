<div class="mainImg cover_img" data-src="/css/layout/banner/chytratv-bg.jpg">
	<div class="middleText">
		<h1 class="title">Chytré funkce TV<small>Až 36 hodin zpětně. Pořad, který chcete vidět znovu, klidně spusťte od začátku.</small></h1>
	</div>
</div>

<br class="clear" />

<div class="article_text">
	<div class="smallest-wrapper">
		<p class="txt-center mt50"><strong class="black uppercase">Udělejte si při sledování pauzu a pozastavte pořad až na 4 hodiny.</strong></p>
		<p class="txt-center mt25">Nechcete přijít ani o okamžik sledovaného pořadu? Káva, telefon, dovaření večeře… nic již pro Vás nebude problém. Díky funkci PAUZA si pořad stopnete, a ve sledování budete dál pokračovat, až si budete přát. Právě vysílaný pořad lze pozastavit až na 4 hodiny.</p>
		
		<p class="txt-center mt50"><strong class="black uppercase">Přehrajte si již běžící pořad znovu, od začátku</strong></p>
		<p class="txt-center mt25">Nestihli jste začátek oblíbeného pořadu? Nic se neděje. Jedním stiskem zeleného tlačítka na dálkovém ovladači aktivujete jeho přehrávání znovu, od začátku. Pořad bude přehráván ze záznamu a Vy v něm budete moci přetáčet – posouvat se kupředu i nazpět, opakovat poutavé záběry i přeskakovat nudná místa.</p>
		
		<p class="txt-center mt50"><strong class="black uppercase">Nahrajte si své oblíbené pořady na 3 měsíce na náš server</strong></p>
		<p class="txt-center mt25">Nestihli jste začátek oblíbeného pořadu? Nic se neděje. Jedním stiskem zeleného tlačítka na dálkovém ovladači aktivujete jeho přehrávání znovu, od začátku. Pořad bude přehráván ze záznamu a Vy v něm budete moci přetáčet – posouvat se kupředu i nazpět, opakovat poutavé záběry i přeskakovat nudná místa.</p>
		
		<p class="txt-center mt50"><strong class="black uppercase">Potřebujete více než 10 či 20 hodin nahrávacího prostoru?</strong></p>
		<p class="txt-center mt25">Nahráváte často pořady a není pro Vás dosavadní prostor na ukládání dostačující? I toto je jedna z funkcí, kterou nabízíme.</p>
		
		<p class="txt-center mt50"><strong class="black uppercase">Jaké jsou výhody nahrávání s chytrými funkcemi PODA TV?</strong></p>
		<ul class="mt25">
			<li>I když nahrávání zapnete v průběhu pořadu, pořad bude nahrán celý, od začátku.</li>
			<li>Po skončení nahrávaného pořadu se nahrávání automaticky vypne.</li>
			<li>Během nahrávání pořadu lze přepínat na jiné kanály.</li>
			<li>Je možné současně nahrávat pořady na více stanicích.</li>
			<li>Máte-li více set-top boxů, sdílejí nahrávky mezi sebou – programy nahrané na jednom set-top boxu můžete sledovat z jiného.</li>
			<li>Nahrávání můžete zapnout přímo při sledování pořadu, naprogramovat předem nebo aktivovat zpětně pro pořady, které již skončily.</li>
		</ul>
		
		<br class="clear" />
		
		<a href="/tv-sluzby/seznam-programu/" class="button blue">Seznam TV programů</a>
	</div>
</div>

<br class="clear" />

<div class="wrapper">
		
	<p class="txt-center mt50"><strong class="big black uppercase bold">Máte zájem?</strong></p>
	<p class="txt-center mt25">Zadejte, prosím, adresu svého domova kde budete TV užívat...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>