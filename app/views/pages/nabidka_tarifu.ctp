<div class="mainImg cover_img" data-src="/css/layout/banner/volani.jpg">
	<?php /*
	<div class="blackRow">
		<ul>
			<li>
				<a href="/internet/" class="button">Nabídka internetu</a>
			</li>
			<li>
				<a href="/tv-sluzby/" class="button">Nabídka TV služeb</a>
			</li>
			<li class="active">
				<a href="/volani/" class="button">Nabídka tarifů</a>
			</li>
		</ul>
	</div>
	*/ ?>
	<div class="middleText">
		<h1 class="title"><?php echo $smallbox_list[6]['Smallbox']['title']; ?></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<div class="article_text"> 
	<div class="wrapper">
		<div class="smaller-wrapper">
			<?php echo $smallbox_list[7]['Smallbox']['text']; ?>
		</div>
		
		<div class="buttons w100p">
			<a href="/volani/cenik-sluzeb/" class="button">Ceníky služeb</a>
			<a href="/volani/telefony-a-prislusenstvi/" class="button blue wider">Telefony a příslušenství</a>
		</div>
			
		<div class="smaller-wrapper">	
			<!--
			<p class="txt-center bold">Další možné náklady PODA Mobil:</p>
			<ul class="txt-center">
				<li>přenos čísla zdarma</li>
				<li>99 Kč poplatek za vystavení další či nové SIM karty v případě ztráty nebo odcizení</li>
				<li>99 Kč reaktivační poplatek v případě pozastavení SIM karty</li>
				<li>LTE lze aktivovat v KLIENTSKÉ ZÓNĚ za 15 Kč měsíčně, u datových balíčků nad 1200 MB zdarma </li>
			</ul>
			-->
			<p class="txt-center mt50"><strong class="big black uppercase bold">Vyberte si službu přesně pro sebe</strong></p>
		</div>
			
		<ul id="sluzba-buttons" class="sluzba-buttons">
			<li><a href="#" data-id="sectionMobilniVolani" class="button active">Mobilní volání</a></li>
			<li><a href="#" data-id="sectionPevnaLinka" class="button">Pevná linka</a></li>
			<li><a href="#" data-id="sectionRoaming" class="button">Roaming</a></li>
			<li><a href="#" data-id="sectionZahranici" class="button">Volání do zahraničí</a></li>
		</ul>
		 
		<br class="clear" />	
	</div>
	
	<div id="sectionMobilniVolani" class="sluzba-section">
		<?php echo $this->renderElement('layout/mobilni-volani'); ?>
        <h2 class="txt-center uppercase mt50">Často kladené otázky k volání</h2>

        <div class="wrapper">
            <div class="faq mt50" id="faq">
                <div class="fRow mt25">
                    <div class="fRowTitle showMoreActual">Mobilní volání <a class="button circle showMoreActual" href="#">+</a></div>

                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jaké služby mám na SIM ihned po aktivaci? <a class="button circle showMoreActual" href="#">+</a></div>

                                <p>
                                    Komunikační jazyk<br>
                                    Příchozí a odchozí hovory<br>
                                    Přesměrování a přidržení hovorů<br>
                                    Skrytí identifikace volajícího<br>
                                    Povolení SMS<br>
                                    MMS<br>
                                    Uvítací roaming SMS<br>
                                    Premium služby (nejsou automaticky aktivní)<br>
                                    Limit v Kč pro roamingová data<br>
                                    Upozornění na dosažení FUP limitu<br>
                                    Hlasovou schránku a nastavení jazyka hlasové schránky<br>

                                </p>

                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">V jaké síti působí PODA mobil? <a class="button circle showMoreActual" href="#">+</a></div>

                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kolik zaplatím za SIM kartu <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak si mohu aktivovat SIM kartu <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám z ČR do zahraničí <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám z ČR do zahraničí na zahraniční číslo <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám ze zahraničí na zahraniční číslo <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jsem v zahraničí a mám příchozí hovor z ČR - kolik to bude stát <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám ze zahraničí na zahraniční číslo, jak se bude hovor účtovat <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jsem v zahraničí a přestal mi fungovat roaming, co mám dělat <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jsem v zahraničí a nikomu se nemohu dovolat, co mám dělat <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Přešel bych k PODĚ s vlastním telefonním číslem, je to možné? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">V případě, že vypovím služby internet a tv nebo jejich kombinaci, ponechá mi PODA mobilní služby? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Podporují PODA SIM karty bankovnictví (ovládání účtu přes SIM kartu (SIM Toolkit)? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak mohu aktivizovat/zablokovat hlasovou schránku? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Nechci, aby mi volala cizí čísla, jak je zablokovat <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Potřebuji identifikaci volajícího <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co je konferenční hovor a jak se provádí <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Za jaké období jsou účtovány mobilní služby <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Je možné používat služby přesměrování hovorů <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co je PIN a PUK a k čemu slouží, jak je zjistím <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Zadal jsem třikrát chybně PIN a telefon požaduje PUK, co s tím <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Mohu zablokovat ztracený nebo odcizený telefon <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak získám novou SIM kartu po ztrátě nebo odcizení a kdy bude aktivní <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                    </ul>
                </div>




                <div class="fRow">
                    <div class="fRowTitle showMoreActual">Data <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Chci data do mobilu, co mám dělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Stačí si zvolit náš PODA datový tarif s balíčkem dat dle uvážení. Informace o cenách naleznete v ceníku.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak mohu navýšit objem dat, anebo dokoupit další data? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Navýšení datového balíčku může být zcela automatické. Pokud vyznačíte v Klientské zóně POKRAČOVAT, pak budete moci bez snížení rychlosti využívat data i nadále. Za data nad rámec sjednaného balíčku zaplatíte jednotkovou cenu vašeho tarifu za každý spotřebovaný MB. Pokud vyznačíte UKONČIT, čerpání dat vám zastavíme při dosažení
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak se dozvím o vyčerpání svého datového limitu? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Jakmile spotřebujete 85 % dat z vašeho zakoupeného balíčku, obdržíte od nás SMS upozornění. Jakmile vyčerpáte 100 % dat z vašeho balíčku data vám přestanou fungovat.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak získám další data ke svému tarifu a jak je zaplatím? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Navýšení datového balíčku může být zcela automatické. Zvolíte-li v Klientské zóně POKRAČOVAT, pak budete moci bez snížení rychlosti využívat data i nadále. Za data nad rámec sjednaného balíčku zaplatíte jednotkovou cenu vašeho tarifu za každý spotřebovaný MB. Pokud zvolíte UKONČIT, čerpání dat vám zastavíme při dosažení objednaného objemu. Vždy vám nejprve pošleme SMS, abyste se mohli včas rozhodnout. Povolená data nad rámec zakoupeného balíčku zaplatíte na konci zúčtovacího období v rámci vyúčtování za mobilní služby.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Převádějí se nevyčerpaná data do dalšího měsíce? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Nevyčerpaná data se nepřevádějí. Platí od prvního do posledního dne v měsíci. Prvního dne dalšího měsíce platí opět datový balíček podle vaši volby.  Měsíční cena balíčku je účtována v plné výši i v případě že nevyčerpáte celkový objem zakoupených dat.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak mi budou účtována data po vyčerpání základního objemu z tarifu? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Zvolíte-li v Klientské zóně POKRAČOVAT, pak budete moci bez snížení rychlosti využívat data i nadále. Za data nad rámec sjednaného balíčku zaplatíte jednotkovou cenu vašeho tarifu za každý spotřebovaný MB. Pokud zvolíte UKONČIT, čerpání dat vám zastavíme při dosažení objednaného objemu. Vždy vám nejprve pošleme SMS abyste se mohli včas rozhodnout.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Přišla mi SMS o vyčerpání 85 % datového limitu, co to znamená? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Oznamovací SMS vám přijde v případě, že už máte prosurfováno 85 % dat ze svého balíčku. Je to upozornění na blížící se úplné vyčerpání dat z vašeho balíčku. Poté bude na vás, zda v Klientské zóně umožníte další čerpání dat nad rámec balíčku, nebo dáte dočasně ukončit data a data vám do konce měsíce nepoběží vůbec.  SMS oznámení od nás obdržíte i v případě úplného vyčerpání vašeho datového balíčku.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Chci používat Mobilní internet v notebooku, co budu potřebovat?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pro použití mobilního internetu v notebooku potřebujete pouze modem a SIM kartu s vhodným datovým tarifem.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Zvládnu s internetem v mobilu totéž co při pevném připojení?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Byť se technologie mobilního internetu stále vyvíjí a rychlost stoupá, mobilní internet má svá omezení oproti pevnému připojení. Nejdůležitější je asi obvyklé omezení maximálních přenesených dat, tedy FUP které je limitováno vašim datovým balíčkem. Přestože umožňujeme přikoupení dalších dat, možnosti mobilního internetu nejsou shodné s pevným připojením. Dle technologie a dostupnosti se samozřejmě bude lišit také faktická rychlost. n obvyklou práci, kontrolu e-mailů, brouzdání po internetu je mobilní internet v pořádku, ale také může záviset na zařízení, které používáte, chcete-li stahovat nebo streamovat filmy, zvláště v HD kvalitě, nebo hrát online hry s požadavkem na rychlou odezvu, internet v mobilu není pro vás. Ale jistě jej využijete na výletě, kdy se budete potřebovat podívat na mapu, nebo přečíst nějakou zajímavost.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Chci používat Mobilní internet v tabletu, co budu potřebovat?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pro použití mobilního internetu v tabletu potřebujete pouze SIM s datovým tarifem a tablet, který mobilní data podporuje.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Zvládnu s internetem v tabletu totéž co při pevném připojení ?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Internet v tabletu je pro práci a surfování komfortnější   než  totéž v mobilu. Ale i zde platí podobná omezení jako jsme vypsali v kapitole Zvládnu s internetem v mobilu totéž co při pevném připojení ? Zejména FUP, dostupnost signálu a různá rychlost.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Mohu v tabletu používat LTE ?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Ano, máte-li LTE aktivováno a vaše zařízení tuto technologii podporuje, nic vám snad kromě lokální nedostupnosti v používání LTE nebrání.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">K čemu slouží datový roaming ?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Datový roaming vám dovoluje datové připojení v síti partnerského operátora v zahraničí, se kterým máme smlouvu. Prakticky bez datového roamingu vám data v zahraničí fungovat nebudou.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co je datový roamingový balíček a jak si jej nastavím?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Nastavení, tedy povolení datového roamingu lze aktivovat na svém účtu v Klientské zóně. Abyste mohli bez obav z vysokých účtů používat data v zahraničí, doporučujeme zakoupení datových roamingových balíčků. Datový balíček je aktivace určitého objemu dat na vaši SIM platný v zahraničí s pevně danou dobu platnosti (obvykle 30dnů). Cena závisí na množství dat, které si zvolíte. Rychlost připojení v zahraničí závisí na typu zařízení, které používáte, dané lokalitě a momentální vytížeností sítě. Tarifikace je 1kB v EU, mimo EU 10kB.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak na připojení k internetu v zahraničí (datový roaming)  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pokud máte aktivní datový balíček z naší nabídky a máte aktivovaný datový roaming, vše vám bude fungovat.  O cenách datových balíčků a způsobech účtování doporučujeme ceník roamingových dat.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Mám obavu z velké spotřeby dat v zahraničí – lze nastavit limit na spotřebu dat v zahraničí?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Limit lze nastavit od 0 do 15 000,- Kč. Platí vždy od prvního připojení v zahraničí až do konce účtovacího období. Nastavení limitu a jeho aktivace v Klientské zóně je zdarma.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co mám udělat, abych mohl sledovat mobilní televizi?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Společnost PODA nabízí ke sledování televize službu PODA net TV. Pro naše zákazníky je ZADARMO. Nejprve je potřeba ve svém klientském centru aktivovat zařízení, stáhnout si pro zařízení s operačním systémem nebo do chytrého telefonu aplikaci. Po zadání párovacího kódu můžete využívat službu Poda net.TV naplno. Upozorňujeme, že sledování televize výrazně spotřebovává data. Doporučujeme proto připojení prostřednictvím wifi.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Je mobilní televize chytrá televize? Jaké mi nabízí funkce?  <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Naše služba PODA net.TV vám zdarma nabízí širokou nabídku programů. K dispozici máte také plnohodnotný archiv 48h nazpět.
                                </p>
                            </div>
                        </li>

                    </ul>
                </div>


                <div class="fRow">
                    <div class="fRowTitle showMoreActual">Vyúčtování a platby <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Za jaké období jsou účtovány mobilní služby? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Mobilní služby jsou účtovány za kalendářní měsíc. Tedy od 1. dne v měsíci do posledního dne daného měsíce. Vyúčtování obdržíte vždy z kraje následujícího měsíce.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Je možné sloučit více karet na jeden zákaznický účet? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pod jedním účtem můžete mít více SIM karet. Vyúčtování mám bude přicházet souhrnné za všechna čísla dohromady s rozpisem jednotlivých útrat.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kde najdu vyúčtování? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Výši částky vyúčtování, údaje a termín úhrady naleznete na faktuře, kterou vám zašleme na e-mailovou adresu uvedenou ve vaší smlouvě. Budeme rádi, když nám obratem oznámíte případné změny vašich kontaktů, abyste předešli případným nesrovnalostem.
                                </p>
                            </div>
                        </li>

                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak mi budete účtovat data po vyčerpání základního objemu z tarifu? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pokud jste si v Klientské zóně vyznačili možnost Pokračovat, pak budete moci bez snížení rychlosti využívat data i po vyčerpání základního objemu. Za data nad rámec sjednaného balíčku zaplatíte jednotkovou cenu vašeho tarifu za každý spotřebovaný MB – je to ještě aktuální?. Pokud však vyznačíte Ukončit, čerpání dat vám zastavíme při dosažení objednaného objemu. Vždycky vás předem prostřednictvím SMS upozorníme, abyste se mohli správně rozhodnout.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Smlouvu už mám skoro rok, co s prodloužením smlouvy? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Nemusíte dělat vůbec nic, vaše smlouva se automaticky prodlužuje. Naše služby budete získávat i nadále v rozsahu, na kterém jsme se společně dohodli.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Tento měsíc jsem nedostal vyúčtování, co mám dělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Vaše vyúčtování si můžete zobrazit a stáhnout z vaší Klientské zóny pod vyúčtováním. Také můžete zavolat na naši Klientskou linku a my vám vyúčtování pošleme, nebo nás osobně navštivte v nejbližším Klientském centru.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Nesouhlasím s částkou na faktuře, chci vyúčtování reklamovat. Jak to mám udělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pokud nesouhlasíte s vyúčtováním, které jsme vám zaslali, doporučujeme se obrátit na naše Klientské centrum, nebo zavolat Klientskou linku pro další řešení.  V Klientské zóně lze také použít kontaktní formulář na zákaznické centrum.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>


                <div class="fRow">
                    <div class="fRowTitle showMoreActual">SLUŽBY SÍTĚ <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Je možné používat službu přesměrování hovorů? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Přesměrování hovorů je spíše záležitostí vašeho mobilního telefonu. Pokud váš přístroj přesměrování hovorů umožňuje, informace o tom jak přesměrování hovorů nastavit naleznete v příručce na ovládání vašeho telefonu.  Síť  PODA přesměrování hovorů umožňuje. Nastavení povolení přesměrování můžete udělat v Klientské zóně.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jakým způsobem mohu nastavit přesměrování hovorů? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Přesměrování hovorů je spíše záležitostí vašeho mobilního telefonu. Pokud váš přístroj přesměrování hovorů umožňuje, informace o tom jak přesměrování hovorů nastavit naleznete v příručce na ovládání vašeho telefonu.  Síť  PODA přesměrování hovorů umožňuje, nastavení povolení přesměrování si zkontrolujte v nastavení SIM v Klientské zóně.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Nechci, aby mi volala cizí čísla, chci je zablokovat, jak na to? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Příchozí hovory lze blokovat pouze na telefonu (podle typu telefonu, velká část chytrých telefonů toto umožňuje). PODA Mobil neposkytuje službu blokace hovorů z určitých čísel. Lze pouze zjistit tel. číslo a identifikaci.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Chci používat skryté číslo <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pro skrytí identifikace volajícího si nejprve v klientské zóně zkontrolujte nastavení služby. Pokud ji máte povolenou, dále ji je potřeba před hovorem nastavit ve vašem mobilním telefonu. Skrytí můžete nastavit na dočasné, trvalé nebo dočasně / trvale zakázáno. Tyto volby se nastavují v Klientské zóně.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak funguje služba přidržení hovoru? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Přidržení hovoru znamená že vás během hovoru upozorníme na další příchozí hovor, se který se můžete spojit, aniž by byl přerušen hovor původní. Pokud přijmete 2. hovor na lince, první bude dočasně přidržen. Jakmile hovor ukončíte, můžete zavěsit i druhý, nebo jej opět přijmout.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>


                <div class="fRow">
                    <div class="fRowTitle showMoreActual">SIM <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jaké parametry se dají na SIM kartě nastavit <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Lze pozastavit SIM kartu na dobu určitou a za jakých podmínek. Musí to být osobně? Je to zpoplatněno? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                    </ul>
                </div>


                <div class="fRow">
                    <div class="fRowTitle showMoreActual">SMS a MMS <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak mohu aktivovat / zablokovat Prémiové služby? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Prémiové služby nejsou automaticky na SIM kartě aktivní. Aktivovat si je můžete osobně na Klientském centru nebo nastavením v Klientské zóně. Po nastavení je služba na SIM kartě aktivní zhruba do jedné hodiny. Zablokování těchto služeb se provádí stejným způsobem.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kolik stojí Premium SMS? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>	Jedná se o prémiové volání nebo SMS, které jsou spojeny s čísly začínající na 90xxx a jsou zpoplatněny speciální sazbou. Příkladem těchto služeb může být zakoupení jízdenek MHD, SMS do TV soutěží, poplatek za parkování, apod.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co jsou dárcovské SMS a jak je odesílat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pokud máte chuť podpořit (různé, zpravidla charitativní) projekty, nemusíte vypisovat složenky nebo zadávat bankovní příkazy. Pro zaslání menších příspěvků stačí poslat DMS – dárcovskou SMS. Dárcovské SMS (DMS Donors Message Service) jsou nástrojem pro velmi rychlé poskytnutí daru, možností, jak přispět na určitou aktivitu skrze placenou SMS zprávu. Je to nejjednodušší a průhledný způsob, jak pomoci zvolené věci. Cena DMS je 30,-Kč, příjemce vaší pomoci obdrží 28,50,-Kč.
                                    <br><br>SMS posílané na číslo 87777 jsou tzv. dárcovské SMS. Jedná se SMS posílané na podporu projektů neziskových organizací. SMS, kterou odesíláte, neplatíte. Zpět na váš mobil obdržíte dárcovskou (potvrzovací) SMS z čísla 87777, která stojí 30,- Kč. Na konto projektu jde nejméně 27,-Kč a zbytek, necelé 3,- Kč, pokrývá náklady, které vznikají Fóru dárců a jeho technickému agregátovi. Mobilním operátorům nehradíte nic.

                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Dají se zablokovat SMS z určitého čísla? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Blokovat příchozí SMS je možné pouze v tom případě, pokud tuto funkci podporuje váš mobilní telefon. Informaci o této možnosti naleznete v návodu vašeho mobilního telefonu.
                                    <br><br>U telefonů, které tuto funkci podporují, stačí zpravidla podržet prst na zprávě od odesílatele, kterého chceme zablokovat a v nabízeném menu se ukáže položka „Přidat do odmítnutých“ či „Přidat do čísel spamu“. Lišit se však může text pro vstup do blokování, ale i způsob zrušení blokování u jednotlivých typů telefonu.

                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak dlouho asi trvá, než dojde odeslaná zpráva příjemci? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Zpracování vaši odeslané SMS probíhá téměř okamžitě. Dobu faktického doručení do zařízení příjemce může ovlivnit značné množství faktorů, jako například: Nedostupnost kvůli vypnutému nebo vybitému zařízení příjemce, pohyb příjemce v oblasti bez pokrytí signálem. V takovém případě bývá zpráva doručena ihned po „oživení“ zařízení příjemce v síti. Za optimálních okolností však příjemce SMS zprávu obdrží v řádech sekund až minut.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Nedaří se mi odesílat SMS co mám dělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Mezi nejčastější příčiny patří:<br>
                                    Nedostupnost signálu nebo chyba komunikace se sítí. Doporučujeme zkontrolovat ukazatel, pokud se zdá být signál v pořádku, restartujte své zařízení.
                                    <br>Zkontrolujte si nastavení vašeho zařízení a ověřte správnost čísla SMS centra v nastavení SMS.
                                    <br>Zkontrolujte správnost čísla, kam posíláte SMS
                                    <br>Vyzkoušejte vaši SIM a odeslání SMS z jiného telefonu pro zjištění kde je chyba.
                                    <br>Pokud při testu vaši SIM v jiném zařízení potíže přetrvávají, zavolejte na naši Zákaznickou linku, nebo navštivte některé z našich Klientských center.
                                    <br>V případě podezření že je chyba ve vašem mobilním zařízení, doporučujeme obrátit se na výrobce

                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Nepřicházejí mi SMS, co mám dělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Zkontrolujte, zda nemáte plnou paměť pro přijaté SMS. Případně zkuste zapnout a vypnout telefon – je možné, že telefon chybně komunikuje se sítí. Pokud toto nepomůže, kontaktujte naši Zákaznickou linku, nebo navštivte některé z našich Klientských center.                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jsem v zahraničí a přijímám i odesílám SMS, kolik mě to bude stát? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Za příjem SMS v zahraničí nic neplatíte. Pokud během svého pobytu v zahraničí SMS zprávy i odesíláte, cena za jednotlivou SMS je určena zemí a Zónou ve které se nacházíte. Podobné informace o cenách SMS ze zahraničí naleznete v ceníku.                                 </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co jsou to MMS? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Multimedia Messaging Service  - MMS neboli multimediální zprávy. Prostřednictvím MMS můžete odeslat text, obrázek, ale i krátké video či zvuk do celkové maximální velikosti 300kB.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kolik mě bude stát posílání MMS ze zahraničí? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Cena MMS závisí na tarifu, který máte nastavený. Více informací o cenách naleznete v ceníku.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Mohu ze zahraničí odesílat MMS? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Máte-li zapnutý a aktivní roaming a vaše mobilní zařízení službu příjmu a odesílání MMS podporuje, odesílání MMS ze zahraniční nebude problém.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Obdržel jsem zprávu, že mi nebylo možné doručit MMS, co mám dělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Zkontrolujte si nastavení telefonu, zda máte aktivní službu MMS a povolené datové služby GPRS/EDGE. Je také možné, že máte plnou paměť telefonu.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Mohu MMS odesílat a přijímat, přestože to můj telefon neumí, nebo není správně nastaven? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pokud vaše mobilní zařízení není správně nastaveno, odeslat ani přijmout MMS nepůjde.  Pokud je problém pouze v nastavení, obraťte se na naše Kontaktní centrum nebo Zákaznickou linku.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak nastavím telefon pro příjem a odesílání MMS? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Jestliže jste si pořídili nové mobilní zařízení, které není nastaveno pro používání našich služeb a nefunguje vám odesílání ani příjem MMS, obraťte se na naši Zákaznickou podporu nebo navštivte naše Klientské centrum. Případně kliknutím na vypsané odkazy si můžete vaše zařízení zkusit nastavit sami.
                                    <a href="https://www.t-mobile.cz/podpora/nastaveni-sluzeb/sms-mms/mms-krok-za-krokem">MMS krok za krokem</a>,<a href="https://www.t-mobile.cz/podpora/nastaveni-telefonu/nastaveni-telefonu-pro-mms-wap-a-e-mail/nastaveni-mms">Nastevení MMS</a>
                                </p>

                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kolik stojí posílání SMS a MMS z a do ciziny? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Cena závisí na tom, z / do jaké země SMS nebo MMS posíláte a také na tarifu, který máte nastavený. Více informací o cenách MMS včetně cen SMS naleznete v ceníku.                                    <a href="https://www.t-mobile.cz/podpora/nastaveni-sluzeb/sms-mms/mms-krok-za-krokem">MMS krok za krokem</a>,<a href="https://www.t-mobile.cz/podpora/nastaveni-telefonu/nastaveni-telefonu-pro-mms-wap-a-e-mail/nastaveni-mms">Nastevení MMS</a>
                                </p>

                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co může být obsahem MMS? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pomocí MMS zprávy můžete poslat obrázky, text, krátké video nebo zvukový záznam. Obsah v rámci MMS lze i kombinovat. Pozor však na velikost. Maximální MMS je do 300kB a ne všechna zařízení maximální velikost podporují.                                 </p>

                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kolik souborů můžu ke zprávě připojit? jaká je jejich maximální velikost? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Do MMS zprávy můžete připojit i více souborů, většina běžných zařízení např. obrázky zkomprimuje (zmenší) tak, aby se pomocí MMS daly odeslat. Čím více souborů však připojíte, tím více je potřeba je zmenšit, aby byla dodržena celková maximální velikost MMS, tedy 300kB.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Odebírá MMS data? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Abyste mohli přijímat a odesílat MMS, je potřeba mít data aktivována a povolen datový provoz. Odesílání a příjem MMS se však na vašich placených spotřebovaných datech neprojeví.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Můj telefon nepodporuje MMS, mohu si přijatou MMS prohlédnout jinak? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Aktuálně jinou možnost zobrazení MMS nenabízíme.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="fRow">
                    <div class="fRowTitle showMoreActual">Nastavení telefonu a SIM <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jaké parametry je možné na SIM kartě nastavit? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Technické parametry na SIM kartě lze nastavit v samoobslužné Klientské zóně, potvrzenou e-mailovou komunikací, nebo osobně na Klientském centru.<br><br>

                                    Dále se jedná o tyto parametry:<br>
                                    Komunikační jazyk<br>
                                    Uvítací roaming SMS<br>
                                    Povolení SMS<br>
                                    Prémium služby MMS<br>
                                    Příchozí hovory<br>
                                    Odchozí hovory<br>
                                    přesměrování hovorů<br>
                                    Přidržení hovorů<br>
                                    Skrytí ID volajícího<br>
                                    Hlasová schránka<br>
                                    Jazyk hlasové schránky<br>
                                    Limit v Kč pro roamingová data
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Lze pozastavit SIM kartu na dobu určitou a za jakých podmínek? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    SIM kartu lze pozastavit na dobu určitou. Pozastavení lze provést v Klientské zóně v sekci Pozastavení SIM.
                                    Zpětná reaktivace SIM karty je zpoplatněna jednorázově ve výši 99,-
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Podporují PODA SIM karty elektronické bankovnictví přes SIM kartu (SIM toolkit)? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    SIM karty PODA tuto službu nepodporují
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak funguje přenos čísla k PODA? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Pro přenos mobilního čísla od stávajícího operátora od Vás budeme potřebovat tzv. ČVOP (číslo výpovědi vašeho opuštěného operátora), které Vám sdělí současný mobilní operátor.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="fRow bottomLine">
                    <div class="fRowTitle showMoreActual">LTE (Long Term Evolution) <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co je LTE ? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    LTE (Long Term Evolution) je technologie, která navazuje na současnou 3G síť a přináší vylepšení hned v několika ohledech. Kromě značného navýšení rychlosti v obou směrech, zlepšuje kvalitu běžných hovorů za stejnou cenu dle vašeho tarifu a bez spotřeby dat. Na technologii LTE je teoreticky možno dosáhnout rychlosti až 225Mb/s pro stahování a 57,6Mb pro odesílání a to s nižší latencí. V praxi bude maximální dosažená rychlost vzhledem k vysoké agregaci samozřejmě o poznání nižší. Aktuálně se jedná o technologii dovolující přenos hovorů v nejvyšší kvalitě bez okolních ruchů, rychlejší spojení a možnost volat i surfovat zároveň.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Chci aktivovat LTE, co mám udělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Abyste LTE mohli využívat, potřebujete mobilní zařízení, které LTE podporuje a paušál s daty.  Pak stačí jen být v místě pokrytém LTE a je to.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Podporují PODA SIM karty elektronické bankovnictví přes SIM kartu (SIM toolkit)? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    SIM karty PODA tuto službu nepodporují
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Proč bych si měl pořídit LTE telefon, má LTE budoucnost? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    LTE v současnosti představuje nejmodernější a nejkvalitnější způsob volání. Jsme přesvědčeni, že s přibývající podporou v telefonech se LTE technologie v budoucnu stane hlavní technologií pro volání.
                                </p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
	</div>

    <div id="sectionPevnaLinka" class="sluzba-section none">
        <?php echo $this->renderElement('layout/pevna'); ?>
        <div class="wrapper">
            <div class="faq mt50" id="faq">
                <div class="fRow mt25">
                    <div class="fRowTitle showMoreActual">Pevná linka <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jakým způsobem mohu aktivovat pevnou linku <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Hlasová schránka <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">SMS v pevné síti <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
	
	<div id="sectionRoaming" class="sluzba-section none">
		<?php echo $this->renderElement('layout/roaming'); ?>
        <div class="wrapper">
            <div class="faq mt50" id="faq">
                <div class="fRow mt25">
                    <div class="fRowTitle showMoreActual">Roaming <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co je to ten roaming? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>	Roaming je poskytování telekomunikačních služeb účastníkovi v jiné síti (v jiné zemi) než kde má účastník zaregistrované své telekomunikační služby. Využívá se vždy, když si ze zahraničí chcete ze svého českého čísla zavolat, poslat SMS, nebo se připojit k síti Internet. Bez aktivovaného Roamingu se v zahraničí nikam nedovoláte a ani nikdo vám (mimo linek tísňového volání).</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kdy roaming využiji? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Roaming je potřeba pro správnou funkci vašich mobilních služeb (volání, SMS, MMS, data) když budete v zahraničí. O volání v roamingu více ve Volám ze zahraničí na české číslo v ČR, Volám ze zahraničí na zahraniční číslo, V zahraničí mám příchozí hovor, Kolik mě přijetí hovoru bude stát? </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Kde si mohu aktivovat Roaming? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>	Službu Roaming lze aktivovat ve vaši Klientské zóně, nebo osobně na našem Klientském centru. Stejným způsobem lze rovněž službu zablokovat. Služba je aktivní do jedné hodiny od nastavení povolení služby. Služba Roaming a datový roaming není automaticky aktivovaná s aktivací SIM karty.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Co zkontrolovat před cestou do zahraničí? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Doporučujeme zkontrolovat povolení Roamingu, služba není automaticky aktivovaná s aktivací SIM karty. Pokud budete v zahraničí potřebovat i připojení k internetu, zkontrolujte navíc také aktivaci datového roamingu.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jsem v zahraničí a přestal mi fungovat roaming, co mám dělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>
                                    Ověřte, že v telefonu a na SIM nemáte zablokovaný roaming. Nastavení roamingu u svého čísla najdete na svém účtu v Klientské zóně. Ve svém telefonu v nastavení si zkontrolujte, že máte roaming povolený. Zde lze také povolit i datový roaming (pokud nevyužíváte, nechejte vypnutý)<br />
                                    Ujistěte se, že jste v lokalitě, kde je pokrytí signálem<br />
                                    Je možné, že váš telefon nepodporuje frekvence, které používá operátor v dané zemi. Jaké frekvence podporuje vaše zařízení se dočtete v technické specifikaci vašeho telefonu, nebo na stránkách výrobce.<br />
                                    Ujistěte se, že nemáte omezené služby kvůli prodlení s platbou vyúčtování. Stav úhrad lze zkontrolovat v Klientské zóně.  Je-li tomu tak, služby co nejdříve uhraďte, poté vám obratem omezení zrušíme.<br />
                                    Zkontrolujte v nastavení vašeho zařízení automatickou volbu sítě.<br />
                                    Pokud jste vše zkontrolovali a je v pořádku, doporučujeme vypnout a zapnout telefon.<br />
                                    Jestliže roaming stále nefunguje, může se jednat o potíže v síti operátora, kterého právě používáte. Zkuste přes ruční výběr sítě vybrat jiného roamingového partnera. Pro přihlášení k jiné síti může být nutné výběr potvrdit i několikrát za sebou. Doporučujeme následně vypnout a zapnout vaše mobilní zařízení.
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Jak je to s roamingem na lodích a trajektech. Do jaké zóny patří? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Roamingový provoz na lodích (velké zaoceánské, výletní lodě, trajekty atd.) se účtuje dle ceníku pro 3. zónu.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
	</div>
	
	<div id="sectionZahranici" class="sluzba-section none">
		<?php echo $this->renderElement('layout/zahranici'); ?>
        <div class="wrapper">
            <div class="faq mt50" id="faq">
                <div class="fRow mt25">
                    <div class="fRowTitle showMoreActual">Volání do zahraničí <a class="button circle showMoreActual" href="#">+</a></div>
                    <ul class="w100p">
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám z ČR do zahraničí na české číslo <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>Pokud jste v ČR a voláte na české číslo, které je momentálně v zahraničí, řídí se cena volání, SMS či MMS ceníkem vašeho domácího tarifu. Volání vás bude stát stejně, jako byste volali v rámci České Republiky. Během hovoru čerpáte volné minuty ze svého domácího tarifu. Zákazník v zahraničí zaplatí cenu příchozího hovoru podle svého roamingového tarifu a země, ve které se nachází.  Před voláním se ujistěte, že máte před číslem českou předvolbu +420.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám z ČR do zahraničí na zahraniční číslo <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>K volání do zahraničí nemusíte nic aktivovat, ani nastavovat. Stačí zadat celé zahraniční volané číslo, včetně mezinárodní předvolby země kam voláte, např. +44 xxx xxx pro Velkou Británii a volat. Nejprve se ale podívejte, do kterého regionu spadá země, do které voláte. Podle toho se liší sazba za 1min hovoru. Zde je <a href="/uploaded/dokumenty/cenik_mobildozahranici.pdf">ceník pro volání do zahraničí</a>. </p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám ze zahraničí na české číslo v ČR <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>	V takovém případě voláte v rámci Roamingu. Cena za 1min odchozího volání z cizí země se řídí ceníkem pro odchozí volání v rámci Roamingu. Cena sazby odchozího volání se liší dle regionu, do které země, ve které se nacházíte spadá.  Přehled cen pro jednotlivé země a regiony.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Volám ze zahraničí na zahraniční číslo <a class="button circle showMoreActual" href="#">+</a></div>
                                <p>	Voláte-li z cizí země na zahraniční číslo téže země, nebo země stejného regionu / zóny, cena za 1 min. odchozího a příchozího hovoru odpovídá ceně volání v roamingu ve stejném regionu. Platí pro EU a EEA. Voláte-li do jiného regionu, cena za volání je shodná s cenou vyšší zóny, do které voláte.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">V zahraničí mám příchozí hovor, kolik mě přijetí hovoru bude stát? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p> Za přijetí příchozího hovoru zaplatíte podle svého roamingového tarifu dle země, ve které právě jste. To platí i pro přijetí mezinárodního hovoru na české číslo nacházející se aktuálně v cizí zemi. Volající z České republiky platí při volání na české číslo aktuálně v zahraničí stejnou cenu, jako při tuzemském volání.</p>
                            </div>
                        </li>
                        <li>
                            <div class="fRow">
                                <div class="fRowTitle showMoreActual">Bydlím blízko hranic, na telefonu mi často naskakuje síť zahraničního operátora, co mám dělat? <a class="button circle showMoreActual" href="#">+</a></div>
                                <p> Chcete-li zamezit automatickému přepínání mezi sítěmi v pohraničí, doporučujeme vypnout automatickou volbu sítě ve vašem mobilním zařízení na ručně nastavenou preferovanou síť. Kde tuto funkci najít se liší od typu mobilního zařízení a systému, který používá. Doporučujeme pročíst návod k obsluze. Nebo zakažte službu roaming.</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
	</div>
	
	
	<div class="wrapper">
		<div class="buttons w100p mb50">
			<a href="/dokumenty-ke-stazeni/" class="button">Návody, dokumenty...</a>
			<a href="/podpora/" class="button blue">Potřebujete poradit?</a>
			<a href="/kontakty/" class="button">Kontaktujte nás</a>
		</div>
	</div> 
	

	
	<br class="clear" />
	
	<a href="/kontakty/kontaktni-formular/" class="button wider">Pomoc, potřebuji více informací</a> 
	<!--<a href="/podpora/dotazy-klientu/volani/" class="button blue">Zobrazit více rad</a>-->
</div>