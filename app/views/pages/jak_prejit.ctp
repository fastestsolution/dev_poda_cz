<div class="mainImg cover_img" data-src="/css/layout/banner/jak_prejit-bg.jpg">
	<div class="middleText">
		<h1 class="title">Jak přejít k Podě?<small>Zcela jednoduše! Jak jinak...</small></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<br class="clear" />

<div class="smaller-wrapper">
	<p class="txt-center mt50"><strong class="black uppercase">Pokud máte obavy ze změny operátora, jsou zbytečné.<br />O vše se bez průtahů a problémů postaráme.</strong></p>
	
	<p class="txt-center mt25">Vyberte si, prosím, jeden z níže uvedených komunikačních kanálů, spojte se s námi nebo se můžeme spojit my s Vámi a společně hladce Vás můžeme přivítat do rodiny PODA.</p>
</div>

<div class="wrapper contacts">
	<ul class="icons-list mt50">  
		<li>
			<img src="/css/layout/icons/penize-icon.png" alt="Neutrácejte zbytečně" />
			
			<strong>Zavolejte nám</strong>
			<p>Jsme Vám k dispozici od pondělí do neděle od 7:00 do 19:30...</p>
			<strong class="fs40 blue mt25">844 844 033<br />730 430 430</strong>
		</li>
		<li>
			<img src="/css/layout/icons/lupa-icon.png" alt="Mějte výdaje pod kontrolou" />
			
			<strong>My zavoláme vám!</strong>
			<p>Nechte nám na Vás kontakt a my Vám zavoláme! Žádný problém...</p>
			
			<div class="load_contact_form" data-type="3"></div>
			<?php //echo $this->requestAction('/contact_forms/3', array('return')); ?>
		</li>
		<li>
			<img src="/css/layout/icons/prasatko-icon.png" alt="Nejvýhodnější cenyt" />
			
			<strong>Nechce se vám volat?</strong>
			<p>Nevadí, napište nám níž uvedený @mail a můžeme vše probrat elektronicky...</p>
			
			<a href="mailto:info@poda.cz"><strong class="fs40 blue mt25 lowercase">info@poda.cz</strong></a>
		</li>
	</ul>
</div>