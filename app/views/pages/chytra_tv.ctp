<div class="mainImg cover_img h270" data-src="/css/layout/banner/chytratv-bg.jpg">
	<div class="middleText">
		<h1 class="title">Chytré funkce TV<small>Až 36 hodin zpětně. Pořad, který chcete vidět znovu, klidně spusťte od začátku.</small></h1>
	</div>
</div> 
 
<br class="clear" />

<div class="article_text txt-center funkce"> 
    <div class="wrapper mt50">
	    
	    <h2>Mých 5, Mých 10 a Mých 15</h2>
	    <a href="/mych-5-mych-10-a-mych-15/" class="button">Dozvědět se více...</a>
	    
	    <br class="clear" />
	    
	    <img src="/uploaded/ikony/01-pauza.png" alt="Pauza" />
        <h2>Chytrá TV - funkce PAUZA</h2>
        <p class="mt25"><strong class="black uppercase">Udělejte si při sledování pauzu a pozastavte pořad až na 4 hodiny</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <p class="hiddenInfo none">Nechcete přijít ani o okamžik sledovaného pořadu? Káva, telefon, dovaření večeře… nic již pro Vás nebude problém. Díky funkci PAUZA si pořad stopnete, a ve sledování budete dál pokračovat, až si budete přát. Právě vysílaný pořad lze pozastavit až na 4 hodiny.</p>
    </div>   
    
    <div class="wrapper mt50">
	    <img src="/uploaded/ikony/02-spusteni.png" alt="Spuštění" />
        <h2>Chytrá TV - SPUŠTĚNÍ POŘADU OD ZAČÁTKU</h2>
        <p class="mt25"><strong class="black uppercase">Přehrajte si již běžící pořad znovu, od začátku</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <p class="hiddenInfo none">Nestihli jste začátek oblíbeného pořadu? Nic se neděje. Jedním stiskem zeleného tlačítka na dálkovém ovladači aktivujete jeho přehrávání znovu, od začátku. Pořad bude přehráván ze záznamu a Vy v něm budete moci přetáčet – posouvat se kupředu i nazpět, opakovat poutavé záběry i přeskakovat nudná místa.</p>
    </div>

	<div class="blok cover_img small mt50" data-src="/css/layout/banner/chytra01.jpg"></div>

    <div class="wrapper mt50">
	    <img src="/uploaded/ikony/03-nahravani.png" alt="Nahrávání" />
        <h2>Chytrá TV - funkce NAHRÁNÍ</h2>
        <p class="mt25"><strong class="black uppercase">Nahrajte si své oblíbené pořady na 3 měsíce na náš server</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <div class="hiddenInfo none">
	        <p>Bojíte se, že nestihnete nový film či oblíbený seriál? Nebo jste právě narazili na něco, co rozhodně musíte vidět znovu? Nahrajte si vybraný pořad a dívejte se na něj, až budete mít čas a chuť. Zůstane Vám uložen k dispozici pro další přehrávání po dobu tří měsíců.</p>
	        <p><strong class="black uppercase">Potřebujete více než 10 či 20 hodin nahrávacího prostoru?</strong></p>
	        <p>Nahráváte často pořady a není pro Vás dosavadní prostor na ukládání dostačující? Se službou "20 za 20" Vám za 20 Kč měsíčně rozšíříme nahrávací prostor o dalších 20 hodin. Tato služba se nevztahuje k tarifu Základní nabídka, jelikož neobsahuje "chytré funkce" PODA TV.</p>
	        <p><strong class="black uppercase">Jaké jsou výhody nahrávání s chytrými funkcemi PODA TV?</strong></p>
	        <ul>
	            <li>I když nahrávání zapnete v průběhu pořadu, pořad bude nahrán celý, od začátku.</li>
	            <li>Po skončení nahrávaného pořadu se nahrávání automaticky vypne.</li>
	            <li>Během nahrávání pořadu lze přepínat na jiné kanály.</li>
	            <li>Je možné současně nahrávat pořady na více stanicích.</li>
	            <li>Máte-li více set-top boxů, sdílejí nahrávky mezi sebou – programy nahrané na jednom set-top boxu můžete sledovat z jiného.</li>
	            <li>Nahrávání můžete zapnout přímo při sledování pořadu, naprogramovat předem nebo aktivovat zpětně pro pořady, které již skončily – viz funkce ARCHIV.</li>
	        </ul>
        </div>
    </div>
    
    <div class="wrapper mt25">
	    <img src="/uploaded/ikony/04-archiv.png" alt="Archiv" />
        <h2 class="mt25">Chytrá TV - funkce ARCHIV</h2>

        <p class="mt25"><strong class="black uppercase">Přehrávejte a nahrávejte i pořady, které již skončily</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <div class="hiddenInfo none">
	         <p>Přišli jste domů pozdě a zmeškali pořad, na který jste se těšili? S naším archivem máte po problému. Ukládáme Vám až 7 dní zpětně pořady, které již skončily. Dokud se zobrazují v Programovém průvodci, máte možnost si je znovu přehrát či nahrát. A stejně jako při spuštění pořadu od začátku využívat možnosti pozastavení a přetáčení.</p>
	        <p><strong class="black uppercase">Nahrávejte z ARCHIVU</strong></p>
	        <p>Svůj oblíbený pořad si můžete nahrát i ze zpětného archivu. Není pro Vás 10 či 20 hodinový nahrávací prostor dostačující? Připravili jsme pro Vás službu "20 za 20".</p>
	        <p class="mt25"><strong class="black uppercase">"20 za 20"</strong></p>
	        <p>Nahráváte často pořady a potřebovali byste ještě více nahrávacího prostoru? Za 20 Kč měsíčně Vám rozšíříme nahrávací prostor o dalších 20 hodin. Tato služba se nevztahuje k tarifu Základní nabídka, jelikož neobsahuje "chytré funkce" PODA TV.</p>
        </div>
    </div>
    
    <div class="blok cover_img small mt50" data-src="/css/layout/banner/chytra02.jpg"></div>

    <div class="wrapper mt50">
	    <img src="/uploaded/ikony/05-pruvodce.png" alt="Průvodce" />
        <h2>Chytrá TV – funkce PROGRAMOVÝ PRŮVODCE</h2>
        <p class="mt25"><strong class="black uppercase">Zjistěte TV program a informace o vysílaných pořadech až 10 dní dopředu a 7 dní zpětně</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <p class="hiddenInfo none">V programovém průvodci jsou zobrazeny všechny pořady, které právě hrají nebo budou v dalších dnech vysílány, či už skončily. Přímo z programového průvodce si můžete spustit pořady až se 7denním zpožděním. Pořady snadno a rychle vyhledáte pomocí fulltextového vyhledávače.</p>
    </div>
    
    <div class="wrapper mt50">
	    <img src="/uploaded/ikony/06-samoobsluha.png" alt="Samoobsluha" />
        <h2>Chytrá TV – funkce SAMOOBSLUHA</h2>
        <p class="mt25"><strong class="black uppercase">Objednávejte si TV služby přímo prostřednictvím svého set-top boxu</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        <p class="hiddenInfo none">Vstupte do Samoobsluhy a sestavte si svou vlastní TV nabídku – objednávejte a odhlašujte programové TV balíčky nebo si rozšiřte svůj nahrávací prostor se službou "20 za 20".</p>
    </div>

	<div class="blok cover_img small mt50" data-src="/css/layout/banner/chytra03.jpg"></div>

    <div class="wrapper mt25">
        <img src="/uploaded/ikony/07-titulky.png" alt="Titulky" />
        <h2 class="mt25">Chytrá TV – funkce TITULKY</h2>

        <p class="mt25"><strong class="black uppercase">Aktivujte si titulky ve vlastním nebo cizím jazyce</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <p class="hiddenInfo none">Chcete sledovat pořad v originálním jazyce a při tom se držet českých titulků? Nebo špatně slyšíte a potřebujete podporu? Již to není problém. Využijte volbu titulků a sledujte televizi tak, jak Vám to nejlépe vyhovuje.</p>

    </div>
    <!--<div class="wrapper mt50">
        <h2>Chytrá TV – funkce TITULKY</h2>
        <p class="mt25"><strong class="black uppercase">Aktivujte si titulky ve vlastním nebo cizím jazyce</strong></p>
        <p>Chcete sledovat pořad v originálním jazyce a při tom se držet českých titulků? Nebo špatně slyšíte a potřebujete podporu? Již to není problém. Využijte volbu titulků a sledujte televizi tak, jak Vám to nejlépe vyhovuje.</p>
    </div>-->
    <div class="wrapper mt50">
	    <img src="/uploaded/ikony/08-seznamy.png" alt="Seznamy" />
        <h2>Chytrá TV – funkce SEZNAMY TV STANIC</h2>
        
        <p class="mt25"><strong class="black uppercase">Je Vaše programová nabídka příliš obsáhlá a nemůžete v množství kanálů najít ty, které Vás zajímají? </strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <p class="hiddenInfo none">Vytvořte si vlastní skupiny kanálů. Rozdělte si nabídku stanic podle jejich typu (např. sportovní, hudební, dětské aj.), nebo podle toho, kdo se na co rád dívá. Při sledování televize pak jen stačí na dálkovém ovladači stisknout nulu a vyberete si ze svých seznamů ten, který Vám právě vyhovuje.</p>
    </div> 
        <br class="clear" />
        
    <div class="wrapper">
        <p class="mt25"><strong class="black uppercase">Podrobný návod pro vytvoření vlastních seznamů stanic a změnu pořadí kanálů</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <ul class="hiddenInfo none">
            <li>Vstupte do „MENU“ a přesuňte se na záložku "Skupiny kanálů".</li>
            <li>Zeleným tlačítkem vytvoříte novou vlastní skupinu stanic.</li>
            <li>Na zobrazené klávesnici zadejte název nového seznamu – přesunujte se pomocí šipek na dálkovém ovladači, písmena volte tlačítkem "OK".</li>
            <li>Poté se přesuňte na pole „Potvrdit“ a stiskem "OK“ založení nového seznamu odsouhlaste.</li>
            <li>Vytvořený seznam je zařazen mezi ostatní seznamy kanálů. Tlačítkem "OK" do něj vstupte.  Z nabídky všech dostupných TV programů volte pomocí "OK" ty, které si přejete mít ve své vlastní skupině.</li>

        </ul>
    </div>   
        <br class="clear" />
     
    <div class="wrapper">   
        <p class="mt25"><strong class="black uppercase">Ve vytvořené skupině lze pomocí tlačítka "OK" a šipek nahoru a dolů programy posunovat a měnit tak jejich pořadí podle vlastní preference.</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <div class="hiddenInfo none">
	        <p>
	            Po ukončení úprav vystupte z nabídky pomocí tlačítka "Back" (symbol zakulacené šipky) a své nové nastavení potvrďte "OK".
	        </p>
	        <p class="mt25"><strong class="black uppercase">Vytvářet vlastní skupiny a pořadí stanic je možné i v Klientské zóně PODA.</strong></p>
        </div>
    </div>
    
    <div class="blok cover_img small mt50" data-src="/css/layout/banner/chytra04.jpg"></div>

    <div class="wrapper mt25">
        <img src="/uploaded/ikony/09-rozkoukane.png" alt="Rozkoukané" />
        <h2 class="mt25">Chytrá TV – ROZKOUKANÉ POŘADY</h2>

        <p class="mt25"><strong class="black uppercase">Aktivujte si titulky ve vlastním nebo cizím jazyce</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <p class="hiddenInfo none">Nestihli jste se dodívat na uložený pořad a nevíte, kde jste skončili? Nevadí! Posledně sledovanou pozici Vám necháváme uloženou. Při spuštění nahrávky si už jen vyberete, jestli ji chcete vidět celou znovu od začátku, nebo si přejete pokračovat tam, kde jste naposledy skončili.</p>

    </div>
    <!--<div class="wrapper mt50">
        <h2>Chytrá TV – ROZKOUKANÉ POŘADY</h2>
        <p class="mt25"><strong class="black uppercase">Aktivujte si titulky ve vlastním nebo cizím jazyce</strong></p>
        <p>Nestihli jste se dodívat na uložený pořad a nevíte, kde jste skončili? Nevadí! Posledně sledovanou pozici Vám necháváme uloženou. Při spuštění nahrávky si už jen vyberete, jestli ji chcete vidět celou znovu od začátku, nebo si přejete pokračovat tam, kde jste naposledy skončili.</p>
    </div>-->
    <div class="wrapper mt50">
	    <img src="/uploaded/ikony/10-sdileni.png" alt="Sdílené" />
        <h2>Chytrá TV – SDÍLENÍ NAHRÁVEK</h2>
        
        <p class="mt25"><strong class="black uppercase">Postup pro sdílení nahrávek</strong></p>
        
        <a href="#" class="button smaller showInfo">Jak na to?</a>
        
        <div class="hiddenInfo none">
        
	        <p>Máte na set-top boxu uloženou skvělou nahrávku, kterou by měl vidět i někdo z Vašich blízkých? Nebo jste si nestihli nahrát zajímavý pořad a víte o někom, kdo jej má uložen na svém set-top boxu PODA? Podělte se o své zážitky! Pomocí nové funkce si posílejte své nahrávky mezi sebou. </p>
	        
	        <ul>
	            <li>Vstupte do MENU, sekce "Nahrané pořady".</li>
	            <li>Stiskněte modré tlačítko na ovladači a vyberte, zda si přejete předat svou nahrávku někomu jinému, nebo si přejete cizí nahrávku zařadit mezi své.</li>
	            <li>Pokud zvolíte poslat svou nahrávku dál, vygenerujte kód a předejte ho tomu, komu ji posíláte.</li>
	            <li>On nahrávku na svém set-top boxu pomocí kódu zobrazí. Pro zařazení nahrávky z jiného set-top boxu mezi vlastní použijte kód, vygenerovaný druhou stranou.</li>
	            <li>TV program, na kterém je zvolený pořad vysílán, musí mít v rámci svého tarifu oba účastníci sdílení.</li>
	            <li>Nahrávky můžete sdílet i prostřednictvím své Klientské zóny.</li>
	
	        </ul>
	        <br />
	        <a href="/uploaded/dokumenty/navod_2ovladace.pdf" target="_blank" class="button blue wider">Návod k obsluze set-top boxu</a>
        </div>
    </div>
</div>

<br class="clear" />

<div class="wrapper">
		
	<p class="txt-center mt50"><strong class="big black uppercase bold">Máte zájem?</strong></p>
	<p class="txt-center mt25">Zadejte, prosím, adresu svého domova kde budete TV užívat...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>

<br class="clear" />


<div class="wrapper mt50">
    <h2 class="txt-center uppercase mt50">Často kladené otázky</h2>

    <div class="wrapper">
        <div class="faq mt50" id="faq"> 
            <div class="fRow mt25">
                <div class="fRowTitle showMoreActual">TV kanály <a class="button circle showMoreActual" href="#">+</a></div>

                <ul class="w100p">
                    <li>
                        <div class="fRow">
                            <div class="fRowTitle showMoreActual">Můžu měnit pořadí TV kanálů? <a class="button circle showMoreActual" href="#">+</a></div>

                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow">
                            <div class="fRowTitle showMoreActual">Může mít každý z rodiny vlastní řazení TV kanálů? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                </ul>

            </div>


            <div class="fRow">
                <div class="fRowTitle showMoreActual">Chytré funkce - zpětné zhlédnutí <a class="button circle showMoreActual" href="#">+</a></div>

                <ul class="w100p">
                    <li>
                        <div class="fRow">
                            <div class="fRowTitle showMoreActual">Jak funguje zpětné zhlédnutí? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>

                        <div class="fRow">
                            <div class="fRowTitle showMoreActual">Na jak dlouho můžu zastavit živé vysílání televize? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>

                    </li>
                    <li>
                        <div class="fRow">
                            <div class="fRowTitle showMoreActual">můžu využívat funkci Zpětné zhlédnutí u kteréhokoliv kanálu? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="fRow">
                <div class="fRowTitle showMoreActual">Chytré funkce - nahrávání <a class="button circle showMoreActual" href="#">+</a></div>
                <ul class="w100p">
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Co potřebuji, abych mohl začít využívat nahrávání? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>

                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Můžu nahrávat pořad, na který se právě dívám? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Jaká je kapacita nahrávání? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Můžu si nastavit opakované nahrávání, třeba u seriálů? <a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Můžu nahrávat pořady, které už byly odvysílané?<a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Co když nahrávaný pořad skončí později, než je uvedeno v programu?<a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Můžu nahrávat více pořadů najednou?<a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Můžu nahrávat všechny pořady ze všech TV kanálů?<a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Můžu během nahrávání sledovat jiný TV kanál?<a class="button circle showMoreActual" href="#">+</a></div>
                            <p>Lorem Cupcake ipsum dolor sit amet pastry jelly-o. Toffee chupa chups chocolate bar. Tart macaroon donut. Oat cake toffee powder donut biscuit tiramisu lollipop chupa chups</p>
                        </div>
                    </li>
                </ul> 
            </div>



            <div class="fRow">
                <div class="fRowTitle showMoreActual">Chytré funkce - archiv <a class="button circle showMoreActual" href="#">+</a></div>
                <ul>
                    <li>
                        <div class="fRow bottomLine">
                            <div class="fRowTitle showMoreActual">Kde zjistím, které pořady se nahrávají do archivu? <a class="button circle showMoreActual" href="#">+</a></div>
                            <ul></ul>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>