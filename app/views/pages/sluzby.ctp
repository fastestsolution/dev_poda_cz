<div class="mainImg cover_img" data-src="/css/layout/banner/tv-bg.jpg">
	<div class="middleText">
		<h1 class="title"><?php echo $smallbox_list[4]['Smallbox']['title']; ?></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<br class="clear" />

<div class="smallest-wrapper">
	<?php echo $smallbox_list[5]['Smallbox']['text']; ?>
</div>

<?php echo $this->renderElement('layout/televizni-nabidka'); ?>

<br class="clear" />

<div class="wrapper no-relative">
	<p class="txt-center mt50"><strong class="big black uppercase bold">Až 80 TV Programů do 115,-</strong></p>
	<p class="txt-center mt25">Abychom Vám mohli nabídnout co nejvíce programů, zadejte adresu svého domova...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div> 
</div>

<div class="shadow-line"></div> 

<br class="clear" />

<div class="blok cover_img " data-src="/css/layout/banner/bg-tv-sec1.png">
	<h2>Chytré funkce TV</h2>
	
	<div class="wrap">
		<p>Přinášíme Vám novou úroveń pohodlí při sledování televize. Využijte funkce "Chytré TV" a přizpůsobte televizní program ideálně svým potřebám. Fanděte a přehrávejte si dokola poutavé záběry.</p>
	    <br class="clear" />
        <a href="/tv-sluzby/chytre-funkce-tv/" class="button color-blue wider">Více o chytrých funkcích</a>
    </div>
</div>

<div class="blok cover_img " data-src="/css/layout/banner/bg-tv-sec2.png">
	<h2>PODA.net TV</h2>
	
	<div class="wrap">
        <p>Televizní vysílání můžete mít dostupné všude tam, kde potřebujete!<br>Opravdu! Kdekoliv!</p>
        <br class="clear" />
        <a href="/tv-sluzby/podanet-tv/" class="button color-blue">Více o poda.<span class="lowercase bold">net</span> TV</a>
    </div>
</div>

<div class="blok cover_img " data-src="/css/layout/banner/bg-tv-sec3.png">
	<h2>Potřebujete televizi?</h2>
	
	<div class="wrap">
        <p>Chcete sledovat sportovní pořad i oblíbený seriál najednou? Připojte si více televizí a sledujte s PODA jedoduše oblíbené pořady podle toho, co Vás skutečně nejvíce těší.</p>
        <br class="clear" />
        <a href="/tv-sluzby/seznam-programu/" class="button color-blue">Seznam programů</a>

    </div>
</div>

<?php /*
<div class="blok cover_img " data-src="/css/layout/banner/bg-tv-sec4.png">
    <h2>Toužíte po šiřší nabídce televizních služeb?</h2>

    <div class="wrap">
        <br class="clear" />
        <a href="#" id="scrollTo" class="button color-red">Ano, mám zájem</a>
    </div>
</div>
*/ ?>

<br class="clear" />

<div class="wrapper no-relative">
	<div class="buttons w100p mb50">
		<a href="/dokumenty-ke-stazeni/" class="button">Návody, dokumenty...</a>
		<a href="/podpora/" class="button blue">Potřebujete poradit?</a>
		<a href="/kontakty/" class="button">Kontaktujte nás</a>
	</div>
	
	<br id="jumpHere" class="clear" />

	<p class="txt-center"><strong class="big black uppercase bold">Není nic jednoduššího než se připojit...</strong></p>
	<p class="txt-center mt25">Zadejte adresu místa, kterou chcete připojit...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>

<div class="shadow-line"></div>

<br class="clear" />

<h2 class="txt-center uppercase mt50">Často kladené otázky k internetu</h2>
<div class="wrapper">
    <div id="faq" class="faq mt50">
        <div class="fRow mt25">
            <div class="fRowTitle showMoreActual">Kde a jak si mohu objednat Vaše služby? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>
        <div class="fRow">
            <div class="fRowTitle showMoreActual">Kdy a jak službu dostanu? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>

        <div class="fRow">
            <div class="fRowTitle showMoreActual">V jaké síti působí poda mobil? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>

        <div class="fRow bottomLine">
            <div class="fRowTitle showMoreActual">Kolik zaplatím za SIM kartu? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>
    </div>
</div>


<br class="clear" />
<a href="/kontakty/kontaktni-formular/" class="button wider">Pomoc, potřebuji více informací</a> 
<!--<a href="/podpora/dotazy-klientu/tv-sluzby/" class="button blue">Zobrazit více rad</a>-->