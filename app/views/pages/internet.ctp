
<div class="mainImg cover_img" data-src="/css/layout/banner/internet-bg.jpg">
	<div class="middleText">
		<h1 class="title"><?php echo $smallbox_list[1]['Smallbox']['title']; ?></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>
 
<br class="clear" />

<div class="smallest-wrapper">
	
	<?php echo $smallbox_list[2]['Smallbox']['text']; ?>
	 
	<!--<a href="#" class="button" id="show-more-text">Zobrazit více...</a>-->
</div>
 
<div id="hidden-text" class="wrapper mt25 txt-center">
	
	<p class="txt-center mb25">Řešení napojení do sítě Internet je robustní a plně redundantní. Hlavní síťový uzel je připojen k tranzitnímu uzlu CeColo v Praze dvěma na sobě nezávislými trasami. Obě trasy mají kapacitu 10 Gb, jsou od odlišných operátorů a nemají žádný styčný bod či úzké místo. V uzlu CeColo máme redundantně dva vysoce výkonné hraniční routery, jež jsou schopny pojmout plnou kapacitu přenosů mezi sítěmi PODA a Internetem.</p>
	
	<ul id="sluzba-buttons" class="sluzba-buttons">
		<li><a href="#" data-id="sectionOptickaSit" class="button">Optická síť</a></li>
		<li><a href="#" data-id="sectionBezdratovaSit" class="button">Bezdrátová síť</a></li>
		<li><a href="#" data-id="sectionPevnyInternet" class="button">Pevný internet</a></li>
		<li><a href="#" data-id="sectionMobilniInternet" class="button">Mobilní internet</a></li>
	</ul>
	
	<div id="sectionOptickaSit" class="sluzba-section none">
        <?php echo $this->renderElement('layout/gpon-internet'); ?>

        <p class="txt-center">Prostřednictvím optické sítě jsou poskytovány služby ostatním provozovatelům veřejných komunikačních sítí a koncovým klientům společnosti PODA a.s. Využití vlastních optických vedení umožňuje poskytování služeb ve vysoké kvalitě a se spolehlivostí, která je u rádiových spojů nedosažitelná.</p>
	</div>
	
	<div id="sectionBezdratovaSit" class="sluzba-section none">
        <?php echo $this->renderElement('layout/bezdratovy-internet'); ?>

        <p class="txt-center">Bezdrátová (rádiová) síť PODA je vybudována s důrazem na kvalitu. V praxi to představuje 350 retranslačních stanic rozmístěných po celé republice a tvořících páteřní síť. Retranslační stanice jsou propojeny optickými vlákny nebo vysokokapacitními bezdrátovými spoji v pásmech 18, 11 či 10,5 GHz. Z retranslačních stanic vede k zákazníkům více než 3 000 koncových bezdrátových spojů.</p>
	</div>
	
	<div id="sectionPevnyInternet" class="sluzba-section none">
        <?php echo $this->renderElement('layout/pevny-internet'); ?>

        <p class="mb25">Prostřednictvím pevného internetu připojujeme jak byty v bytových jednotkách, tak rodinné domy. <strong>Připojení rodinných domů</strong> je realizováno pomocí rádiového spoje ve volném pásmu 5GHz. Spoj je vybudován mezi místem připojení a nejbližším bodem páteřní sítě PODA a.s. Pro realizaci spojení se používají nejmodernější zařízení výrobců MikroTik a Ubiquiti Networks. Rychlost připojení dosahuje až 30Mbit/s. Jde o sdílené připojení s agregačním poměrem 1:3 příp. 1:2. Toto připojení neumožňuje rozšíření o službu IPTV. Bez možnosti samoinstalace.</p>
	
		<p class="mb25"><strong>Připojení bytů v bytových jednotkách</strong> realizujeme optickými kabely. Optický signál přivedeme do domu, kde je ukončen aktivním rozbočovacím prvkem. Jednotliví uživatelé jsou pak k aktivnímu prvku připojeni UTP kabelem. Přípojka je v bytě ukončena datovou zásuvkou se dvěma porty RJ45. Maximální rychlost dosahuje až 100Mbit/s bez agregace. Prostřednictvím optického kabelu poskytujeme také vysílání IPTV (digitální televize). Pro Vaše pohodlí a finanční úsporu můžete získat obě služby současně. Možnost samoinstalace.</p>
		
		<p class="mb25">Firemní klienty připojujeme k internetu optickým kabelem nebo radiovým spojem. Pro radiové připojení využíváme volná pásma 5 GHz, 17GHz, 80 GHz a placené pásmo 10GHz.</p>
		
		<p class="mb25"><a class="blue" href="/gpon/">Připojení technologií GPON</a>. Jedná se o moderní technologii pro provoz internetu, televize a hlasových služeb. Přímo do domácností zákazníků přivedeme optický kabel, který je ukončen převodníkem optického signálu (tzv. zakončovací jednotkou). Tato jednotka má i funkci jednoduchého Wi-Fi routeru. Maximální rychlost vysokorychlostního internetu dosahuje až 300Mbit/s bez agregace.</p>
		
		<p class="mb25">Prostřednictvím optického kabelu poskytujeme také vysílání IPTV (digitální televize), o které je možné rozšířit internetové připojení. Pro Vaše pohodlí a finanční úsporu můžete získat obě služby současně. Možnost samoinstalace.</p>




    </div>
	
	<div id="sectionMobilniInternet" class="sluzba-section none">
        <?php echo $this->renderElement('layout/mobilni-internet'); ?>
        <p class="mb25">LTE (Long Term Evolution) je zkratka pro novou generaci mobilní datové sítě. Jedná se mezistupeň mezi 3G a 4G sítěmi a přináší vylepšení v několika ohledech. Tím nejzásadnějším je samozřejmě navýšení rychlosti v obou směrech – stahování i nahrávání.</p>

		<p class="mb25">LTE je schopno vyvinout teoretickou maximální rychlost datových přenosů až 175 Mb/s pro stahování a až 57,6 Mb/s pro odesílání dat v jednom frekvenčním pásmu při šířce kanálu 20 MHz. V praxi je rychlost samozřejmě o poznání nižší, neboť je dělena mezi spoustu jiných uživatelů.</p>
		
		<p>Síť LTE přináší velký komfort, stačí si jen vybrat objem přenesených dat podle svých potřeb a surfovat bez obav kdykoli a kdekoli.</p>
	</div>
</div>


<br class="clear" />

<div class="wrapper no-relative">
	<p class="txt-center mt50"><strong class="big black uppercase bold">Internet až rychlostí 300 Mbps od 250,-</strong></p>
	<p class="txt-center mt25">Zadejte adresu místa, kterou chcete připojit...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?> 
	</div>
</div>
 
<div class="shadow-line"></div>

<br class="clear" />

<div class="blok cover_img " data-src="/css/layout/banner/internetBlok1.jpg">
	<h2>K čemu je rychlý internet?</h2>
	
	<div class="wrap">
		<p>Vysokorychlostní internet zaručuje dostatečně plynulé přehrávání filmů nebo videí a pokud je vás doma více, již nikdy, díky internetu PODA, neuvidíte načítací kolečko...
Rychlá funkčnost YouTube HD, Skype, Facebook, atd...</p>
	</div>
</div>

<div class="blok cover_img " data-src="/css/layout/banner/internetBlok2.jpg">
	<h2>Proč zrovna internet od Pody?</h2>
	 
	<div class="wrap">
		<p>Internet poskytujeme už nějaký ten pátek, takže víme, že jak laik, tak i profík potřebují profesionální přístup a někoho, kdo mu na druhé straně rozumí.</p>
	</div>
</div>

<div class="blok cover_img " data-src="/css/layout/banner/internetBlok3.jpg">
	<h2>Toužíte po rychlejším internetu nebo si přejete některou z doplňkových služeb?</h2>
	
	<div class="wrap">
		<br class="clear" />
		<a href="#" id="scrollTo" class="button color-red">Ano, mám zájem</a>
	</div>
</div>

<br class="clear" />

<div class="wrapper no-relative">
	<div class="buttons w100p mb50">
		<a href="/dokumenty-ke-stazeni/" class="button">Návody, dokumenty...</a>
		<a href="/podpora/" class="button blue">Potřebujete poradit?</a>
		<a href="/kontakty/" class="button">Kontaktujte nás</a>
	</div>
	
	<br id="jumpHere" class="clear" />

	<p class="txt-center"><strong class="big black uppercase bold">Není nic jednoduššího než se připojit...</strong></p>
	<p class="txt-center mt25">Zadejte adresu místa, kterou chcete připojit...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>

<div class="shadow-line"></div>

<br class="clear" />

<h2 class="txt-center uppercase mt50">Často kladené otázky k internetu</h2> 

<div class="wrapper">
	<div class="faq mt50" id="faq"> 
	    <div class="fRow mt25">
	        <div class="fRowTitle showMoreActual">Dostupnost a připojení internetu od PODY <a class="button circle showMoreActual" href="#">+</a></div>
	
	        <ul class="w100p">
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Jaké výhody získám využíváním služby PODA Internet? <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>⁃	kvalitní a spolehlivé internetové připojení za dobrou cenu<br />
							⁃	garantovanou rychlost a bez časových nebo datových omezení<br />
							⁃	volitelné varianty nastavení stahování a odesílání dat na optického síti<br />
							⁃	televizní vysílání do mobilních zařízení (počítač, tablet nebo mobil) PODA net.TV</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Jakým způsobem se dostane kabel do bytu? <a class="button circle showMoreActual" href="#">+</a></div>
	                    <p>Naši technici jej protáhnou průrazem ve zdi. V bytě je ukončen předávacím rozhraním, konkrétně krabičkou s ethernetovým rozhraním RJ45, nebo optickou zásuvkou.</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Potřebuju pro využití PODA Internet speciální software? <a class="button circle showMoreActual" href="#">+</a></div>
	                    <p>Nepotřebujete. Stačí Váš PC s operační systémem (např. Windows), internetový prohlížeč (např. Chrome nebo Internet Explorer) a e-mailový klient (např. Thunderbird).</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Lze k internetu připojit více počítačů najednou? <a class="button circle showMoreActual" href="#">+</a></div>
	                    <p>Ano, existuje několik řešení, záleží však na způsobu připojení a typu zařízení, pomocí kterého chcete počítače připojit. Pro nalezení ideálního řešení nás prosím kontaktujte.</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Mohu připojit vlastní (bezdrátový) router k PODA Internetu? <a class="button circle showMoreActual" href="#">+</a></div>
	                    <p>Jistě, doporučujeme Vám však správně nastavit zabezpečení Vašeho routeru. Stejně tak doporučujeme router s podrobným českým manuálem, aby pro Vás bylo nastavení co nejjednodušší.</p>
	                </div>
	            </li>
	        </ul>
	    </div>
	    <div class="fRow">
	        <div class="fRowTitle showMoreActual">Rychlost připojení <a class="button circle showMoreActual" href="#">+</a></div>
	        <ul class="w100p">
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Jak si mohu změřit rychlost připojení? <a class="button circle showMoreActual" href="#">+</a></div>
	                    <p>Měření rychlosti je možné provést na některém z odkazů na internetu. Můžete použít třeba tento - <a href="http://beta.speedtest.net" target="_blank">http://beta.speedtest.net</a>. Pro přesné měření je nutné ukončit veškeré aplikace, které využívají připojení k internetu (ICQ, Skype, P2P, Download manažery a jiná stahování). Nepřesné měření může způsobit i škodlivý software v PC, napadený prohlížeč nebo viry.</p>
	                </div>
	            </li>
	        </ul>
	    </div>
	    <div class="fRow">
	        <div class="fRowTitle showMoreActual">IP adresa <a class="button circle showMoreActual" href="#">+</a></div>
	        <ul class="w100p">
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Co je IP adresa? <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>Zkratka IP znamená Internet Protocol, což je protokol, pomocí kterého spolu komunikují všechna zařízení v internetu, dnes nejčastěji označován IPv4.IP adresa je číselné označení, které jednoznačně identifikuje síťové rozhraní v počítačové síti. Síťovým rozhraním může být síťová karta (Ethernet, Wi-Fi), ale může být i virtuální zařízení.</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Jak zjistím svou IP adresu? <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>Svou adresu zjistíte bud přímo v počítači, nebo na některém z odkazů na internetu. Můžete použít třeba tento - <a href="https://www.mojeip.cz/" target="_blank" class="blue">www.mojeip.cz</a></p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Co je IPv6 a podporuje síť PODA tento protokol? <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>IPv6 (internetový protokol verze 6) je v označení nastupujícího protokolu pro komunikaci v současném Internetu (resp. v počítačových sítích, které Internet vytvářejí). IPv6 nahrazuje dosluhující protokol IPv4. Přináší zejména masivní rozšíření adresního prostoru a zdokonalení schopnosti přenášet vysokorychlostně data. Pro používání IPv6 není ze strany uživatele nutná žádná speciální příprava, stejně jako pro IPv4 je však nutné příslušné nastavení síťových prvků (routerů) a aby IPv6 podporoval poskytovatel internetového připojení. Síť PODA podporuje protokol IPv6.</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Co je statická (pevná) IP adresa <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>Adresa, která je rezervovaná pro konkrétní počítač, zůstává po celou dobu služby stejná a nemění se. Tato adresa počítači zůstává i po vypnutí nebo restartu modemu či systému.</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Co je dynamická IP adresa <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>Tato IP adresa je pouze dočasná a při každém novém připojení se může změnit.</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Co je veřejná IP adresa? <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>Tato IP adresa je v celé síti Internet viditelná a zjistitelná. Je možné se na PC s veřejnou IP adresou odkudkoliv na světě přihlásit a pracovat, jako by u něj uživatel seděl doma. Vše, co má veřejnou IP adresu, může být využito odkudkoliv z internetu (tiskárny, IP kamery atd.). Díky viditelnosti a dohledatelnosti veřejné IP adresy ztrácí člověk na internetu anonymitu a je snáze napadnutelný malware (souhrnný název pro počítačové viry, trojské koně atd.)</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Co je privátní (neveřejná) IP adresa <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>Privátní IP adresa je na webu prakticky neviditelná, je schována za pomyslnou bránu poskytovatele. S touto IP adresou můžete komunikovat s internetem, brouzdat po www stránkách, stahovat poštu atd., ale počítače z internetu nemohou navázat spojení.</p>
	                </div>
	            </li>
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Jakou IP adresu přiděluje PODA? <a class="button circle showMoreActual" href="#">+</a></div>
	
	                    <p>Jednu statickou privátní IPv4 adresu a zároveň s privátní IPv4 přidělujeme i statickou veřejnou IPv6 adresu. Veřejnou IPv4 adresu přidělíme jen na žádost klienta. Veřejnou IPv4 adresu si můžete zakoupit za své Věrnostní body v PODA Věrnostním programu.</p>
	                </div>
	            </li>
	        </ul>
	    </div>
	     <div class="fRow bottomLine">
	        <div class="fRowTitle showMoreActual">Datová omezení <a class="button circle showMoreActual" href="#">+</a></div>
	        <ul class="w100p">
	            <li>
	                <div class="fRow">
	                    <div class="fRowTitle showMoreActual">Co je to tzv. Fair Use Policy (FUP)? <a class="button circle showMoreActual" href="#">+</a></div>
	                    <p>FUP neboli Fair Use Policy, je označení pro datový limit stažených dat. Jedná se o objem dat, které máte k dispozici v jednom účtovacím cyklu. Společnost PODA poskytuje službu Internet bez časových nebo datových omezení.</p>
	                </div>
	            </li>
	        </ul>
	    </div>
	</div>
</div>

<br class="clear" />

<a href="/kontakty/kontaktni-formular/" class="button wider">Pomoc, potřebuji více informací</a> 
<!--<a href="/podpora/dotazy-klientu/internet/" class="button blue">Zobrazit více rad</a>-->