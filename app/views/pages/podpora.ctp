<div class="mainImg cover_img h270" data-src="/css/layout/banner/podpora-bg.jpg">
	<div class="middleText">
		<h1 class="title">Podpora<small> A péče, kterou od nás můžete čekat...</small></h1>
	</div>
</div>

<br class="clear" />


<div class="smaller-wrapper">
	<p class="txt-center mt50"><strong class="black uppercase">Dříve než budete kontaktovat naši Zákaznickou linku (oddělení technické podpory), ověřte si rychlost spojení stažením testovacího souboru. </strong></p>
	
	<p class="txt-center mt25">Postup pro měření rychlosti internetu naleznete <a href="/dokumenty-ke-stazeni/mereni-rychlosti-internetu/" class="blue">ZDE</a>.</p>
</div>

<div class="wrapper contacts">
	<ul class="kontakt-list">
		<li class="blue">
			<h2>Zákaznická linka</h2>
			<p>Máte-li jakýkoliv dotaz a potřebujete nám zavolat, využijte, prosím, níže uvedená telefonní čísla</p>
			
			<strong>
			844 844 033 <br />
			730 430 430 
			</strong>
		</li>
		<li class="blue">
			<h2>Kontaktní e-mail pro obchod</h2>
			<p>Máte-li jakýkoliv dotaz a potřebujete nám napsat, využijte, prosím, níže uvedený email</p>
			
			<a href="mailto:info@poda.cz"><strong>info@poda.cz</strong></a>
		</li>
		<li class="red">
			<h2>Kontaktní e-mail pro technickou kontrolu</h2>
			<p>Máte-li jakýkoliv technický dotaz, využijte, prosím, naší e-mailovou adresu pro technickou podporu</p>
			
			<a href="mailto:podpora@poda.cz"><strong>podpora@poda.cz</strong></a>
		</li>
	</ul>
	
	<br class="clear" />
	
	<div class="boxes">
		<a href="/dokumenty-ke-stazeni/" class="pBox">Návody a manuály</a>
		<a href="/podpora/dotazy-klientu/" class="pBox">Dotazy klientů (FAQ)</a>
		<a href="/podpora/nastaveni-pripojeni/" class="pBox">Nastavení připojení</a>
		<a href="/podpora/platba-za-sluzby/" class="pBox">Platba za služby</a>
		
		<a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank" class="pBox">Klientská zóna</a>
		<a href="/podpora/servisni-podminky/" class="pBox">Servisní podmínky</a>
		<a href="/podpora/vseobecne-smluvni-podminky/" class="pBox">Všeobecné smluvní podmínky</a>
		<a href="/podpora/vernostni-program/" class="pBox">Věrnostní program (FAQ)</a>
	</div>
</div>