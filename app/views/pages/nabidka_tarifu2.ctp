<div class="mainImg cover_img" data-src="/css/layout/banner/volani.jpg">
	<div class="middleText">
		<h1 class="title">Telefon doma i na ven<small>Mobilní telefon nebo Pevná linka. Umíme obojí!</small></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<br class="clear" />

<div class="smaller-wrapper">
	<p class="txt-center mt50"><strong class="black uppercase">Neutrácejte zbytečně, mějte výdaje pevně pod kontrolou! Připravili jsme pro Vás nabídky ZA velmi výhodnÉ cenY za volání a SMS nejen v síti PODA.</strong></p>
	
	<p class="txt-center mt25">Tato nabídka je bonusem pro zákazníky služby <a href="/internet/" class="blue">internet</a> a <a href="/tv-sluzby/" class="blue">televize</a> nebo jejich vzájemné kombinace. Být klientem PODA se vyplatí! Volání je pouze pro klienty PODA a nelze je samostatně zakoupit.</p>
</div>

<div class="buttons w100p">
	<a href="/volani/cenik-sluzeb/" class="button">Ceníky služeb</a>
	<a href="/volani/telefony-a-prislusenstvi/" class="button blue wider">Telefony a příslušenství</a>
	<a href="/volani/informace-o-datech/" class="button">Informace o datech</a>
</div>
 
<br class="clear" />

<?php /*
<div class="wrapper">
	<p class="txt-center mt50" id="jumpHere"><strong class="big black uppercase bold">TElefon v balíčku s InterneteM nebo TV?</strong></p>
	<p class="txt-center mt25">Zadejte adresu místa, kterou chcete připojit...</p>
	
	<div class="inline-button txt-center">
		<?php //echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>
*/ ?>
<div class="shadow-line"></div>

<br class="clear" />

<div class="blok cover_img more-text" data-src="/css/layout/banner/blokMobil2.jpg">
	<h2>Doplňkové služby</h2>
	
	<div class="wrap">
		<ul class="txt-center">
			<li>Možnost zachování stávajících telefonních čísel</li>
			<li>Zdarma on-line okamžitý podrobný výpis hovorů</li>
			<li>Služba CLIP - identifikace volajícího</li>
			<li>Automatické přesměrování, podmíněné přesměrování</li>
			<li>Přesměrování na více čísel</li>
			<li>Sériové linky</li>
			<li>Možnost předávání/přepojování hovorů</li>
			<li>Více hovorů současně</li>
			<li>Identifikace zmeškaných hovorů přímo na telefonu</li>
		</ul>
	</div>
</div>

<?php /*
<div class="blok cover_img mt25" data-src="/css/layout/banner/internetBlok3.jpg">
	<h2>Chcete více dat ke svému tarifu?</h2>
	
	<div class="wrap">
		<br class="clear" />
		<a href="#" id="scrollTo" class="button color-red">Ano, mám zájem</a>
	</div>
</div> 
*/ ?>

<div class="wrapper">
	<div class="buttons w100p mb50">
		<a href="/dokumenty-ke-stazeni/" class="button">Návody, dokumenty...</a>
		<a href="/podpora/" class="button blue">Potřebujete poradit?</a>
		<a href="/kontakty/" class="button">Kontaktujte nás</a>
	</div>
</div> 

<h2 class="txt-center uppercase mt50">Často kladené otázky k volání</h2>
 
<div class="wrapper">
	<div id="faq" class="faq mt50">	
		<div class="fRow mt25">
			<div class="fRowTitle showMoreActual">Kde a jak si mohu objednat Vaše služby? <a href="#" class="button circle showMoreActual">+</a></div>
			
			<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
		</div>
		<div class="fRow">
			<div class="fRowTitle showMoreActual">Kdy a jak službu dostanu? <a href="#" class="button circle showMoreActual">+</a></div>
			
			<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
		</div>
		
		<div class="fRow">
			<div class="fRowTitle showMoreActual">V jaké síti působí poda mobil? <a href="#" class="button circle showMoreActual">+</a></div>
			
			<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
		</div>
		
		<div class="fRow bottomLine">
			<div class="fRowTitle showMoreActual">Kolik zaplatím za SIM kartu? <a href="#" class="button circle showMoreActual">+</a></div>
			
			<p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p> 
		</div>
	</div>
</div>

<br class="clear" />

<a href="/podpora/dotazy-klientu/volani/" class="button blue">Zobrazit více rad</a>