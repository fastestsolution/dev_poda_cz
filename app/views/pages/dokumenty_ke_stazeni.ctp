
<div class="mainImg cover_img bgposbottom h270" data-src="/css/layout/banner/bg-dokumenty.png">
	<div class="middleText">
		<h1 class="title">Dokumenty ke stažení<small>Péče a podpora. Více svobody. Více rodiny. Propojujeme generace...</small></h1>
	</div>
</div>

<br class="clear" />
<div class="smallest-wrapper mt50">
    <p class="txt-center">
        <strong class="uppercase black">
            Níže naleznete odkazy na dokumenty, návody, instruktážní videa, výroční zprávy či odkazy při řešení problémů.
        </strong>
    </p>
    <br />
    <p class="txt-center">
        Nevíte si rady při zapojení nebo nerozumíte svému vyúčtování?<br>Nevadí, návodné dokumenty Vám budou nápomocné...
    </p>

    <br class="clear" />
</div>

<div class="wrapper mt50">
	<div class="tarifs box-3 dokumenty">
		<div class="tarifBox doc pt85">
			<div class="title">Dokumenty</div>
            <ul>
                <!--<li>
                    <a href="https://vp.poda.cz/cs/obchodni-podminky-eshopu.html" target="_blank" class="showMore">Všeobecné podmínky Věrnostního programu společnosti PODA</a>
                    <div class="back hide">
                        <div class="close"></div>
                        <p>Všeobecné podmínky Věrnostního programu společnosti PODA</p>
                        <ul>
                            <li><a href="/uploaded/dokumenty/">Obchodní podmínky e-shopu</a></li>
                        </ul>
                    </div>
                </li>-->
                <li><a href="https://vp.poda.cz/cs/obchodni-podminky-eshopu.html" target="_blank">Obchodní podmínky e-shopu</a></li>
                <li><a href="/uploaded/dokumenty/cenik_home.pdf" target="_blank">Ceník pro domácnost</a></li>
                <li><a href="/podpora/vseobecne-smluvni-podminky/">Všeobecné obchodní podmínky</a></li>
                <!--<li><a href="/uploaded/dokumenty/" target="_blank">Ceník WORLD roaming</a></li>
                <li><a href="/uploaded/dokumenty/" target="_blank">Ceník mezinárodní volání</a></li>-->
                <li><a href="/uploaded/dokumenty/oznameni_dle_zakona.pdf" target="_blank">Oznámení dle zákona</a></li>
                <li><a href="/uploaded/dokumenty/podminky_dle_zakona.pdf" target="_blank">Podmínky dle zákona</a></li>
                <li><a href="/uploaded/dokumenty/prenesitelnost_tel_cisla.pdf" target="_blank">Přenesitelnost telefonního čísla</a></li>
                <li><a href="/uploaded/dokumenty/zadost_preneseni_tel_cisla.pdf" target="_blank">Žádost o přenesení tel. čísla</a></li>
                <li><a href="/uploaded/dokumenty/residomo.pdf" target="_blank">Residomo</a></li>
				<li><a href="/uploaded/dokumenty/cenik_mobildozahranici.pdf" target="_blank">Ceník volání do zahraničí</a></li>
				<li><a href="/uploaded/dokumenty/roaming_cerven2017.pdf" target="_blank">Ceník world roaming</a></li>
            </ul>
		</div>
        <div class="tarifBox manualy pt85">
            <div class="title">Manuály</div>
            <ul>
                <li>
                    <a href="#" target="_blank" class="showMore">Nastavení set top boxu</a>
                    <div class="back hide">
                        <div class="close"></div>
                        <p>Nastavení set top boxu</p>
                        <ul>
                            <li><a href="/uploaded/dokumenty/navod_2ovladace.pdf" target="_blank">Návod k obsluze set-top boxu a chytrých funkcí PODA TV – nejnověj-ší verze uživatelského prostředí 2017</a></li>
                            <li><a href="/uploaded/dokumenty/Obsluha_ChytraTV_Settopbox.pdf" target="_blank">Návod k obsluze set-top boxu a chytrých funkcí PODA TV</a></li>
                            <li><a href="/uploaded/dokumenty/stb_motorola_1003_navod.pdf" target="_blank">Instalace a obsluha set-top boxu Motorola VIP1003</a></li>
                            <li><a href="/uploaded/dokumenty/navod_ovladac_RF.pdf" target="_blank">Návod na spárování dálkového ovladače a set-top boxu Arris VIP1003 s funkcí RF</a></li>
                            <li><a href="/uploaded/dokumenty/stb_motorola_1910_navod.pdf" target="_blank">Instalace a obsluha set-top boxu Motorola VIP1910</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="#" target="_blank" class="showMore">Nastavení počítače a Wi-Fi</a>
                    <div class="back hide">
                        <div class="close"></div>
                        <p>Nastavení počítače a Wi-Fi</p>
                        <ul>
                            <li><a href="/uploaded/dokumenty/nastaveni_posty.pdf" target="_blank">Návod na nastavení e-mailové pošty</a></li>
                            <li><a href="/uploaded/dokumenty/IP_protokol.pdf" target="_blank">Návod k nastavení IP adresy</a></li>
                            <li><a href="/uploaded/dokumenty/nastpr_ip.pdf" target="_blank">Nastavení protokolu IP</a></li>
                            <li><a href="/uploaded/dokumenty/navod_d615.pdf" target="_blank">Návod nastavení Wi-Fi vysílače na routeru D-Link DIR-615</a></li>
                            <li><a href="/uploaded/dokumenty/navodTL-WR740N.pdf" target="_blank">Návod nastavení Wi-Fi vysílače na routeru TL-WR740N</a></li>
                            <li><a href="/uploaded/dokumenty/navod_tp-link-170201-01.pdf" target="_blank">Návod nastavení Wi-Fi routeru TP-Link TL-WR840 v2</a></li>
                            <li><a href="/uploaded/dokumenty/navod_zyxel_170116-05.pdf" target="_blank">Návod na zprovoznění internetu s modemem ZYXEL VMG 1312-B30B</a></li>
                            <li><a href="/uploaded/dokumenty/nastaveni_onu_pres_KZ.pdf" target="_blank">Návod na nastavení či změnu parametrů optické zakončova-cí jednotky Huawei</a></li>
                        </ul>
                    </div>

                </li>
                <li><a href="#" target="_blank" class="showMore">Nastavení VoIP telefonu</a>
                    <div class="back hide">
                        <div class="close"></div>
                        <p>Nastavení VoIP telefonu</p>
                        <ul>
                            <li><a href="/uploaded/dokumenty/spa901_manual.pdf" target="_blank">Návod k instalaci a obsluze VoIP telefonu LinkSys Spa901</a></li>
                            <li><a href="/uploaded/dokumenty/spa921_manual.pdf" target="_blank">Návod k instalaci a obsluze VoIP telefonu LinkSys Spa921</a></li>
                            <li><a href="/uploaded/dokumenty/budgetone100_manual.pdf" target="_blank">Návod k instalaci a obsluze VoIP telefonu Budgetone 101</a></li>
                            <li><a href="/uploaded/dokumenty/gxp2000_manual.pdf" target="_blank">Návod k instalaci a obsluze firemního VoIP telefonu Grand-stream GXP-2000</a></li>
                            <li><a href="/uploaded/dokumenty/gxp1160_manual.pdf" target="_blank">Návod nastavení Wi-Fi vysílače na routeru TL-WR740N</a></li>
                            <li><a href="/uploaded/dokumenty/spa2100_1001adapter.pdf" target="_blank">Návod k instalaci a obsluze VoIP telefonního adaptéru</a></li>
                        </ul>
                    </div>
                </li>
                
                <li><a href="/dokumenty-ke-stazeni/mereni-rychlosti-internetu/">Postup pro měření rychlosti internetu</a></li>
                <li><a href="#" target="_blank" class="showMore">Instruktážní videa</a>
                    <div class="back hide">
                        <div class="close"></div>
                        <p>Instruktážní videa</p>
                        <ul>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/yLT4H9cC6Kc">Videonávod na tovární nastavení set top boxu Arris 1113 a Arris RF</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/vpueDHtViK8">Videonávod na tovární nastavení set top boxu Motorola 1903, 1910 a 1003</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/2e9CHX9Ha0k">Jak nastavit IP adresy router Asus</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/sJbvk9X6OkY">Jak nastavit IP adresy router TP-Link</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/FeECpU9xnCI">Jak nastavit IP adresy router Tenda</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/OhfLul5_XMQ">Nastavení IPv4 Windows 10</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/4HpZK86pHbE">Jak změnit heslo Wi-Fi</a></li>
                            <!--<li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/J83sZrz9XTc">Jak na dětský účet Windows 10</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/_HTXMhKWqnA">Nastavení routerů</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/l0DoQYGZt8M">Nastavení Wi-Fi kanálu na routeru</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/J83sZrz9XTc">Nastavení</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/_HTXMhKWqnA">Jak nastavit IP adresy router Asus</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/l0DoQYGZt8M">Jak nastavit IP adresy router TP-Link</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/J83sZrz9XTc">Jak nastavit IP adresy router Tenda</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/_HTXMhKWqnA">Nastavení IPv4 Windows 10</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/l0DoQYGZt8M">Nastavení IPv4 Windows 7</a></li>
                            <li><a class="startVideo" href="#instructionVideo" data-src="https://www.youtube.com/embed/J83sZrz9XTc">Jak změnit heslo Wi-Fi</a></li>-->
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="tarifBox doc_others pt85">
            <div class="title">Ostatní dokumenty</div>
            <ul>
	            <li><a href="/uploaded/dokumenty/vop-spotrebitel_2017.pdf" target="_blank">Všeobecné podmínky pro domácnost platné od 1. 8. 2017</a></li>
                <li><a href="/uploaded/dokumenty/vop-podnikatel_2017.pdf" target="_blank">Všeobecné podmínky pro podnikatelské subjekty platné od 1. 8. 2017</a></li>
                <li><a href="/uploaded/dokumenty/Pouceni_spotrebitel_odstoupeni_od_smlouvy.pdf" target="_blank">Poučení pro spotřebitele a odstoupení od smlouvy</a></li>
                <li><a href="#" target="_blank" class="showMore">Vzory smluv</a>
                    <div class="back hide">
                        <div class="close"></div>
                        <p>Vzory smluv</p>
                        <ul>
                            <li><a href="/uploaded/dokumenty/smlouva_o_poskytovani_sluzeb_el_komunikaci.pdf" target="_blank">Smlouva o poskytování služeb elektronických komunikací</a></li>
                            <li><a href="/uploaded/dokumenty/smlouva_PODAmobil_vzor.pdf" target="_blank">Smlouva o poskytování mobilních služeb PODA</a></li>
                            <li><a href="/uploaded/dokumenty/smlouva_PODAvoip_vzor.pdf" target="_blank">Smlouva o poskytnutí neveřejné telefonní služby VoIP</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="#" target="_blank" class="showMore">Výroční zprávy</a>
                    <div class="back hide">
                        <div class="close"></div>
                        <p>Výroční zprávy</p>
                        <ul>
                            <!--<li><a href="/uploaded/dokumenty/">Aktuální výroční zpráva</a></li>-->
                            <li><a href="#">Výroční zpráva 2017</a></li>
                            <li><a href="/uploaded/dokumenty/VZ_2016_03042017_FINAL.pdf" target="_blank">Výroční zpráva 2016</a></li>
                            <li><a href="/uploaded/dokumenty/VZ_2015_web.pdf" target="_blank">Výroční zpráva 2015</a></li>
                            <li><a href="/uploaded/dokumenty/VZ_Poda_2014_Final.pdf" target="_blank">Výroční zpráva 2014</a></li>
                            <li><a href="/uploaded/dokumenty/VZ_Poda_web.pdf" target="_blank">Výroční zpráva 2013</a></li>
                            <li><a href="/uploaded/dokumenty/PODA_VZ12.pdf" target="_blank">Výroční zpráva 2012</a></li>
                            <li><a href="/uploaded/dokumenty/PODA_VZ11.pdf" target="_blank">Výroční zpráva 2011</a></li>
                            <li><a href="/uploaded/dokumenty/PODA_VZ10.pdf" target="_blank">Výroční zpráva 2010</a></li>
                            <li><a href="/uploaded/dokumenty/PODA_VZ09.pdf" target="_blank">Výroční zpráva 2009</a></li>
                            <li><a href="/uploaded/dokumenty/PODA_VZ_2008.pdf" target="_blank">Výroční zpráva 2008</a></li>
                            <li><a href="/uploaded/dokumenty/PODA_VZ07.pdf" target="_blank">Výroční zpráva 2007</a></li>
                        </ul>
                    </div>
                </li>
                <li><a href="/dokumenty-ke-stazeni/loga/">Loga ke stažení</a></li>
            </ul>
        </div>
	</div>
    <br class="clear" />
</div>
<div class="smallest-wrapper mt50 pt75 idea">

    <p class="txt-center">
        <strong class="uppercase color-red">
            Kontaktní email pro technickou kontrolu
        </strong>
    </p>
    <br />
    <p class="txt-center">
        Informace nepomohly? Nevadí. Využijte, prosím, naší e-mailovou adresu pro technickou podporu níže...
    </p>

    <a href="mailto:podpora@poda.cz" class="mt25 txt-center dblock w100p color-red">
        <strong class="fs40 color-red lowercase">
            podpora@poda.cz
        </strong>
    </a>

    <br class="clear" />
</div>
<div id="instructionVideo_frame">
    <div class="x"></div>
    
</div>

<div id="overlay"></div>
