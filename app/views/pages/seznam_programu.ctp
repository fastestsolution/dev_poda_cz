<div id="wrapper-preloader">
    <div id="preloader">
        <img src="/css/layout/icons/bars.svg" class="loader-icon" width="90" alt="">
    </div>
</div>

<div class="mainImg cover_img" data-src="/css/layout/banner/chytratv-bg.jpg">
	<div class="middleText">
		<h1 class="title">Seznam TV programů<small>Už nikdy se nebudete nudit...</small></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<br class="clear" />

<div class="wrapper-programs pt50 programs">

        <h2>Programová nabídka</h2>
        <p>Poskytovatel si vyhrazuje právo měnit skladbu programové nabídky.</p>
        <h3>Základní programová nabídka a TV Start</h3>
            <ul>
                
                <li>
                    <img src="/css/layout/icons-programs/logo_ct1.gif" alt="" />
                    <div class="about">
                        <strong>
                            ČT1
                        </strong>
                        <span>
Jednička České televize přináší filmy, seriály, pořady pro děti a zpravodajství.
<br>Zaměření: veřejnoprávní televize
<br>Jazyk: česky
                        </span>
                    </div>
                </li>

                    <li><img src="/css/layout/icons-programs/logo_ct1_hd.gif" alt="ČT1 HD" /><div class="about"><strong>ČT1 HD </strong><span>Jednička České televize přináší filmy, seriály, pořady pro děti a zpravodajství, to vše v HD kvalitě.
<br>Zaměření: veřejnoprávní televize
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct2.gif" alt="ČT2" /><div class="about"><strong>ČT2 </strong><span>Druhý program České televize vysílá 24 hodin denně a přináší bohatou nabídkou seriálů, dokumentů a klasických filmových děl.
<br>Zaměření: veřejnoprávní televize
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct2_hd.gif" alt="ČT2 HD" /><div class="about"><strong>ČT2 HD </strong><span>Druhý program České televize vysílá 24 hodin denně a přináší bohatou nabídkou seriálů, dokumentů a klasických filmových děl, vše v HD kvalitě.
<br>Zaměření: veřejnoprávní televize
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct24.gif" alt="ČT24" /><div class="about"><strong>ČT24 </strong><span>Čtyřiadvacítka vysílá 24 hodin denně aktuální zprávy z domácí i zahraniční scény. Přináší živé přenosy, rozhovory a komentáře v HD rozlišení.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/CT24_HD.png" alt="ČT24 HD" /><div class="about"><strong>ČT24 HD </strong><span>Čtyřiadvacítka vysílá 24 hodin denně aktuální zprávy z domácí i zahraniční scény. Přináší živé přenosy, rozhovory a komentáře v HD rozlišení.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct4_spor.gif" alt="ČT4 Sport" /><div class="about"><strong>ČT4 Sport </strong><span>24 hodin denně vysílá sportovní program České televize.
<br>Zaměření: sportovní kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct4_sport_hd.gif" alt="ČT4 Sport HD" /><div class="about"><strong>ČT4 Sport HD </strong><span>24 hodin denně vysílá sportovní program České televize, vše v HD rozlišení.
<br>Zaměření: sportovní kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/ct_d_art_hd.png" alt="ČT :D HD" /><div class="about"><strong>ČT :D HD</strong><span>Déčko je webové hřiště a zároveň program pro děti a pro dospělé, kteří z dětství nikdy nevyrostli. Každý den od rána do 20:00 nabízí balíček zábavy pro všechny, kteří si chtějí hrát, sledovat televizi, objevovat, zkoumat a poznávat nové věci nebo na známé věci získat zcela nový pohled. V HD kvalitě.
<br>Zaměření: dětský kanál
<br>Jazyk: česky
</span></div></li>
                <li><img src="/css/layout/icons-programs/ct_d_art_hd.png" alt="ČT art HD" /><div class="about"><strong>ČT art HD</strong><span>Program se střídá s programem ČT: D. Je určen příznivcům kultury, zaměřen na české i světové umění. Mezi 20:00 do 6:00 nabízí záznamy koncertů, divadelních a tanečních představení, dokumenty, původní cykly a mnoho dalšího. V HD kvalitě.
<br>Zaměření: filmový a dokumentární kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/nova.png" alt="Nova" /><div class="about"><strong>Nova </strong><span>24 hodin denně vysílá plnoformátový kanál Nova. Nabízí seriály, sitcomy, filmy či publicistické pořady, v programu nechybí původní tvorba ani zahraniční akvizice. Nova vysílá filmy, seriály, dokumenty, sport, domácí a zahraniční zpravodajství.
<br>Zaměření: komerční všeobecný kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/Nova2.png" alt="Nova 2" /><div class="about"><strong>Nova 2 </strong><span>Nova 2 nabízí premiérový lokální obsah, včetně populárních mezinárodních zábavných formátů a divácky oblíbených komediálních pořadů. V HD kvalitě.
<br>Zaměření: zábavný, komediální kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaCinema.png" alt="Nova Cinema " /><div class="about"><strong>Nova Cinema  </strong><span>První filmový program z rodiny Novy přináší 24 hodin denně skvěle namíchaný koktejl filmů a seriálů pro každého.
<br>Zaměření: filmový kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaAction.png" alt="Nova Action" /><div class="about"><strong>Nova Action </strong><span>Česká komerční televizní stanice, která je zaměřena hlavně na mužské publikum s vysíláním akčních, kriminálních či sci-fi filmů, seriálů a sportu.
<br>Zaměření: sportovní, akční, erotický
</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaGold.png" alt="Nova Gold" /><div class="about"><strong>Nova Gold </strong><span>Stanice přináší 24 hodin denně nestárnoucí zábavné hity a oblíbené pořady pro všechny věkové kategorie. V programu se objevuje kompletní archiv stanice Nova. V HD kvalitě.
<br>Zaměření: zábavný kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima.png" alt="Prima" /><div class="about"><strong>Prima </strong><span>Prima je plnoformátovou televizní stanicí, která vysílá 24 hodin denně. Nabízí bohatou nabídku jak vlastních pořadů, tak filmových nebo seriálových hitů.
<br>Zaměření: všeobecný kanál
</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_max.png" alt="Prima MAX" /><div class="about"><strong>Prima MAX </strong><span>Zábava na MAX to jsou filmové trháky a novinky. Program stanice primárně míří na diváky ve věku 25–45 let a na rodiče s dětmi se <br>Zaměřením především na zahraniční tvorbu. Objevují se zde i reprízy některých pořadů vlastní tvorby z hlavního kanálu.
<br>Zaměření: filmový kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_cool.png" alt="Prima COOL" /><div class="about"><strong>Prima COOL </strong><span>Program se 24 hodin denně zaměřuje spíše na pánskou část publika: cool filmy a cool seriály.
<br>Zaměření: zábava, adrenalin
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_love.png" alt="Prima Love" /><div class="about"><strong>Prima Love </strong><span>Třetí program televize Prima je nabitý těmi nejlepšími zahraničními i českými seriály, nabízí víkendové večery s romantickými filmy i oblíbenými seriály.
<br>Zaměření: zábavný kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_comedy_central.png" alt="Prima Com. Central" /><div class="about"><strong>Prima Com. Central </strong><span>Program stanice se 24 hodin denně zaměřuje na komediální seriály sítě Comedy Central.
<br>Zaměření: zábavní, komediální kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_zoom.png" alt="Prima ZOOM" /><div class="about"><strong>Prima ZOOM </strong><span>Program nabízí aktuality a zajímavosti ze světa vesmíru, přírody, válek, historie. Ve 14 rozmanitých tematických blocích najdou diváci nejlepší dokumenty od světových produkčních společností v čele s BBC, National Geographic, Discovery a ZDF, ale i oblíbenou českou tvorbu.
<br>Zaměření: zábavný a vzdělávací kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/barrandov_TV.png" alt="TV Barrandov" /><div class="about"><strong>TV Barrandov </strong><span>Plnoformátová stanice, alternativa ke stávajícím komerčním televizím. Nabízí převzaté pořady, vlastní tvorbu, zábavu, dokumenty, ale i nezávislou a studentskou filmovou tvorbu.
<br>Zaměření: všeobecný kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/barrandov-family_logo.png" alt="Barrandov Family" /><div class="about"><strong>Barrandov Family </strong><span>24 hodin vysílá rodinný program premiéry vlastních zábavních pořadů a seriálů. Je doplněný hudebními bloky. Vlastní tvorba tvoří zhruba polovinu pořadů, druhou polovinu pak zahraniční akvizice.
<br>Zaměření: rodinný kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/kino_barran.png" alt="Kino Barrandov" /><div class="about"><strong>Kino Barrandov </strong><span>24 hodin vysílá Kino Barrandov program plný filmů a seriálů evropské i světové produkce.
<br>Zaměření: multižánrový kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/barranPlus_logo.png" alt="Barrandov plus" /><div class="about"><strong>Barrandov plus </strong><span>Dopoledne program myslí na mladší publikum s blokem pořadů pro děti, odpoledne a večer pak na velkou diváckou obec s dokumenty a seriály na téma příroda, cestování, kulinářství.
<br>Zaměření: 5.00-20.00 hod. dětský, 20.00-5.00 hod. dokumentární
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/tuty_logo.jpg" alt="TUTY TV" /><div class="about"><strong>TUTY TV </strong><span>Tuty TV
Dětská televize TUTY je určena českým a slovenským divákům hlavně ve věku od 4 do 17 let. TUTY vysílá to, co nejvíc letí. Nejhustší animáky, nejvymakanější videa, nejsuprovější muziku, pořady vlastní produkce, a to nejlepší z YouTube. V HD kvalitě.
<br>Zaměření: kanál pro děti a mládež
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/sport5.png" alt="Sport 5" /><div class="about"><strong>Sport 5 </strong><span>Významnou roli v programu hraje motorismus, stanice vysílá ale i další sporty a volnočasové aktivity.
<br>Zaměření: sportovní a motoristický kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/UP_logo.png" alt="UP Network" /><div class="about"><strong>UP Network </strong><span>První televizní stanice v Evropě zaměřená výhradně na letectví. Svým divákům nabízí prakticky vše o letectví – od prazákladů až po nejaktuálnější novinky.
<br>Zaměření: letadla a všechno, co se děje ve vzduchu
<br>Jazyk: česky a v původním znění
</span></div></li>
                    <li><img src="/css/layout/icons-programs/polar.png" alt="Polar" /><div class="about"><strong>Polar </strong><span>Regionální program z oblasti Ostravska, dostupný v domácnostech na severní Moravě.
<br>Zaměření: regionální kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/seznam_logo.jpg" alt="TV Seznam HD" /><div class="about"><strong>TV Seznam HD </strong><span>informuje o zásadních událostech, přináší aktuality z domova i zahraničí. Věnuje se kauzám, které hýbou českou společností, a představuje nejzajímavější osobnosti. Nezávislé zpravodajství z Česka i ze světa, živé přenosy.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/prahaTV.gif" alt="Praha TV" /><div class="about"><strong>Praha TV </strong><span>Metropolitní regionální televize, která přináší aktuální informace z hlavního města
<br>Zaměření: regionální kanál
<br>Jazyk: česky
</span></div></li>
                <li><img src="/css/layout/icons-programs/btv_logo.png" alt="Brno TV" /><div class="about"><strong>Brno TV</strong><span>Brněnská televize přináší zpravodajství, publicistické pořady, zajímavosti, zábavu a další oblíbené pořady. Přináší nový pohled na zdánlivě obyčejné lidi a události kolem nás. Informace, humor, úvahy i tematické otázky, se kterými se potkává moderní člověk v každodenním životě.
<br>Zaměření: regionální vysílání
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/TV_slovacko.jpg" alt="TV Slovácko" /><div class="about"><strong>TV Slovácko </strong><span>Základní charakteristikou programové skladby je zpravodajství z kraje Jihomoravského i Zlínského. <br>Zaměření: regionální kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/jihoceska_tv.png" alt="Jihočeská televize" /><div class="about"><strong>Jihočeská televize </strong><span>Program přináší aktuální zpravodajství z Jihočeského kraje.
<br>Zaměření: regionální kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/Jecko-TV.png" alt="TV Jéčko" /><div class="about"><strong>TV Jéčko </strong><span>Program přináší aktuální zpravodajství z Jihočeského kraje.
<br>Zaměření: regionální kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/tik_bohumín.png" alt="TIK Bohumín HD" /><div class="about"><strong>TIK Bohumín HD </strong><span>Hlavním cílem televize TIK Bohumín (Televizní informační kanál) je informovanost obyvatel města Bohumína. V HD kvalitě.
<br>Zaměření: regionální televize
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/kinosvet.png" alt="Kino Svět" /><div class="about"><strong>Kino Svět </strong><span>Kinosvět je česká dokumentární televize, plná záhad a tajemství. Vysílá dokumenty, filmy i seriály.
<br>Zaměření: dokumentární kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/relax.png" alt="Relax" /><div class="about"><strong>Relax </strong><span>24 hodin každý den v týdnu vysílá Relax zábavné hudební pořady, ale i reportáže z celého světa. Televize pro volný čas celé rodiny. Program se skládá z několika lifestylových a hudebních pořadů a hudebních klipů, díky kterým je stanice oceňována mezi diváky.
<br>Zaměření: rodinný kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/noe.png" alt="Noe" /><div class="about"><strong>Noe </strong><span>Televize dobrých zpráv je jediná česká nekomerční televize a zároveň jediná česká televize specializující se velkou částí svého vysílání na křesťanského diváka, předkládá ale divákům i různé nekonfesní vzdělávací pořady z oblasti historie a přírodních věd (zejména astronomie a biologie) či pohádky pro děti.
<br>Zaměření: křesťanský kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/ocko.png" alt="ÓČKO" /><div class="about"><strong>ÓČKO </strong><span>24 hodin denně vysílá Óčko český hudební program, který se zaměřuje na českou a slovenskou populární hudbu.
<br>Zaměření: hudební kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/ockogold.png" alt="ÓČKO GOLD" /><div class="about"><strong>ÓČKO GOLD </strong><span>Druhý program Óčka zaměřený na největší hudební hity a hvězdy od 80. let do současnosti. Divákům nabízí největší hvězdy světové i domácí pop scény, jejich slavné hity i současnou tvorbu.
<br>Zaměření: hudební kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/ocko_expres.png" alt="ÓČKO EXPRES" /><div class="about"><strong>ÓČKO EXPRES </strong><span>Hudební stanice zaměřená na progresívní a kvalitní světovou hudební scénu. České Óčko Expres přináší to nejlepší ze současné hudby.
<br>Zaměření: hudební kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/retro_music.gif" alt="Retro Music TV" /><div class="about"><strong>Retro Music TV </strong><span>Populární celoplošná hudební televize se <br>Zaměřením na největší hity a hudební informace od šedesátek do počátku nového milénia. Přináší proud videoklipů řazených dle data a žánru do různě pojmenovaných programových bloků s nádechem nostalgie.
<br>Zaměření: hudební kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_slusnej_kanal.png" alt="Slušnej kanál TV" /><div class="about"><strong>Slušnej kanál TV </strong><span>Slušnej Kanál TV je progresivní rocková hudební televize, která dává vedle klasiky velký prostor novým kapelám ať už českým, nebo evropským a světovým. Stanice představuje české i zahraniční klipy od 60. let do současnosti, dává prostor také začínajícím amatérským kapelám.
<br>Zaměření: hudební kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/rebel.png" alt="Rebel" /><div class="about"><strong>Rebel </strong><span>Každý den nové informace ze světa bigbítu, to nejlepší ze světové i domácí scény pohledem odborníků, nemoderované žánrové hudební bloky, koncerty a výběr toho nejlepšího od 80. let do současnosti.
<br>Zaměření: hudební, rockový kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/slagr_tv.png" alt="Šlágr TV" /><div class="about"><strong>Šlágr TV </strong><span>Původní český hudební program s národní hudbou. První televize, která hraje písničky, které máte rádi: lidovky, dechovky, harmoniky, trampské a osvědčené šlágry.
<br>Zaměření: hudební kanál s prvky teleshopingu
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_mnam.png" alt="MŇAM TV HD" /><div class="about"><strong>MŇAM TV HD </strong><span>24hodinové vysílání zejména vlastních pořadů o vaření a zdravém životním stylu. V HD kvalitě.
<br>Zaměření: kulinářský program
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/MnauTV.png" alt="MŇAU TV HD" /><div class="about"><strong>MŇAU TV HD </strong><span>První český televizní kanál věnovaný jen a pouze zvířecím mazlíčkům. Původní́ televizní́ stanice, která přináší originální, zábavný obsah pro majitele a milovníky domácích mazlíčků. Psi, kočky, králíci, křečci, ale i opice nebo papoušci jsou hlavními celebritami. V HD kvalitě.
<br>Zaměření: kanál pro milovníky domácích mazlíčků
<br>Jazyk: česky, původní znění
</span></div></li>
                    <li><img src="/css/layout/icons-programs/stv1_hd.png" alt="STV 1 HD" /><div class="about"><strong>STV 1 HD </strong><span>Jednotka nabízí divákům vše, od zpravodajství, přes řadu seriálů, až po filmy, vzdělávací pořady, či úzce profilované programy. Dala by se přirovnat k veřejnoprávní ČT1, která vysílá v tuzemsku.
<br>Zaměření: veřejnoprávní televize
<br>Jazyk: slovensky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/Stv2_hd.png" alt="STV 2 HD" /><div class="about"><strong>STV 2 HD </strong><span>Slovenská Dvojka vysílá většinou úzce zaměřené programy, ne pro široké masy diváků. Přesto je koncipována tak, aby si tu každý našel nějaký čas, kdy je program zaměřený právě na jeho zájmy. Aktuálně by se tento kanál dal přirovnat k domácí ČT2.
<br>Zaměření: veřejnoprávní televize
<br>Jazyk: slovensky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/ta3.png" alt="TA3 HD" /><div class="about"><strong>TA3 HD </strong><span>Zpravodajská televize, která nabízí divákům komplexní zpravodajský a informační servis o dění doma i v zahraničí.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: slovensky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/JOJ_Family.png" alt="JOJ Family HD" /><div class="about"><strong>JOJ Family HD </strong><span>JOJ Family je televize, která má talent! Už v názvu stanice je zakódováno její cílení na rodinného diváka, pro kterého televize vysílá především společné česko-slovenské formáty s tradicí. Přináší nabídku pořadů pro fanoušky dobré televizní zábavy, pohledy do zákulisí, rozhovory a zajímavé příběhy.
<br>Zaměření: rodinný kanál
<br>Jazyk: slovensky
</span></div></li>
                    <!--<li><img src="/css/layout/icons-programs/WauHD-logo.png" alt="Wau HD" /><div class="about"><strong>Wau HD </strong><span>Wau HD, třetí stanice skupiny JOJ je zacílená převážně na ženy a vysílá zahraniční reality show, seriály a reprízy starých částí původních seriálů JOJ. Jazyk: slovensky.</span></div></li>
                    -->
                    <li><img src="/css/layout/icons-programs/tvp1.png" alt="TVP1 HD" /><div class="about"><strong>TVP1 HD </strong><span>Polské zprávy, komentáře a přehledy o dění v Polsku i ve světě. V HD kvalitě.
<br>Zaměření: veřejnosprávní
<br>Jazyk: polsky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp2.png" alt="TVP2 HD" /><div class="about"><strong>TVP2 HD </strong><span>Zaměření: veřejnosprávní televize.  Vysílá především dokumentární a zpravodajské pořady, speciální události a diskusní pořady. V HD kvalitě.
<br>Jazyk: polsky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/TVP_info.png" alt="TVP Info HD" /><div class="about"><strong>TVP Info HD </strong><span>TVP Info HD, zaměření: zpravodajský kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvpkatovice.png" alt="TVP Katowice" /><div class="about"><strong>TVP Katowice </strong><span>TVP Katowice, zaměření: regionální kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp_rozrywka.png" alt="TVP Rozrywka" /><div class="about"><strong>TVP Rozrywka </strong><span>TVP Rozrywka, zaměření: zábavný kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/TVPKultura.png" alt="TVP Kultura" /><div class="about"><strong>TVP Kultura </strong><span>TVP Kultura, zaměření: kulturní kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/TVP_Historia.png" alt="TVP Historia" /><div class="about"><strong>TVP Historia </strong><span>TVP Historia, zaměření: dokumentární kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/polsat_2.png" alt="POLSAT" /><div class="about"><strong>POLSAT </strong><span>Polsat
Polsat je celopolská komerční televize , která vysílá zpravodajství, magazíny, publicistické a zábavné pořady, hudební pořady, kvízy, talk show či reality show, seriály a zahraniční filmy a akvizice.
<br>Zaměření: zpravodajský a dokumentární kanál
<br>Jazyk:polsky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/super_polsat_logo.png" alt="SUPER POLSAT" /><div class="about"><strong>SUPER POLSAT </strong><span>Super Polsat je třetí ze tří hlavních kanálů skupiny. Stanice vysílá nejúspěšnější pořady z hlavního kanálu Polsat, program je doplněn také zahraničními akvizicemi.
<br>Zaměření: zábavný kanál
<br>Jazyk: polsky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvpuls.png" alt="TV Puls" /><div class="about"><strong>TV Puls </strong><span>TV Puls, zaměření: všeobecný, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/puls_2.png" alt="TV Puls 2" /><div class="about"><strong>TV Puls 2 </strong><span>TV PULS 2, zaměření: všeobecný kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/atmrozrywka.png" alt="ATM Rozrywka" /><div class="about"><strong>ATM Rozrywka </strong><span>ATM Rozrywka, zaměření: zábavný kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/polo_tv.png" alt="TV Polo" /><div class="about"><strong>TV Polo </strong><span>TV Polo, zaměření: hudební kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvn.png" alt="TVN" /><div class="about"><strong>TVN </strong><span>TVN, zaměření: všeobecný kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/TVN_7.png" alt="TVNsiedem" /><div class="about"><strong>TVNsiedem </strong><span>TVNsiedem, zaměření: všeobecný, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tv4_1.png" alt="TV4" /><div class="about"><strong>TV4 </strong><span>TV4, zaměření: všeobecný kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/TV6_1.png" alt="TV6" /><div class="about"><strong>TV6 </strong><span>TV6, zaměření: zábava pro mládež, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ATV2_logo.png" alt="ATV II" /><div class="about"><strong>ATV II </strong><span>ATV II, zaměření: populární filmy, seriály a zábava, jazyk: německy</span></div></li>
                    <!--<li><img src="/css/layout/icons-programs/deutcheWelle.png" alt="Deutche Welle" /><div class="about"><strong>Deutche Welle </strong><span>Deutche Welle TV, zaměření: zpravodajský kanál, jazyk: anglicky</span></div></li>
                    -->
                    <li><img src="/css/layout/icons-programs/zdf_hd.png" alt="ZDF HD" /><div class="about"><strong>ZDF HD </strong><span>Televizní stanice vysílá zprávy, dokumentární filmy, celovečerní filmy a také sportovní přenosy. V HD kvalitě.
<br>Zaměření: všeobecný kanál
<br>Jazyk: německy
</span></div></li>
                    <li><img src="/css/layout/icons-programs/SkySportsNews.png" alt="Sky Sport News" /><div class="about"><strong>Sky Sport News </strong><span>Sportovní zpravodajský kanál na vysoké úrovni v režimu 24/7, tedy 24 hodin denně, 7 dní v týdnu. Přináší zájemcům kompletní a aktuální sportovní výsledky.
<br>Zaměření: sportovní kanál
<br>Jazyk: německy
</span></div></li>
                    <li><img src="/css/layout/icons-programs/arte_logo.png" alt="ARTE" /><div class="about"><strong>ARTE </strong><span>Specifikou stanice Arte jsou tematické večery, které jsou věnovány různým problémům, v rámci takového vysílání jsou pak ukazovány reportáže, rozhovory, filmy, komentáře a podobné pořady k danému tématu. Stanice Arte je specificky zaměřena na kulturu v širokém slova smyslu a je velmi seriózní. Jedná se o bilingvální stanici, která vysílá paralelně oběma <br>Jazyky.
<br>Zaměření: dokumentární a analytický kanál
<br>Jazyk: německy, francouzsky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/phoenix_logo.png" alt="PHOENIX" /><div class="about"><strong>PHOENIX </strong><span>Phoenix je německá veřejnoprávní televize. Vysílá především dokumentární a zpravodajské pořady, speciální události a diskusní pořady.
<br>Zaměření: dokumentární kanál
<br>Jazyk: německy
</span></div></li>
                    <li><img src="/css/layout/icons-programs/orf1_3.png" alt="ORF Eins" /><div class="about"><strong>ORF Eins </strong><span>ORF eins je rakouský veřejnoprávní televizní kanál, který vysílá televizní seriály a celovečerní filmy.
<br>Zaměření: všeobecný kanál
<br>Jazyk: německy
</span></div></li>
                    <li><img src="/css/layout/icons-programs/orf2_1.png" alt="ORF 2" /><div class="about"><strong>ORF 2 </strong><span>Program ORF 2 se zaměřuje na kulturní a informační programy. Na druhém rakouském kanále je vícero pořadů a dokumentů o Rakousku, politice, společnosti a samozřejmě zpravodajství.
<br>Zaměření: všeobecný kanál
<br>Jazyk: německy
</span></div></li>
                    <li><img src="/css/layout/icons-programs/ARD1.jpg" alt="ARD 1" /><div class="about"><strong>ARD 1 </strong><span>ARD One
Německé televizní stanice ARD veřejnoprávní Další veřejnoprávní německá televizní stanice, na které se podílí zemské televizní stanice z celého Německa.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: německy
</span></div></li>
                    <li><img src="/css/layout/icons-programs/Tagesschau24.jpg" alt="Tagesschau 24" /><div class="about"><strong>Tagesschau 24 </strong><span>Německý kanál přináší českým divákům aktuální zpravodajské a publicistické pořady, zprávy, dokumenty a talk show.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: německy
</span></div></li>
                    <li><img src="/css/layout/icons-programs/bbc.png" alt="BBC World " /><div class="about"><strong>BBC World  </strong><span>Britské zprávy, komentáře a přehledy o dění ve světě.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: anglicky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/france24.png" alt="France 24" /><div class="about"><strong>France 24 </strong><span>France 24 klade důraz především na analýzu, která má za úkol objasnit zcela komplexním a vyváženým způsobem hlavní mezinárodní události. Dává prostor názorům, které nejsou v současné době dostatečně prezentovány v již existujících mezinárodních zpravodajských kanálech.
<br>Zaměření: zpravodajský kanál
<br>Jazyk: francouzsky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/pervij_logo.png" alt=" Pervyj kanal" /><div class="about"><strong> Pervyj kanal </strong><span>Ruská veřejnoprávní televize.
<br>Zaměření: všeobecný kanál
<br>Jazyk: rusky
y</span></div></li>
                    <li><img src="/css/layout/icons-programs/vtv4.png" alt="VTV4" /><div class="about"><strong>VTV4 </strong><span>VTV4 je televizní kanál, určený vietnamským spoluobčanům žijícím v zahraničí
<br>Zaměření: všeobecný kanál
<br>Jazyk: vietnamsky
</span></div></li>
            </ul>
            <br class="clear" />

            <h3>Rozšířená nabídka TV programů</h3>
            <ul>
                    <li><img src="/css/layout/icons-programs/FILMBOX.png" alt="FILMBOX" /><div class="about"><strong>FILMBOX </strong><span>Filmbox je kanál v hollywoodském stylu. Nabízí stovky předních titulů v televizní premiéře. V jeho nabídce naleznete skvělé akční filmy, vtipné komedie, dojemná dramata, seriály a minisérie – všechno jsou to kvalitní vysokorozpočtové snímky s hvězdným obsazením.
<br>Zaměření: filmový kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/AXN.png" alt="AXN" /><div class="about"><strong>AXN </strong><span>Stylový kanál pro fanoušky seriálů a filmů, s nejlepšími pořady, exkluzivními videi, novinkami, galeriemi o hereckých hvězdách a pohledy do světa reality show.
<br>Zaměření: lifestyle kanál
<br>Jazyk: česky, anglicky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/AXN_Black.png" alt="AXN Black" /><div class="about"><strong>AXN Black </strong><span>Na AXN Black očekávejte akci a dobrodružství! Program je zaměřen jak na dokumentární a improvizační show, stejně jako na klasické žánry.
<br>Zaměření: vědeckofantastický kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/AXN_White.png" alt="AXN White" /><div class="about"><strong>AXN White </strong><span>Bílý AXN přináší divákům výpravné seriály z USA i skvělou evropskou produkci.
<br>Zaměření: filmový kanál, krimi
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/Ceskoslovensko_logo.png" alt="Československo HD" /><div class="about"><strong>Československo HD </strong><span>Program se zaměřuje na tvorbu celého období existence Československa od roku 1918 až do roku 1992, kdy proběhlo rozdělení Československa. V nočních hodinách je doplněno prémiovým filmovým kanálem Festival HD, který nabízí špičku světové festivalové kinematografie.
<br>Zaměření: zpravodajský, dokumentární a filmový kanál
<br>Jazyk: česky, slovensky
</span></div></li>
                    <!--<li><img src="/css/layout/icons-programs/Festival_logo.png" alt="FESTIVAL HD" /><div class="about"><strong>FESTIVAL HD </strong><span>Festival Cinema HD, vysílací čas: 22:00-04:00 hod., zaměření: festivalová kinematografie, jazyk: původní znění</span></div></li>
                    -->
                    <li><img src="/css/layout/icons-programs/AMC_logo.png" alt="AMC" /><div class="about"><strong>AMC </strong><span>Jedna z nejpopulárnějších a nejoceňovanějších televizních značek, která nabízí 7 dnů v týdnu filmy a seriály vlastní produkce. Nabízí i ceněné filmy z celosvětově uznávaných filmových knihoven.
<br>Zaměření: filmový kanál, seriály
<br>Jazyk: česky, anglicky a maďarsky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/Nickelodeon.png" alt="NICKELODEON" /><div class="about"><strong>NICKELODEON </strong><span>Nickelodeon vysílá animovanou tvorbu a inovativní pořady zaměřené na každodenní život předškolních dětí. Stanice vysílá 24 hodin denně.
<br>Zaměření: dětský kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/nick_jr.png" alt="Nick Jr." /><div class="about"><strong>Nick Jr. </strong><span>Nick Jr. je jako školka v televizi, ve které děti zažívají ta největší dobrodružství a spolu se svými hrdiny, ať již to jsou zvířátka, nadpřirozené bytosti, či jiné oblíbené postavičky, objevují ten báječný svět kolem sebe. Vysílá nepřetržitě 24 hodin denně a je zaměřen především na děti předškolního věku.
<br>Zaměření: dětský kanál
<br>Jazyk: česky
</span></div></li>
                    <!--<li><img src="/css/layout/icons-programs/jimjam.png" alt="JimJam" /><div class="about"><strong>JimJam </strong><span>JimJam, zaměření: dětský kanál, jazyk: česky</span></div></li>
                    -->
                    <li><img src="/css/layout/icons-programs/disney.png" alt="DISNEY CHANNEL" /><div class="about"><strong>DISNEY CHANNEL </strong><span>Zábavné pořady pro dětské diváky všeho věku a k tomu filmy z původní produkce Disney Channel vysílá tato stanice denně mezi šestou ráno a půlnocí.
<br>Zaměření: dětský kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/minginx.png" alt="Minimax/GINX" /><div class="about"><strong>Minimax/GINX </strong><span>Stanice Minimax, to jsou příběhy pro děti a mladé diváky. Animované i vzdělávací pořady podává zábavnou formou.
<br>Zaměření: dětský kanál
<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaSport1.png" alt="Nova SPORT1" /><div class="about"><strong>Nova SPORT1 </strong><span>Nova SPORT1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaSport1HD.png" alt="Nova SPORT1 HD" /><div class="about"><strong>Nova SPORT1 HD </strong><span>Nova SPORT1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaSport2HD.png" alt="Nova SPORT2 HD" /><div class="about"><strong>Nova SPORT2 HD </strong><span>Nova SPORT2 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/eurosport_1.png" alt="EUROSPORT 1" /><div class="about"><strong>EUROSPORT 1 </strong><span>EUROSPORT 1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Eurosport_2.png" alt="EUROSPORT 2" /><div class="about"><strong>EUROSPORT 2 </strong><span>EUROSPORT 2, zaměření: sportovní kanál, jazyk: česky, anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/SPORT_1HD.png" alt="Sport 1 HD" /><div class="about"><strong>Sport 1 HD </strong><span>Sport 1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/sport2_HD.jpg" alt="Sport 2 HD" /><div class="about"><strong>Sport 2 HD </strong><span>Sport 2 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/extreme_logo.png" alt="Extreme Sports" /><div class="about"><strong>Extreme Sports </strong><span>Extreme sports, zaměření: sportovní, jazyk: anglicky, německy, francouzsky, maďarsky, turecky, rusky</span></div></li>
                    <li><img src="/css/layout/icons-programs/MTV_LIVE_logo.png" alt="MTV Live HD" /><div class="about"><strong>MTV Live HD </strong><span>MTV Live HD, hudební stanice vysílající přímé přenosy a živá vysílání hudebních koncertů a vystoupení, jazyk: anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/VH1_logo.png" alt="VH1" /><div class="about"><strong>VH1 </strong><span>VH1, zaměření: hudební kanál, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/VH1Classic_logo.png" alt="VH1 Classic" /><div class="about"><strong>VH1 Classic </strong><span>VH1 Classic, zaměření: hudební, retro kanál, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/discavery_channel.png" alt="Discovery Channel" /><div class="about"><strong>Discovery Channel </strong><span>Discovery Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/spectrum.png" alt="SPEKTRUM" /><div class="about"><strong>SPEKTRUM </strong><span>SPEKTRUM, zaměření: dokumentární, jazyk: česky, maďarsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NatGeo_logo.png" alt="National Geographic" /><div class="about"><strong>National Geographic </strong><span>National Geographic, zaměření: dokumentární, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/animal_planet.png" alt="ANIMAL PLANET" /><div class="about"><strong>ANIMAL PLANET </strong><span>ANIMAL PLANET, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_nature.png" alt="VIASAT NATUR" /><div class="about"><strong>VIASAT NATUR </strong><span>VIASAT NATUR, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/history_channel.png" alt="History Channel" /><div class="about"><strong>History Channel </strong><span>History Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_hystory.png" alt="VIASAT HISTORY" /><div class="about"><strong>VIASAT HISTORY </strong><span>VIASAT HISTORY, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Crime_Investigation_NEW_logo.png" alt="Crime &amp; Investigations" /><div class="about"><strong>Crime &amp; Investigations </strong><span>CRIME AND INVESTIGATIONS, zaměření: dokumenty a kriminální dramata, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/travel.png" alt="TRAVEL CHANNEL" /><div class="about"><strong>TRAVEL CHANNEL </strong><span>TRAVEL CHANNEL, zaměření: dokumentární, jazyk: česky, anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tlc_logo.png" alt="TLC" /><div class="about"><strong>TLC </strong><span>TLC, zaměření: kanál pro ženy, jazyk: česky, anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_explore.png" alt="Explore/Spice" /><div class="about"><strong>Explore/Spice </strong><span>VIASAT EXPLORE / SPICE, zaměření: dokumentární / erotický kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Doq_TVlogo.png" alt="DoQ" /><div class="about"><strong>DoQ </strong><span>DoQ, zaměření: dokumentární kanál (příroda, historie, aj.), jazyk: česky, anglicky, maďarsky, rumunsky</span></div></li>

                    <li><img src="/css/layout/icons-programs/NovaHD.png" alt="Nova HD" /><div class="about"><strong>Nova HD </strong><span>Nova HD, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Nova_2_HD_logo.png" alt="Nova 2 HD" /><div class="about"><strong>Nova 2 HD </strong><span>Nova 2 HD, zábavný kanál nabízející v HD kvalitě premiérový lokální obsah, včetně populárních zábavných formátů a divácky oblíbených komediálních pořadů, jazyk: česky.</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaCinemaHD.png" alt="Nova CINEMA HD" /><div class="about"><strong>Nova CINEMA HD </strong><span>Nova CINEMA HD, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaActionHD.png" alt="Nova ACTION HD" /><div class="about"><strong>Nova ACTION HD </strong><span>Nova ACTION HD, zaměření: sportovní, akční, erotický, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Nova_Gold_HD_logo.png" alt="Nova Gold HD" /><div class="about"><strong>Nova Gold HD </strong><span>Nova Gold HD, zábavná stanice přináší 24 hodin denně v HD kvalitě nestárnoucí zábavné hity a oblíbené pořady pro všechny věkové kategorie. V programu se objevuje kompletní archiv stanice Nova, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_hd.png" alt="Prima HD" /><div class="about"><strong>Prima HD </strong><span>Prima HD, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/cool_hd.png" alt="Prima COOL HD" /><div class="about"><strong>Prima COOL HD </strong><span>Prima COOL HD, zaměření: zábava, adrenalin, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/PrimaLoveHD.png" alt="Prima LOVE HD" /><div class="about"><strong>Prima LOVE HD </strong><span>Prima LOVE HD, zaměření: zábava, romantika, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/PrimaZoomHD.png" alt="Prima ZOOM HD" /><div class="about"><strong>Prima ZOOM HD </strong><span>Prima ZOOM HD, zaměření: dokumentární, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/joj_cinema.png" alt="JOJ Cinema HD" /><div class="about"><strong>JOJ Cinema HD </strong><span>JOJ CINEMA HD, bohatý TV program pro všechny filmové fanoušky. Diváci se mohou těšit na akční, komediální, krimi, dramatické, hororové, muzikálové a životopisné filmy. Jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/markiza_inter.png" alt="Markíza International HD" /><div class="about"><strong>Markíza International HD </strong><span>MARKÍZA INTERNATIONAL HD přináší svým divákům v ČR ve stejném čase jako TV Markíza na Slovensku všechny klíčové formáty původní tvorby, včetně televizních novin, seriálů, magazínů aj. Jazyk: slovensky, anglicky, německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/filmbox_plus.png" alt="Filmbox Plus" /><div class="about"><strong>Filmbox Plus </strong><span>Filmbox Plus, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/film_plus.png" alt="Film +" /><div class="about"><strong>Film + </strong><span>Film +, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/FilmEuropeHD.png" alt="Film Europe HD" /><div class="about"><strong>Film Europe HD </strong><span>Film Europe HD, zaměření: evropská a světová, nezávislá kinematografie, jazyk: originální (nutno aktivovat titulky)</span></div></li>
                    <li><img src="/css/layout/icons-programs/cs_mini_film.png" alt="CS Mini, CS Film" /><div class="about"><strong>CS Mini, CS Film </strong><span>CS Film nabízí bezkonkurenčně nejvyšší počet českých a slovenských filmů během každého dne a je stálicí televizního trhu.  Zaměření: veřejnoprávní kanál (CS Mini - 6.00-12.00 h. vysílání pro děti 3-12 let, CS Film - 12.00-24.00 h.), jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/horor_logo.png" alt="HOROR Film" /><div class="about"><strong>HOROR Film </strong><span>HOROR FILM, zaměření: hororový kanál, vysílání od 0:00 - 6.30 hod. po programu CS Film, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Eurosport_1_HD.png" alt="EUROSPORT 1 HD" /><div class="about"><strong>EUROSPORT 1 HD </strong><span>EUROSPORT 1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Eurosport_2_HD.png" alt="EUROSPORT 2 HD" /><div class="about"><strong>EUROSPORT 2 HD </strong><span>EUROSPORT 2 HD, zaměření: sportovní kanál, jazyk: česky, polsky, dánsky, srbsky, turecky</span></div></li>
                    <li><img src="/css/layout/icons-programs/automoto_logo.png" alt="Auto motor a sport HD" /><div class="about"><strong>Auto motor a sport HD </strong><span>Auto motor a sport HD,  novinky ze světa motorismu, testy, zajímavosti nejen ze světa výrobců automobilů a motocyklů, ale i z oblasti motoristického sportu. Jazyk: česky, německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/Golf_logo.png" alt="Golf Channel HD" /><div class="about"><strong>Golf Channel HD </strong><span>Golf Channel HD, zaměření: sportovní, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/MTV_logo.png" alt="MTV Europe" /><div class="about"><strong>MTV Europe </strong><span>MTV Europe, zaměření: hudební kanál, jazyk: anglicky s českými titulky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Mtv_Rocks_logo.png" alt="MTV Rocks" /><div class="about"><strong>MTV Rocks </strong><span>MTV Rocks, zaměření: hudební kanál, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/MTV_Dance_logo.png" alt="MTV Dance" /><div class="about"><strong>MTV Dance </strong><span>MTV Dance, zaměření: hudební kanál, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/DIS_SHOWCASE.png" alt="Discovery HD" /><div class="about"><strong>Discovery HD </strong><span>Discovery SHOWCASE HD, zaměření: dokumentární, jazyk: česky, anglicky  </span></div></li>
                    <li><img src="/css/layout/icons-programs/discovery-science-hd.png" alt="Discovery Science HD" /><div class="about"><strong>Discovery Science HD </strong><span>Discovery Science HD, zaměření: vesmír, věda, technika, jazyk: česky, anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_spektrum_hd.png" alt="Spektrum HD" /><div class="about"><strong>Spektrum HD </strong><span>Spektrum HD, zaměření: dokumentární, jazyk: česky, maďarsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Animal-Planet-HD.png" alt="ANIMAL PLANET HD" /><div class="about"><strong>ANIMAL PLANET HD </strong><span>ANIMAL PLANET HD, zaměření: přírodopisný kanál, jazyk: česky, anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/natgeowi.png" alt="National Geo. Wild" /><div class="about"><strong>National Geo. Wild </strong><span>National Geographic Wild, zaměření: přírodopisný kanál, jazyk: česky, anglicky, maďarsky, holandsky, rusky, polsky, turecky, bulharsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_F+H.png" alt="Fishing &amp; Hunting" /><div class="about"><strong>Fishing &amp; Hunting </strong><span>Fishing & Hunting, zaměření: lov a rybolov, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/paprika.png" alt="TV Paprika" /><div class="about"><strong>TV Paprika </strong><span>TV Paprika, zaměření: kulinářské speciality, vaření, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Fine_living_HD.png" alt="Fine Living HD" /><div class="about"><strong>Fine Living HD </strong><span>Fine Living HD, zaměření: bydlení a životní styl, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/trav_hd.png" alt="Travel Channel HD" /><div class="about"><strong>Travel Channel HD </strong><span>Travel Channel HD, zaměření: dokumentární kanál, jazyk: česky, anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/English_club_logo.png" alt="English Club TV" /><div class="about"><strong>English Club TV </strong><span>English Club TV, zaměření: výuka angličtiny, jazyk: anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/E_TV_logo.png" alt="E&#33; HD" /><div class="about"><strong>E&#33; HD </strong><span>E! HD, nejčerstvější zprávy ze světa celebrit, pravdivé příběhy z Hollywoodu, živé přenosy z červeného koberce během udílení cen Oskar, Emmy nebo Grammy, jazyk: anglicky, 65 % česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Brazzers.png" alt="Brazzers TV Europe" /><div class="about"><strong>Brazzers TV Europe </strong><span>Brazzers TV Europe, zaměření: erotický kanál, jazyk: anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/hustler_logo.png" alt="HUSTLER" /><div class="about"><strong>HUSTLER </strong><span>HUSTLER, zaměření: erotický kanál, jazyk: anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/playboy_logo.png" alt="PLAYBOY TV" /><div class="about"><strong>PLAYBOY TV </strong><span>PLAYBOY TV, zaměření: erotický kanál, jazyk: anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Leo_logo.png" alt="LEO TV" /><div class="about"><strong>LEO TV </strong><span>LEO TV, zaměření: erotický kanál, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/Extasy_logo.png" alt="EXTASY HD" /><div class="about"><strong>EXTASY HD </strong><span>EXTASY HD, zaměření: erotický kanál, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_war.png" alt="TV WAR" /><div class="about"><strong>TV WAR </strong><span>TV WAR, zaměření: válka historicky až po budoucnost, jazyk: česky</span></div></li>
            </ul>
            <br class="clear" />
            <h3>Rozhlasové stanice</h3>

            <ul>
                    <li><img src="/css/layout/icons-programs/CRo_Zurnal.png" alt="ČRo Radiožurnál" /><div class="about"><strong>ČRo Radiožurnál </strong><span>ČRo Radiožurnál, zaměření: celoplošná veřejnoprávní zpravodajsko-publicistická stanice, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/cro_dvojka.png" alt="ČRo Dvojka" /><div class="about"><strong>ČRo Dvojka </strong><span>ČRo Dvojka, zaměření: celoplošná veřejnoprávní tradiční stanice, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/cro_vltava.png" alt="ČRo Vltava" /><div class="about"><strong>ČRo Vltava </strong><span>ČRo Vltava, zaměření: celoplošné veřejnoprávní kulturní vysílání, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/cro_plus_logo.png" alt="ČRo Plus" /><div class="about"><strong>ČRo Plus </strong><span>ČRo Plus, zaměření: celoplošné veřejnoprávní vysílání zaměření na analytickou publicistiku, komentáře, diskuse, interaktivní pořady, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Cro_Junior.png" alt="ČRo Junior" /><div class="about"><strong>ČRo Junior </strong><span>ČRo Junior, zaměření: celoplošné veřejnoprávní vysílání pro děti, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Cro_wave.png" alt="ČRo Wave" /><div class="about"><strong>ČRo Wave </strong><span>Čro Wave, zaměření: celoplošné veřejnoprávní vysílání pro mladě smýšlející posluchače, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ddur_logo.png" alt="ČRo D-Dur" /><div class="about"><strong>ČRo D-Dur </strong><span>ČRo D-Dur, zaměření: celoplošné veřejnoprávní vysílání pro "vážné zájemce o vážnou hudbu", jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/radio_cas.png" alt="Rádio Čas" /><div class="about"><strong>Rádio Čas </strong><span>Rádio Čas, zaměření: regionální rozhlasová stanice, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_casrock.png" alt="Rádio Čas Rock" /><div class="about"><strong>Rádio Čas Rock </strong><span>Rádio Čas Rock, zaměření: regionální rozhlasová stanice pro fanoušky rockové hudby, jazyk: česky</span></div></li>
            </ul>

            <br class="clear" />
            <h3>Nabídka programů PODA net.TV 20</h3>

            <ul>
                    <li><img src="/css/layout/icons-programs/logo_ct1.gif" alt="ČT1" /><div class="about"><strong>ČT1 </strong><span>ČT1, zaměření: veřejnoprávní televize, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct1_hd.gif" alt="ČT1 HD" /><div class="about"><strong>ČT1 HD </strong><span>ČT1 HD, zaměření: veřejnoprávní televize, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct2.gif" alt="ČT2" /><div class="about"><strong>ČT2 </strong><span>ČT2, zaměření: veřejnoprávní televize, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct24.gif" alt="ČT24" /><div class="about"><strong>ČT24 </strong><span>ČT24, zaměření: zpravodajský kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct4_spor.gif" alt="ČT4" /><div class="about"><strong>ČT4 </strong><span>ČT4 Sport, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ct_art.png" alt="ČT :D, ČT art" /><div class="about"><strong>ČT :D, ČT art </strong><span>ČT :D, ČT art, zaměření: veřejnoprávní kanál (ČT :D - dětský kanál, ČT art - filmový a dokumentární kanál), jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/nova.png" alt="Nova" /><div class="about"><strong>Nova </strong><span>NOVA, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Nova2.png" alt="Nova 2" /><div class="about"><strong>Nova 2 </strong><span>Nova 2, zábavný kanál nabízející premiérový lokální obsah, včetně populárních zábavných formátů a divácky oblíbených komediálních pořadů, jazyk: česky.</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaAction.png" alt="Nova Action" /><div class="about"><strong>Nova Action </strong><span>Nova Action, zaměření: sportovní, akční, erotický, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaCinema.png" alt="Nova Cinema" /><div class="about"><strong>Nova Cinema </strong><span>Nova Cinema, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaGold.png" alt="Nova Gold" /><div class="about"><strong>Nova Gold </strong><span>Nova Gold, zábavná stanice přináší 24 hodin denně nestárnoucí zábavné hity a oblíbené pořady pro všechny věkové kategorie. V programu se objevuje kompletní archiv stanice Nova, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima.png" alt="Prima" /><div class="about"><strong>Prima </strong><span>Prima, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_cool.png" alt="Prima COOL" /><div class="about"><strong>Prima COOL </strong><span>Prima COOL, zaměření: zábava, adrenalin, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/primaMax.png" alt="Prima MAX" /><div class="about"><strong>Prima MAX </strong><span>Prima MAX, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_love.png" alt="Prima Love" /><div class="about"><strong>Prima Love </strong><span>Prima Love, zaměření: zábava, romantika, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_zoom.png" alt="Prima Zoom" /><div class="about"><strong>Prima Zoom </strong><span>Prima ZOOM, zaměření: dokumentární, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/barrandov_TV.png" alt="TV Barrandov" /><div class="about"><strong>TV Barrandov </strong><span>TV Barrandov, zaměření: všeobecný, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/kino_barran.png" alt="Kino Barrandov" /><div class="about"><strong>Kino Barrandov </strong><span>24 hodin vysílá Kino Barrandov program plný filmů a seriálů evropské i světové produkce.<br>Zaměření: multižánrový kanál<br>Jazyk: česky
</span></div></li>
                    <li><img src="/css/layout/icons-programs/sport5.png" alt="Sport 5" /><div class="about"><strong>Sport 5 </strong><span>Sport 5, zaměření: sportovní a motoristický kanál, jazyk: česky</span></div></li>
                <li><img src="/css/layout/icons-programs/btv_logo.png" alt="Brněnská televize" /><div class="about"><strong>Brněnská televize </strong><span>Brněnská televize přináší zpravodajství, publicistické pořady, zajímavosti, zábavu a další oblíbené pořady. Přináší nový pohled na zdánlivě obyčejné lidi a události kolem nás. Informace, humor, úvahy i tematické otázky, se kterými se potkává moderní člověk v každodenním životě.<br>Zaměření: regionální vysílání<br>Jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/jihoceska_tv.png" alt="Jihočeská televize" /><div class="about"><strong>Jihočeská televize </strong><span>Jihočeská televize, zaměření: regionální vysílání, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/kinosvet.png" alt="Kino Svět" /><div class="about"><strong>Kino Svět </strong><span>Kino Svět, zaměření: dokumentární program, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/noe.png" alt="Noe" /><div class="about"><strong>Noe </strong><span>Noe, zaměření: rodinný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ocko.png" alt="ÓČKO" /><div class="about"><strong>ÓČKO </strong><span>ÓČKO, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/retro_music.gif" alt="Retro Music TV" /><div class="about"><strong>Retro Music TV </strong><span>Retro Music Television, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/slagr_tv.png" alt="Šlágr TV" /><div class="about"><strong>Šlágr TV </strong><span>Šlágr TV, zaměření: hudební, teleshopping, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/stv1_hd.png" alt="STV 1 HD" /><div class="about"><strong>STV 1 HD </strong><span>STV 1 HD, zaměření: veřejnoprávní televize, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Stv2_hd.png" alt="STV 2 HD" /><div class="about"><strong>STV 2 HD </strong><span>STV 2 HD, zaměření: veřejnoprávní televize, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ta3.png" alt="TA3 HD" /><div class="about"><strong>TA3 HD </strong><span>TA3 HD, zaměření: zpravodajský kanál, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/JOJ_Family.png" alt="JOJ Family HD" /><div class="about"><strong>JOJ Family HD </strong><span>JOJ Family HD, zaměření: všeobecný kanál, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp1.png" alt="TVP 1" /><div class="about"><strong>TVP 1 </strong><span>TVP1, zaměření: veřejnoprávní televize, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp2.png" alt="TVP 2" /><div class="about"><strong>TVP 2 </strong><span>TVP2, zaměření: veřejnoprávní televize, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/super_polsat_logo.png" alt="SUPER POLSAT" /><div class="about"><strong>SUPER POLSAT </strong><span>SUPER POLSAT, zaměření: filmy, zábava, živé sportovní přenosy a zpravodajství, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/polo_tv.png" alt="TV Polo " /><div class="about"><strong>TV Polo  </strong><span>TV Polo, zaměření: hudební kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/zdf.png" alt="ZDF" /><div class="about"><strong>ZDF </strong><span>ZDF, zaměření: zpravodajský kanál, jazyk: německy, francouzsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/bbc.png" alt="BBC World" /><div class="about"><strong>BBC World </strong><span>BBC World News, zaměření: zpravodajský kanál, jazyk: anglicky</span></div></li>
            </ul>
            <br class="clear" />
            <h3>Nabídka programů PODA net.TV 40</h3>
            <ul>
                    <li><img src="/css/layout/icons-programs/logo_ct1.gif" alt="ČT1" /><div class="about"><strong>ČT1 </strong><span>ČT1, zaměření: veřejnoprávní televize, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct2.gif" alt="ČT2" /><div class="about"><strong>ČT2 </strong><span>ČT2, zaměření: veřejnoprávní televize, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct24.gif" alt="ČT24" /><div class="about"><strong>ČT24 </strong><span>ČT24, zaměření: zpravodajský kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct4_spor.gif" alt="ČT4" /><div class="about"><strong>ČT4 </strong><span>ČT4 Sport, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ct_art.png" alt="ČT :D, ČT art" /><div class="about"><strong>ČT :D, ČT art </strong><span>ČT :D, ČT art, zaměření: veřejnoprávní kanál (ČT :D - dětský kanál, ČT art - filmový a dokumentární kanál), jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/nova.png" alt="Nova" /><div class="about"><strong>Nova </strong><span>NOVA, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Nova2.png" alt="Nova 2" /><div class="about"><strong>Nova 2 </strong><span>Nova 2, zábavný kanál nabízející premiérový lokální obsah, včetně populárních zábavných formátů a divácky oblíbených komediálních pořadů, jazyk: česky.</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaAction.png" alt="Nova Action" /><div class="about"><strong>Nova Action </strong><span>Nova Action, zaměření: sportovní, akční, erotický, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaCinema.png" alt="Nova Cinema" /><div class="about"><strong>Nova Cinema </strong><span>Nova Cinema, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaGold.png" alt="Nova Gold" /><div class="about"><strong>Nova Gold </strong><span>Nova Gold, zábavná stanice přináší 24 hodin denně nestárnoucí zábavné hity a oblíbené pořady pro všechny věkové kategorie. V programu se objevuje kompletní archiv stanice Nova, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima.png" alt="Prima" /><div class="about"><strong>Prima </strong><span>Prima, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_cool.png" alt="Prima COOL" /><div class="about"><strong>Prima COOL </strong><span>Prima COOL, zaměření: zábava, adrenalin, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_max.png" alt="Prima MAX" /><div class="about"><strong>Prima MAX </strong><span>Prima MAX, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_love.png" alt="Prima Love" /><div class="about"><strong>Prima Love </strong><span>Prima Love, zaměření: zábava, romantika, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_zoom.png" alt="Prima Zoom" /><div class="about"><strong>Prima Zoom </strong><span>Prima ZOOM, zaměření: dokumentární, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/barrandov_TV.png" alt="TV Barrandov" /><div class="about"><strong>TV Barrandov </strong><span>TV Barrandov, zaměření: všeobecný, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/kino_barran.png" alt="Kino Barrandov" /><div class="about"><strong>Kino Barrandov </strong><span>Kino Barrandov, zaměření. multižánrový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/barranPlus_logo.png" alt="Barrandov plus" /><div class="about"><strong>Barrandov plus </strong><span>Barrandov plus, zaměření: 5.00-20.00 hod. dětský, 20.00-5.00 hod. dokumentární, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/sport5.png" alt="Sport 5" /><div class="about"><strong>Sport 5 </strong><span>Sport 5, zaměření: sportovní a motoristický kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/seznam_logo.jpg" alt="TV Seznam HD" /><div class="about"><strong>TV Seznam HD </strong><span>TV Seznam HD - informuje o zásadních událostech, přináší aktuality z domova i zahraničí v HD kvalitě. Věnuje se kauzám, které hýbou českou společností, a představuje nejzajímavější osobnosti. Nezávislé zpravodajství z ČR i ze světa, živé přenosy.</span></div></li>
                    <li><img src="/css/layout/icons-programs/jihoceska_tv.png" alt="Jihočeská televize" /><div class="about"><strong>Jihočeská televize </strong><span>Jihočeská televize, zaměření: regionální vysílání, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/kinosvet.png" alt="Kino Svět" /><div class="about"><strong>Kino Svět </strong><span>Kino Svět, zaměření: dokumentární program, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/relax.png" alt="Relax" /><div class="about"><strong>Relax </strong><span>Relax, zaměření: rodinný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/noe.png" alt="Noe" /><div class="about"><strong>Noe </strong><span>Noe, zaměření: rodinný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ocko.png" alt="ÓČKO" /><div class="about"><strong>ÓČKO </strong><span>ÓČKO, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/retro_music.gif" alt="Retro Music TV" /><div class="about"><strong>Retro Music TV </strong><span>Retro Music Television, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_slusnej_kanal.png" alt="Slušnej kanál TV" /><div class="about"><strong>Slušnej kanál TV </strong><span>Slušnej kanál TV, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/rebel.png" alt="Rebel" /><div class="about"><strong>Rebel </strong><span>Rebel, zaměření: hudební, rockový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/slagr_tv.png" alt="Šlágr TV" /><div class="about"><strong>Šlágr TV </strong><span>Šlágr TV, zaměření: hudební, teleshopping, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_mnam.png" alt="MŇAM TV HD" /><div class="about"><strong>MŇAM TV HD </strong><span>MŇAM TV HD, zaměření: kulinářský program, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/MnauTV.png" alt="MŇAU TV HD" /><div class="about"><strong>MŇAU TV HD </strong><span>MŇAU TV HD, zaměření: kanál pro milovníky domácích mazlíčků, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/stv1_hd.png" alt="STV 1 HD" /><div class="about"><strong>STV 1 HD </strong><span>STV 1 HD, zaměření: veřejnoprávní televize, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Stv2_hd.png" alt="STV 2 HD" /><div class="about"><strong>STV 2 HD </strong><span>STV 2 HD, zaměření: veřejnoprávní televize, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ta3.png" alt="TA3 HD" /><div class="about"><strong>TA3 HD </strong><span>TA3 HD, zaměření: zpravodajský kanál, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/JOJ_Family.png" alt="JOJ Family HD" /><div class="about"><strong>JOJ Family HD </strong><span>JOJ Family HD, zaměření: všeobecný kanál, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp1.png" alt="TVP 1 HD" /><div class="about"><strong>TVP 1 HD </strong><span>TVP1 HD, zaměření: veřejnoprávní televize, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp2.png" alt="TVP 2 HD" /><div class="about"><strong>TVP 2 HD </strong><span>TVP2 HD, zaměření: veřejnoprávní televize, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/super_polsat_logo.png" alt="SUPER POLSAT" /><div class="about"><strong>SUPER POLSAT </strong><span>SUPER POLSAT, zaměření: filmy, zábava, živé sportovní přenosy a zpravodajství, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/polo_tv.png" alt="TV Polo" /><div class="about"><strong>TV Polo </strong><span>TV Polo, zaměření: hudební kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/zdf.png" alt="ZDF" /><div class="about"><strong>ZDF </strong><span>ZDF, zaměření: zpravodajský kanál, jazyk: německy, francouzsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/bbc.png" alt="BBC World" /><div class="about"><strong>BBC World </strong><span>BBC World News, zaměření: zpravodajský kanál, jazyk: anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/phoenix_logo.png" alt="PHOENIX" /><div class="about"><strong>PHOENIX </strong><span>PHOENIX, zaměření: veřejnoprávní televize, jazyk: původní znění - německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/orf1_3.png" alt="ORF Eins" /><div class="about"><strong>ORF Eins </strong><span>ORF Eins, zaměření: veřejnoprávní televize, jazyk: německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/orf2_1.png" alt="ORF 2" /><div class="about"><strong>ORF 2 </strong><span>ORF 2, zaměření: veřejnoprávní televize, jazyk: německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/vtv4.png" alt="VTV4" /><div class="about"><strong>VTV4 </strong><span>VTV4, zaměření: televizní kanál určený vietnamským spoluobčanům žijícím v zahraničí, jazyk: vietnamsky</span></div></li>
            </ul><br class="clear" />
            <h3>Nabídka programů PODA net.TV 50+</h3>
            <ul>
                    <li><img src="/css/layout/icons-programs/logo_ct1.gif" alt="ČT1" /><div class="about"><strong>ČT1 </strong><span>ČT1, zaměření: veřejnoprávní televize, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct2.gif" alt="ČT2" /><div class="about"><strong>ČT2 </strong><span>ČT2, zaměření: veřejnoprávní televize, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct24.gif" alt="ČT24" /><div class="about"><strong>ČT24 </strong><span>ČT24, zaměření: zpravodajský kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_ct4_spor.gif" alt="ČT4" /><div class="about"><strong>ČT4 </strong><span>ČT4 Sport, zaměření: sportovní kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ct_art.png" alt="ČT :D, ČT art" /><div class="about"><strong>ČT :D, ČT art </strong><span>ČT :D, ČT art, zaměření: veřejnoprávní kanál (ČT :D - dětský kanál, ČT art - filmový a dokumentární kanál), jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/nova.png" alt="Nova" /><div class="about"><strong>Nova </strong><span>NOVA, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Nova2.png" alt="Nova 2" /><div class="about"><strong>Nova 2 </strong><span>Nova 2, zábavný kanál nabízející premiérový lokální obsah, včetně populárních zábavných formátů a divácky oblíbených komediálních pořadů, jazyk: česky.</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaAction.png" alt="Nova Action" /><div class="about"><strong>Nova Action </strong><span>Nova Action, zaměření: sportovní, akční, erotický, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaCinema.png" alt="Nova Cinema" /><div class="about"><strong>Nova Cinema </strong><span>Nova Cinema, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/NovaGold.png" alt="Nova Gold" /><div class="about"><strong>Nova Gold </strong><span>Nova Gold, zábavná stanice přináší 24 hodin denně nestárnoucí zábavné hity a oblíbené pořady pro všechny věkové kategorie. V programu se objevuje kompletní archiv stanice Nova, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima.png" alt="Prima" /><div class="about"><strong>Prima </strong><span>Prima, zaměření: všeobecný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_cool.png" alt="Prima COOL" /><div class="about"><strong>Prima COOL </strong><span>Prima COOL, zaměření: zábava, adrenalin, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/primaMax.png" alt="Prima MAX" /><div class="about"><strong>Prima MAX </strong><span>Prima MAX, zaměření: filmový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_love.png" alt="Prima Love" /><div class="about"><strong>Prima Love </strong><span>Prima Love, zaměření: zábava, romantika, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/prima_zoom.png" alt="Prima Zoom" /><div class="about"><strong>Prima Zoom </strong><span>Prima ZOOM, zaměření: dokumentární, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/barrandov_TV.png" alt="TV Barrandov" /><div class="about"><strong>TV Barrandov </strong><span>TV Barrandov, zaměření: všeobecný, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/kino_barran.png" alt="Kino Barrandov" /><div class="about"><strong>Kino Barrandov </strong><span>Kino Barrandov, zaměření. multižánrový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/barranPlus_logo.png" alt="Barrandov plus" /><div class="about"><strong>Barrandov plus </strong><span>Barrandov plus, zaměření: 5.00-20.00 hod. dětský, 20.00-5.00 hod. dokumentární, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tuty_logo.jpg" alt="TUTY HD" /><div class="about"><strong>TUTY HD </strong><span>TUTY HD, dětská televize určena českým a slovenským divákům ve věku od 4 do 17 let. Vysílá právě to, co děti nejvíc baví. Skvělé animáky, videa, oblíbenou muziku, vlastní produkci a zajímavosti z YouTube.</span></div></li>
                    <li><img src="/css/layout/icons-programs/sport5.png" alt="Sport 5" /><div class="about"><strong>Sport 5 </strong><span>Sport 5, zaměření: sportovní a motoristický kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/UP_logo.png" alt="UP Network" /><div class="about"><strong>UP Network </strong><span>UP Network, zaměření: pořady o letadlech a o všem, co se děje ve vzduchu, jazyk: česky a původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/polar.png" alt="Polar" /><div class="about"><strong>Polar </strong><span>Polar, zaměření: regionální vysílání, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/seznam_logo.jpg" alt="TV Seznam HD" /><div class="about"><strong>TV Seznam HD </strong><span>TV Seznam HD - informuje o zásadních událostech, přináší aktuality z domova i zahraničí v HD kvalitě. Věnuje se kauzám, které hýbou českou společností, a představuje nejzajímavější osobnosti. Nezávislé zpravodajství z ČR i ze světa, živé přenosy.</span></div></li>
                    <li><img src="/css/layout/icons-programs/prahaTV.gif" alt="Praha TV" /><div class="about"><strong>Praha TV </strong><span>Praha TV, zaměření: regionální vysílání, jazyk: česky</span></div></li>
                <li><img src="/css/layout/icons-programs/btv_logo.png" alt="Brněnská televize" /><div class="about"><strong>Brněnská televize </strong><span>Brněnská televize přináší zpravodajství, publicistické pořady, zajímavosti, zábavu a další oblíbené pořady. Přináší nový pohled na zdánlivě obyčejné lidi a události kolem nás. Informace, humor, úvahy i tematické otázky, se kterými se potkává moderní člověk v každodenním životě.<br>Zaměření: regionální vysílání<br>Jazyk: česky</span></div></li>
                 <li><img src="/css/layout/icons-programs/jihoceska_tv.png" alt="Jihočeská televize" /><div class="about"><strong>Jihočeská televize </strong><span>Jihočeská televize, zaměření: regionální vysílání, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Jecko-TV.png" alt="Jéčko" /><div class="about"><strong>Jéčko </strong><span>TV Jéčko, zaměření: regionální TV Jižní Čechy, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/kinosvet.png" alt="Kino Svět" /><div class="about"><strong>Kino Svět </strong><span>Kino Svět, zaměření: dokumentární program, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/relax.png" alt="Relax" /><div class="about"><strong>Relax </strong><span>Relax, zaměření: rodinný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/noe.png" alt="Noe" /><div class="about"><strong>Noe </strong><span>Noe, zaměření: rodinný kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ocko.png" alt="ÓČKO" /><div class="about"><strong>ÓČKO </strong><span>ÓČKO, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ockogold.png" alt="ÓČKO GOLD" /><div class="about"><strong>ÓČKO GOLD </strong><span>ÓČKO GOLD, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ocko_expres.png" alt="ÓČKO EXPRES" /><div class="about"><strong>ÓČKO EXPRES </strong><span>ÓČKO EXPRES, zaměření: metropolitní hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/retro_music.gif" alt="Retro Music TV" /><div class="about"><strong>Retro Music TV </strong><span>Retro Music Television, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_slusnej_kanal.png" alt="Slušnej kanál TV" /><div class="about"><strong>Slušnej kanál TV </strong><span>Slušnej kanál TV, zaměření: hudební kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/rebel.png" alt="Rebel" /><div class="about"><strong>Rebel </strong><span>Rebel, zaměření: hudební, rockový kanál, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/slagr_tv.png" alt="Šlágr TV" /><div class="about"><strong>Šlágr TV </strong><span>Šlágr TV, zaměření: hudební, teleshopping, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_mnam.png" alt="MŇAM TV HD" /><div class="about"><strong>MŇAM TV HD </strong><span>MŇAM TV HD, zaměření: kulinářský program, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/MnauTV.png" alt="MŇAU TV HD" /><div class="about"><strong>MŇAU TV HD </strong><span>MŇAM TV HD, zaměření: kulinářský program, jazyk: česky</span></div></li>
                    <li><img src="/css/layout/icons-programs/stv1_hd.png" alt="STV 1 HD" /><div class="about"><strong>STV 1 HD </strong><span>STV 1 HD, zaměření: veřejnoprávní televize, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/Stv2_hd.png" alt="STV 2 HD" /><div class="about"><strong>STV 2 HD </strong><span>STV 2 HD, zaměření: veřejnoprávní televize, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/ta3.png" alt="TA3 HD" /><div class="about"><strong>TA3 HD </strong><span>TA3 HD, zaměření: zpravodajský kanál, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/JOJ_Family.png" alt="JOJ Family HD" /><div class="about"><strong>JOJ Family HD </strong><span>JOJ Family HD, zaměření: všeobecný kanál, jazyk: slovensky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp1.png" alt="TVP 1 HD" /><div class="about"><strong>TVP 1 HD </strong><span>TVP1 HD, zaměření: veřejnoprávní televize, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/tvp2.png" alt="TVP 2 HD" /><div class="about"><strong>TVP 2 HD </strong><span>TVP2 HD, zaměření: veřejnoprávní televize, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/super_polsat_logo.png" alt="SUPER POLSAT" /><div class="about"><strong>SUPER POLSAT </strong><span>SUPER POLSAT, zaměření: filmy, zábava, živé sportovní přenosy a zpravodajství, jazyk: původní znění</span></div></li>
                    <li><img src="/css/layout/icons-programs/polo_tv.png" alt="TV Polo" /><div class="about"><strong>TV Polo </strong><span>TV Polo, zaměření: hudební kanál, jazyk: polsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/zdf.png" alt="ZDF" /><div class="about"><strong>ZDF </strong><span>ZDF, zaměření: zpravodajský kanál, jazyk: německy, francouzsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/bbc.png" alt="BBC World" /><div class="about"><strong>BBC World </strong><span>BBC World News, zaměření: zpravodajský kanál, jazyk: anglicky</span></div></li>
                    <li><img src="/css/layout/icons-programs/arte_logo.png" alt="ARTE" /><div class="about"><strong>ARTE </strong><span>ARTE, zaměření: filmy, dokumenty a pořady o umění a kultuře, jazyk: původní znění - německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/phoenix_logo.png" alt="PHOENIX" /><div class="about"><strong>PHOENIX </strong><span>PHOENIX, zaměření: veřejnoprávní televize, jazyk: původní znění - německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/orf1_3.png" alt="ORF Eins" /><div class="about"><strong>ORF Eins </strong><span>ORF Eins, zaměření: veřejnoprávní televize, jazyk: německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/orf2_1.png" alt="ORF 2" /><div class="about"><strong>ORF 2 </strong><span>ORF 2, zaměření: veřejnoprávní televize, jazyk: německy</span></div></li>
                    <li><img src="/css/layout/icons-programs/pervij_logo.png" alt="Pervyj kanal" /><div class="about"><strong>Pervyj kanal </strong><span>PERVYJ KANAL VSEMIRNAJA SIET, zaměření: všeobecné, jazyk: rusky</span></div></li>
                    <li><img src="/css/layout/icons-programs/vtv4.png" alt="VTV4" /><div class="about"><strong>VTV4 </strong><span>VTV4, zaměření: televizní kanál určený vietnamským spoluobčanům žijícím v zahraničí, jazyk: vietnamsky</span></div></li>
                    <li><img src="/css/layout/icons-programs/logo_war.png" alt="TV WAR" /><div class="about"><strong>TV WAR </strong><span>TV WAR, zaměření: válka historicky až po budoucnost, jazyk: česky</span></div></li>
            </ul><br class="clear" />


</div>
<? #Balíčky se starými texty ze starého webu ?>
<!--

<div class="wrapper mt50 balicky-all none">
    <p class="txt-center mt50"><strong class="big black uppercase bold">Balíčky k přiobjednání</strong></p>
    <br class="clear" />

    <ul id="sluzba-buttons" class="sluzba-buttons">
        <li><a href="#" data-id="HBOaCinemax" class="button active">HBO a Cinemax</a></li>
        <li><a href="#" data-id="DiscoveryHD" class="button">Discovery HD</a></li>
        <li><a href="#" data-id="DiscoverySD" class="button">Discovery SD</a></li>
        <li><a href="#" data-id="Eurosport" class="button">Eurosport</a></li>
        <li><a href="#" data-id="Sport" class="button">Sport</a></li>
        <li><a href="#" data-id="Mladez" class="button">Mladez</a></li>
        <li><a href="#" data-id="Filmy" class="button">Filmy</a></li>
        <li><a href="#" data-id="Dokumenty" class="button">Dokumenty</a></li>
        <li><a href="#" data-id="Priroda" class="button">Příroda</a></li>
        <li><a href="#" data-id="ProPany" class="button">Pro pány</a></li>
        <li><a href="#" data-id="ProDamy" class="button">Pro dámy</a></li>
        <li><a href="#" data-id="TVPlus" class="button">TV Plus</a></li>
        <li><a href="#" data-id="TVMaxi" class="button">TV Maxi</a></li>
    </ul>
    <br class="clear" />

    <div id="HBOaCinemax" class="sluzba-section mt50 programs balicky">
        <h2>HBO a Cinemax</h2>
        <h3>HBO</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/logo_war.png" alt="TV WAR" /><div class="about"><strong>TV WAR </strong><span>TV WAR, zaměření: válka historicky až po budoucnost, jazyk: česky</span></div></li>

            <li><img src="/css/layout/icons-programs/hbohd.png" alt="HBO HD" /><div class="about"><strong>HBO HD </strong><span>HBO HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/hbo2.png" alt="HBO 2 HD" /><div class="about"><strong>HBO 2 HD </strong><span>HBO 2 HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/hbo3.png" alt="HBO 3 HD" /><div class="about"><strong>HBO 3 HD </strong><span>HBO 3 HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
        </ul>
        <br class="clear" />
        <h3>Cinemax</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/cinemaxhd.png" alt="Cinemax HD" /><div class="about"><strong>Cinemax HD </strong><span>Cinemax HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/cinemax2hd.png" alt="Cinemax 2 HD" /><div class="about"><strong>Cinemax 2 HD </strong><span>Cinemax 2 HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
        </ul>
        <br class="clear" />
        <h3>HBO/Cinemax</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/hbohd.png" alt="HBO HD" /><div class="about"><strong>HBO HD </strong><span>HBO HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/hbo2.png" alt="HBO 2 HD" /><div class="about"><strong>HBO 2 HD </strong><span>HBO 2 HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/hbo3.png" alt="HBO 3 HD" /><div class="about"><strong>HBO 3 HD </strong><span>HBO 3 HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/cinemaxhd.png" alt="Cinemax HD" /><div class="about"><strong>Cinemax HD </strong><span>Cinemax HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/cinemax2hd.png" alt="Cinemax 2 HD" /><div class="about"><strong>Cinemax 2 HD </strong><span>Cinemax 2 HD, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>

        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                    <th>Balíček</th>
                    <th>Cena/kalendářní měsíc*</th>
                    <th>Aktivace set-up boxu</th>
                    <th>Aktivace TV služby</th>
                    <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>HBO</td>
                <td>250 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>3 TV kanály</td>
            </tr>
            <tr>
                <td>Cinemax</td>
                <td>140 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>2 TV kanály</td>
            </tr>
            <tr>
                <td>HBO/Cinemax</td>
                <td>320 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>5 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="DiscoveryHD" class="sluzba-section mt50 programs balicky none">
        <h3>Discovery HD</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/DIS_SHOWCASE.png" alt="Discovery HD" /><div class="about"><strong>Discovery HD </strong><span>Discovery SHOWCASE HD, zaměření: dokumentární, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/discovery-science-hd.png" alt="Discovery Science HD" /><div class="about"><strong>Discovery Science HD </strong><span>Discovery Science HD, zaměření: vesmír, věda, technika, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/Animal_Planet_HD.png" alt="Animal Planet HD" /><div class="about"><strong>Animal Planet HD </strong><span>ANIMAL PLANET HD, zaměření: dokumentární kanál, jazyk: česky, anglicky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
            <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Discovery HD</td>
                <td>120 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>3 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="DiscoverySD" class="sluzba-section mt50 programs balicky none">
        <h3>Discovery SD</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/discavery_channel.png" alt="Discovery Channel" /><div class="about"><strong>Discovery Channel </strong><span>Discovery Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/animal_planet.png" alt="ANIMAL PLANET" /><div class="about"><strong>ANIMAL PLANET </strong><span>ANIMAL PLANET, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/tlc_logo.png" alt="TLC" /><div class="about"><strong>TLC </strong><span>TLC, zaměření: kanál pro ženy, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/eurosport_1.png" alt="EUROSPORT 1" /><div class="about"><strong>EUROSPORT 1 </strong><span>EUROSPORT 1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_2.png" alt="EUROSPORT 2" /><div class="about"><strong>EUROSPORT 2 </strong><span>EUROSPORT 2, zaměření: sportovní kanál, jazyk: česky, anglicky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Discovery SD</td>
                <td>65 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>5 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="Eurosport" class="sluzba-section mt50 programs balicky none">
        <h3>Eurosport</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/eurosport_1.png" alt="Eurosport 1" /><div class="about"><strong>Eurosport 1 </strong><span>EUROSPORT 1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_2.png" alt="Eurosport 2" /><div class="about"><strong>Eurosport 2 </strong><span>EUROSPORT 2, zaměření: sportovní kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_1_HD.png" alt="Eurosport 1 HD" /><div class="about"><strong>Eurosport 1 HD </strong><span>EUROSPORT 1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_2_HD.png" alt="Eurosport 2 HD" /><div class="about"><strong>Eurosport 2 HD </strong><span>EUROSPORT 2 HD, zaměření: sportovní kanál, jazyk: česky, polsky, dánsky, srbsky, turecky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Eurosport</td>
                <td>70 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>4 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="Sport" class="sluzba-section mt50 programs balicky none">
        <h3>Sport</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/NovaSport1.png" alt="Nova SPORT 1" /><div class="about"><strong>Nova SPORT 1 </strong><span>Nova SPORT1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/sport1.png" alt="Sport 1 HD" /><div class="about"><strong>Sport 1 HD </strong><span>Sport 1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/sport2_HD.jpg" alt="Sport 2 HD" /><div class="about"><strong>Sport 2 HD </strong><span>Sport 2 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/eurosport_1.png" alt="Eurosport" /><div class="about"><strong>Eurosport </strong><span>EUROSPORT, zaměření: sportovní kanál, jazyk: česky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Sport</td>
                <td>80 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>4 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="Mladez" class="sluzba-section mt50 programs balicky none">
        <h3>Mládež</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/minginx.png" alt="Minimax/GINX" /><div class="about"><strong>Minimax/GINX </strong><span>Minimax/GINX, zaměření: dětské kanály, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/disney_logo.png" alt="DISNEY CHANNEL" /><div class="about"><strong>DISNEY CHANNEL </strong><span>DISNEY CHANNEL, zaměření: pro děti, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/cs_mini_film.png" alt="CS Mini, CS Film" /><div class="about"><strong>CS Mini, CS Film </strong><span>CS Mini, CS Film, zaměření: veřejnoprávní kanál (CS Mini - vysílání pro děti, CS Film - filmový kanál), jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/MTV_logo.png" alt="MTV Europe" /><div class="about"><strong>MTV Europe </strong><span>MTV Europe, zaměření: hudební kanál, jazyk: anglicky s českými titulky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Mládež</td>
                <td>40 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>4 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="Filmy" class="sluzba-section mt50 programs balicky none">
        <h3>Filmy</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/filmbox.png" alt="FILMBOX" /><div class="about"><strong>FILMBOX </strong><span>FILBOX, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/logo-filmbox-plus.jpg" alt="FILMBOX PLUS" /><div class="about"><strong>FILMBOX PLUS </strong><span>FILMBOX PLUS, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/film_plus.png" alt="Film +" /><div class="about"><strong>Film + </strong><span>Fim +, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/cs_mini_film.png" alt="CS Mini, CS Film" /><div class="about"><strong>CS Mini, CS Film </strong><span>CS Mini, CS Film, zaměření: veřejnoprávní kanál (CS Mini - vysílání pro děti, CS Film - filmový kanál), jazyk: česky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Filmy</td>
                <td>120 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>4 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="Dokumenty" class="sluzba-section mt50 programs balicky none">
        <h3>Dokumenty</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/natgeo.png" alt="National Geographic" /><div class="about"><strong>National Geographic </strong><span>National Geographic, zaměření: dokumentární, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/F-n-H.jpg" alt="Fishing &amp; Hunting" /><div class="about"><strong>Fishing &amp; Hunting </strong><span>Fishing & Hunting, zaměření: lov a rybolov, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/explor_spi.png" alt="EXPLORE / SPICE" /><div class="about"><strong>EXPLORE / SPICE </strong><span>VIASAT EXPLORE / SPICE, zaměření: dokumentární / erotický kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/viasat_history.png" alt="VIASAT HISTORY" /><div class="about"><strong>VIASAT HISTORY </strong><span>VIASAT HISTORY, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Dokumenty</td>
                <td>40 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>4 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="Priroda" class="sluzba-section mt50 programs balicky none">
        <h3>Příroda</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/natgeowi.png" alt="National Geo. Wild" /><div class="about"><strong>National Geo. Wild </strong><span>National Geographic Wild, jazyk: přírodopisný kanál, jazyk: anglicky s českými titulky, maďarsky, holandsky, rusky, turecky, bulharsky</span></div></li>
            <li><img src="/css/layout/icons-programs/Viasatnature.png" alt="VIASAT NATUR" /><div class="about"><strong>VIASAT NATUR </strong><span>VIASAT NATUR, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/animal_planet.png" alt="ANIMAL PLANET" /><div class="about"><strong>ANIMAL PLANET </strong><span>ANIMAL PLANET, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Příroda</td>
                <td>30 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>3 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="ProPany" class="sluzba-section mt50 programs balicky none">
        <h3>Pro pány</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/sport5.png" alt="Sport 5" /><div class="about"><strong>Sport 5 </strong><span>Sport 5, zaměření: sportovní, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Brazzers.png" alt="Brazzers TV Europe" /><div class="about"><strong>Brazzers TV Europe </strong><span>Brazzers TV Europe, zaměření: erotický kanál, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/travel.png" alt="TRAVEL CHANNEL" /><div class="about"><strong>TRAVEL CHANNEL </strong><span>TRAVEL CHANNEL, zaměření: dokumentární, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/discavery_channel.png" alt="Discovery" /><div class="about"><strong>Discovery </strong><span>Discovery Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_2.png" alt="Eurosport 2" /><div class="about"><strong>Eurosport 2 </strong><span>Eurosport 2, zaměření: sportovní kanál, jazyk: česky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Pro pány</td>
                <td>60 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>5 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="ProDamy" class="sluzba-section mt50 programs balicky none">
        <h3>Pro dámy</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/paprika.png" alt="TV Paprika" /><div class="about"><strong>TV Paprika </strong><span>TV Paprika, zaměření: kulinářské speciality, vaření, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/history_channel.png" alt="History Channel" /><div class="about"><strong>History Channel </strong><span>History Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/spectrum.png" alt="SPEKTRUM" /><div class="about"><strong>SPEKTRUM </strong><span>SPEKTRUM, zaměření: dokumentární, jazyk: česky, maďarsky</span></div></li>
            <li><img src="/css/layout/icons-programs/film_plus.png" alt="Film +" /><div class="about"><strong>Film + </strong><span>Fim +, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/explor_spi.png" alt="Explore/Spice" /><div class="about"><strong>Explore/Spice </strong><span>VIASAT EXPLORE / SPICE, zaměření: dokumentární / erotický kanál, jazyk: česky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>Pro dámy</td>
                <td>60 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>5 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="TVPlus" class="sluzba-section mt50 programs balicky none">
        <h3>TV Plus</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/FILMBOX.png" alt="FILMBOX" /><div class="about"><strong>FILMBOX </strong><span>FILMBOX, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/AXN.png" alt="AXN" /><div class="about"><strong>AXN </strong><span>AXN, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/AXN_Black.png" alt="AXN Black" /><div class="about"><strong>AXN Black </strong><span>AXN BLACK, zaměření: vědeckofantastický kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/AXN_White.png" alt="AXN White" /><div class="about"><strong>AXN White </strong><span>AXN WHITE, zaměření: krimi kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Ceskoslovensko_logo.png" alt="Československo HD" /><div class="about"><strong>Československo HD </strong><span>Československo HD, vysílací čas: 04:00-22:00 hod., Program se zaměřuje na tvorbu celého období existence Československa od roku 1918 až do roku 1992, kdy proběhlo rozdělení Československa. Zaměření: filmový, dokumentární kanál, jazyk: česky, slovensky</span></div></li>
            <li><img src="/css/layout/icons-programs/Festival_logo.png" alt="FESTIVAL HD" /><div class="about"><strong>FESTIVAL HD </strong><span>Festival Cinema HD, vysílací čas: 22:00-04:00 hod., zaměření: festivalová kinematografie, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/AMC_logo.png" alt="AMC" /><div class="about"><strong>AMC </strong><span>AMC, zaměření: aktuální filmy a seriály USA, jazyk: česky, anglicky, maďarsky</span></div></li>
            <li><img src="/css/layout/icons-programs/Nickelodeon.png" alt="NICKELODEON" /><div class="about"><strong>NICKELODEON </strong><span>NICKELODEON, zaměření: dětský kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/nick_jr.png" alt="Nick Jr." /><div class="about"><strong>Nick Jr. </strong><span>Nick Jr., zaměření: dětský kanál, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/jimjam.png" alt="JimJam" /><div class="about"><strong>JimJam </strong><span>JimJam, zaměření: dětský kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/disney.png" alt="DISNEY CHANNEL" /><div class="about"><strong>DISNEY CHANNEL </strong><span>DISNEY CHANNEL, zaměření: pro děti, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/minginx.png" alt="Minimax/GINX" /><div class="about"><strong>Minimax/GINX </strong><span>Minimax/GINX, zaměření: dětské kanály, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/NovaSport1.png" alt="Nova SPORT1" /><div class="about"><strong>Nova SPORT1 </strong><span>Nova SPORT1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/NovaSport1HD.png" alt="Nova SPORT1 HD" /><div class="about"><strong>Nova SPORT1 HD </strong><span>Nova SPORT1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/NovaSport2HD.png" alt="Nova SPORT2 HD" /><div class="about"><strong>Nova SPORT2 HD </strong><span>Nova SPORT2 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/eurosport_1.png" alt="EUROSPORT 1" /><div class="about"><strong>EUROSPORT 1 </strong><span>EUROSPORT 1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_2.png" alt="EUROSPORT 2" /><div class="about"><strong>EUROSPORT 2 </strong><span>EUROSPORT 2, zaměření: sportovní kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/SPORT_1HD.png" alt="Sport 1 HD" /><div class="about"><strong>Sport 1 HD </strong><span>Sport 1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/sport2_HD.jpg" alt="Sport 2 HD" /><div class="about"><strong>Sport 2 HD </strong><span>Sport 2 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/extreme_logo.png" alt="Extreme Sports" /><div class="about"><strong>Extreme Sports </strong><span>Extreme sports, zaměření: sportovní, jazyk: anglicky, německy, francouzsky, maďarsky, turecky, rusky</span></div></li>
            <li><img src="/css/layout/icons-programs/MTV_LIVE_logo.png" alt="MTV Live HD" /><div class="about"><strong>MTV Live HD </strong><span>MTV Live HD, hudební stanice vysílající přímé přenosy a živá vysílání hudebních koncertů a vystoupení, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/VH1_logo.png" alt="VH1" /><div class="about"><strong>VH1 </strong><span>VH1, zaměření: hudební kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/VH1Classic_logo.png" alt="VH1 Classic" /><div class="about"><strong>VH1 Classic </strong><span>VH1 Classic, zaměření: hudební, retro kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/discavery_channel.png" alt="Discovery Channel" /><div class="about"><strong>Discovery Channel </strong><span>Discovery Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/spectrum.png" alt="SPEKTRUM" /><div class="about"><strong>SPEKTRUM </strong><span>SPEKTRUM, zaměření: dokumentární, jazyk: česky, maďarsky</span></div></li>
            <li><img src="/css/layout/icons-programs/NatGeo_logo.png" alt="National Geographic" /><div class="about"><strong>National Geographic </strong><span>National Geographic, zaměření: dokumentární, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/animal_planet.png" alt="ANIMAL PLANET" /><div class="about"><strong>ANIMAL PLANET </strong><span>ANIMAL PLANET, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/logo_nature.png" alt="VIASAT NATUR" /><div class="about"><strong>VIASAT NATUR </strong><span>VIASAT NATUR, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/history_channel.png" alt="History Channel" /><div class="about"><strong>History Channel </strong><span>History Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/logo_hystory.png" alt="VIASAT HISTORY" /><div class="about"><strong>VIASAT HISTORY </strong><span>VIASAT HISTORY, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Crime_Investigation_NEW_logo.png" alt="Crime &amp; Investigations" /><div class="about"><strong>Crime &amp; Investigations </strong><span>CRIME AND INVESTIGATIONS, zaměření: dokumenty a kriminální dramata, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/travel.png" alt="TRAVEL CHANNEL" /><div class="about"><strong>TRAVEL CHANNEL </strong><span>TRAVEL CHANNEL, zaměření: dokumentární, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/tlc_logo.png" alt="TLC" /><div class="about"><strong>TLC </strong><span>TLC, zaměření: kanál pro ženy, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/logo_explore.png" alt="Explore/Spice" /><div class="about"><strong>Explore/Spice </strong><span>VIASAT EXPLORE / SPICE, zaměření: dokumentární / erotický kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Doq_TVlogo.png" alt="DoQ" /><div class="about"><strong>DoQ </strong><span>DoQ, zaměření: dokumentární kanál (příroda, historie, aj.), jazyk: česky, anglicky, maďarsky, rumunsky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>TV Plus</td>
                <td>500 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>29 TV kanálů</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>
    <div id="TVMaxi" class="sluzba-section mt50 programs balicky none">
        <h3>TV Maxi</h3>
        <ul>
            <li><img src="/css/layout/icons-programs/FILMBOX.png" alt="FILMBOX" /><div class="about"><strong>FILMBOX </strong><span>FILMBOX, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/AXN.png" alt="AXN" /><div class="about"><strong>AXN </strong><span>AXN, zaměření: filmový kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/AXN_Black.png" alt="AXN Black" /><div class="about"><strong>AXN Black </strong><span>AXN BLACK, zaměření: vědeckofantastický kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/AXN_White.png" alt="AXN White" /><div class="about"><strong>AXN White </strong><span>AXN WHITE, zaměření: krimi kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Ceskoslovensko_logo.png" alt="Československo HD" /><div class="about"><strong>Československo HD </strong><span>Československo HD, vysílací čas: 04:00-22:00 hod., Program se zaměřuje na tvorbu celého období existence Československa od roku 1918 až do roku 1992, kdy proběhlo rozdělení Československa. Zaměření: filmový, dokumentární kanál, jazyk: česky, slovensky</span></div></li>
            <li><img src="/css/layout/icons-programs/Festival_logo.png" alt="FESTIVAL HD" /><div class="about"><strong>FESTIVAL HD </strong><span>Festival Cinema HD, vysílací čas: 22:00-04:00 hod., zaměření: festivalová kinematografie, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/AMC_logo.png" alt="AMC" /><div class="about"><strong>AMC </strong><span>AMC, zaměření: aktuální filmy a seriály USA, jazyk: česky, anglicky, maďarsky</span></div></li>
            <li><img src="/css/layout/icons-programs/Nickelodeon.png" alt="NICKELODEON" /><div class="about"><strong>NICKELODEON </strong><span>NICKELODEON, zaměření: dětský kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/nick_jr.png" alt="Nick Jr." /><div class="about"><strong>Nick Jr. </strong><span>Nick Jr., zaměření: dětský kanál, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/jimjam.png" alt="JimJam" /><div class="about"><strong>JimJam </strong><span>JimJam, zaměření: dětský kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/disney.png" alt="DISNEY CHANNEL" /><div class="about"><strong>DISNEY CHANNEL </strong><span>DISNEY CHANNEL, zaměření: pro děti, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/minginx.png" alt="Minimax/GINX" /><div class="about"><strong>Minimax/GINX </strong><span>Minimax/GINX, zaměření: dětské kanály, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/NovaSport1.png" alt="Nova SPORT1" /><div class="about"><strong>Nova SPORT1 </strong><span>Nova SPORT1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/NovaSport1HD.png" alt="Nova SPORT1 HD" /><div class="about"><strong>Nova SPORT1 HD </strong><span>Nova SPORT1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/NovaSport2HD.png" alt="Nova SPORT2 HD" /><div class="about"><strong>Nova SPORT2 HD </strong><span>Nova SPORT2 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/eurosport_1.png" alt="EUROSPORT 1" /><div class="about"><strong>EUROSPORT 1 </strong><span>EUROSPORT 1, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_2.png" alt="EUROSPORT 2" /><div class="about"><strong>EUROSPORT 2 </strong><span>EUROSPORT 2, zaměření: sportovní kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/SPORT_1HD.png" alt="Sport 1 HD" /><div class="about"><strong>Sport 1 HD </strong><span>Sport 1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/sport2_HD.jpg" alt="Sport 2 HD" /><div class="about"><strong>Sport 2 HD </strong><span>Sport 2 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/extreme_logo.png" alt="Extreme Sports" /><div class="about"><strong>Extreme Sports </strong><span>Extreme sports, zaměření: sportovní, jazyk: anglicky, německy, francouzsky, maďarsky, turecky, rusky</span></div></li>
            <li><img src="/css/layout/icons-programs/MTV_LIVE_logo.png" alt="MTV Live HD" /><div class="about"><strong>MTV Live HD </strong><span>MTV Live HD, hudební stanice vysílající přímé přenosy a živá vysílání hudebních koncertů a vystoupení, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/VH1_logo.png" alt="VH1" /><div class="about"><strong>VH1 </strong><span>VH1, zaměření: hudební kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/VH1Classic_logo.png" alt="VH1 Classic" /><div class="about"><strong>VH1 Classic </strong><span>VH1 Classic, zaměření: hudební, retro kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/discavery_channel.png" alt="Discovery Channel" /><div class="about"><strong>Discovery Channel </strong><span>Discovery Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/spectrum.png" alt="SPEKTRUM" /><div class="about"><strong>SPEKTRUM </strong><span>SPEKTRUM, zaměření: dokumentární, jazyk: česky, maďarsky</span></div></li>
            <li><img src="/css/layout/icons-programs/NatGeo_logo.png" alt="National Geographic" /><div class="about"><strong>National Geographic </strong><span>National Geographic, zaměření: dokumentární, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/animal_planet.png" alt="ANIMAL PLANET" /><div class="about"><strong>ANIMAL PLANET </strong><span>ANIMAL PLANET, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/logo_nature.png" alt="VIASAT NATUR" /><div class="about"><strong>VIASAT NATUR </strong><span>VIASAT NATUR, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/history_channel.png" alt="History Channel" /><div class="about"><strong>History Channel </strong><span>History Channel, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/logo_hystory.png" alt="VIASAT HISTORY" /><div class="about"><strong>VIASAT HISTORY </strong><span>VIASAT HISTORY, zaměření: dokumentární kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Crime_Investigation_NEW_logo.png" alt="Crime &amp; Investigations" /><div class="about"><strong>Crime &amp; Investigations </strong><span>CRIME AND INVESTIGATIONS, zaměření: dokumenty a kriminální dramata, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/travel.png" alt="TRAVEL CHANNEL" /><div class="about"><strong>TRAVEL CHANNEL </strong><span>TRAVEL CHANNEL, zaměření: dokumentární, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/tlc_logo.png" alt="TLC" /><div class="about"><strong>TLC </strong><span>TLC, zaměření: kanál pro ženy, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/logo_explore.png" alt="Explore/Spice" /><div class="about"><strong>Explore/Spice </strong><span>VIASAT EXPLORE / SPICE, zaměření: dokumentární / erotický kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Doq_TVlogo.png" alt="DoQ" /><div class="about"><strong>DoQ </strong><span>DoQ, zaměření: dokumentární kanál (příroda, historie, aj.), jazyk: česky, anglicky, maďarsky, rumunsky</span></div></li>

            <li><img src="/css/layout/icons-programs/joj_cinema.png" alt="JOJ Cinema" /><div class="about"><strong>JOJ Cinema </strong><span>JOJ CINEMA, zaměření: filmový, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/markiza_inter.png" alt="Markíza International" /><div class="about"><strong>Markíza International </strong><span>MARKÍZA INTERNATIONAL, zaměření: filmový kanál, jazyk: slovensky, anglicky, německy</span></div></li>
            <li><img src="/css/layout/icons-programs/filmbox.png" alt="FILMBOX PLUS" /><div class="about"><strong>FILMBOX PLUS </strong><span>FILMBOX PLUS, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/film_plus.png" alt="Film+" /><div class="about"><strong>Film+ </strong><span>Fim +, zaměření: filmový kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/cs_mini_film.png" alt="CS Mini, CS Film" /><div class="about"><strong>CS Mini, CS Film </strong><span>CS Mini, CS Film, zaměření: veřejnoprávní kanál (CS Mini - vysílání pro děti, CS Film - filmový kanál), jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/horor_logo.png" alt="HOROR Film" /><div class="about"><strong>HOROR Film </strong><span>HOROR FILM, zaměření: hororový kanál, vysílání od 0:00 - 6.30 hod. po programu CS Film, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_1_HD.png" alt="EUROSPORT 1 HD" /><div class="about"><strong>EUROSPORT 1 HD </strong><span>EUROSPORT 1 HD, zaměření: sportovní kanál, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Eurosport_2_HD.png" alt="EUROSPORT 2 HD" /><div class="about"><strong>EUROSPORT 2 HD </strong><span>EUROSPORT 2 HD, zaměření: sportovní kanál, jazyk: česky, polsky, dánsky, srbsky, turecky</span></div></li>
            <li><img src="/css/layout/icons-programs/slovaksport2.png" alt="Slovak Sport 2 HD" /><div class="about"><strong>Slovak Sport 2 HD </strong><span>SLOVAK SPORT 2 HD, zaměření: sportovní, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/sport5.png" alt="Sport 5" /><div class="about"><strong>Sport 5 </strong><span>Sport 5, zaměření: sportovní, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/F-n-H.jpg" alt="Fishing and Hunting" /><div class="about"><strong>Fishing and Hunting </strong><span>Fishing & Hunting, zaměření: lov a rybolov, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Animal_Planet_HD.png" alt="ANIMAL PLANET HD" /><div class="about"><strong>ANIMAL PLANET HD </strong><span>ANIMAL PLANET HD, zaměření: dokumentární kanál, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/Golf_logo.png" alt="Golf Channel HD" /><div class="about"><strong>Golf Channel HD </strong><span>Golf Channel HD, zaměření: sportovní, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/MTV_logo.png" alt="MTV Europe" /><div class="about"><strong>MTV Europe </strong><span>MTV Europe, zaměření: hudební kanál, jazyk: anglicky s českými titulky</span></div></li>
            <li><img src="/css/layout/icons-programs/Mtv_Rocks_logo.png" alt="MTV Rocks" /><div class="about"><strong>MTV Rocks </strong><span>MTV Rocks, zaměření: hudební kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/MTV_Dance_logo.png" alt="MTV Dance" /><div class="about"><strong>MTV Dance </strong><span>MTV Dance, zaměření: hudební kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/discovery-hd-showcase-logo.jpg" alt="Discovery HD" /><div class="about"><strong>Discovery HD </strong><span>Discovery SHOWCASE HD, zaměření: dokumentární, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/discovery-science-hd.png" alt="Discovery Science HD" /><div class="about"><strong>Discovery Science HD </strong><span>Discovery Science HD, zaměření: vesmír, věda, technika, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/natgeowi.png" alt="National Geo. Wild" /><div class="about"><strong>National Geo. Wild </strong><span>National Geographic Wild, jazyk: přírodopisný kanál, jazyk: anglicky s českými titulky, maďarsky, holandsky, rusky, turecky, bulharsky</span></div></li>
            <li><img src="/css/layout/icons-programs/paprika.png" alt="TV Paprika" /><div class="about"><strong>TV Paprika </strong><span>TV Paprika, zaměření: kulinářské speciality, vaření, jazyk: česky</span></div></li>
            <li><img src="/css/layout/icons-programs/travel_hd.png" alt="Travel Channel HD" /><div class="about"><strong>Travel Channel HD </strong><span>TRAVEL CHANNEL HD, zaměření: dokumentární, jazyk: česky, anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/E_TV_logo.png" alt="E&#33; HD" /><div class="about"><strong>E&#33; HD </strong><span>E! HD, nejčerstvější zprávy ze světa celebrit, pravdivé příběhy z Hollywoodu, živé přenosy z červeného koberce během udílení cen Oskar, Emmy nebo Grammy: anglicky, 65 % česky</span></div></li>
            <li><img src="/css/layout/icons-programs/Brazzers.png" alt="Brazzers TV Europe" /><div class="about"><strong>Brazzers TV Europe </strong><span>Brazzers TV Europe, zaměření: erotický kanál, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/hustler_logo.png" alt="HUSTLER" /><div class="about"><strong>HUSTLER </strong><span>HUSTLER, zaměření: erotický kanál, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/playboy_logo.png" alt="PLAYBOY TV" /><div class="about"><strong>PLAYBOY TV </strong><span>PLAYBOY TV, zaměření: erotický kanál, jazyk: anglicky</span></div></li>
            <li><img src="/css/layout/icons-programs/Leo_logo.png" alt="LEO TV" /><div class="about"><strong>LEO TV </strong><span>LEO TV, zaměření: erotický kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/Extasy_logo.png" alt="EXTASY HD" /><div class="about"><strong>EXTASY HD </strong><span>EXTASY HD, zaměření: erotický kanál, jazyk: původní znění</span></div></li>
            <li><img src="/css/layout/icons-programs/logo_war.png" alt="TV WAR" /><div class="about"><strong>TV WAR </strong><span>TV WAR, zaměření: válka historicky až po budoucnost, jazyk: česky</span></div></li>
        </ul>
        <br class="clear" />
        <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead><tr class="poda">
                <th>Balíček</th>
                <th>Cena/kalendářní měsíc*</th>
                <th>Aktivace set-up boxu</th>
                <th>Aktivace TV služby</th>
                <th>Programová nabídka</th>
            </tr></thead>
            <tr>
                <td>TV Maxi</td>
                <td>800 Kč</td>
                <td>0 Kč</td>
                <td>0 Kč</td>
                <td>45 TV kanály</td>
            </tr>
        </table>
        </div>
        <p>*měsíční poplatek za programový balíček se přičítá k Základní nabídce, TV Start či k balíčkům TV+internet.</p>
        <br class="clear" />
    </div>




</div>-->

<div class="wrapper mt50">
		
	<p class="txt-center mt50"><strong class="big black uppercase bold">Máte zájem?</strong></p>
	<p class="txt-center mt25">Zadejte, prosím, adresu svého domova kde budete TV užívat...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>