<div class="mainImg cover_img" data-src="/css/layout/banner/volani.jpg">
	<?php /*
	<div class="blackRow">
		<ul>
			<li>
				<a href="/internet/" class="button">Nabídka internetu</a>
			</li>
			<li>
				<a href="/tv-sluzby/" class="button">Nabídka TV služeb</a>
			</li>
			<li class="active">
				<a href="/volani/" class="button">Nabídka tarifů</a>
			</li>
		</ul>
	</div>
	*/ ?>
	<div class="middleText">
		<h1 class="title">Klientská centra<small>vše co potřebujete vědět</small></h1>
	</div>

	<div id="arrow_bottom_sipka"></div>
</div>

<div class="article_text">
	<div class="wrapper mt50">
        <div class="tarifs hauto">
            <div class="tarifBox left">
                <p><strong class="black uppercase">Ostrava</strong></p>

                <p>28. října 1168/102<br />
                702 00 Ostrava-Moravská Ostrava</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>8:00 - 17:00 hod.</td>
                            </tr>
                            <tr>
                                <th class="poda">Úterý, čtvrtek, pátek</th>
                                <td>8:00 - 16:00 hod.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="tarifBox right">
                <p><strong class="black uppercase">Ostrava - Jih</strong></p>

                <p>Dr. Martínka 1508/3<br />
                700 30 Ostrava-Jih - Hrabůvka</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.    </td>
                            </tr>
                            <tr>
                                <th class="poda">Úterý, čtvrtek, pátek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 16.00 hod.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <div class="clear"> </div>
            <div class="tarifBox left">
                <p><strong class="black uppercase">Ostrava - Poruba</strong></p>

                <p>
                    Opavská 1140/40<br />
                    708 00 Ostrava-Poruba
                </p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.    </td>
                            </tr>
                            <tr>
                                <th class="poda">Úterý, čtvrtek, pátek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 16.00 hod.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tarifBox right">
                <p><strong class="black uppercase">Havířov</strong></p>

                <p>Hlavní třída 171/52<br />
                    736 01 Havířov-Město</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.    </td>
                            </tr>
                            <tr>
                                <th class="poda">Úterý, čtvrtek, pátek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 16.00 hod.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"> </div>



            <div class="tarifBox left">
                <p><strong class="black uppercase">Bohumín</strong></p>

                <p>
                    Studentská 753<br />
                    735 81 Nový Bohumín
                </p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Úterý, pátek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 16.00 hod.    </td>
                            </tr>
                            <tr>
                                <th class="poda">Středa</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tarifBox right">
                <p><strong class="black uppercase">Karviná</strong></p>

                <p>tř. Osvobození 1720/11<br />
                    735 06 Karviná</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.</td>
                            </tr>
                            <tr>
                                <th class="poda">čtvrtek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 16.00 hod.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"> </div>

            <div class="tarifBox left">
                <p><strong class="black uppercase">Orlová</strong></p>

                <p>Masarykova třída 1301<br/>
                    735 14 Orlová-Lutyně</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.</td>
                            </tr>
                            <tr>
                                <th class="poda">čtvrtek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 16.00 hod.</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="tarifBox right">
                <p><strong class="black uppercase">Znojmo</strong></p>

                <p>Pražská 1653/30<br />
                    669 02 Znojmo</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>9.00 - 17.00 hod.</td>
                            </tr>
                            <tr>
                                <th class="poda">Úterý, čtvrtek, pátek</th>
                                <td>9.00 - 16.00 hod.</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div class="clear"> </div>

            <div class="tarifBox left">
                <p><strong class="black uppercase">Praha Jarov</strong></p>

                <p>Koněvova 2496/223,<br/>
                    130 00 Praha 3 - Žižkov</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div class="tarifBox right">
                <p><strong class="black uppercase">Praha Ládví</strong></p>

                <p>nadzemní část vestibulu metra Ládví, Střelničná,<br/>
                    182 00 Praha 8 - Kobylisy</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Úterý, čtvrtek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"> </div>


            <div class="tarifBox left">
                <p><strong class="black uppercase">Praha Stodůlky</strong></p>

                <p>Zázvorkova 23/1,<br/>
                    155 00 Praha 13 - Stodůlky</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">úterý, čtvrtek</th>
                                <td>9.00 - 12.00 hod., 12.30 - 17.00 hod.</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>

            <div class="tarifBox right">
                <p><strong class="black uppercase">Brno</strong></p>

                <p>Gajdošova 4392/7<br />
                    615 00 Brno-Židenice</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí, středa</th>
                                <td>8.00 - 12.00 hod., 12.30 - 17.00 hod.</td>
                            </tr>

                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"> </div>
            <div class="tarifBox left">
                <p><strong class="black uppercase">Třeboň</strong></p>

                <p>Boženy Němcové 817/II<br/>
                    379 01  Třeboň</p>
                <div class="fst-table">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <tr>
                                <th class="poda">Pondělí - pátek</th>
                                <td>9.00 - 16.00 hod.</td>
                            </tr>


                        </table>
                    </div>
                </div>
            </div>
            <div class="clear"> </div>

        </div>
</div>
