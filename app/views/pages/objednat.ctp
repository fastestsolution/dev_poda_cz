<div class="mainImg cover_img h270" data-src="/css/layout/banner/objednat-bg.jpg">
	<div class="middleText">
		<h1 class="title">Nezávazná objednávka<small>Pro zdárné dokončení, Prosím, vyplňte níže uvedené údaje...</small></h1>
	</div>
</div>

<br class="clear" />

<div class="smaller-wrapper">
	<p class="txt-center mt50">Chcete objednat nebo rozšířit Vaše služby, nebojte se dokončit objednávku.<br />Rozšíříme či navýšíme Vaše stávající služby  nové objednané.</p>
	
	<p class="txt-center mt50"><strong class="bigger black uppercase bold">Vaše údaje</strong></p>
	
	<div class="load_contact_form" data-type="4"></div>
	<?php //echo $this->requestAction('/contact_forms/4', array('return')); ?>
</div> 

<?php if(isset($balicky) && !empty($balicky)) { ?>
	<?php foreach($balicky as $balicek) { ?>
		<?php if($balicek->tarif_id == $this->params['pass'][0]) { ?>
			<div id="PickedTarif" class="none" data-id="<?php echo $balicek->tarif_id; ?>" data-name="<?php echo $balicek->tarif_nazev; ?>">
				<div class="tarifBox"> 
					<div class="title">Mám zájem o tuto službu</div>
					
					<div class="row">
						<label>Programová nabídka</label> <strong><?php echo $balicek->param_tv; ?></strong>
					</div>
					<div class="row">
						<label>Internet</label> <strong><?php echo $balicek->param_net; ?></strong>
					</div>
					<div class="row">
						<label>Volání</label> <strong><?php echo $balicek->param_pvr; ?></strong>
					</div>	
					<div class="row">
						<label>Archiv</label> <strong><?php echo $balicek->param_tvarchiv; ?></strong>
					</div>	
					<div class="row">
						<label>Aktivace služby</label> <strong><?php echo $balicek->kc_aktivace; ?>,-</strong>
					</div>	
					<div class="row">
						<label>Připojení dalšího set-topboxu/měsíc</label> <strong>50,-</strong>
					</div>	
					<div class="row">
						<label>Aktivace 2. a další televize</label> <strong>50,-</strong>
					</div>	
					
					<div class="price"><!--<span>od</span>--> <?php echo $balicek->kc_pausal; ?>,-</div>	 	
				</div>
			</div>
		<?php } ?>
	<?php } ?>
<?php } ?>