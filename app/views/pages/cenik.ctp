<div class="mainImg cover_img" data-src="/css/layout/banner/bg-cenik.png">
	<div class="middleText">
		<h1 class="title">Víte, že u nás můžete mít služby od 0 korun?<small>Vážně! Žádný vtip!</small></h1>
	</div>
</div>

<br class="clear" />

<div class="smallest-wrapper">
	<p class="txt-center mt50"><strong class="uppercase black">Nemůžeme Vám říci přesně cenu bez toho, aniž bychom věděli, co potřebujete připojit.</strong></p>
	
	<p class="txt-center mt25">Pro to, abychom mohli sdělit výši tarifu, rychlost, nabízený set-box či cokoliv jiného, potřebujeme sdělit adresu, na kterou chcete objednané služby aktivovat.</p>
</div>

<br class="clear" />

<div class="wrapper">
	<p class="txt-center mt50"><strong class="big black uppercase bold">Zjistit cenu je vážně jednoduché</strong></p>
	<p class="txt-center mt25">Zadejte adresu místa, kterou chcete připojit...</p>
	
	<div class="inline-button txt-center">
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>

<div class="shadow-line"></div> 

<br class="clear" />


<h2 class="txt-center uppercase mt50">Často kladené otázky k internetu</h2>
<div class="wrapper">
    <div id="faq" class="faq mt50">
        <div class="fRow mt25">
            <div class="fRowTitle showMoreActual">Kde a jak si mohu objednat Vaše služby? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>
        <div class="fRow">
            <div class="fRowTitle showMoreActual">Kdy a jak službu dostanu? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>

        <div class="fRow">
            <div class="fRowTitle showMoreActual">V jaké síti působí poda mobil? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>

        <div class="fRow bottomLine">
            <div class="fRowTitle showMoreActual">Kolik zaplatím za SIM kartu? <a href="#" class="button circle showMoreActual">+</a></div>

            <p>Zkontrolujte propojení set-top boxu s Vaší televizí. Restartujte set-top box a zkontrolujte na Vaší televizi, že máte nastaven správný vstup. Pokud se nic nezmění, zkuste HDMI nebo SCART kabel zapojit do jiného portu. Nezpomeňte jen po změně také přenastavit v televizi.</p>
        </div>
    </div>
</div>


<br class="clear" />

<a href="/podpora/" class="button blue">Zobrazit více rad</a>