<div class="mainImg cover_img" data-src="/css/layout/banner/proc-bg.jpg">
	<div class="middleText">
		<h1 class="title">Proč právě Poda?<small>Čistě proto, že nám přijde zbytečné utrácet více.</small></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<br class="clear" />

<div class="wrapper">
	<ul class="icons-list mt50"> 
		<li>
			<img src="/css/layout/icons/penize-icon.png" alt="Neutrácejte zbytečně" />
			
			<strong>Neutrácejte zbytečně</strong>
			<p>Máme pro Vás připravené nabídky za velmi výhodných cen za Internet, TV i volání...</p>
		</li>
		<li>
			<img src="/css/layout/icons/lupa-icon.png" alt="Mějte výdaje pod kontrolou" />
			
			<strong>Mějte výdaje pod kontrolou</strong>
			<p>Jednoduchá správa Vašeho účtu v naší <a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank">klientské zóně</a>, přehledné vyúčtování, či jakékoliv změny...</p>
		</li>
		<li>
			<img src="/css/layout/icons/prasatko-icon.png" alt="Nejvýhodnější cenyt" />
			
			<strong>Nejvýhodnější ceny</strong>
			<p>Jsme na trhu už velmi dlouhou dobu a díky tomu Vám můžeme nabídnout bezkonkurenční ceny...</p>
		</li>
	</ul> 
	
	<br class="clear" />
	
	<div class="smaller-wrapper">
		<p class="txt-center mt25"><strong class="uppercase black">Máte zájem o super rychlý internet k tomu navíc Spoustu TV Programů, levné volánía ještě šetřit peníze?
Není nic jednoduššího než si vybrat PODU!</strong></p>

		<p class="txt-center mt25">Internet společně s IPTV poskytujeme už nějaký ten pátek, takže víme, že jak laik, tak i profík potřebují profesionální přístup a někoho, kdo mu na druhé straně rozumí.</p>
		
		<p class="txt-center mt50"><strong class="big black uppercase bold">Chci vyzkoušet Podu</strong></p>
		<p class="txt-center mt25">Zadejte adresu místa, kterou chcete připojit...</p>
	</div>
	
	<div class="chciPodu">	
		<?php echo $this->renderElement('layout/chci_podu'); ?>
	</div>
</div>

<div class="mainImg cover_img relative50" data-src="/css/layout/banner/sluzby.jpg">
	<div class="bottomText">
		<h2 class="title">TV + Internet za 399,-<small>udělejte rodině radost</small></h2>
		
		<a href="/tv-sluzby/" class="button">Mám zájem o tuto nabídku</a>
	</div>
</div>