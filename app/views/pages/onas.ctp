<div class="mainImg cover_img" data-src="/css/layout/banner/onas-bg.jpg">
	<div class="middleText">
		<h1 class="title"><?php echo $smallbox_list[8]['Smallbox']['title']; ?></h1>
	</div>
	
	<div id="arrow_bottom_sipka"></div>
</div>

<div class="article_text">
	<div class="wrapper">
		<div class="smaller-wrapper">
			<?php echo $smallbox_list[9]['Smallbox']['text']; ?>
		</div>
		
		<p class="txt-center mt50"><strong class="big black uppercase bold">Dostupnost klientské péče</strong></p>
		
		<ul class="icons-list">  
			<li>
				<img src="/css/layout/onas/ico-1.png" alt="Zvyšujeme naši dostupnost" />
				
				<strong>Zvyšujeme naši dostupnost</strong>
				<p>abyste nás mohli kontaktovat kdekoli a kdykoli to potřebujete.</p>
			</li>
			<li>
				<img src="/css/layout/onas/ico-2.png" alt="Technická podpora" />
				
				<strong>Technická podpora </strong>
				<p>v nesnázích jsme tu pro Vás</p>
			</li>
			<li>
				<img src="/css/layout/onas/ico-3.png" alt="100% dostupnost" />
				
				<strong>100% dostupnost</strong>
				<p>na webových stránkách a on-line chatu</p>
			</li>
			<li>
				<img src="/css/layout/onas/ico-4.png" alt="Navštivte nás osobně" />
				
				<strong>Navštivte nás osobně</strong>
				<p>Klientská centra jsou vám k dispozici již na 11 místech</p>
			</li>
		</ul> 
	</div>
</div> 

<?php echo $smallbox_list[10]['Smallbox']['text']; ?>

<p class="txt-center mt50"><strong class="big black uppercase bold">Historie společnosti</strong></p>

<div class="wrapper">
	<div class="timeline">
		<div class="half">
			<div class="time">
				<div class="year">2017</div>
				<p>nákup <strong>práv k využívání rádiových kmitočtů v pásmu 3600–3800 MHz pro vysokorychlostní datové sítě – úspěšně</strong> jsme vydražili 5G frekvence</p>
			</div>
			
			<div class="time">
				<div class="year">2015</div>
				<p>PODA se stává <strong>operátorem s největším počtem optických přípojek v ČR</strong></p>
			</div>
			
			<div class="time">
				<div class="year">2013</div>
				<p>spuštění provozu mobilních služeb PODA Mobil a sloučením společností PODA a.s. a SkyNet, a.s. vzniká <strong>4. největší alternativní telekomunikační operátor v ČR</strong></p>
			</div>
			
			<div class="time">
				<div class="year">2011</div>
				<p>otevření datacentra PODA</p>
			</div>	
			
			<div class="time">
				<div class="year">2009</div>
				<p>začátek výstavby vlastní optické sítě v Brně, akvizice ERKOR Ostrava, s.r.o.</p>
			</div>
			
			<div class="time">
				<div class="year">2007</div>
				<p>PODA začíná budovat optickou infrastrukturu na Ostravsku</p>
			</div>
			
			<div class="time">
				<div class="year">2005</div>
				<p>probíhá fúze PODA a.s. se společností TELTECH COM a.s., známou pod značkou Netopýr</p>
			</div>
			
			<div class="time">
				<div class="year">2002</div>
				<p>PODA rozšiřuje portfolio služeb i pro domácnosti</p>
			</div>
			
			<div class="time">
				<div class="year">1997</div>
				<p>PODA rozšiřuje portfolio služeb i pro domácnosti</p>
			</div>	
		</div>
		<div class="half">
			<div class="time">
				<div class="year">2016</div>
				<p><strong>20 let působení na trhu</strong></p>
			</div>
			
			<div class="time">
				<div class="year">2014</div>
				<p>zahájení výstavby vlastní optické sítě v Praze, odkup části závodu GREPA Networks, organizační složka Praha</p>
			</div>
			
			<div class="time">
				<div class="year">2012</div>
				<p>PODA se propojuje se společností SkyNet, a.s. a stává se <strong>největším IPTV operátorem v ČR</strong></p>
			</div>
			
			<div class="time">
				<div class="year">2010</div>
				<p>PODA realizuje projekt digitalizace TV v 34.000 bytech společnosti RPG Byty, s.r.o. (dnešní RESIDOMO)</p>
			</div>
			
			<div class="time">
				<div class="year">2008</div>
				<p>zahájení televizního vysílání PODA TV po kabelu IPTV, změna právní formy na akciovou společnost</p>
			</div>
			
			<div class="time">
				<div class="year">2006</div>
				<p>probíhá fúze PODA a.s. se společností TELTECH COM a.s., známou pod značkou Netopýr</p>
			</div>
			
			<div class="time">
				<div class="year">2004</div>
				<p>otevření pobočky v Brně</p>
			</div>
			
			<div class="time">
				<div class="year">1998</div>
				<p>zápis PODA s.r.o. do obchodního rejstříku</p>
			</div>
			
			<div class="time mb0">
				<div class="year">1996</div>
				<p>PODA připojuje první klienty k internetu</p>
			</div>
		</div>
	</div>
	
	<br class="clear" />
	
	<div class="buttons wide">
		<a href="/dokumenty-ke-stazeni/" class="button blue">Ke stažení</a>
		<a href="/dokumenty-ke-stazeni/" class="button">Výroční zprávy</a>
		<a href="/ekonomicke-udaje/" class="button">Ekonomické ukazatele</a>
	</div>
</div>


























