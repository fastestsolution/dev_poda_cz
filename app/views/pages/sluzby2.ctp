<div class="mainImg cover_img" data-src="/css/layout/banner/sluzby.jpg">
	<div class="blackRow">
		<ul>
			<li>
				<a href="/internet/" class="button">Nabídka internetu</a>
			</li>
			<li class="active">
				<a href="/tv-sluzby/" class="button">Nabídka TV služeb</a>
			</li>
			<li>
				<a href="/volani/" class="button">Nabídka tarifů</a>
			</li>
		</ul>
	</div>
	<div class="bottomText">
		<h1 class="title">TV + Internet za 399,- <small>udělejte rodině radost</small></h1>
		
		<a href="" class="button">Mám zájem o tuto nabídku</a>
	</div>
</div>

<div class="article_text">
	<div class="wrapper">
		<div class="smaller-wrapper">
			<p class="txt-center"><strong class="black">Udělejte rodině radost! Vyberte si z více než 80 programů, z nich je více než 80 v HD kvalitě a udělejte ze svého obýváku místo plné radosti a zábavy.</strong></p>
			<p class="txt-center">Posaďte se společně k televizi a využijte naplno širokou nabídku televiziních programů, PODA net. TV a navíc k tomu všechny chytré funkce, které Vám nabízíme.</p>
		
			<p class="txt-center mt50"><strong class="big black uppercase bold">Vyberte si službu přesně pro sebe</strong></p>
		</div>
		
		<div class="tarifs">
			<div class="tarifBox"> 
				<div class="title">Základní TV nabídka</div>
				
				<div class="row">
					<label>Programová nabídka</label> <a href="" class="black"><strong>80 TV KANÁLŮ</strong></a>
				</div>
				<div class="row">
					<label>PODA net.TV</label> <img src="/css/layout/none.png" alt="none" />
				</div>
				<div class="row">
					<label>PODA Mobil</label> <img src="/css/layout/none.png" alt="none" />
				</div>
				<div class="row">
					<label>Chytré funkce</label> <img src="/css/layout/none.png" alt="none" />
				</div>
				<div class="row">
					<label>Aktivace služby</label> <strong>1 150,-</strong>
				</div>
				<div class="row">
					<label>Připojení dalšího set-top boxu/měsíc</label> <strong>50,-</strong>
				</div>
				<div class="row">
					<label>Aktivace 2. a další televize</label> <strong>325,-</strong>
				</div>
				
				<div class="price"><span>od</span> 115,-</div>
				
				<br class="clear" />
				<a href="" class="button">Chci základní TV</a>
			</div>
			
			<div class="tarifBox red">
				<div class="title"><span>Akční nabídka</span>TV start</div>
				
				<div class="row">
					<label>Programová nabídka</label> <a href="" class="black"><strong>80 TV KANÁLŮ</strong></a>
				</div>
				<div class="row">
					<label>PODA net.TV</label> <strong><span class="colour">ZDARMA</span> 1 ZAŘÍZENÍ</strong>
				</div>
				<div class="row">
					<label>PODA Mobil</label> <strong><span class="colour">ZDARMA</span> 1 SIM</strong>
				</div>
				<div class="row">
					<label>Chytré funkce</label> <strong>ANO*</strong>
				</div>
				<div class="row">
					<label>Aktivace služby</label> <strong>1 150,-</strong>
				</div>
				<div class="row">
					<label>Připojení dalšího set-top boxu/měsíc</label> <strong>50,-</strong>
				</div>
				<div class="row">
					<label>Aktivace 2. a další televize</label> <strong>650,-</strong>
				</div>
				
				<p class="abs">*Netýká se programů skupiny Nova a Prima. U těchto programů si můžete chytré funkce přiobjednat přes set-top box anebo v <a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank">Klientské zóně</a>.</p>
				
				<div class="price"><span>od</span> 280,-</div>
				
				<br class="clear" />
				<a href="" class="button">Chci základní TV</a>
			</div>
			
			<br class="clear" />
			
			<a href="/" class="button">Programová nabídka</a>
			
			<br class="clear" />
			<br class="clear" />
			
			<div class="smaller-wrapper mt50">
				<p class="txt-center"><strong class="black">PODA.net</strong></p>
				<p class="txt-center">Podívejte se z Vašeho tabletu, počítače nebo smartphonu, notebooku nebo tabletu na velký přehled TV programů díky naší bezplatné aplikaci PODAnet.TV a zároveň využijte i další funkce. Své oblíbené pořady si můžete vychutnat, i když nebudete na Wi-Fi síti, ale třeba na mobilním připojení k internetu.</p>
			</div>
			
			<div class="buttons">
				<a href="" class="button">Více o Podanet.tv</a>
				<a href="" class="button blue">Ověřit dostupnost</a>
			</div>
		</div>
	</div>
	
	<br class="clear" />
		
	<div class="blok cover_img" data-src="/css/layout/banner/blok1.jpg">
		<h2>Chytré funkce</h2>
		<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolputate velit esse molestie.</p>
	</div>
	<div class="blok cover_img" data-src="/css/layout/banner/blok2.jpg">
		<h2>Vylepšete si vaši TV</h2>
		<p>Lorem ipsum dolor sit amet lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore adipiscing elit, sed magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolputate velit esse molestie.</p>
	</div>
	<div class="blok cover_img more-text" data-src="/css/layout/banner/blok3.jpg">
		<h2>Máte doma více TV?</h2>
		<p>Chcete sledovat sportovní pořad i oblíbený seriál najednout? Připojte si více televizí a sledujte oblíbené pořady podle toho, co Vás skutečně nejvíce těší.</p>
		
		<div class="wrap">
			<div class="wideBox">
				<div class="title">Základní TV nabídka</div>
				
				<div class="row"><label>Připojení dalšího set-top boxu/měsíc</label> <strong>50,-</strong></div>
				<div class="row"><label>Aktivace 2. a další televize</label> <strong>325,-</strong></div>
			</div>
			
			<div class="wideBox right">
				<div class="title">TV Start</div>
				
				<div class="row"><label>Připojení dalšího set-top boxu/měsíc</label> <strong>50,-</strong></div>
				<div class="row"><label>Aktivace 2. a další televize</label> <strong>650,-</strong></div>
			</div>
		</div>
	</div>
	<div class="blok cover_img more-text" data-src="/css/layout/banner/blok4.jpg">
		<h2>Co potřebujete, aby Vám tv hrála?</h2>
		<p>Televizi s HDMI/AV vstupem a internet. Dále pak zapojit telku už je jednoduché a určitě to zvládnet i bez technika:</p>
		
		<div class="wrap">
			<ol>
				<li>Propojte STB a televizi HDMI/AV kabelem</li>
				<li>Připojte STB k internetu pomocí síťového kabelu</li>
				<li>Zapojte STB do zásuvky a zapněte (<a href="">zde je návod ke stažení</a>)</li>
			</ol>
			
			<p class="align-left">Jaké příslušenství je nejlepší zvolit? Vyberte si z nabídky níže.</p>
		</div>
	</div>
	
	<br class="clear" />
	<h2 class="txt-center">PŘÍSLUŠENSTVÍ</h2> 
	<br class="clear" />
	
	<div class="wrapper">
		<div class="tarifs">
			<div class="tarifBox h640">
				<img src="/uploaded/prislusenstvi/arris.jpg" alt="Arris" />
				
				<p><strong class="black uppercase">Arris Vip1113</strong></p>
				
				<p>Arris VIP1113 je kompaktní, charakterově plný, HDTV IP set-top box, který nabízí zákazníkům krystalicky čistý HD obraz a surround zvuk. Připojení televize je možno jak přes rozhraní SCART, tak HDMI.</p>

				<ul class="bullets">
					<li>inovativní elagantní design</li>
					<li>high definition TV vysílání (interaktivní aplikace, OTT)</li>
					<li>MPEG 2, MPEG 4, SD, HD, H.264</li>
				</ul>
				
				<div class="price spanTop"><span>cena zařízení + dph</span> 1.625,-</div>
			</div>
			
			<div class="tarifBox h640">
				<img src="/uploaded/prislusenstvi/motorola.jpg" alt="Motorola" />
				
				<p><strong class="black uppercase">Motorola VIP1003</strong>
				<p>Motorola VIP1003 je kompaktní, efektivní, HDTV IP set-top box.</p>
				
				<ul class="bullets">
					<li>krystalicky čistý HD obraz a surround zvuk</li>
					<li>parametry: STi705 high-end system-on-a-chip (SoC) Single 450 MHz 800+ DMIPS mikroprocesor se zabudovanou grafikou pro dekódování HD a SD digitální TV a výstupem vysoce kvalitního surround audia</li>
					<li>kapacita: Two-way IP data communication 256 MB DRAM Paměť 64 MB Flash Standardní audio, video a data</li>
					<li>vstupy/výstupy: zadní panel obsahuje Ethernet, USB, SCART, HDMI, al S/PDIF interface přední panel obsahuje stavové LED a IR přijímač</li>
				</ul>
				<div class="price spanTop"><span>cena zařízení + dph</span> 1.700,-</div>
			</div>
			
			<br class="clear" />
			
			<p class="mt25 txt-center">Ceník platný od 1.1.2018</p>
			
			<a href="" class="button blue">Potřebujete poradit?</a>
			
			<?php echo $this->renderElement('layout/faq-tarify'); ?>
		</div>
</div>