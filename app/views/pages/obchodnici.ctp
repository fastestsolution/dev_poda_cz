<div class="mainImg cover_img h270" data-src="/css/layout/banner/obchodnici-bg.jpg">
	<div class="middleText">
		<h1 class="title">Domácí obchodníci<small>Níže naleznete kontakty na své domácí ochodníky</small></h1>
	</div>
</div>

<br class="clear" />

<div class="wrapper">
	
	<ul id="zakaznicka-linka" class="kontakt-list">
		<li class="blue">
			<h2>Zákaznická linka</h2>
			<p>Máte-li jakýkoliv dotaz, zavolejte nám na telefonní čísla každý den od <b>7:00 - 19:30</b></p>
			
			<strong>
			844 844 033 <br />
			730 430 430 
			</strong> 
		</li>
		<li class="blue">
			<h2>Kontaktní e-mail pro obchod</h2>
			<p>Každé podnikání má své požadavky. My vám navrhneme řešení na míru. Kontaktujte nás.</p>
			
			<a href="mailto:info@poda.cz"><strong>info@poda.cz</strong></a>
		</li>
		<li class="red">
			<h2>Kontaktní e-mail pro technickou kontrolu</h2>
			<p>Nepřetržitý přístup zdarma z počítače nebo chytrého telefonu.</p>
			
			<a href="mailto:podpora@poda.cz"><strong>podpora@poda.cz</strong></a>
		</li>
	</ul>
	<br class="clear" />
	<h2 class="mt50 mb0">Ostrava</h2>
	<table class="obchodnici-table mt10">
		<tr>
			<td><img src="/uploaded/obchodnici/prochackova.jpg" alt="Procháčkova" /></td>
			<td><strong>Dana Procháčková</strong></td>
			<td class="txt-right"><strong class="blue">734 766 282</strong></td>
			<td class="txt-right"><a href="mailto:bastova@poda.cz" class="blue">bastova@poda.cz</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/hlavka.jpg" alt="Hlavka" /></td>
			<td><strong>František Hlavka</strong></td>
			<td class="txt-right"><strong class="blue">734 646 057</strong></td>
			<td class="txt-right"><a href="mailto:hlavka@poda.cz" class="blue">hlavka@poda.cz</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/rydl.jpg" alt="Rýdl" /></td>
			<td><strong>Jakub Rýdl</strong></td>
			<td class="txt-right"><strong class="blue">730 431 359</strong></td>
			<td class="txt-right"><a href="mailto:rydl@poda.cz" class="blue">rydl@poda.cz</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/terc.jpg" alt="Terč" /></td>
			<td><strong>Milan Terč</strong></td>
			<td class="txt-right"><strong class="blue">730 431 313</strong></td>
			<td class="txt-right"><a href="mailto:terc@poda.cz" class="blue">terc@poda.cz</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/hudziak.jpg" alt="Hudziak" /></td>
			<td><strong>Daniel Hudziak</strong></td>
			<td class="txt-right"><strong class="blue">730 431 488</strong></td>
			<td class="txt-right"><a href="mailto:Dranger@seznam.cz" class="blue">Dranger@seznam.cz</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/lukac.png" alt="Lukač" /></td>
			<td><strong>Daniel Lukáč</strong></td>
			<td class="txt-right"><strong class="blue">730 431 180</strong></td>
			<td class="txt-right"><a href="mailto:daniellukac@yahoo.com" class="blue">daniellukac@yahoo.com</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/witaskova.jpg" alt="Witasková" /></td>
			<td><strong>Lucie Witasková</strong></td>
			<td class="txt-right"><strong class="blue">739 092 027</strong></td>
			<td class="txt-right"><a href="mailto:lucie.witaskova@seznam.cz" class="blue">lucie.witaskova@seznam.cz</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/pavlas.jpg" alt="Pavlas" /></td>
			<td><strong>Radim Pavlas</strong></td>
			<td class="txt-right"><strong class="blue">730 431 107</strong></td>
			<td class="txt-right"><a href="mailto:salvap.r@seznam.cz" class="blue">salvap.r@seznam.cz</a></td>
		</tr>
		<tr>
			<td><img src="/uploaded/obchodnici/adamczyk.jpg" alt="Adamczyk" /></td>
			<td><strong>Vlastimil Adamczyk</strong></td>
			<td class="txt-right"><strong class="blue">730 431 462</strong></td>
			<td class="txt-right"><a href="mailto:eva.adamczykova@gmail.com" class="blue">eva.adamczykova@gmail.com</a></td>
		</tr>
	</table>
	
	<br class="clear" />
	
	<p class="txt-center mt50"><strong class="big black uppercase bold">Nezjistili jste, co jste hledali?</strong></p>
	<p class="txt-center mt25">Vyberte si, prosím, další možné kontakty níže...</p>
	
	<ul class="kontakt-list">
		<li class="blue">
			<h2>Zákaznická linka</h2>
			<p>Máte-li jakýkoliv dotaz a potřebujete nám zavolat, využijte, prosím, níže uvedená telefonní čísla</p>
			
			<strong>
			844 844 033 <br />
			730 430 430
			</strong>
		</li>
		<li class="blue">
			<h2>Kontaktní e-mail pro obchod</h2>
			<p>Ani jedna generace se nebude nudit s naší nabídkou stovek programů.</p>
			
			<a href="mailto:info@poda.cz"><strong>info@poda.cz</strong></a>
		</li>
		<li class="red">
			<h2>Kontaktní e-mail pro technickou kontrolu</h2>
			<p>Nepřetržitý přístup zdarma z počítače nebo chytrého telefonu.</p>
			
			<a href="mailto:podpora@poda.cz"><strong>podpora@poda.cz</strong></a>
		</li>
	</ul>
</div>