<div id="mainPage"></div>

<ul class="services-list">
	<li>
		<h2>Internet</h2>
		<p>Pro Vás i celou Vaší rodinu nabídka vysokorychlostního internetu.</p>
		
		<a href="/internet/" class="button">Nabídka internetu</a>
	</li>
	<li>
		<h2>TV</h2>
		<p>Ani jedna generace se nebude nudit s naší nabídkou stovek programů.</p>
		 
		<a href="/tv-sluzby/" class="button">Nabídka TV služeb</a>
	</li>
	<li>
		<h2>Volání</h2>
		<p>Propojte se výhodně nejen v rodině, ale také s celým světem.</p>
		
		<a href="/volani/" class="button">Nabídka tarifů</a>
	</li>
	<li>
		<h2>Klientská zóna</h2>
		<p>Nepřetržitý přístup zdarma z počítače nebo chytrého telefonu.</p>
		
		<a href="https://klient.poda.cz/moduly/uzivatel/login.php" target="_blank" class="button color-red">Váš účet</a>
	</li>
</ul>

<br class="clear" /> 

<div class="promo">
	<a href="" class="button">Chci slevu</a>
</div>
 
<br class="clear" />

<div class="smaller-wrapper">
	<p class="txt-center lh1-5"><strong>PODU pro vás budujeme už od roku 1996.</strong> Jsme víc, než jen poskytovatel datových a telekomunikačních služeb. Jsme zapálený tým, který zakládá úspěch na maličkostech, protože víme, že jenom tak vznikají velké věci. Je nám jasné, že i zdánlivá drobnost může váš komfort posunout o několik řádů výš. Důvěra je pro nás prioritou. Stavíme na osobní zodpovědnosti za každý náš krok. Vždy u nás najdete přátelský přístup a pokud můžeme cokoliv zlepšit, věřte, že to rádi uděláme...</p>
	
	<br class="clear" /> 
	
	<a href="/o-nas/" class="button">Více o nás</a>
</div>

<?php /*
<div class="sll">  
	<div class="article">
		<h3><a href="/novinka/<?php echo $actuals[0]['Actual']['alias_']; ?>/<?php echo $actuals[0]['Actual']['id']; ?>/"><?php echo $actuals[0]['Actual']['name']; ?></a></h3>
		
		<p><?php echo $actuals[0]['Actual']['perex']; ?></p>
	</div>
</div>

<div class="slr">	
	<div class="article">
		<h3><a href="/novinka/<?php echo $actuals[1]['Actual']['alias_']; ?>/<?php echo $actuals[1]['Actual']['id']; ?>/"><?php echo $actuals[1]['Actual']['name']; ?></a></h3>
		
		<p><?php echo $actuals[1]['Actual']['perex']; ?></p>
	</div>
</div>
*/ ?>