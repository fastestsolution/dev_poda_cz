<?php
function list_element_sitemap($data,$lang, $settings = array(),$level = 0 ){
	$modelName = 'Defualt';
	$fieldName = 'name';
	$ul_id = 'tree_ul_id';
	$prefix = '';	
	extract($settings);
	
	$tabs = "\n" . str_repeat("\t", $level * 2);
	$li_tabs = $tabs . "\t";
    if($data[0][$modelName]['name']!='') 
	   $output = $tabs. "<ul".(($ul_id != null)?" class='".$ul_id."'":"").">";
	else 
		$output='';   
	
	foreach ($data as $key=>$val){
    	if($val[$modelName]['name']!=''){
    		$settings['prefix'] = $prefix;
    		// START vygenerovani href
    		$href=($lang!="cz" ? "/".$lang : "");
    	  	$href.=((empty($val[$modelName]['spec_url']))?$prefix.'/'.$val[$modelName]['alias_'].'/':$val[$modelName]['spec_url']);
    	  	// END vygenerovani href
    	  	
    		$output .= $li_tabs . "<li><strong><a title='".$val[$modelName]['name']."' href='".$href."'>".$val[$modelName]['name']."</a></strong>";
    		if(isset($val['children'][0])){
    			$settings['ul_id'] = null;
    			$settings['prefix'] = $settings['prefix'].'/'.$val[$modelName]['alias_'];
    			$output .= list_element_sitemap($val['children'],$lang, $settings, $level+1);
    			$output .= $li_tabs . "</li>";  
    		} else {
    			$output .= "</li>";
    		}
		}
	}
	if($data[0][$modelName]['name']!='') 
	   $output .= $tabs . "</ul>";
    return $output;
 } 
?>
<?php //pr($menu_item_sitemap); ?>
<?php echo list_element_sitemap($menu_item_sitemap,CURRENT_LANGUAGE , array('modelName'=>'MenuItem','prefix'=>'','subfix'=>'id', 'fieldName'=>'name','ul_id'=>'sitemap'));?>
<?php
	
	echo '<h2 class="sitemap">'.lang_ostatni_odkazy.'</h2>';
		echo '<ul class="sitemap">';		
				echo "<li><a title='".lang_prohlaseni_o_pristupnosti."' href='".$lang."prohlaseni-o-pristupnosti/' class='ajax_href'>".lang_prohlaseni_o_pristupnosti."</a></li>";
				echo "<li><a title='".lang_rss_aktuality."' href='/rss-aktuality/1/' class='ajax_href'>".lang_rss_aktuality."</a></li>";
				
		echo '</ul>';
		
?>