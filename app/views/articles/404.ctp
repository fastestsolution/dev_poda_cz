<div class="article_text">
<?php 
$error404['cz'] = '
		<p>Je nám líto, ale zadaná stránka nemůže být zobrazena.</p>
		<h2>Možné příčiny Vašeho problému</h2>
			<ul>
				<li>Chybně zadaná adresa stránky</li>
				<li>Zadaná stránka neexistuje</li>
				<li>Stránka je dočasně nedostupná, zkuste se vrátit později</li>
				<li>Nemáte oprávnění k prohlížení zadané stránky</li>
				<li>Zkuste navštívit <a href="/" title="Zpět na Úvodní stránku">úvodní stránku</a></li>
			</ul>';

$error404['en'] = '
		<p> Sorry, but given page can not be displayed. </p>
		<h2> Possible causes of your problem </h2>
		<ul>
		<li> Wrongly entered the page address </li>
		<li> Specified page does not exist </li>
		<li> This page is temporarily unavailable, please try later returns </li>
		<li> You do not have permission to view the specified page </li>
		<li>Try to visit  <a href="/en" title="Back to Home page">home page</a> </li>
		</ul>
		<h2> Sitemap </h2>
		<p> Use the site map to find the information you wanted. </p>';

$error404['pl'] = '
		<p> Przepraszamy, ale podana strona nie może zostać wyświetlona. </p>
		<h2> Możliwe przyczyny problemu </h2>
		<ul>
		<li>Niewłaściwie  wprowadzony adres strony </li>
		<li> Podana strona nie istnieje </li>
		<li> Ta strona jest chwilowo niedostępna, spróbuj później wraca </li>
		<li> Nie masz uprawnień, aby wyświetlić określoną stronę </li>
		 <li>Spróbuj odwiedzić <a href="/pl" title="Home stránku"> strona główna </a> </li>
		</ul>
		<h2> Sitemap </h2>
		<p> Využite mapa znalezienie poszukiwanych informacji. </p>';

$error404['de'] = '
		<p> Sorry, aber angesichts Seite kann nicht angezeigt werden. </p>
		<h2> Mögliche Ursachen für Ihr Problem </h2>
		<ul>
		<li> Falsch eingegebene Adresse der Seite </li>
		<li> Spezifiziertes Seite ist nicht vorhanden </li>
		<li> Diese Seite ist vorübergehend nicht verfügbar, bitte versuchen Sie es später kehrt </li>
		<li> Sie haben keine Berechtigung, um die angegebene Seite </li>
		Versuchen Sie zu besuchen <a href="/de" title="Homepage"> Homepage </a> </li>
		</ Ul>
		<h2> Sitemap </h2>
		<p> Verwenden Sie die Informationen finden, die Sie wollen </p>';

$error404['hu'] = '
		<p> Sajnáljuk, de mivel az oldal nem jelenik meg. </p>
		<h2> lehetséges okai a problémát </h2>
		<ul>
		Hibásan <li> be az oldal címét </li>
		<li> megadott oldal nem létezik </li>
		<li> Az oldal átmenetileg nem elérhető, kérjük próbálja meg később visszatér </li>
		<li> Nincs engedélye, hogy a megadott oldalra </li>
		Próbáld látogatás <li> <a href="/" title="Zpět otthon stránku"> home page </a> </li>
		</Ul>
		<h2> Sitemap </h2>
		<p> használata Oldaltérkép megtalálni az információt, amit akart. </p> ';
		

$error404['fr'] = '
		<p> Désolé, mais compte tenu de la page ne peut pas être affichée. </p>
		<h2> Les causes possibles de votre problème </h2>
		<ul>
		<li> indûment inscrit l\'adresse de la page </li>
		<li> page indiquée n\'existe pas </li>
		<li> Cette page est temporairement indisponible, réessayer plus tard s\'il vous plaît retours </li>
		<li> Vous n\'avez pas la permission d\'afficher la page spécifiée </li>
		Essayez de visiter <li> <a href="/" title="Zpět à la maison stránku"> page d\'accueil </a> </li>
		</Ul>
		<h2> Plan du site </h2>
		<p> utilisation du site pour trouver l\'information que vous voulez. </p>';
		

$error404['it'] = '		
		<p> Siamo spiacenti, ma determinata pagina non possono essere visualizzate. </p>
		<h2> possibili cause del problema </h2>
		<ul>
		Indebita <li> entrato l\'indirizzo della pagina </li>
		<li> pagina specificata non esiste </li>
		<li> Questa pagina è temporaneamente non disponibile, si prega di riprovare più tardi ritorna </li>
		<li> Non hai i permessi per visualizzare la pagina specificata </li>
		Prova a visitare <li> <a href="/" title="Zpět a casa stránku"> home page </a> </li>
		</UL>
		<h2> Mappa del sito </h2>
		Usa <p> Mappa del sito per trovare le informazioni che si voleva. </p>';

$error404['ru'] = '		
		<p> Извините, но данная страница не может быть отображен. </P>
		<h2> Возможные причины проблемы </h2>
		<ul>
		<li> неправильно поступил в адрес страницы </LI>
		<li> Указанная страница не существует </LI>
		<li> Эта страница временно недоступен, попробуйте позже возвращает </LI>
		<li> У вас нет разрешения на просмотр определенной страницы </LI>
		Попробуйте посетить <li> <a href="/" title="Zpět на главную stránku"> Главная страница </A> </LI>
		</UL>
		<h2> сайта </h2>
		<p> использования сайта, чтобы найти информацию, которую вы хотели. </P>';

$error404['sk'] = '
		<p> Je nám ľúto, ale zadaná stránka nemôže byť zobrazená. </p>
		<h2> Možné príčiny Vášho problému </h2>
		<ul>
		<li> Nesprávne zadaná adresa stránky </li>
		<li> Zadaná stránka neexistuje </li>
		<li> Stránka je dočasne nedostupná, skúste sa vráti neskôr </li>
		<li> Nemáte oprávnenie na prehliadanie zadanej stránky </li>
		<li> Skúste navštíviť <a href="/" title="Zpět na Úvodná stránku"> úvodnú stránku </a> </li>
		</Ul>
		<h2> Mapa stránok </h2>
		<p> Použite mapu stránok na nájdenie Vami hľadaných údajov. </p>';

$error404['sp'] = '
		<p> Lo sentimos, pero en vista de la página no se puede mostrar. </p>
		<h2> las posibles causas de su problema </h2>
		<ul>
		<li> incorrectamente la dirección de la página </li>
		<li> especificado página no existe </li>
		<li> Esta página no está disponible temporalmente, por favor, intente más tarde regresa </li>
		<li> Usted no tiene permiso para ver la página especificada </li>
		Trate de visitar <li> <a href="/" title="Zpět en stránku"> página de Inicio </a> </li>
		</Ul>
		<h2> Mapa del sitio </h2>
		Mapa del sitio <p> uso para encontrar la información que quería. </p>';	
		
echo $error404[CURRENT_LANGUAGE];
?>
		
</div>

<?php
function list_element_sitemap($data,$lang, $settings = array(),$level = 0 ){
	$modelName = 'Defualt';
	$fieldName = 'name';
	$ul_id = 'tree_ul_id';
	$prefix = '';	
	extract($settings);
	
	$tabs = "\n" . str_repeat("\t", $level * 2);
	$li_tabs = $tabs . "\t";
    if($data[0][$modelName]['name']!='') 
	   $output = $tabs. "<ul".(($ul_id != null)?" class='".$ul_id."'":"").">";
	else 
		$output='';   
	
	foreach ($data as $key=>$val){
    	if($val[$modelName]['name']!=''){
    		$settings['prefix'] = $prefix;
    		// START vygenerovani href
    		$href=($lang!="cz" ? "/".$lang : "");
    	  	$href.=((empty($val[$modelName]['spec_url']))?$prefix.'/'.$val[$modelName]['alias_'].'/':$val[$modelName]['spec_url']);
    	  	// END vygenerovani href
    	  	
    		$output .= $li_tabs . "<li><strong><a title='".$val[$modelName]['name']."' href='".$href."'>".$val[$modelName]['name']."</a></strong>";
    		if(isset($val['children'][0])){
    			$settings['ul_id'] = null;
    			$settings['prefix'] = $settings['prefix'].'/'.$val[$modelName]['alias_'];
    			$output .= list_element_sitemap($val['children'],$lang, $settings, $level+1);
    			$output .= $li_tabs . "</li>";  
    		} else {
    			$output .= "</li>";
    		}
		}
	}
	if($data[0][$modelName]['name']!='') 
	   $output .= $tabs . "</ul>";
    return $output;
 } 
?>
<?php /*
 if (isset($menu_item) && !empty($menu_item))
echo list_element_sitemap($menu_item,CURRENT_LANGUAGE , array('modelName'=>'MenuItem','prefix'=>'','subfix'=>'id', 'fieldName'=>'name','ul_id'=>'sitemap'));?>
<?php
	
	echo '<h2 class="sitemap">'.lang_ostatni_odkazy.'</h2>';
		echo '<ul class="sitemap">';		
				echo "<li><a title='".lang_prohlaseni_o_pristupnosti."' href='/".lang_alias_prohlaseni_o_pristupnosti."/' class='ajax_href'>".lang_prohlaseni_o_pristupnosti."</a></li>";
				echo "<li><a title='".lang_rss_aktuality."' href='/rss-aktuality/aktuality/1' class='ajax_href'>".lang_rss_aktuality."</a></li>";
				
		echo '</ul>'; */
		
?>