<?php
class FastestHelper extends Helper {
	var $helpers = array('Html');
	
	
	function img($img_set){
		// path noimg
		$noimg_path = '/css/layout/noimg.png';
		
		if (!isset($img_set['nosize'])){
			$img_set['nosize'] = false;
		}
		
		$noimg = false;
		if (isset($img_set['imgs'])){
			$imgs = json_decode($img_set['imgs'],true);
		}
		if (isset($img_set['first'])){
			if (is_array($imgs))
			foreach($imgs AS $key=>$img){
				if ($key > 0){
					unset($imgs[$key]);
				}
			}
		}
		
		// ostatni fotky pro shop
		if (isset($img_set['other_imgs'])){
			$other_imgs = array();
			foreach($imgs AS $key=>$img){
				if ($key > 0){
					$other_imgs[] = $img;
					unset($imgs[$key]);
				}
			}
		}
		if (!is_array($imgs)){
			$imgs = array(
				0=>array('file'=>'#')
			);
		}
		//pr($imgs);die();
		
		foreach($imgs AS $img){
			
			
			$img_title = ((isset($img['title']) && !empty($img['title']))?$img['title']:(isset($img_set['title']) && !empty($img_set['title']))?$img_set['title']:'Foto');
			
			$img_link = '/img/';
			
			$img_params = array();
			//pr($img_set);
			if (strpos($img['file'],'http') !== false){
				$img['file'] = $img['file'];
			} else {
			
				
				if (isset($img_set['path'])){
					$img['file'] = $img_set['path'].$img['file'];
				}
			}
			//pr($img);die();
			//if ($)
			//pr($img_set);
			if (strpos($img['file'],'://') !== false){
			
				$img_params['file'] = $img['file'];
				if (!$this->url_exists($img_params['file'])){
					 $noimg = true;
					 $img_params['file'] = '.'.$noimg_path;
				}
			} else {
				$img_params['file'] = '.'.$img['file'];
				if (!file_exists($img_params['file'])){
					 $noimg = true;
					 $img_params['file'] = '.'.$noimg_path;
				}
			
			}
			
			//pr($img_params);pr($noimg);die();
			if (isset($img_set['class'])) $img_class = $img_set['class'];
			if (isset($img_set['bg'])) $img_params['bg'] = $img_set['bg'];
			if (isset($img_set['width'])) $img_params['w'] = $img_set['width'];
			if (isset($img_set['height'])) $img_params['h'] = $img_set['height'];
			
			
			$img_params_other['w'] = (isset($img_set['width_other'])?$img_set['width_other']:120);
			$img_params_other['h'] = (isset($img_set['height_other'])?$img_set['height_other']:120);
			
			//pr($img_params);die();
			
			$img_code = $img_link.'?params='.$this->encode_long_url($img_params);
			//pr($img_code);die();
			$out = '';
			if (isset($img_set['other_imgs'])){
				$out .= '<div class="solo_image" id="main_image">';
			}
			if (isset($img_set['only_url'])){
				$out = $img_code;
				return $out;
			}
			$html = new HtmlHelper(); 
		
			// zoom foto
			if (isset($img_set['zoom'])){
				
				if ($noimg == false){
			
					$image = $html->link('<div data-original="'.$img_code.'" class=" img lazy '.(isset($img_class)?$img_class:'').'"></div>',ltrim($img_params['file'],'.'),array('title'=>$img_title, 'class'=>'swipebox','escape'=>false));
					//pr($image);die();
					//$image = $this->Html->link($this->Html->image('#',array('data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'zoom_foto lazy '.(isset($img_class)?$img_class:'')),null,false),$img_params['file'],array('title'=>$img_title,'data-rel'=>'lightbox-atomium','escape'=>false));
				}

				else {
			
					$image = '<div data-original="'.$img_code.'" class="img lazy '.(isset($img_class)?$img_class:'').'"></div> ';
					//$image = $this->Html->image('#',array('data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'lazy '.(isset($img_class)?$img_class:'')),null,false);
				}
			
			// link to url
			} elseif (isset($img_set['link'])){
				//pr($img_set['link']);die();
				//$image = $this->Html->link($this->Html->image('#',array('title'=>$img_title,'data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'zoom_foto lazy '.(isset($img_class)?$img_class:'')),null,false),$img_set['link'],array('title'=>$img_title,'escape'=>false));
				$image = $this->Html->link('<div data-original="'.$img_code.'" class="zoom_foto lazy img '.(isset($img_class)?$img_class:'').'"></div>',$img_set['link'],array('title'=>$img_title,'escape'=>false));
			
			// only img
			} else {
				$image = '<div data-original="'.$img_code.'" class="img lazy '.(isset($img_class)?$img_class:'').'"></div> ';
					
				//$image = $this->Html->image('#',array('data-original'=>$img_code,'data-width'=>$img_set['width'],'data-height'=>$img_set['height'],'data-nosize'=>$img_set['nosize'],'title'=>$img_title,'alt'=>$img_title,'class'=>'lightbox-atomium  lazy '.(isset($img_class)?$img_class:'')),null,false);
			}

			
			$out .= $image;
			//pr($out);die();
			if (isset($img_set['other_imgs'])){
				$out .= '</div>';
			}
			if (isset($other_imgs) && count($other_imgs)>0){
				$out .= '<div class="other_imgs" id="other_imgs">';
					foreach($other_imgs AS $img2){
						$img_link = '/image_resize.php';
						$img_params = array();
						//pr($img2);
						if (isset($img_set['path'])){
							$img2['file'] = $img_set['path'].$img2['file'];
						}
						
						if (strpos($img2['file'],'://') !== false){
							$img2['file'] = $img2['file'];
							if (!$this->url_exists($img2['file'])){
								 $noimg = true;
								 $img2['file'] = $noimg_path;
							}
						} else {
							$img2['file'] = '.'.$img2['file'];
							if (!file_exists($img2['file'])){
								 $noimg = true;
								 $img2['file'] = $noimg_path;
							}
						
						}
						$img_params['file'] = $img2['file'];
						
						$img_params['bg'] = $img_set['bg'];
						$img_params['w'] = $img_params_other['w'];
						$img_params['h'] = $img_params_other['h'];
						
						//pr($img_params);die();
						
						$img_code = $img_link.'?params='.$this->encode_long_url($img_params);
						//$out .= $this->Html->link($this->Html->image('#',array('title'=>$img_title,'alt'=>$img_title,'data-original'=>$img_code,'data-width'=>$img_params['w'],'data-height'=>$img_params['h'],'data-nosize'=>$img_set['nosize'],'class'=>'lazy zoom_foto '.(isset($img_class)?$img_class:'')),null,false),$img_params['file'],array('title'=>$img_title,'data-rel'=>'lightbox-atomium','class'=>'link','escape'=>false));
						$out .= $this->Html->link('<div data-original="'.$img_code.'" class="lazy img zoom_foto '.(isset($img_class)?$img_class:'').'"></div>',$img_params['file'],array('title'=>$img_title,'data-rel'=>'lightbox-atomium','class'=>'link','escape'=>false));
						
						
					}
				$out .= '</div>';
				
			}
			
		}
		//pr($out);die();
		echo $out;
	}
	
	/*** overeni zda url je dostupne **/
	function url_exists($url) {
			// Version 4.x supported
			$handle = curl_init($url);
			if (false === $handle) {
				return false;
			}
			curl_setopt($handle, CURLOPT_HEADER, false);
			curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
			curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15")); // request as if Firefox   
			curl_setopt($handle, CURLOPT_NOBODY, true);
			curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
			$connectable = curl_exec($handle);
			
			curl_close($handle);
			return $connectable;
	}
	
	function img_hash($img_set){
		$img_params = array();
		$img_link = '/img/';
				
		if (isset($img_set['file'])) $img_params['file'] = $img_set['file'];
		if (isset($img_set['bg'])) $img_params['bg'] = $img_set['bg'];
		if (isset($img_set['width'])) $img_params['w'] = $img_set['width'];
		if (isset($img_set['height'])) $img_params['h'] = $img_set['height'];
				
		$img_code = $img_link.'?params='.$this->encode_long_url($img_params);
		return $img_code;
		//pr($img_code);
	}
	
	function convert_url($url){
		if(isset($url)){
			$tmp_url = array();
			
			if (count($url)>0){
				
				foreach($url AS $k=>$u){
					$tmp_url[strtr($k,array('amp;'=>''))] = $u;
				}
				$url = $tmp_url;
			}
			return $url;
		} else {
			return $url;
		}
	}
	/*** ENCODE LONG URL **/	
	function encode_long_url($data){
         return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
    }
    /*** DECODE LONG URL **/	
	function decode_long_url($data){
    	$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
    }
	
	
	/** cislo na slovo **/
	function CisloNaSlovo($cislo, $nula = false,$typ='normal') {
		if ($typ =='sklon'){
			$jednotky = array("", "jednoho","dvou","tří","čtyř","pěti","šesti","sedmi","osmi","devíti");
			$mezi = array(11=>"jedenácti",12=>"dvanácti",13=>"třinácti",14=>"čtrnácti",15=>"patnácti",16=>"šestnácti",17=>"sedmnácti",18=>"osmnácti",19=>"devatenácti");
			$desitky = array("", "deset","dvaceti","třiceti","čtyřiceti","padesáti","šedesáti","sedmdesáti","osmdesáti","devadesáti");
		} else {
			$jednotky = array("", "jedna","dva","tři","čtyři","pět","šest","sedm","osm","devět");
			$mezi = array(11=>"jedenáct",12=>"dvanáct",13=>"třináct",14=>"čtrnáct",15=>"patnáct",16=>"šestnáct",17=>"sedmnáct",18=>"osmnáct",19=>"devatenáct");
			$desitky = array("", "deset","dvacet","třicet","čtyřicet","padesát","šedesát","sedmdesát","osmdesát","devadesát");
		}

		$cislo = (string) ltrim(round($cislo), 0);
		$delka = strlen($cislo);

		if($cislo==0)  return $nula ? "nula":false;             //ošetření 0
		elseif($delka==1)        
			return $jednotky[$cislo];  //1 řád - jednotky
		elseif($delka==2) {                                 //2 řády - desítky
			$desitkyAJednotky = $cislo{0}.$cislo{1};
			if($desitkyAJednotky==10) echo "deset";
			elseif($desitkyAJednotky<20) {
				return $mezi[$desitkyAJednotky];
			} else {
				return $desitky[$cislo{0}].$jednotky[$cislo{1}];
			}
		}
		elseif($delka==3) {                                 //3 řády - stovky
			if($cislo{0}==1)     
				return "sto".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}==2) 
				return "dvěstě".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}==3 OR $cislo{0}==4) 
				return $jednotky[$cislo{0}]."sta".$this->CisloNaSlovo(substr($cislo,1));
			else                 
				return $jednotky[$cislo{0}]."set".$this->CisloNaSlovo(substr($cislo,1));
		}
		elseif($delka==4) {                                //4 řády - tisíce
			if($cislo{0}==1) 
				return "tisíc".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}<5) 
				return $jednotky[$cislo{0}]."tisíce".$this->CisloNaSlovo(substr($cislo,1));
			else             
				return $jednotky[$cislo{0}]."tisíc".$this->CisloNaSlovo(substr($cislo,1));
		}
		elseif($delka==5) {                                //5 řádů - desítky tisíc
			$desitkyTisic = $cislo{0}.$cislo{1};
			if($desitkyTisic==10)      
				return "desettisíc".$this->CisloNaSlovo(substr($cislo,2));
			elseif($desitkyTisic<20)   
				return $mezi[$desitkyTisic]."tisíc".$this->CisloNaSlovo(substr($cislo,2));
			elseif($desitkyTisic<100)  
				return $desitky[$cislo{0}].$jednotky[$cislo{1}]."tisíc".$this->CisloNaSlovo(substr($cislo,2));
		}
		elseif($delka==6) {                                //6 řádů - stovky tisíc
			if($cislo{0}==1)  {
				if($cislo{1}.$cislo{2}==00)         
					return "stotisíc".$this->CisloNaSlovo(substr($cislo,3));
				else
	                                return "sto".$this->CisloNaSlovo(substr($cislo,1));
			} elseif($cislo{0}==2)                  
				return "dvěstě".$this->CisloNaSlovo(substr($cislo,1));
			elseif($cislo{0}==3 OR $cislo{0}==4)  
				return $jednotky[$cislo{0}]."sta".$this->CisloNaSlovo(substr($cislo,1));
			else
				return $jednotky[$cislo{0}]."set".$this->CisloNaSlovo(substr($cislo,1));
		}
		return false;
	}

	/**
	/* Formatovani ceny
	**/ 
	function price($price,$mena_suf = array()){
		
		//pr($mena_suf);
		$symbol_before = '';
		$kurz = null;
		$count = 1;
		$decimal = 0;
		$symbol_after = ',-';
		extract($mena_suf);
		
    	$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbol_before.number_format(strtr($price,$white), $decimal, '.', ' ').$symbol_after;
		else
			return $symbol_before.number_format(strtr(0.00,$white), $decimal, '.', ' ').$symbol_after;	
	}
	
	function price_array($price,$mena_suf){
    	$white = array(" " => "");
		if ($mena_suf['kurz']!=null)
			$price = ($price/strtr($mena_suf['kurz'],',','.'))*$mena_suf['pocet'];
		return $mena_suf['pref'].number_format(strtr($price,$white), $mena_suf['mista'], '.', ' ').$mena_suf['suf'];
	
	}
	
	function array2list($data,$model,$primary = 'id', $secundary = 'name',$lang = false){
		$output = array();
		foreach($data as $item){
			if ($lang != false)
				$output[$item[$model][$primary]] = $item[$model][$secundary][$lang];
			else
				$output[$item[$model][$primary]] = $item[$model][$secundary];
		}
		return $output;
	}
	
	function CzechDate($date){
    	if (!empty($date) && $date!='0000-00-00'){
		$date = strtotime($date);
    	$mesice = array ("ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince");	
    	//return date('d',$date).'. '.$mesice[date ("n",$date) - 1].' '.date('Y',$date);
		return date('d',$date).'.'.date ("m",$date).'.'.date('Y',$date);
		}
	}
	
	function CzechDateTime($date){
    	if ($date!='0000-00-00 00:00:00'){
		
		$date = strtotime($date);
    	$mesice = array ("ledna", "února", "března", "dubna", "května", "června", "července", "srpna", "září", "října", "listopadu", "prosince");	
    	return date('d',$date).'.'.date ("m",$date).'.'.date('y',$date).' '.date('H:i:s',$date);
		}
	}
	
	/* FAST LINK */
	function fastLink($array = null){
		$html = new HtmlHelper(); 
		$output = '';
		$count = count($array);
		$i = 0;
		if (isset($array)):
		foreach ($array as $caption => $link){
			$i++;
			if ($i == 1) {
				$output .= $html->link($caption,$link,array('title'=>$caption)) .' > '; 
			} elseif ($i < $count){
				$output .= $html->link($caption,$link.'/',array('title'=>$caption)) .' > '; 
			} else {
				$output .= 	'<b>'.$caption.'</b>';
			}
		}
		endif;
		return $output;
	} 

	function page_title($array = null,$basic=''){	
		if (!empty($array)){
		
		$array = array_keys($array) ;
		krsort($array);
		$array[]=$basic;
		return implode (' | ',$array);
		} else {
			return $basic;
		}
	}
		
	
	/* orezani textu */
	function orez($text,$limit)
	{
		$in 	= array("&nbsp;","&","\n","\t","    ");
		$out 	= array(" ","&amp;"," ",""," ");
		$replace = array(
			'&nbsp;'=>' ',
			'&'=>'&amp;',
			"\n"=>' ',
			"\t"=>' ',
			'  '=>' ',
			'   '=>' ',
			'?'=>'? ',
		);
		
		if (mb_strlen($text) <= $limit) {
			$output = strip_tags ($text);
		} else {
		    mb_internal_encoding("UTF-8");
			$output =  strip_tags($text);
            $output = mb_substr($output, 0, $limit);
           // $output = str_replace($in,$out,$output);
            $output = strtr($output,$replace);
            $pos = mb_strrpos($output, " ");
          	$output = trim(mb_substr($output, 0, $pos) . "...");
		}
		$tra = array(
			'"'=>' ',
			'\''=>' ',
			"\n"=>' ',
		); 
		$output = htmlspecialchars("$output", ENT_QUOTES); 
		$output = strtr($output, $tra);
		return $output;
	} 
	
	
	/* orezani textu */
	function orez2($text,$limit,$img=false)
	{
		if (strlen($text) <= $limit) {
			$output = strip_tags ($text);
		} else {
			//$text =  strip_tags($text);
		    $text = substr($text, 0, $limit+1);
			$pos = strrpos($text, " "); // v PHP 5 by se dal použít parametr offset
			if ($img=true)
				$output = substr($text, 0, ($pos ? $pos : -1)) . "...";
			else
			$output = substr($text, 0, ($pos ? $pos : -1) . "...");
		}
		return $output;
	}
	
	/* velikost souboru */
	function velikost($soubor) {
	
		$size = @filesize("./uploaded".$soubor);
		if($size < 1024) {$size = ($size); $k = " B";}
		if($size >= 1024) {$size = ($size / 1024); $k = " kB";}
		if($size >= 1024) {$size = ($size / 1024); $k = " MB";}
		return round($size, 1).$k; /* 1 = zaokrouhlování na jedno desetinné místo */
	} 
	
	
	
	
	/* ikona souboru */
	function file_icon($attach)
	{
		$pripona = strtolower(end(Explode(".", $attach)));
			$ikony = array(
				'jpeg'=>'img',
				'png'=>'img',
				'gif'=>'img',
				'xls'=>'excel',
				'ods'=>'excel',
				'odt'=>'word',
				'doc'=>'word',
				'pdf'=>'pdf',
				'html'=>'html',
				'htm'=>'html',
				'php'=>'php',
				'css'=>'html',
				'ctp'=>'html',
				'ppt'=>'ppt',
				'ptc'=>'ppt',
				'zip'=>'zip',
				'rar'=>'zip',
				'avi'=>'video',
				'mpeg'=>'video',
				'mpg'=>'video',
				'jpg'=>'img',
				
			);	
			if (array_key_exists($pripona, $ikony)){
				$icon=$this->Html->image('../css/fastest/filebrowser/'.$ikony[$pripona].'.gif',array('class'=>'icons','alt'=>strtoupper($pripona),'title'=>strtoupper($pripona)));
			} else
				$icon=$this->Html->image('../css/fastest/filebrowser/unknown.gif',array('class'=>'icons','alt'=>strtoupper($pripona),'title'=>strtoupper($pripona)));
			
			
	return $icon;
	return $pripona;
	}

function modify_url($url,$mod)
{
	$url_tmp = explode('?',$url);
	$url = $url_tmp[1];
    //$url = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    $query = explode("&", $url);
	/*
	$new_query = array();
	foreach($query AS $replace){
		$new_query[] = preg_replace('/amp;/','',$replace); 
	}
	//pr($new_query);
	$query = $new_query;
	*/
	//pr($query);
	// modify/delete data
    foreach($query as $q)
    {
		if (isset($q)){
		
		list($key, $value) = explode("=", $q);
        
		if(array_key_exists($key, $mod))
        {
			if($mod[$key])
            {
                $url = preg_replace('/'.$key.'='.$value.'/', $key.'='.$mod[$key], $url);
            }
            else
            {
                $url = preg_replace('/&?'.$key.'='.$value.'/', '', $url);
            }
        }
		}
    }
    // add new data
    foreach($mod as $key => $value)
    {
		
        if($value && !preg_match('/'.$key.'=/', $url))
        {
            
			$url .= '&'.$key.'='.$value;
        }
    }
	//pr($url);
	$url = $url_tmp[0].'?'.$url;
	//pr($url);
    return $url;
} 
function get_size($size)
     {
         if ($size < 1024)
          {
              return round($size,2).' Byte';
          }
         elseif ($size < (1024*1024))
          {
              return round(($size/1024),2).' Kb';
          }
         elseif ($size < (1024*1024*1024))
          {
              return round((($size/1024)/1024),2).' Mb';
          }
         elseif ($size < (1024*1024*1024*1024))
          {
              return round(((($size/1024)/1024)/1024),2).' Gb';
          }
          elseif ($size < (1024*1024*1024*1024*1024))
          {
              return round((((($size/1024)/1024)/1024)/1024),2).' Tb';
          }
    }

	function check_img($img,$path){
			
		if (isset($img) && !empty($img)){
			list($img,$img_name) = explode('|',$img);
			if(file_exists('.'.$path.'small/'.$img)){
				$result = array(
					'small'=>$path.'small/'.$img,
					'normal'=>$path.'normal/'.$img,
					'large'=>$path.'large/'.$img,
				);
				return $result; 
			} else {
				$result = false;
			} 
		} else {
			$result = false;
		}
		return $result;
	}
	
	function svatek(){
		 //POKUD $jmeno začíná ! tak se bude vypisovat jen obsah poměnné bez uvedení SVÁTEK MÁ
		$svatek=Array(1=>array(1=>"!nový rok", "Karina", "Radmila", "Diana", "Dalimil", "!tři králové", "Vilma", "Čestmír", "Vladan", "Břetislav", "Bohdana", "Pravoslav", "Edita", "Radovan", "Alice", "Ctirad", "Drahoslav", "Vladislav", "Doubravka", "Ilona", "Běla", "Slavomír", "Zdeněk", "Milena", "Miloš", "Zora", "Ingrid", "Otýlie", "Zdislava", "Robin", "Marika"),
           array(1=>"Hynek", "Nela", "Blažej", "Jarmila", "Dobromila", "Vanda", "Veronika", "Milada", "Apolena", "Mojmír", "Božena", "Slavěna", "Věnceslav", "Valentýn", "Jiřina", "Ljuba", "Miloslava", "Gizela", "Patrik", "Oldřich", "Lenka", "Petr", "Svatopluk", "Matěj", "Liliana", "Dorota", "Alexandr", "Lumír", "Horymír"),
           array(1=>"Bedřich", "Anežka", "Kamil", "Stela", "Kazimír", "Miroslav", "Tomáš", "Gabriela", "Františka", "Viktorie", "Anděla", "Řehoř" ,"Růžena" ,"Root / Matylda" ,"Ida" ,"Elena / Herbert" ,"Vlastimil" ,"Eduard" ,"Josef" ,"Světlana" ,"Radek" ,"Leona" ,"Ivona" ,"Gabriel" ,"Marián" ,"Emanuel" ,"Dita" ,"Soňa" ,"Taťána" ,"Arnošt" ,"Kvido"),
           array(1=>"Hugo" ,"Erika" ,"Richard" ,"Ivana" ,"Miroslava" ,"Vendula" ,"Heřman / Hermína" ,"Ema" ,"Dušan" ,"Darja" ,"Izabela" ,"Julius" ,"Aleš" ,"Vincenc" ,"Anastázie" ,"Irena" ,"Rudolf" ,"Valérie" ,"Rostislav" ,"Marcela" ,"Alexandra" ,"Evženie" ,"Vojtěch" ,"Jiří" ,"Marek" ,"Oto" ,"Jaroslav" ,"Vlastislav" ,"Robert" ,"Blahoslav"),
            array(1=>"!svátek práce" ,"Zikmund" ,"Alexej" ,"Květoslav" ,"Klaudie" ,"Radoslav" ,"Stanislav" ,"!den osvobození" ,"Ctibor" ,"Blažena" ,"Svatava" ,"Pankrác" ,"Servác" ,"Bonifác" ,"Žofie" ,"Přemysl" ,"Aneta" ,"Nataša" ,"Ivo" ,"Zbyšek" ,"Monika" ,"Emil" ,"Vladimír" ,"Jana" ,"Viola" ,"Filip" ,"Valdemar" ,"Vilém" ,"Maxmilián" ,"Ferdinand" ,"Kamila"),
           array(1=>"Laura" ,"Jarmil" ,"Tamara" ,"Dalibor" ,"Dobroslav" ,"Norbert" ,"Iveta / Slavoj" ,"Medard" ,"Stanislava" ,"Gita" ,"Bruno" ,"Antonie" ,"Antonín" ,"Roland" ,"Vít" ,"Zbyněk" ,"Adolf" ,"Milan" ,"Leoš" ,"Květa" ,"Alois" ,"Pavla" ,"Zdeňka" ,"Jan" ,"Ivan" ,"Adriana" ,"Ladislav" ,"Lubomír" ,"Petr / Pavel" ,"Šárka"),
           array(1=>"Jaroslava" ,"Patricie" ,"Radomír" ,"Prokop" ,"!Ciril & Metoděj" ,"!+ Jan Hus" ,"Bohuslava" ,"Nora" ,"Drahoslava" ,"Libuše / Amálie" ,"Olga" ,"Bořek" ,"Markéta" ,"Karolína" ,"Jindřich" ,"Luboš" ,"Martina" ,"Drahomíra" ,"Čeněk" ,"Ilja" ,"Vítězslav" ,"Magdaléna" ,"Libor" ,"Kristýna" ,"Jakub" ,"Anna" ,"Věroslav" ,"Viktor" ,"Marta" ,"Bořivoj" ,"Ignác"),
           array(1=>"Oskar" ,"Gustav" ,"Miluše" ,"Dominik" ,"Kristián" ,"Oldřiška" ,"Lada" ,"Soběslav" ,"Roman" ,"Vavřinec" ,"Zuzana" ,"Klára" ,"Alena" ,"Alan" ,"Hana" ,"Jáchym" ,"Petra" ,"Helena" ,"Ludvík" ,"Bernard" ,"Johana" ,"Bohuslav" ,"Sandra" ,"Bartoloměj" ,"Radim" ,"Luděk" ,"Otakar" ,"Augustýn" ,"Evelína" ,"Vladěna" ,"Pavlína"),
           array(1=>"Linda / Samuel" ,"Adéla" ,"Bronislav" ,"Jindřiška" ,"Boris" ,"Boleslav" ,"Regína" ,"Mariana" ,"Daniela" ,"Irma" ,"Denisa" ,"Marie" ,"Lubor" ,"Radka" ,"Jolana" ,"Ludmila" ,"Naděžda" ,"Kryštof" ,"Zita" ,"Oleg" ,"Matouš" ,"Darina" ,"Berta" ,"Jaromír" ,"Zlata" ,"Andrea" ,"Jonáš" ,"Václav" ,"Michal" ,"Jeroným"),
           array(1=>"Igor" ,"Olívie / Oliver" ,"Bohumil" ,"František" ,"Eliška" ,"Hanuš" ,"Justýna" ,"Věra" ,"Štefan / Sára" ,"Marina" ,"Andrej" ,"Marcel" ,"Renáta" ,"Agáta" ,"Tereza" ,"Havel" ,"Hedvika" ,"Lukáš" ,"Michaela" ,"Vendelín" ,"Brigita" ,"Sabina" ,"Teodor" ,"Nina" ,"Beáta" ,"Erik" ,"Šarlota / Zoe" ,"!státní svátek" ,"Silvie" ,"Tadeáš" ,"Štěpánka"),
           array(1=>"Felix" ,"!památka zesnulých" ,"Hubert" ,"Karel" ,"Miriam" ,"Liběna" ,"Saskie" ,"Bohumír" ,"Bohdan" ,"Evžen" ,"Martin" ,"Benedikt" ,"Tibor" ,"Sáva" ,"Leopold" ,"Otmar" ,"Mahulena" ,"Romana" ,"Alžběta" ,"Nikola" ,"Albert" ,"Cecílie" ,"Klement" ,"Emílie" ,"Kateřina" ,"Artur" ,"Xenie" ,"René" ,"Zina" ,"Ondřej"),
           array(1=>"Iva" ,"Blanka" ,"Svatoslav" ,"Barbora" ,"Jitka" ,"Mikuláš" ,"Benjamín" ,"Květoslava" ,"Vratislav" ,"Julie" ,"Dana" ,"Simona" ,"Lucie" ,"Lýdie" ,"Radana / Radan" ,"Albína" ,"Daniel" ,"Miloslav" ,"Ester" ,"Dagmar" ,"Natálie" ,"Šimon" ,"Vlasta" ,"Adam / Eva" ,"!1.svátek vánoení" ,"Štěpán" ,"Žaneta" ,"Bohumila" ,"Judita" ,"David" ,"Silvestr"));
		$jmeno = $svatek[((int)Date("n"))][((int)Date("j"))];
		if ($jmeno[0] != "!") 
			$vypis_svatek = ($jmeno); 
		else 
			$vypis_svatek = substr($jmeno, 1);
		return $vypis_svatek;
	
	}
	
}

	
?>