<div id="formID" data-id="<?php echo $form_id; ?>"></div>

<?php 
//pr($form_data);
if($form_id == 3 || $form_id == 6) { ?>
	<form class="form mt25" action='/contact_forms/<?php echo $form_id?>/' method="post" onsubmit='return false;' id="ContactForm<?php echo $form_id?>">
		<?php echo $htmlExt->hidden('ContactForm/spam',array('class'=>'spam'));?> 
		
		<?php
			/*
			1 => 'Textové pole',
		  	2 => 'Textová oblast',  
		  	3 => 'Výběrový seznam', 
		  	4 => 'Zatrhávací pole',
			*/
			//pr($form_data);
			if (isset($form_data['ContactFormGroup']['data'])){ 
				foreach($form_data['ContactFormGroup']['data'] AS $key=>$item){
					list($label,$type,$required,$data) = explode('|',$item);
					$find_const = strpos($label,'lang_'); 
					if ($find_const !== false)
						$label = constant($label);
					
					switch($type){
						case 1: // input
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->input('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'required'=>true,'placeholder'=>$label,'class'=>'text input '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
						case 2: // textarea
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->textarea('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
						case 3: // select
							$data_list_tmp = explode(';',$data);
							$data_list = array();
							foreach ($data_list_tmp AS $d)
								$data_list[$d] = $d;
							 
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->selectTag('ContactForm/text/'.$key.'/value',$data_list,null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
						case 4: // checkbox
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->checkbox('ContactForm/text/'.$key.'/value',null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'checkbox '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
					}
				}
			}
			?>
			
		<?php echo $htmlExt->submit('Zavolejte mi',array('id'=>'ContactForm'.$form_id.'_send','class'=>'button blue SaveForm')).'<br />';?>	
	</form>
<?php } else if($form_id == 5) { ?>
	<div class="contact_form noprint">
<form class="form" action='/contact_forms/<?php echo $form_id?>/' method="post" onsubmit='return false;' id="ContactForm<?php echo $form_id?>">
	<?php echo $htmlExt->hidden('ContactForm/spam',array('class'=>'spam'));?>

	<?php //pr($form_data['ContactFormGroup']['data']); ?>
	<?php
	/*
	1 => 'Textové pole',
  	2 => 'Textová oblast',
  	3 => 'Výběrový seznam', 
  	4 => 'Zatrhávací pole',
	*/
	
	if (isset($form_data['ContactFormGroup']['data'])){ 
		foreach($form_data['ContactFormGroup']['data'] AS $key=>$item){
			//pr($item);
			list($label,$type,$required,$data) = explode('|',$item);
			$find_const = strpos($label,'lang_');
			if ($find_const !== false)
				$label = constant($label);
			
			switch($type){
				case 1: // input
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->input('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text '.(($key == 0 || $key == 2)?'long':'').' input '.(($required == 1)?'validate[\'required\']':'')));
					
					if($key == 0) {
						echo $htmlExt->input('ContactForm/name',array('tabindex'=>999,'required'=>true,'class'=>'text input validate[\'required\',\'email\']','tabindex'=>1000,'placeholder'=>'Váš e-mail'));
					}
				break;
				case 2: // textarea
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->textarea('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text input long '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
				case 3: // select
					$data_list_tmp = explode(';',$data);
					$data_list = array();
					foreach ($data_list_tmp AS $d)
						$data_list[$d] = $d;
					
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->selectTag('ContactForm/text/'.$key.'/value',$data_list,null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
				case 4: // checkbox
					//echo '<div class="none">';
						if($key == 4) {
							echo '<div class="sll mb25 labels agree">';
							echo '<h2 class="mb25">Region</h2>';
						}
						
						if($key == 9) {
							echo '</div>';
							echo '<div class="slr mb25 labels agree">';
							echo '<h2 class="mb25">Služby</h2>';
						}
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
						echo $htmlExt->checkbox('ContactForm/text/'.$key.'/value',null,array('tabindex'=>1001+$key,'label_pos'=>true,'label'=>$label,'label2'=>$label,'class'=>'checkbox '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						
						if($key == 13) {
							echo '</div>';
						}
						
					//echo '</div>';
				break;
			}
		} 
	}
	?>
	
	<?php echo $htmlExt->submit('Nezávazně poptat',array('id'=>'ContactForm'.$form_id.'_send','class'=>'button blue w100p SaveForm')).'<br />';?>	
</form>
</div>
<?php } else if($form_id == 7) { ?>
	<form class="form mt25" action='/contact_forms/<?php echo $form_id?>/' method="post" onsubmit='return false;' id="ContactForm<?php echo $form_id?>">
		<?php echo $htmlExt->hidden('ContactForm/spam',array('class'=>'spam'));?> 
		
		<?php
			/*
			1 => 'Textové pole',
		  	2 => 'Textová oblast',  
		  	3 => 'Výběrový seznam', 
		  	4 => 'Zatrhávací pole',
			*/
			//pr($form_data);
			if (isset($form_data['ContactFormGroup']['data'])){ 
				foreach($form_data['ContactFormGroup']['data'] AS $key=>$item){
					list($label,$type,$required,$data) = explode('|',$item);
					$find_const = strpos($label,'lang_');
					if ($find_const !== false)
						$label = constant($label);
					
					switch($type){
						case 1: // input
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->input('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'required'=>true,'placeholder'=>$label,'class'=>'text input '.(($required == 1)?'validate[\'required\']':'')));
						break;
						case 2: // textarea
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->textarea('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
						case 3: // select
							$data_list_tmp = explode(';',$data);
							$data_list = array();
							foreach ($data_list_tmp AS $d)
								$data_list[$d] = $d;
							
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->selectTag('ContactForm/text/'.$key.'/value',$data_list,null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
						case 4: // checkbox
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->checkbox('ContactForm/text/'.$key.'/value',null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'checkbox '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
					}
				}
			}
			?>
			
		<?php echo $htmlExt->submit('Zavolejte mi',array('id'=>'ContactForm'.$form_id.'_send','class'=>'button blue SaveForm')).'<br />';?>	
	</form>
<?php } else if($form_id == 4) { ?>

<div class="contact_form noprint">
<form class="form" action='/contact_forms/<?php echo $form_id?>/' method="post" onsubmit='return false;' id="ContactForm<?php echo $form_id?>">
	<div id="section-1">
	<?php echo $htmlExt->hidden('ContactForm/spam',array('class'=>'spam'));?>

	<?php //pr($form_data['ContactFormGroup']['data']); ?>
	<?php
	/*
	1 => 'Textové pole',
  	2 => 'Textová oblast',
  	3 => 'Výběrový seznam', 
  	4 => 'Zatrhávací pole',
	*/
	
	if (isset($form_data['ContactFormGroup']['data'])){ 
		foreach($form_data['ContactFormGroup']['data'] AS $key=>$item){
			//pr($item);
			list($label,$type,$required,$data) = explode('|',$item);
			$find_const = strpos($label,'lang_');
			if ($find_const !== false)
				$label = constant($label);
			
			if($key >3) { break; }
			
			$adresa = array(
				2 => $_SESSION['mesto'],
				3 => $_SESSION['ulice'],
				4 => $_SESSION['cp']	
			); 
			
			switch($type){
				case 1: // input
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->input('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'value'=>(isset($adresa[$key])?$adresa[$key]:''),'placeholder'=>$label,'class'=>'text '.(($key == 0)?'long':'').' '.(isset($adresa[$key])?'none':'').' input '.(($required == 1)?'validate[\'required\']':'')));
					
					if($key == 0) {
						echo $htmlExt->input('ContactForm/name',array('tabindex'=>999,'required'=>true,'class'=>'text input validate[\'required\',\'email\']','tabindex'=>1003,'placeholder'=>'Váš e-mail'));
					}
				break;
				case 2: // textarea
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->textarea('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
				case 3: // select
					$data_list_tmp = explode(';',$data);
					$data_list = array();
					foreach ($data_list_tmp AS $d)
						$data_list[$d] = $d;
					
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
					echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
					echo $htmlExt->selectTag('ContactForm/text/'.$key.'/value',$data_list,null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
				break;
				case 4: // checkbox
					echo '<div class="none">';
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
						echo $htmlExt->checkbox('ContactForm/text/'.$key.'/value',null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'checkbox '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
					echo '</div>';
				break;
			}
		} 
	}
	?>
	
	<hr class="mt50" />
	<p class="txt-center mt25 mb25"><strong class="bigger black uppercase bold">Vaše adresa</strong></p>
	
	<p>
		<label>Město:</label> <?php echo $_SESSION['mesto']; ?><br />
		<label>Ulice:</label> <?php echo $_SESSION['ulice']; ?><br />
		<label>Číslo popisné:</label> <?php echo $_SESSION['cp']; ?>
	</p>
	<hr class="mt25 mb25" />
	
	<div id="tarify" class="tarifs box-1">
		
	</div>
	 
	<br class="clear" />
	
	<p class="txt-center mt50 mb25"><strong class="bigger black uppercase bold">Potřebujete pro instalaci servisního technika?</strong></p>
	
	<div id="technik" class="button">Ano, potřebuji</div>
	
	<br class="clear" />
	
	<div class="agree mt25 mb25">
		<?php echo $htmlExt->checkbox('ContactForm/agree',null,array('class'=>'checkbox')); ?>
		<label for="ContactFormAgree">Zvolením „Pokračovat“ beru na vědomí, že společnost PODA, a.s., se sídlem 28. října 1168/102, 702 00 Ostrava - Moravská, IČ 25816179, je oprávněna zpracovávat veškeré údaje, které jsem jí dobrovolně zpřístupnil při vyplnění tohoto formuláře, a to za účelem a po dobu nezbytnou pro jednání o uzavření smlouvy o poskytování veřejně dostupných služeb elektronických komunikací v souladu se zákonem č. 101/2000 Sb. o ochraně osobních údajů a Zásadami ochrany soukromí spotřebitelů.</label>
	</div>
	
	<br class="clear" />
		
		<a href="#" id="continue" class="button blue w100p">Pokračovat</a>
	</div>
	
	<div id="section-2" class="none">
		
		<p class="txt-center">Žádáme Vás o vyplnění doplňujících informací. Vyplnění formuláře <strong>je dobrovolné.</strong></p> <br />
		
	<?php foreach($form_data['ContactFormGroup']['data'] AS $key=>$item){
			//pr($item);
			list($label,$type,$required,$data) = explode('|',$item);
			$find_const = strpos($label,'lang_');
			if ($find_const !== false)
				$label = constant($label);
			
			if($key > 3) {
				$adresa = array(
					2 => $_SESSION['mesto'],
					3 => $_SESSION['ulice'],
					4 => $_SESSION['cp']	
				); 
				
				switch($type){
					case 1: // input
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
						echo $htmlExt->input('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'value'=>(isset($adresa[$key])?$adresa[$key]:''),'placeholder'=>$label,'class'=>'text '.(($key == 7)?'right':'').' '.(isset($adresa[$key])?'none':'').' input '.(($required == 1)?'validate[\'required\']':'')));
					
					break;
					case 2: // textarea
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
						echo $htmlExt->textarea('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
					break;
					case 3: // select
						$data_list_tmp = explode(';',$data);
						$data_list = array();
						foreach ($data_list_tmp AS $d)
							$data_list[$d] = $d;
						
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
						echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
						
						$data_list[0] = $label;
						ksort($data_list);
						
						echo $htmlExt->selectTag('ContactForm/text/'.$key.'/value',$data_list,null,array('tabindex'=>1001+$key,'class'=>'text right '.(($required == 1)?'validate[\'required\']':'')),null,false).'<br class="clear" />';
					break;
					case 4: // checkbox
						echo '<div class="none">';
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->checkbox('ContactForm/text/'.$key.'/value',null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'checkbox '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						echo '</div>';
					break;
				}
			}
		} ?>
		
		<p><strong>Nemáte nyní všechny potřebné údaje k dispozici?</strong> Pro zpracování objednávky jsou nutné. Proto si je, prosíme, připravte. Po odeslání objednávky vyčkejte na telefonický kontakt našeho pracovníka, kterému můžete tyto údaje sdělit.</p>
		
		<?php echo $htmlExt->submit('Nezávazně objednat',array('id'=>'ContactForm'.$form_id.'_send','class'=>'button blue w100p SaveForm')).'<br />';?>	
	</div>
	
	<br class="clear" />
	
	
</form>
</div>
<?php } else { ?>
<form class="form mt25" action='/contact_forms/<?php echo $form_id?>/' method="post" onsubmit='return false;' id="ContactForm<?php echo $form_id?>">
		<?php echo $htmlExt->hidden('ContactForm/spam',array('class'=>'spam'));?> 
		
		<?php
			/*
			1 => 'Textové pole',
		  	2 => 'Textová oblast',  
		  	3 => 'Výběrový seznam', 
		  	4 => 'Zatrhávací pole',
			*/
			//pr($form_data);
			if (isset($form_data['ContactFormGroup']['data'])){ 
				foreach($form_data['ContactFormGroup']['data'] AS $key=>$item){
					list($label,$type,$required,$data) = explode('|',$item);
					$find_const = strpos($label,'lang_'); 
					if ($find_const !== false)
						$label = constant($label);
					
					switch($type){
						case 1: // input
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->input('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'required'=>true,'placeholder'=>$label,'class'=>'text input '.(($required == 1)?'validate[\'required\']':'')));
						break;
						case 2: // textarea
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->textarea('ContactForm/text/'.$key.'/value',array('tabindex'=>1001+$key,'placeholder'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
						case 3: // select
							$data_list_tmp = explode(';',$data);
							$data_list = array();
							foreach ($data_list_tmp AS $d)
								$data_list[$d] = $d;
							 
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->selectTag('ContactForm/text/'.$key.'/value',$data_list,null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'text '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
						case 4: // checkbox
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/type',array('value'=>$type));
							echo $htmlExt->hidden('ContactForm/text/'.$key.'/name',array('value'=>$label));
							echo $htmlExt->checkbox('ContactForm/text/'.$key.'/value',null,array('tabindex'=>1001+$key,'label'=>$label,'class'=>'checkbox '.(($required == 1)?'validate[\'required\']':''))).'<br class="clear" />';
						break;
					}
				}
			}
			?>
			
		<?php echo $htmlExt->submit('Odeslat dotaz',array('id'=>'ContactForm'.$form_id.'_send','class'=>'button blue SaveForm')).'<br />';?>	
	</form>
<?php } ?>

<script type="text/javascript" language="javascript">
//<![CDATA[
	//var contact_form_id = 'ContactForm<?php echo $form_id?>';
	//$('ContactFormSpam').value = 123;
	//contact_form(contact_form_id);
//]]>
</script>