<rss version="0.91">
  <channel>
	<title><?php echo $setting['name'].' - '.$rss[0]['ActualGroup']['name']; ?></title>
    <link>http://<?php echo $_SERVER['SERVER_NAME']?></link>
    <description><?php echo $setting['name'] ?></description>
    <language>cs</language>
    <pubDate><?php echo date("D, j M Y H:i:s", gmmktime()) . ' GMT';?></pubDate>
    
    <docs>http://<?php echo $_SERVER['SERVER_NAME'] ?></docs>
    <managingEditor>info@fastest.cz</managingEditor>
    <webMaster>info@fastest.cz</webMaster>
    <?php  foreach ($rss as $item): ?>
    <item>
      <title><?php echo ucfirst($item['Actual']['name']); ?></title>
      <link>http://<?php echo $_SERVER['SERVER_NAME'].'/'.lang_alias_aktualita.'/'.$item['Actual']['alias_'].'/'.$item['Actual']['id'].'/'?></link>
      <description><![CDATA[ 
	  <?php if(!empty($item['Actual']['img'])) echo $html->image('http://'.$_SERVER['SERVER_NAME'].'/'.$item['Actual']['img'],array('width'=>100,'title'=>$item['Actual']['name'],'alt'=>$item['Actual']['name']));?>
	  <?php echo Str_Replace ("&nbsp;", "&#160;",$fastest->orez($item['Actual']['text'],800)); ?>]]></description>
       <pubDate><?php echo $item['Actual']['updated']; ?></pubDate>
      <guid>http://<?php echo $_SERVER['SERVER_NAME'].'/'.lang_alias_aktualita.'/'.$item['Actual']['alias_'].'/'.$item['Actual']['id']?></guid>
    </item>
	<?php endforeach; ?>
  </channel>
</rss>