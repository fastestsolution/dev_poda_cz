<div class="mainImg cover_img" data-src="/css/layout/banner/novinky-bg.jpg">
	<div class="middleText">
		<h1 class="title">Novinky<small> Víme, co potřebujete a v tom jsme jedineční <br />#propojujemegenerace #poda</small></h1>
	</div>
</div>

<ul id="roky" class="sluzba-buttons years">
	<li><a href="/novinky/#roky" class="button <?php echo (empty($this->params['pass'][0]) || $this->params['pass'][0] == date('Y')?'active':''); ?>"><?php echo date('Y'); ?></a></li>
	<li><a href="/novinky/<?php echo date('Y') - 1; ?>/#roky" class="button <?php echo (isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 1) ?'active':''); ?>"><?php echo date('Y') - 1; ?></a></li>
	<li><a href="/novinky/<?php echo date('Y') - 2; ?>/#roky" class="button <?php echo (isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 2) ?'active':''); ?>"><?php echo date('Y') - 2; ?></a></li>
	<li><a href="/novinky/<?php echo date('Y') - 3; ?>/#roky" class="button <?php echo (isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 3) ?'active':''); ?>"><?php echo date('Y') - 3; ?></a></li>
	<li><a href="/novinky/<?php echo date('Y') - 4; ?>/#roky" class="button <?php echo (isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 4) ?'active':''); ?>"><?php echo date('Y') - 4; ?></a></li>
	<li><a href="/novinky/1997/#roky" class="button <?php echo (isset($this->params['pass'][0]) && $this->params['pass'][0] == 1997 ?'active':''); ?>">Starší...</a></li>
</ul>

<div id='items' class="wrapper">
	<?php echo $this->renderElement('../actuals/index_items');?>
</div>

<br class="clear" />

<?php 
	if(empty($this->params['pass'][0])) {
		$olderNewsLink = '/novinky/'.(date('Y') - 1).'#roky';	
		$rok = (date('Y'));
	} else if(isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 1)) {
		$olderNewsLink = '/novinky/'.(date('Y') - 2).'#roky';	
		$rok = (date('Y') - 1);
	} else if(isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 2)) {
		$olderNewsLink = '/novinky/'.(date('Y') - 3).'#roky';	
		$rok = (date('Y') - 2);
	} else if(isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 3)) {
		$olderNewsLink = '/novinky/'.(date('Y') - 4).'#roky';	
		$rok = (date('Y') - 3);
	} else {
		$olderNewsLink = '/novinky/1997#roky';	
		$rok = '';
	}
	
	if(isset($this->params['pass'][0]) && $this->params['pass'][0] == (date('Y') - 4)) { 
		$rok = (date('Y') - 4);
	}
?>

<a href="<?php //echo $olderNewsLink; ?>" id="showMoreActuals" class="button blue wider mt50">Přejít na starší novinky <?php echo $rok; ?></a> 