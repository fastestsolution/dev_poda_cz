<?php $pagination->setPaging($paging); 
$pocet=0; 
echo '<div class="article_text">';
if (isset($actuals_list_all) AND count($actuals_list_all)>0){

	foreach ($actuals_list_all AS $actual){
  	if($actual['Actual']['name']!=''){
	  	$pocet++;
	  	
	  	echo '<article class="'.($pocet > 3?'none':'').'">';
  		$actual_url = '/novinka/'.$actual['Actual']['alias_'].'/'.$actual['Actual']['id'].'/';
  		
  		$created = new DateTime($actual['Actual']['created']);
  		$year = $created->format("Y");
  		$day = $created->format("j");
  		$month = $created->format("n");
  		
  		echo '<div class="created"><strong>'.$day.'.'.$month.'</strong><small>'.$year.'</small></div>';
		
		echo '<div class="actual-text">';
			echo '<h2><a href="'.$actual_url.'" class="showMoreActual">'.$actual['Actual']['name'].'</a></h2>';
			echo '<p>'.$actual['Actual']['perex'].'</p>';
			echo '<a href="#" class="button circle showMoreActual">+</a>';
			echo '<div class="hidden-section none">'.$actual['Actual']['text'].'</div>';

		echo '</div>';
		echo '</article>';
  	}
	}
	if($pocet==0) echo lang_nenalezeny_zadne_aktuality;
}
else echo '<p class="txt-center"><strong>'.lang_nenalezeny_zadne_aktuality.'</strong></p>';
echo '</div>';
?>

<?php echo $this->renderElement('pagination'); ?>


<script>
	news();
</script>
