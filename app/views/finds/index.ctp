<?php //pr($results); ?>
<div class="domtabs admin_dom_find_links">
	<ul class="zalozky_search">
		<?php foreach($results as $res):?>
		<li class="ousko"><a href="#krok1"><?php echo $res['caption'];?> [<?php echo count($res['find'])?>]</a></li>
		<?php endforeach;?>
	</ul>
</div>
<div class="domtabs_search_over admin_dom_find">
	<?php foreach($results as $model=>$res):?>
		<div class="domtabs_search field">
		<?php if(isset($res['find']) && count($res['find'])>0):?>
			<?php foreach($res['find'] as $find_item):?>
				<?php 
					$replace = array();
					foreach($find_item[$model] as $col=>$val)
						$replace["{".$model.".".$col."}"] = $val;
				?>
				<h3><a href= '<?php echo strtr($res['url'],$replace);?>'><?php echo $find_item[$model]['name']?></a></h3>
				<?php 
					switch($model){
						case 'ShopProduct':
							if (isset($find_item[$model]['imgs'][0])):
								@list($img_src, $title) = explode('|',$find_item[$model]['imgs'][0]);
								$img_path = '/uploaded/shop/products/small/'.$img_src;
							else:
								$img_path = path_to_no_image;
							endif;
							echo '<p>'.$html->link(
									$html->image(
										$img_path,
										array(
										'alt'=>(isset($img_title) && !empty($img_title))?$img_title:strip_tags($find_item[$model]['text']),
										'title'=>(isset($img_title) && !empty($img_title))?$img_title:$find_item[$model]['name'],
										'align'=>'left',
										'style'=>'max-height:50px;_hegiht:50px; margin-right:5px'
									)
								),
								strtr($res['url'],$replace),
								array(
								//	'target'=>isset($img)?'_blank':'self', 
									'title'=>$find_item[$model]['name'],
								//	'rel'=>'clearbox[shop_detail]',
								//	'onclick'=>'return false;',
								),
								false,
								false
							).''.$fastest->orez($find_item[$model]['text'],300).'</p><br />';
							break;
						default:
							echo '<p>'.$fastest->orez($find_item[$model]['text'],300).'</p>';			
							break;
					}
				?>
				
			<?php endforeach;?>
		<?php else:?>
			<em>Nic nebylo nalezeno</em>
		<?php endif;?>
		</div>
	<?php endforeach;?>	
</div>
<script>
	var domtab = new DomTabs({'className':'admin_dom_find'});
</script>