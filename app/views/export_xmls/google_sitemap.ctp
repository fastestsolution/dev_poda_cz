<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
<?php if (isset($menu) && is_array($menu) && count($menu)>0):?>
	<?php foreach($menu as $men):?>
		<?php 
		$url = (($men['url']!='/')?$men['url']:'').'/'; 
		$pos = (strpos($url,'http://'));
		if ($pos === false) 
			$url =  'http://'.$_SERVER['SERVER_NAME'].strtr($url,array('//'=>'/'));
		else $url = strtr($url,array('//'=>'/'));
		?>
		<url>
			<loc><?php echo $url; ?></loc>
			<lastmod><?php echo Date("Y-m-d",strtotime($men['updated'])); ?></lastmod>
			<changefreq>weekly</changefreq>
			<priority><?php echo (($men['url']!='/')?'0.6':'1')?></priority>
		</url>
	<?php endforeach;?>
<?php endif;?>
</urlset>