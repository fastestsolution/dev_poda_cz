<div id="fotogalerie">
<?php if(isset($foto_list)){ ?>
	<?php 
	//pr($foto_list['Fotogalerie']['imgs']);
	
	foreach($foto_list['Fotogalerie']['imgs'] AS $foto){
		list($foto_link, $foto_caption) = explode('|',$foto);
		$img_url_large = '/uploaded/fotogalerie/large/'.$foto_link;
		$img_url_small = '/image_resize.php?file=./uploaded/fotogalerie/small/'.$foto_link.'&amp;w=95&amp;h=95';
		$img_url_thumb = '/uploaded/fotogalerie/small/'.$foto_link;
			if (isset($foto_caption) && !empty($foto_caption)){
				$pos = strpos($foto_caption,'.');
				if ($pos === false)
					$foto_caption = $foto_caption;
				else	
					$foto_caption = $foto_list['Fotogalerie']['name'];
			} else
				$foto_caption = $foto_list['Fotogalerie']['name'];
			
		echo '<div class="foto">';
			//echo $html->link( $html->image($img_url_small, array('alt'=>$foto_list['Fotogalerie']['name'])) ,$img_url_large,array('rel'=>'clearbox[gallery=foto,,tnhrf='.$img_url_thumb.']','title'=>$foto_list['Fotogalerie']['name']),null,false);
			echo $html->link( $html->image($img_url_small, array('alt'=>$foto_list['Fotogalerie']['name'])) ,$img_url_large,array('rel'=>'lightbox-atomium','title'=>$foto_caption),null,false);
		echo '</div>';
	}
	
	?>
<?php } ?>
</div>
<?php //pr($foto_list); ?>