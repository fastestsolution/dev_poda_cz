<div id="fotogalerie">
<?php 
if (isset($group_list)){
	foreach($group_list AS $key=>$group){
		if (isset($foto_list[$key]) && count($foto_list[$key])>0 && count($group_list)>1)
			echo '<h2 class="clear">'.$group.'</h2>';
		
	
		if(isset($foto_list[$key]) && count($foto_list[$key])>0){
			
			foreach($foto_list[$key] AS $foto){
				if (isset($foto['Fotogalerie']['imgs'][0])){
					list($foto_link1, $foto_caption) = explode('|',$foto['Fotogalerie']['imgs'][0]);
					$img_url_small1 = '/image_resize.php?file=./uploaded/fotogalerie/small/'.$foto_link1.'&amp;w=95&amp;h=95';
				}
				if (isset($foto['Fotogalerie']['imgs'][1])){
					list($foto_link2, $foto_caption) = explode('|',$foto['Fotogalerie']['imgs'][1]);
					$img_url_small2 = '/image_resize.php?file=./uploaded/fotogalerie/small/'.$foto_link2.'&amp;w=95&amp;h=95';
				}
				if (isset($foto['Fotogalerie']['imgs'][2])){
					list($foto_link3, $foto_caption) = explode('|',$foto['Fotogalerie']['imgs'][2]);
					$img_url_small3 = '/image_resize.php?file=./uploaded/fotogalerie/small/'.$foto_link3.'&amp;w=95&amp;h=95';
				}
					
				echo '<div class="foto">';
					echo $html->link( $html->image($img_url_small1, array('alt'=>$foto['Fotogalerie']['name'])).'<span class="tooltip">'.$foto['Fotogalerie']['name'].'</span>','/'.fotogalerie_link.'/'.$foto['Fotogalerie']['alias_'].'/'.$foto['Fotogalerie']['id'].'/',array(),null,false);
					if (isset($foto['Fotogalerie']['imgs'][1]))
					echo $html->link( $html->image($img_url_small2, array('alt'=>$foto['Fotogalerie']['name'])).'<span class="tooltip">'.$foto['Fotogalerie']['name'].'</span>','/'.fotogalerie_link.'/'.$foto['Fotogalerie']['alias_'].'/'.$foto['Fotogalerie']['id'].'/',array(),null,false);
					if (isset($foto['Fotogalerie']['imgs'][2]))
					echo $html->link( $html->image($img_url_small3, array('alt'=>$foto['Fotogalerie']['name'])).'<span class="tooltip">'.$foto['Fotogalerie']['name'].'</span>','/'.fotogalerie_link.'/'.$foto['Fotogalerie']['alias_'].'/'.$foto['Fotogalerie']['id'].'/',array(),null,false);
				echo '</div>'; 
			}
		}
		echo '<br class="clear" />';

	}
}
?>
</div>
<script type="text/javascript" language="javascript">
//<![CDATA[
   
window.addEvent("domready", function() {
	$$(".foto").each(function(album) {
		new PhotoStack(album);
	});
});
//]]>
</script>
<?php //pr($foto_list); ?>