<?php
	@define('actuals_link','novinky');
	@define('actuals_link_detail','novinka');
	@define('fotogalerie_link','fotogalerie');
	@define('guestbook_link','seznamka');
	
	Router::connect('/clear_cache/', array('controller' => 'pages','action'=>'clear_cache'));
	
	Router::connect('/'.guestbook_link.'/', array('controller' => 'guestbooks','action'=>'index',1));
	Router::connect('/pages/isUnique/*', array('controller' => 'pages','action'=>'isUnique'));
	Router::connect('/search/*', array('controller' => 'finds','action'=>'index'));
	Router::connect('/searchService/*', array('controller' => 'pages','action'=>'searchService'));
	Router::connect('/searchAddressSave/*', array('controller' => 'pages','action'=>'saveAddressSession'));
	Router::connect('/ispmail/*', array('controller' => 'pages','action'=>'isp_mail'));
	Router::connect('/test/*', array('controller' => 'pages','action'=>'test'));
	
	Router::connect('/kalendar/*', array('controller' => 'calendars','action'=>'index'));
	Router::connect('/kalendar-load/*', array('controller' => 'calendars','action'=>'load'));
	
	Router::connect('/fst_upload/*', array('controller' => 'pages','action'=>'fst_upload'));
	
	# START clanky
	Router::connect('/clanek/:alias/:id',	array('controller' => 'articles',	'action' =>	'detail'),array('pass' => array('alias','id'),'id' => '[0-9]+'));
	Router::connect('/clanek-fotogalerie/:id',	array('controller' => 'articles',	'action' =>	'article_fotogalerie'),array('pass' => array('id'),'id' => '[0-9]+'));
	
	Router::connect('/hledej/*',	array('controller' => 'articles',	'action' =>	'view'));
	
	Router::connect('/actuals_box/:id',	array('controller' => 'actuals', 'action'=>'actuals_box'),array('pass' => array('id'),'id' => '[0-9]+'));
	Router::connect('/'.actuals_link.'/:year/*',	array('controller' => 'actuals', 'action'=>'index'),array('pass' => array('year')));
	Router::connect('/'.actuals_link_detail.'/:alias/:id',	array('controller' => 'actuals', 'action'=>'detail'),array('pass' => array('alias','id'),'id' => '[0-9]+'));

	Router::connect('/'.fotogalerie_link.'/',	array('controller' => 'fotogaleries', 'action'=>'index'));
	Router::connect('/'.fotogalerie_link.'/*',	array('controller' => 'fotogaleries', 'action'=>'detail'));
	Router::connect('/google_maps/*',	array('controller' => 'google_maps', 'action'=>'index'));
	Router::connect('/polls/*',	array('controller' => 'polls', 'action'=>'index'));
	//Router::connect('/polls-vote/*',	array('controller' => 'polls', 'action'=>'index'));
	
	//specialni linky
	Router::connect('/prohlaseni-o-pristupnosti/',	array('controller' => 'articles',	'action' =>	'detail','special_prohlaseni',1));//cz
	Router::connect('/google_sitemap/',	array('controller' => 'export_xmls',	'action' =>	'google_sitemap'));//cz
	
	Router::connect('/mapa-stranek/',	array('controller' => 'articles',	'action' =>	'sitemap'));//cz
	Router::connect('/rss-aktuality/*',	array('controller' => 'actuals',	'action' =>	'rss'));//cz
	
	# contact forms
	Router::connect('/contact_forms/*', array('controller' => 'contact_forms','action'=>'index'));
   
	Router::connect('/smallbox/:id', array('controller' => 'pages','action'=>'smallbox'), array('pass' => array('id'),'id' => '[0-9]+'));
	Router::connect('/loadContactForm/*', array('controller' => 'pages', 'action' => 'loadContactForm'));
	
	
	Router::connect('/', 					array('controller' => 'pages', 	'action' => 'homepage'));
	Router::connect('/tv-sluzby/podanet-tv/', 		array('controller' => 'pages', 	'action' => 'podanet_tv'));
	Router::connect('/tv-sluzby/seznam-programu/', 		array('controller' => 'pages', 	'action' => 'seznam_programu'));
	Router::connect('/tv-sluzby/chytre-funkce-tv/', 		array('controller' => 'pages', 	'action' => 'chytra_tv'));
	Router::connect('/tv-sluzby/', 		array('controller' => 'pages', 	'action' => 'sluzby'));
	
	Router::connect('/ekonomicke-udaje/', 			array('controller' => 'pages', 	'action' => 'ekonomicke_ukazatele'));
	Router::connect('/podpora/', 			array('controller' => 'pages', 	'action' => 'podpora'));
	Router::connect('/cenik/', 			array('controller' => 'pages', 	'action' => 'cenik'));
	Router::connect('/internet/*', 			array('controller' => 'pages', 	'action' => 'internet'));
	Router::connect('/dokumenty-ke-stazeni/', 		array('controller' => 'pages', 	'action' => 'dokumenty_ke_stazeni'));
	Router::connect('/dokumenty-ke-stazeni/mereni-rychlosti-internetu/', 		array('controller' => 'pages', 	'action' => 'mereni_rychlosti_internetu'));
    Router::connect('/kontakty/obchodnici/*', 			array('controller' => 'pages', 	'action' => 'obchodnici'));
    Router::connect('/kontakty/klientska-centra/', 			array('controller' => 'pages', 	'action' => 'klientska_centra'));
    Router::connect('/volani/', 			array('controller' => 'pages', 	'action' => 'nabidka_tarifu'));
    Router::connect('/volani/informace-o-datech/', 	array('controller' => 'pages', 	'action' => 'informace_o_datech'));
    Router::connect('/volani/chci-tarif/', 	array('controller' => 'pages', 	'action' => 'chci_tarif'));
	Router::connect('/o-nas/*', 			array('controller' => 'pages', 	'action' => 'onas'));
	Router::connect('/kontakty/', 			array('controller' => 'pages', 	'action' => 'kontakty'));
	Router::connect('/proc-prave-poda/', 	array('controller' => 'pages', 	'action' => 'proc_poda'));
	Router::connect('/chci-podu/', 			array('controller' => 'pages', 	'action' => 'chci_podu'));
	Router::connect('/objednat/*', 			array('controller' => 'pages', 	'action' => 'objednat'));
	Router::connect('/jak-prejit-k-pode/', 		array('controller' => 'pages', 	'action' => 'jak_prejit'));
	Router::connect('/kontakty/obchodnici/*', 			array('controller' => 'pages', 	'action' => 'obchodnici'));
	Router::connect('/kontakty/firemni-sluzby/*', 			array('controller' => 'pages', 	'action' => 'firemni_sluzby'));
	Router::connect('/jsme-firma/*', 			array('controller' => 'pages', 	'action' => 'firemni_sluzby'));
	
	Router::connect('/mapa/', 					array('controller' => 'pages', 	'action' => 'mapa'));
	Router::connect('/import_address/', 					array('controller' => 'pages', 	'action' => 'importAddress'));
	Router::connect('/importCities/', 					array('controller' => 'pages', 	'action' => 'importCities'));
    Router::connect('/importStreets/', 					array('controller' => 'pages', 	'action' => 'importStreets'));


	Router::connect('/searchAddress/*', 					array('controller' => 'pages', 	'action' => 'searchAddress3'));
	Router::connect('/examples/*',			array('controller' => 'pages', 	'action' => 'examples'));
	Router::connect('/*', 					array('controller' => 'articles', 	'action' => 'view'));
?>