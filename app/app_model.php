<?php
class AppModel extends Model {
	function __construct(){ 
         parent::__construct(); 
         static $changedSet=false; 
         if(!$changedSet){
              $changedSet=true;
              $this->query("SET NAMES 'utf8'");
         }
    }
	
	function createAlias($string){
		$trans = array(","=>"","á"=>"a", "ä"=> "a", "č"=>"c", "ď"=>"d", "é"=>"e", "ě"=>"e", "ë"=>"e", "í"=>"i", "&#239;"=>"i", "ň"=>"n", "ó"=>"o", "ö"=>"o", "ř"=>"r", "š"=>"s", "ś"=>"s", "ť"=>"t", "ú"=>"u", "ů"=>"u", "ü"=>"u", "ý"=>"y", "&#255;"=>"y", "ž"=>"z", "Á"=>"A", "Ä"=>"A", "Č"=>"C", "Ď"=>"D", "É"=>"E", "Ě"=>"E", "Ë"=>"E", "Í"=>"I", "&#207;"=>"I", "Ň"=>"N", "Ó"=>"O", "Ö"=>"O", "Ř"=>"R", "Š"=>"S","Ť"=>"T", "Ú"=>"U", "Ů"=>"U", "Ü"=>"U", "Ý"=>"Y", "&#376;"=>"Y", "Ž"=>"Z"," "=>"-","."=>"-","/"=>"-","+"=>'-','"'=>'','?'=>'',';'=>'');
		return strtolower(strtr($string, $trans));
	}
	
	function generateLangList($lang = 'cz',$conditions = null,  $order = null, $limit = null){
 		$result = $this->find('all',array('conditions'=>$conditions, 'order'=> $order, 'limit'=>$limit));
 		$finalVals = array();
 		foreach ($result as $item){
 			$item[$this->name][$this->displayField][$lang];
 			$finalVals[$item[$this->name][$this->primaryKey]] = $item[$this->name][$this->displayField][$lang];
 		}
 		return $finalVals;
 	}
	
	function getpath($id = null, $fields = null, $recursive = null) {
		if (is_array($id)) {
			extract (array_merge(array('id' => null), $id));
		}
		$overrideRecursive = $recursive;
		if (empty ($id)) {
			$id = $this->id;
		}
		//extract($this->settings[$this->alias]);
		if (!is_null($overrideRecursive)) {
			$recursive = $overrideRecursive;
		}
		$result = $this->find('first', array('conditions' => array($this->escapeField() => $id), 'fields' => array('lft', 'rght'), 'recursive' => $recursive));
		//pr($result);
		if ($result) {
			$result = array_values($result);
		} else {
			return null;
		}
		$item = $result[0];
		//$fields = array();
		$results = $this->find('all', array(
			//'conditions' => array('level >'=>0, $this->escapeField('lft') . ' <=' => $item['lft'], $this->escapeField('rght') . ' >=' => $item['rght']),
			'conditions' => array( $this->escapeField('lft') . ' <=' => $item['lft'], $this->escapeField('rght') . ' >=' => $item['rght']),
			'fields' => $fields, 'order' => array($this->escapeField('lft') => 'asc'), 'recursive' => $recursive
		));
		//pr($results);
		return $results;
	}
}
?>