<?php 
/**
 * 
 * @author Zbyněk Strnad - Fastest Solution s.r.o.
 * @created 20.9.2009
 */
class TransBehavior extends ModelBehavior {
	
	var $_defaults = array(
		'cols'	=>	array('name'),
		'scope' => '1=1',
		'lang'	=> CURRENT_LANGUAGE
	);
	
	var $to_translation = array();
	/**
	 * 
	 * @param $Model
	 * @param $config
	 * @return unknown_type
	 */
	function setup(&$Model, $config = array()) {
		
		
		if (!is_array($config)) {
			$config = array('type' => $config);
		}
		
		if (!isset($config['className']))
			$config['className'] = $Model->name;
		
		$settings = array_merge($this->_defaults, $config);
	
		if (in_array($settings['scope'], $Model->getAssociated('belongsTo'))) {
			$data = $Model->getAssociated($settings['scope']);
			$parent =& $Model->{$settings['scope']};
			$settings['scope'] = $Model->alias . '.' . $data['foreignKey'] . ' = ' . $parent->alias . '.' . $parent->primaryKey;
			$settings['recursive'] = 0;
		}
		//pr($settings);
		$this->settings[$Model->alias] = $settings;
		
	}
	/**
	 * 
	 * @param $Model
	 * @param $queryData
	 * @return unknown_type
	 */
	function beforeFind(&$Model, $queryData){
		
		$settings = $this->settings[$Model->alias];
		//pr($queryData);
		/* na bindovani klasickych spojeni,
		 * Zatim pouze pro belongsTo
		 */ 
		$db =& ConnectionManager::getDataSource($Model->useDbConfig);
		$tablePrefix = $db->config['prefix'];
		
		foreach($Model->belongsTo as $modelClass => $bel){
			
			$model = ClassRegistry::init($modelClass, 'Model');
			$foreignKey = $bel['foreignKey'];
			$queryData['joins'][] = array(
				'type' => 'LEFT',
				'alias' => $model->name,
				'table' => $db->name($tablePrefix . $model->useTable),
				'conditions' => array(
					$Model->alias . '.' . $foreignKey => $db->identifier("{$modelClass}.{$Model->primaryKey}"),				
				),
				'fields' => $bel['fields']
			);		
		}
		
		// bindTranslation
		$queryData['joins'] = am($queryData['joins'], self::generateTranslation($Model));
		if (isset($Model->belongsTo)) foreach($Model->belongsTo  as $model => $arr)  if (isset($arr['tran'])) $queryData['joins'] = am($queryData['joins'],self::generateTranslation($Model,$model));
		if (isset($Model->hasOne)) 	  foreach($Model->hasOne 	 as $model => $arr)  if (isset($arr['tran'])) $queryData['joins'] = am($queryData['joins'], self::generateTranslation($Model,$model));

		$Model->belongsTo = array();
		
		
		$condition_string = $db->conditionKeysToString((isset($queryData['conditions'])?$queryData['conditions']:array()));
		$replace = array();
		//pr($settings['cols']);
		foreach($settings['cols'] as $field){
			$is_serialize = strpos($field,'/');
			
			if ($is_serialize !== false){
				$field = explode('/',$field);
				$replace['`'.$Model->name.'`.`'.$field[0].'`'] = '`Translation'.$Model->name.ucfirst($field[0]).'`.`value`';
				//pr($replace);
			} else {
			
				$replace['`'.$Model->name.'`.`'.$field.'`'] = '`Translation'.$Model->name.ucfirst($field).'`.`value`';
				//pr($replace);
			}
		}
		//pr($condition_string);
		foreach($condition_string as &$con) $con = strtr($con,$replace);
		
		$queryData['conditions'] = $condition_string;  
		//pr($queryData['fields']);
		//pr($this->to_translation);
		// upraveni fields
		if(is_array($queryData['fields']) && count($queryData['fields']) > 0){
			foreach($this->to_translation as $tran_model_name => $arr){
				$mm = ($Model->name == $settings['className'])?$arr['conditions'][$tran_model_name.'.modul']:$Model->name;
				if(is_array($arr['conditions'][$tran_model_name.'.col']))
					$arr['conditions'][$tran_model_name.'.col'] = 'value';
					
				$col = $mm.'.'.$arr['conditions'][$tran_model_name.'.col'];
				
				//pr($arr['conditions'][$tran_model_name.'.col']);
				//pr($queryData['fields']);
				$key = array_search($col, $queryData['fields']);
				//pr($key);
				
				//pr($tran_model_name.'.value');
				
				if ($key !== false){
						
						if ($Model->name == 'Setting'){
							
							//$queryData['fields'][$key] = $tran_model_name.'.value';
							 
							$queryData['fields'][2] = $queryData['fields'][$key]; 
							$queryData['fields'][$key] = $tran_model_name.'.value';
						} else
						
							$queryData['fields'][$key] = $tran_model_name.'.value'; 
				}
				
			}
		}
		//pr($queryData['fields']);
		//pr($queryData['fields']);		
		// upraveni order
		//pr($queryData['order']);
		if (is_array($queryData['order'])){
			foreach($queryData['order'] as &$or_col){
				if (!empty($or_col)){
					if (is_array($or_col)){
						$direction = current($or_col);	
						$col = key($or_col);	
					} else {
						@list($col, $direction) = explode(" ", $or_col);
					}
					if(strpos($col,'.') !== false){
						list($model, $field) = explode(".",$col);
						if ($model == $Model->name && in_array($field,$settings['cols'])){
							$or_col = 'Translation'.$Model->name.ucfirst($field).'.value '.$direction;
						}
					}
				}				
			}
		}
		//pr($queryData);
		return $queryData;
	}
	
	/**
	 * Prepise text na youtube
	 * @param $text
	 * @return $text
	 */
	function text_replace_spec($text){		
		if (!function_exists('create_script')){
			function create_script($code){
				$out = array();
				
				$code_tmp = explode('"',$code);
				$out[] = '<iframe class="noprint youtube" width="560" height="349" src="http://www.youtube.com/embed/'.$code_tmp[0].'?theme=light&amp;color=#4CC5C8" frameborder="0"></iframe>';
                
                return implode("\n", $out);
			}
		}

		preg_match_all('/<img class="youtube" title="(.*)\/>/ismU', $text, $oooo);
		for($i=0; $i<count($oooo[1]);$i++){
			$text = str_replace($oooo[0][$i], create_script($oooo[1][$i]), $text);
		}
		return $text;
	}
	
	/**
	 * 
	 * @param $Model
	 * @param $data
	 * @return unknown_type
	 */
	function afterFind($Model, $data){
		//pr($data);
		$settings = $this->settings[$Model->alias];
		if (isset($data) && $data != null  && count($data)>0){
			foreach($data as &$value){
				//pr($value);
				foreach($this->to_translation as $tran_model_name => $arr){
					
					if (isset($value[$tran_model_name])){
						$mm = ($Model->name == $settings['className'])?$arr['conditions'][$tran_model_name.'.modul']:$Model->name;
						//$ser = @unserialize($value[$tran_model_name]['value']);
						//if(!$ser){
						//pr($value[$tran_model_name]['value']);
						if ($Model->name == 'Setting'){
							$setting_value = unserialize($value['Setting']['value']);
							$trans_value = unserialize($value['TranslationSettingValue']['value']);
							
							//pr($setting_value);
							//pr($trans_value);
							$setting_trans = am($setting_value,$trans_value);
							//pr($setting_trans);
							$value[$mm][$arr['conditions'][$tran_model_name.'.col']] = serialize($setting_trans);
						}
						elseif ($Model->name == 'Article'){
							$value[$mm][$arr['conditions'][$tran_model_name.'.col']] = self::text_replace_spec($value[$tran_model_name]['value']);
						} else {
							
								$value[$mm][$arr['conditions'][$tran_model_name.'.col']] = $value[$tran_model_name]['value'];
						} 
						
						/*
						} else {
							//pr($ser);
							$value[$mm][$arr['conditions'][$tran_model_name.'.col']] = serialize($ser);
					
						}
						*/
						//unset($value[$tran_model_name]);		
					}
				}
			}
		}
		return $data;
	}
	
	function generateTranslation(&$Model, $childModel = null){
		
		$model = ($childModel == null)?$Model:$Model->$childModel;
		$settings = $this->settings[$model->alias];
		$model_name = ($settings['className'] != $model->name)?$settings['className']:$model->name;
		
		$db =& ConnectionManager::getDataSource($model->useDbConfig);
		$tablePrefix = $db->config['prefix'];
		
		$this->runtime[$model->alias]['model'] = ClassRegistry::init('Translation', 'Model');
		$RuntimeModel =& $this->runtime[$model->alias]['model'];
		
		foreach($settings['cols'] as $col){
			$is_serial = strpos($col,'/');
			
			if ($is_serial !== false){
				$col = explode('/',$col);
				$modelClass = 'Translation'.$model_name.ucfirst($col[0]);	
			} else 
			
				$modelClass = 'Translation'.$model_name.ucfirst($col);	
			
			$this->to_translation[$modelClass] = array(
				'className'		=> 'Translation',
				'foreignKey'	=> 'parent_id',
				'conditions'	=> array(
					$modelClass.'.modul'	=> $settings['className'],
					$modelClass.'.lang'		=> $settings['lang'],
					$modelClass.'.history'	=> 0,
					$modelClass.'.col'		=> $col
				)
			);
		} 
		$join  = array();
		foreach($settings['cols'] as $col){
			$modelClass = 'Translation'.$model_name.ucfirst($col);	
			$join[]  = array(
				'type' => 'LEFT',
				'alias' => $modelClass,
				'table' => $db->name($tablePrefix . $RuntimeModel->useTable),
				'conditions' => array(
					$model->alias . '.' . $model->primaryKey => $db->identifier("$modelClass.parent_id"),
					$modelClass.'.modul' => $settings['className'],
					$modelClass.'.col' => $col,
					$modelClass.'.history' => 0,
					$modelClass.'.lang' => CURRENT_LANGUAGE,
					
				)
			);
		}
		return $join;
	
	}
}
?>