<?php $this->set('telefon_pref',$telefon_pref = array(
	'00420'=>'+420',
	'00421'=>'+421',
));
$this->set('dph_list', $this->dph_list = array(
	'1.20' => '20%',
	'1.10'  => '10%',
	'1.2' => '20%',
	'1.1'  => '10%'
));
$this->set('stat_list',$this->stat_list = array(
	1=>'Česká republika',
	2=>'Slovenská republika',
));
$this->set('ano_ne',$ano_ne = array(
	0=>'Ne',
	1=>'Ano',
));
//kontaktni formular
$this->set('type_form_item', $this->type_form_item = array(
	'cz'	=> array(
	  0 => 'input',
		1 => 'textarea',
		2 => 'checkbox',
		3 => 'nadpis',
		4 => 'select'
	)
));

$this->set('pocet_na_stranku',array(
	10 	=> 10,
	20 	=> 20,
	30 	=> 30,
	40 	=> 40,
	50 => 50,
));

// eshop typy cen
$this->set('typ_ceny_ar', array(
			's_dph' => array(
				0	=> 'cena_s_dph',
				1	=> 'cena_dop_s_dph',
				2	=> 'doprava_cena_s_dph',
				3 	=> 'platba_cena_s_dph',
				4 	=> 'slevove_kupon_cena_s_dph',
				5	=> 'celkem_produkty_cena_s_dph',
				6	=> 'celkem_cena_s_dph',
				7	=> 'row_cena_s_dph',
				8	=> 'zaokrouhleni_s_dph'
			),
			'bez_dph' => array(
				0	=> 'cena_bez_dph',
				1	=> 'cena_dop_bez_dph',
				2	=> 'doprava_cena_bez_dph',
				3 	=> 'platba_cena_bez_dph',
				4 	=> 'slevove_kupon_cena_bez_dph',
				5	=> 'celkem_produkty_cena_bez_dph',
				6	=> 'celkem_cena_bez_dph',
				7	=> 'row_cena_bez_dph',
				8	=> 'zaokrouhleni_bez_dph'
			)
		));	



?>