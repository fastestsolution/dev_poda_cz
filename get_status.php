 <?php 
	function read_dir($root){
		$root_dir = opendir($root);
		$time = null;
		while($file_file = readdir($root_dir)){
		if($file_file != '' && $file_file != '.' && $file_file != '..' && substr($file_file,0,3) == 'php')
			{
				
				$time_now = fileatime($root.'/'.$file_file);
				if ($time == null || $time_now > $time[0]['time']){
					$time[0]['time'] = $time_now;
					$time[0]['file'] = $file_file;
				}
			}
		}
		$time[0]['size'] = filesize($root.'/'.$time[0]['file']);
		return $time;
	}

	function sabsi($array, $index, $order='asc', $natsort=FALSE, $case_sensitive=FALSE)
	{
		
		if(is_array($array) && count($array)>0)
		{
			foreach(array_keys($array) as $key) @$temp[$key]=$array[$key][$index];
			if(!$natsort) ($order=='asc')? asort($temp) : arsort($temp);
			else
			{
				($case_sensitive)? natsort($temp) : natcasesort($temp);
				if($order!='asc') $temp=array_reverse($temp,TRUE);
			}
			foreach(array_keys($temp) as $key) (is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
			return $sorted;
		}
		return $array;
	}
 
	function get_status(){
		if (!isset($_GET['file'])) 
			$file = null;
		else 
			$file = $_GET['file'];
			
		$upload_tmp_dir = '/home/www/fastest.cz/tmp/';
		
		if ($file == null || empty($file)){
			$tmpdata = read_dir($upload_tmp_dir);
			//print_r($tmpdata);
			$tmpdata = sabsi($tmpdata,'time');
			
			
			$stat = stat($upload_tmp_dir.'/'.@$tmpdata[0]['file']);
			$tmpdata[0]['size']= $stat['size'];
			$tmpdata[0]['block_size']= $stat['blksize'];
				
			$size = $tmpdata[0]['size'];
		} else {
			if (file_exists($upload_tmp_dir.'/'.$file)){
				$tmpdata[0]['file'] = $file;
				//$tmpdata[0]['size'] = filesize($upload_tmp_dir.'/'.$file);
				$stat = stat($upload_tmp_dir.'/'.$file);
				$tmpdata[0]['size']= $stat['size'];
				$tmpdata[0]['block_size']= $stat['blksize'];
				
				$size = $tmpdata[0]['size'];
				
				
			} else {
				$tmpdata[0]['file'] = '';//$file;
				$size = 'done';
			}
		}
		echo json_encode(array('upload_info'=>array('size'=>$size,'file'=>@$tmpdata[0]['file'])));
		die();
	}
	
	get_status();
 ?> 