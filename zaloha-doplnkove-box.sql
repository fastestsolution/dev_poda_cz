-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 08, 2018 at 01:44 PM
-- Server version: 5.5.59-0ubuntu0.14.04.1-log
-- PHP Version: 5.5.9-1ubuntu4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `c58_poda`
--

-- --------------------------------------------------------

--
-- Table structure for table `fastest__smallboxes`
--

CREATE TABLE IF NOT EXISTS `fastest__smallboxes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `text` text NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `kos` tinyint(1) NOT NULL DEFAULT '0',
  `nd` tinyint(1) NOT NULL DEFAULT '0',
  `cms_user_id` int(11) DEFAULT NULL,
  `title` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `name` (`name`),
  KEY `status` (`status`),
  KEY `kos` (`kos`),
  KEY `cms_user_id` (`cms_user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `fastest__smallboxes`
--

INSERT INTO `fastest__smallboxes` (`id`, `name`, `text`, `created`, `updated`, `status`, `kos`, `nd`, `cms_user_id`, `title`) VALUES
(1, 'Internet - nadpis', '', '2018-03-08 13:23:30', '2018-03-08 13:27:44', 1, 0, 0, NULL, 'Rychlý internet, hodně zábavy! <small>a s aktivací od 0 korun!</small>'),
(2, 'Internet - perex', '<p class="txt-center mt50"><strong class="uppercase black">Jsme na\ntrhu již více než 20 let a v moderních technologiích se vyznáme. My\nVám rozumíme, takže s námi můžete mluvit tak, jak jste\nzvyklí...</strong></p>\n<p class="txt-center mt25">Internet poskytujeme už nějaký ten\npátek, takže víme, že jak laik, tak i profík potřebují\nprofesionální přístup a někoho, kdo mu na druhé straně rozumí.</p>\n<p class="txt-center mt25">Internetové připojení realizujeme\nrádiovými spoji nebo na optické síti.&nbsp; Nezávisle na způsobu je\ninternetové připojení PODA vždycky stabilní, s garantovanou\nrychlostí a bez časových nebo datových omezení.</p>\n', '2018-03-08 13:28:55', '2018-03-08 13:28:55', 1, 0, 0, NULL, ''),
(3, 'Internet - text', '<p class="txt-center mb25">Řešení napojení do sítě Internet je\nrobustní a plně redundantní. Hlavní síťový uzel je připojen\nk&nbsp;tranzitnímu uzlu CeColo v Praze dvěma na sobě nezávislými\ntrasami. Obě trasy mají kapacitu 10&nbsp;Gb, jsou od odlišných\noperátorů a nemají žádný styčný bod či úzké místo. V uzlu CeColo\nmáme redundantně dva vysoce výkonné hraniční routery, jež jsou\nschopny pojmout plnou kapacitu přenosů mezi sítěmi PODA a\nInternetem.</p>\n<h2 class="txt-center mb10">Optická síť</h2>\n<p class="txt-center mb25">Prostřednictvím optické sítě jsou\nposkytovány služby ostatním provozovatelům veřejných komunikačních\nsítí a koncovým klientům společnosti PODA a.s. Využití vlastních\noptických vedení umožňuje poskytování služeb ve vysoké kvalitě a se\nspolehlivostí, která je u rádiových spojů nedosažitelná.</p>\n<h2 class="mb10">Bezdrátová (rádiová) síť</h2>\n<p class="mb25">Bezdrátová (rádiová) síť PODA je vybudována s\ndůrazem na kvalitu. V praxi to představuje 350&nbsp;retranslačních\nstanic rozmístěných po celé republice a tvořících páteřní síť.\nRetranslační stanice jsou propojeny optickými vlákny nebo\nvysokokapacitními bezdrátovými spoji v pásmech 18, 11 či 10,5 GHz.\nZ retranslačních stanic vede k&nbsp;zákazníkům více než 3 000\nkoncových bezdrátových spojů.</p>\n<h2 class="mb10">Pevný internet</h2>\n<p class="mb25">Prostřednictvím pevného internetu připojujeme jak\nbyty v&nbsp;bytových jednotkách, tak rodinné domy.\n<strong>Připojení rodinných domů</strong> je realizováno pomocí\nrádiového spoje ve volném pásmu 5GHz. Spoj je vybudován mezi místem\npřipojení a nejbližším bodem páteřní sítě PODA a.s. Pro realizaci\nspojení se používají nejmodernější zařízení výrobců MikroTik a\nUbiquiti Networks. Rychlost připojení dosahuje až 30Mbit/s. Jde o\nsdílené připojení s agregačním poměrem 1:3 příp. 1:2. Toto\npřipojení neumožňuje rozšíření o službu IPTV. Bez možnosti\nsamoinstalace.</p>\n<p class="mb25"><strong>Připojení bytů v&nbsp;bytových\njednotkách</strong> realizujeme optickými kabely. Optický signál\npřivedeme do domu, kde je ukončen aktivním rozbočovacím prvkem.\nJednotliví uživatelé jsou pak k aktivnímu prvku připojeni UTP\nkabelem. Přípojka je v&nbsp;bytě ukončena datovou zásuvkou se dvěma\nporty RJ45. Maximální rychlost dosahuje až 100Mbit/s bez agregace.\nProstřednictvím optického kabelu poskytujeme také vysílání IPTV\n(digitální televize). Pro Vaše pohodlí a finanční úsporu můžete\nzískat obě služby současně. Možnost samoinstalace.</p>\n<p class="mb25">Firemní klienty připojujeme k internetu optickým\nkabelem nebo radiovým spojem. Pro radiové připojení využíváme volná\npásma 5 GHz, 17GHz, 80 GHz a placené pásmo 10GHz.</p>\n<p class="mb25"><a href="/gpon/" class="blue">Připojení technologií\nGPON</a>. Jedná se o moderní technologii pro provoz internetu,\ntelevize a hlasových služeb. Přímo do domácností zákazníků\npřivedeme optický kabel, který je ukončen převodníkem optického\nsignálu (tzv. zakončovací jednotkou). Tato jednotka má i funkci\njednoduchého Wi-Fi routeru. Maximální rychlost vysokorychlostního\ninternetu dosahuje až 300Mbit/s bez agregace.</p>\n<p class="mb25">Prostřednictvím optického kabelu poskytujeme také\nvysílání IPTV (digitální televize), o které je možné rozšířit\ninternetové připojení. Pro Vaše pohodlí a finanční úsporu můžete\nzískat obě služby současně. Možnost samoinstalace.</p>\n<h2 class="mb10">Mobilní internet</h2>\n<p class="mb25">LTE (Long Term Evolution) je zkratka pro novou\ngeneraci mobilní datové sítě. Jedná se mezistupeň mezi 3G a 4G\nsítěmi a přináší vylepšení v několika ohledech. Tím nejzásadnějším\nje samozřejmě navýšení rychlosti v obou směrech – stahování i\nnahrávání.</p>\n<p class="mb25">LTE je schopno vyvinout teoretickou maximální\nrychlost datových přenosů až 175 Mb/s pro stahování a až 57,6 Mb/s\npro odesílání dat v jednom frekvenčním pásmu při šířce kanálu 20\nMHz. V praxi je rychlost samozřejmě o poznání nižší, neboť je\ndělena mezi spoustu jiných uživatelů.</p>\n<p>Síť LTE přináší velký komfort, stačí si jen vybrat objem\npřenesených dat podle svých potřeb a surfovat bez obav kdykoli a\nkdekoli.</p>\n', '2018-03-08 13:29:27', '2018-03-08 13:29:27', 1, 0, 0, NULL, ''),
(4, 'TV Služby - nadpis', '', '2018-03-08 13:32:06', '2018-03-08 13:32:33', 1, 0, 0, NULL, 'Rychlý internet, hodně zábavy! <small>Už konečně žádná válka o ovladač.</small>'),
(5, 'TV služby - perex', '<p class="txt-center mt50"><strong class="uppercase black">Udělejte\nrodině radost! Vyberte z více než 80 programů, z nich je více než\n... v HD kvalitě a udělejte ze svého obýváku místo plné radosti a\nzábavy.</strong></p>\n<p class="txt-center mt25">Posaďte se společně k televizi a\nvyužijte naplno širokou nabídku televizních programů, PODA net. TV\na navíc k tomu všechny chytré funkce, které Vám PODA TV nabízí.</p>\n', '2018-03-08 13:33:26', '2018-03-08 13:33:26', 1, 0, 0, NULL, ''),
(6, 'Volání - nadpis', '', '2018-03-08 13:34:15', '2018-03-08 13:34:15', 1, 0, 0, NULL, 'Informace o datech <small>vše co potřebujete vědět</small>'),
(7, 'Volání - perex', '<p class="txt-center mt50"><strong class=\n"black uppercase">Neutrácejte zbytečně, mějte výdaje pevně pod\nkontrolou!<br />\nPřipravili jsme pro Vás nabídku velmi výhodných cen za volání a SMS\nnejen v síti PODA.</strong></p>\n<p class="txt-center">Tato nabídka je bonusem pro zákazníky služby\n<a href="/internet/" class="blue">internetu</a> a <a href=\n"/tv-sluzby/" class="blue">televize</a> nebo jejich vzájemné\nkombinace. Být klientem PODA se vyplatí!</p>\n', '2018-03-08 13:34:39', '2018-03-08 13:34:39', 1, 0, 0, NULL, ''),
(8, 'O nás - nadpis', '', '2018-03-08 13:36:09', '2018-03-08 13:36:09', 1, 0, 0, NULL, 'Propojujeme generace <small>Respektováním vašich přání a potřeb se vytvořila přidaná hodnota, která by bez vás nejspíš nikdy nevznikla</small>'),
(9, 'O nás - text', '<p class="txt-center mt50"><strong>PODU pro vás budujeme už od roku\n1996.</strong> Jsme víc, než jen poskytovatel datových a\ntelekomunikačních služeb. Jsme zapálený tým, který zakládá úspěch\nna maličkostech, protože víme, že jenom tak vznikají velké věci. Je\nnám jasné, že i zdánlivá drobnost může váš komfort posunout o\nněkolik řádů výš. Důvěra je pro nás prioritou. Stavíme na osobní\nzodpovědnosti za každý náš krok. Vždy u nás najdete přátelský\npřístup a pokud můžeme cokoliv zlepšit, věřte, že to rádi\nuděláme.</p>\n<p class="txt-center">A jistí nás nejmodernější technologie a\nneustálé inovace. Respektováním vašich přání a potřeb vznikla\npřidaná hodnota, která by bez vás nejspíš nikdy nevznikla -\npropojujeme generace. Tak, jak to každá z nich potřebuje. Přejeme\nsi, abyste mohli být stále online. Pokud potřebujete žít na síti,\nnáš nadčasový internet vám to splní. Jestliže je pro vás\nnejdůležitější společná pohoda u vašeho oblíbeného filmu, naše\nchytrá televize je tu pro vás. A pokud se potřebujete zeptat\nvnoučat, jak to jde ve škole, naše mobilní komunikace vám to\numožní, spolehlivě a bez zbytečných výdajů.</p>\n', '2018-03-08 13:36:57', '2018-03-08 13:36:57', 1, 0, 0, NULL, ''),
(10, 'O nás - obrázky', '<div class="full-imgs">\n<div class="fImg cover_img" data-src="/css/layout/onas/img-1.jpg">\n<div class="abs">\n<div class="letter">J</div>\n<strong>Jednoduchost</strong>\n<p>Vnímáme, že v jednoduchosti je síla a v rychlosti konkurenční\nvýhoda.</p>\n</div>\n</div>\n<div class="fImg cover_img" data-src="/css/layout/onas/img-2.jpg">\n<div class="abs">\n<div class="letter">S</div>\n<strong>Spolehlivost</strong>\n<p>Co řekneme, to platí. Slibujeme jen to, co můžeme splnit, a naše\nsliby bereme jako závazky.</p>\n</div>\n</div>\n<div class="fImg cover_img" data-src="/css/layout/onas/img-3.jpg">\n<div class="abs">\n<div class="letter">M</div>\n<strong>Možná i zázraky</strong>\n<p>My všichni zajišťujeme tu nejlepší kvalitu našich služeb.\nNechceme si moc tahat kšandy, ale pro vás zkusíme i zázraky.</p>\n</div>\n</div>\n<div class="fImg cover_img" data-src="/css/layout/onas/img-4.jpg">\n<div class="abs">\n<div class="letter">E</div>\n<strong>Efektivita</strong>\n<p>Jednáme rychle a spolehlivě. Čas a spolupráce s námi nesmí být\npro vás promarněnou investicí. Jsme přesvědčeni, že čím efektivněji\npracujeme, tím více všichni společně získáváme.</p>\n</div>\n</div>\n<div class="fImg cover_img" data-src="/css/layout/onas/img-5.jpg">\n<div class="abs">\n<div class="letter">P</div>\n<strong>Prestiž</strong>\n<p>na tom, jak nás vnímá naše okolí, nám hodně záleží. Prestiž\nfirmy však nebereme jako povrchní pozlátko. Budujeme ji proto nejen\nsvou vstřícností a vysokou profesionalitou, ale i intenzivní pomocí\ndruhým.</p>\n</div>\n</div>\n<div class="fImg cover_img" data-src="/css/layout/onas/img-6.jpg">\n<div class="abs">\n<div class="letter">O</div>\n<strong>Ochota</strong>\n<p>Každému z nás jsme vždy připraveni věnovat nadstandardní zájem a\npéči. Věříme, že právě i díky ochotě s vámi máme pevné a stabilní\nvztahy.</p>\n</div>\n</div>\n<div class="fImg cover_img" data-src="/css/layout/onas/img-7.jpg">\n<div class="abs">\n<div class="letter">D</div>\n<strong>Důvtip</strong>\n<p>Nejde nám jen o to prodávat služby. Přejeme si ušít každému z\nvás řešení na míru, tak jak právě potřebujete. A to se bez skvělých\nnápadů nikdy neobejdete.</p>\n</div>\n</div>\n<div class="fImg cover_img" data-src="/css/layout/onas/img-8.jpg">\n<div class="abs">\n<div class="letter">A</div>\n<strong>Ano</strong>\n<p>Ano, máme ambice být pro vás víc než poskytovatelem služeb, naší\nambicí je být vašimi přáteli, lidmi, na které se obrátíte, kdykoliv\nbudete potřebovat.</p>\n</div>\n</div>\n</div>\n', '2018-03-08 13:37:35', '2018-03-08 13:37:35', 1, 0, 0, NULL, ''),
(11, 'Kontakt - nadpis', '', '2018-03-08 13:38:38', '2018-03-08 13:38:38', 1, 0, 0, NULL, 'Potřebujete se s námi spojit? <small>Jsme vám k dispozici od pondělí do neděle. Vyberte si způsob, který vám nejlépe vyhovuje.</small>');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
