<div class="main_header">
    <div class="header_cont">
        <div class="fb-like" data-href="https://www.facebook.com/lacetocz/" data-layout="box_count" data-action="like" data-size="large" data-show-faces="false" data-share="true"></div>

        <?= $this->renderElement('carousel', array('carousel_data' => $carouselTopHp )); ?>
    </div>
</div>

<div class="clear"></div>

<?php foreach($smallboxes_hp as $i => $smallbox): ?>
<div class="section-<?php echo ($i + 2);?> section-set <?= (($i + 2) != 3 ? 'clip-path' : ''); ?>">
    <div class="laceto-container">
        <div class="section-cont <?= $i % 2 == 0 ? 'right':'left'; ?>">
            <?php echo $smallbox['Smallbox']['name'];?>
            <?php echo $smallbox['Smallbox']['text'];?>
        </div>
    </div>
    <div class="<?= $i % 2 == 0 ? 'position-right':'position-left'; ?>">
        <?= $this->renderElement('product_panel', array('product_items' => @$produkty['cat_'.$smallbox['Smallbox']['category_id']], 'panel_category' => $shop_menu_list[$smallbox['Smallbox']['category_id']])); ?>
    </div>
</div>
<?php endforeach; ?>

