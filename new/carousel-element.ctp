<?php if(isset($carousel_data['Carousel']['items'])): ?>
    <div id="banner">
            <div id="arrow-left2"></div>
            <div id="arrow-right2"></div>
            <div id="arrow-bottom2"></div>

            <div id="banner_pagination">
                <a class="point"></a>
                <a class="point"></a>
                <a class="point"></a>
                <a class="point"></a>
            </div>
            <?php foreach($carousel_data['Carousel']['items'] as $item):  ?>
                <a href='<?= $item['href']; ?>'>
                    <div class="img" data-src="<?= $item['img']; ?>">
                        <h2><?= $item['name']; ?></h2>
                        <p><?= $item['text']; ?></p>
                    </div>
                </a>
            <?php endforeach; ?>

        </div>

        <script>
            window.addEvent('domready', function() {
                header_slider('banner', 'img', 'arrow-left2', 'arrow-right2','arrow-bottom2', true, 4000);
            });
        </script>
<?php endif; ?>