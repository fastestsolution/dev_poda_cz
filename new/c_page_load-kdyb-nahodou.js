function createCookie(name,value,days) {
    var expires = "";
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days*24*60*60*1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function on_page_load(){
    window.addEvent('domready', function(){

        if($('btn-cookie')){
            $('btn-cookie').addEvent('click', function(e){
                e.stop();
                createCookie('useCookieConfirmation', true);
                this.getParent('.cookie').dispose();
            });
        }

        if ($$('.product-panel').length > 0){
            $$('.product-panel').each(function(item){
                ImgSlider(item);
            });
        }


        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/cs_CZ/sdk.js#xfbml=1&version=v2.9";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));


        preload_links();
        // load scroll
        var mySmoothScroll = new Fx.SmoothScroll({
            links: '.scroll_moo',
            wheelStops: false
        });
        search_field();
        placeholder();
        flipcards();
        thumb_photo();
        mobile_menu();

        $$('.youtube').setProperty('allowfullscreen');
        $('obal').getElements('a').each(function(link){
            link.addEvent('click',function(e){
                var link_title = link.getProperty('title');
                var link_href = link.getProperty('href');
                ga('send', 'event', {
                    eventCategory: 'Všechny odkazy',
                    eventAction: 'click',
                    eventLabel: link_title,
                    eventValue: 0
                });
                //_gaq.push(['_trackEvent', 'Všechny odkazy','Kliknuti', link_title]);
            });
        });

        length_dom = ('http://'+window.location.host).length;
        length_url = window.location.href.length;
        url_href = window.location.href.substr(length_dom,length_url);
        url_name = url_href.replace(/\//g,"");

        $$('.tabs .tab').addEvent('click', function(e){
            e.stop();
            var target = this.get('rel');
            $$('.tabs .tab.active').removeClass('active');
            $$('.tab_content.active').removeClass('active');
            this.addClass('active');
            if($(target)){
                $(target).addClass('active');
            }
        });

        $$('.show-more').addEvent('click', function(e){
            e.stop();
            var additional = this.getParent('li').getElement('.additional').toggle();
            if(additional.getStyle('display') != 'none'){
                activateMapPoint(this.getElement('.selectable-row').get('value'));
                setActiveRow('seller-list', this.getParent('li'));
            }else{
                setActiveRow('seller-list', this.getParent('li'), false, true);
            }
        });

        $$('.selectable-row').addEvent('change', function(e){
            if(this.get('checked') == true){
                activateMapPoint(this.get('value'));
                setActiveRow('seller-list', this.getParent('li'), true);
            }
        });

        $$('.setOrder').addEvent('click', function(){
            $('Order').set('value', this.get('data-order'));
            this.getParent('form').submit();
        });
		/* $$('.filtration_cat').addEvent('click', function(e){
		 e.stop();
		 if(e.target.tagName != 'SELECT'){
		 var select = this.getElement('select');
		 console.log(select);

		 }
		 });*/
    });
    //fb_plugin('180','111277538962711');

}

function header_slider(obal, img, prev, next, bottom, autorun, time) {

    var params = {
        'autorun':autorun,
        'autorun_time':time,
        'type':'fade',
        'obal':obal,
        'element_class':img,
        'prev':prev,
        'next':next,
    }

    tab_slider(params);

    if(bottom != null && $(bottom)) {

        if($(bottom).hasClass('on-main-page')) {
            if($(bottom) && $(bottom).hasClass('on-main-page')) {
                $(bottom).addEvent('click', function() {
                    var myElement = $(document.body);
                    var myFx = new Fx.Scroll(myElement).start(0, $(this.get('data-pos')).getPosition().y - 100);
                });
            }
        } else {
            $(bottom).addEvent('mouseenter', function() {
                $(params.obal+'_pagination').addClass('open');
                $(bottom).addClass('open');
            });

            $(params.obal+'_pagination').addEvent('mouseleave', function() {
                $(params.obal+'_pagination').removeClass('open');
                $(bottom).removeClass('open');
            });
        }
    }

    $(obal).getElements('.'+img).each(function(it){
        if(it.getProperty('data-src') != '') {
            it.setStyle('background-image','url("'+it.getProperty('data-src')+'")');
        }
    });

    $(obal).getElements('.pag-img').each(function(it){
        it.setStyle('background-image','url("'+it.getProperty('data-src')+'")');
    });
}

function tab_slider(params){

    $(params.obal).getElements('.'+params.element_class).each(function(item,k){
        if (k >0){
            if (params.type == 'fade'){
                item.fade('hide');
                item.addClass('close');
            } else {
                item.addClass('none');
            }
        }
    });

    var contents = $(params.obal).getElements('.'+params.element_class);

    var dur_time = 4400;
    if($(params.obal).get('data-time')) {
        dur_time = 450;
    }

    contents.set('tween', {
        duration: dur_time,
        transition: 'quad:out'
    });

    if ($(params.obal+'_pagination')){
        $(params.obal+'_pagination').getElement('a').addClass('active');
        $(params.obal+'_pagination').getElements('a').each(function(link,k){
            link.addEvent('click',function(e){
                new Event(e).stop();
                if (!this.hasClass('noclear'))
                    clearInterval(timer);
                else
                    this.removeClass('noclear');
                $(params.obal+'_pagination').getElements('a').removeClass('active');
                this.addClass('active');

                if(this.hasClass('amp')) {
                    var amp = this.get('data-id');
                    $$('.amp-title').removeClass('active');
                    $(amp).addClass('active');
                }

                if (params.type == 'fade'){
                    contents.addClass('close');
                    contents.fade(0);
                    if (contents[k]){
                        contents[k].removeClass('close');
                        contents[k].fade(1);
                    }
                } else {
                    contents.addClass('close');
                    contents.addClass('none');
                    if (contents[k]){
                        contents[k].removeClass('close');
                        contents[k].removeClass('none');
                    }
                }
            });
        });

        if (params.next){
            if ($(params.next)){
                $(params.next).addEvent('click',function(e){
                    new Event(e).stop();
                    clearInterval(timer);
                    var list = $(params.obal+'_pagination').getElements('a');
                    var count = $(params.obal+'_pagination').getElements('a').length;
                    var current = $(params.obal+'_pagination').getElement('.active');
                    var active = list.indexOf(current);

                    if(current.getNext('a')){
                        current.getNext('a').addClass('noclear');
                        current.getNext('a').fireEvent('click',current.getNext('a'));
                    } else {
                        list[0].addClass('noclear');
                        list[0].click();
                    }
                });
            }

        }

        if (params.prev){
            if ($(params.prev)){
                $(params.prev).addEvent('click',function(e){
                    new Event(e).stop();
                    clearInterval(timer);
                    var list = $(params.obal+'_pagination').getElements('a');
                    var count = $(params.obal+'_pagination').getElements('a').length;
                    var current = $(params.obal+'_pagination').getElement('.active');
                    var active = list.indexOf(current);
                    if(current.getPrevious('a')){
                        current.getPrevious('a').addClass('noclear');
                        current.getPrevious('a').fireEvent('click',current.getPrevious('a'));
                    } else {
                        list[count-1].addClass('noclear');
                        list[count-1].click();
                    }
                });
            }

        }

        if (params.autorun){
            function autorun(){
                var list = $(params.obal+'_pagination').getElements('a');
                var count = $(params.obal+'_pagination').getElements('a').length;
                var current = $(params.obal+'_pagination').getElement('.active');
                var active = list.indexOf(current);

                if(current.getNext('a')){
                    current.getNext('a').addClass('noclear');
                    current.getNext('a').fireEvent('click',current.getNext('a'));
                } else {
                    list[0].addClass('noclear');
                    list[0].click();
                }
            }
            if (!params.autorun_time) params.autorun_time = 1000;
            var timer = autorun.periodical(params.autorun_time);
        }

    }

}

function activateMapPoint(id){
    if(typeof window.markerObjects != 'undefined' && typeof window.map != 'undefined'){
        try{
            Array.each(window.markerObjects, function(marker, i){

                if(marker.par_id == id){
                    if(typeof window.markerCluster != 'undefined'){
                        //window.markerCluster.setMaxZoom(1);
                        window.markerCluster.setGridSize(1);
                        window.markerCluster.redraw();
                    }
                    window.infowindow.close();
                    window.infowindow.setContent(marker.html);
                    var position = marker.getPosition();

                    window.map.setCenter(position);
                    window.map.setZoom(10);
                    window.infowindow.open(window.map, marker);
                    throw 'Active Point' + id;
                }
            });
        }catch(e){
            console.log(e);
        }
    }
}

function kontakt_map(sites) {
    window.addEvent('domready',function() {

        function setMarkers(map, markers) {
            for (var i = 0; i < markers.length; i++) {
                var site = markers[i];
                var siteLatLng = new google.maps.LatLng(site[1], site[2]);

                var content = "<h3>" + site[0] + "<br>" + site[5] + "<br><a href='mailto:" + site[4] + "'>"+site[4]+"</a><br>" + (site[7] != '' ? '<a href="http://' +site[7].replace('http://', '') + '">'+site[7].replace('http://', '')+'</a><br>':"") + site[3] + "</h3>";

                var marker = new google.maps.Marker({
                    position: siteLatLng,
                    map: map,
                    title: site[0],
                    html: content,
                    icon: '/css/fastest/icons/slza11.png',
                    animation: google.maps.Animation.DROP
                });

                google.maps.event.addListener(marker, "click", function () {
                    window.infowindow.setContent(this.html);
                    window.infowindow.open(map, this);
                });
            }
        }

        var map;
        var my_canvas = $('map_canvas2');

        var lat = 49.804468;//my_canvas.getProperty('data-lat').toFloat();
        var lng = 15.526189;//my_canvas.getProperty('data-lng').toFloat();
        var z = my_canvas.getProperty('data-zoom').toFloat();

        var myOptions = {
            zoom: 8,
            center: new google.maps.LatLng(lat, lng),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: false,
            scrollwheel: false,
			/*styles: [{
			 "featureType":"water","elementType":"geometry","stylers":[
			 {"color":"#e9e9e9"},{"lightness":17}]},
			 {"featureType":"landscape","elementType":"geometry","stylers":[
			 {"color":"#f5f5f5"},
			 {"lightness":5}]},
			 {"featureType":"road.highway","elementType":"geometry.fill","stylers":[
			 {"color":"#ffffff"},
			 {"lightness":14}]},
			 {"featureType":"road.highway","elementType":"geometry.stroke","stylers":[
			 {"color":"#ffffff"},
			 {"lightness":29},
			 {"weight":0.2}]},
			 {"featureType":"road.arterial","elementType":"geometry","stylers":[
			 {"color":"#ffffff"},
			 {"lightness":18}]},
			 {"featureType":"road.local","elementType":"geometry","stylers":[
			 {"color":"#ffffff"},
			 {"lightness":16}]},
			 {"featureType":"poi","elementType":"geometry","stylers":[
			 {"color":"#f5f5f5"},
			 {"lightness":21}]},
			 {"featureType":"poi.park","elementType":"geometry","stylers":[
			 {"color":"#dedede"},
			 {"lightness":21}]},
			 {"elementType":"labels.text.stroke","stylers":[
			 {"visibility":"on"},
			 {"color":"#ffffff"},
			 {"lightness":16}]},
			 {"elementType":"labels.text.fill","stylers":[
			 {"saturation":36},
			 {"color":"#333333"},
			 {"lightness":40}]},
			 {"elementType":"labels.icon","stylers":[
			 {"visibility":"off"}]},
			 {"featureType":"transit","elementType":"geometry","stylers":[
			 {"color":"#f2f2f2"},
			 {"lightness":19}]},
			 {"featureType":"administrative","elementType":"geometry.fill","stylers":[
			 {"color":"#fefefe"},
			 {"lightness":20}]},
			 {"featureType":"administrative","elementType":"geometry.stroke","stylers":[
			 {"color":"#fefefe"},
			 {"lightness":17},
			 {"weight":1.2}]}]*/
            styles: [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]
        };

        map = new google.maps.Map(my_canvas, myOptions);
        setMarkers(map, sites);

        infowindow = new google.maps.InfoWindow({
            content: "Loading..."
        });


		/*var marker_pos = {
		 'icon' : '/css/fastest/layout/marker.png',
		 'lat' : (i==1)? 49.8813329:56.2510284,
		 'lng' : (i==1)? 17.8771486:43.8151166
		 };

		 var options = {
		 position: new google.maps.LatLng(marker_pos.lat, marker_pos.lng),
		 icon: marker_pos.icon
		 };

		 var marker = new google.maps.Marker(options);
		 marker.setMap(map);	*/

        //var shadow = new Element('div',{'class':'shadow'});
        //my_canvas.inject($('map-holder'));

    });
}


function mobile_menu(){
    window.addEvent('domready',function() {

        var bwidth = $('body').getSize().x;

        if (bwidth < 1050){

            var menu_button = new Element('div',{'id':'mobile_menu'}).set('html','&#xf0c9;').inject($('body'));
            var menu = $('navmenu').clone().inject($('body'));
            menu.setProperty('id','mobile_nav');
            $('navmenu').addClass('none');

            menu_button.addEvent('click',function() {
                menu.set('tween', {duration:200});
                if (!this.hasClass('open')){
                    this.addClass('open');
                    this.set('html','&#xf00d;');
                    $('mobile_nav').tween('left',0);
                } else {
                    this.removeClass('open');

                    this.set('html','&#xf0c9;');

                    $('mobile_nav').tween('left',-500);
                }
            });

        } else {

            if ($('mobile_menu'))
                $('mobile_menu').dispose();
            if ($('mobile_nav'))
                $('mobile_nav').dispose();
        }

        window.addEvent('resize:throttle(500)',function(){
            mobile_menu();
        });
    });
}



function thumb_photo() {
    $$('.cover_img').each(function(item) {
        var src = item.get('data-src');
        item.setStyles({
            'background-image':'url("'+src+'")',
        });
    });
}

function flipcards(){
    window.addEvent('domready',function(){
        if (Browser.ie && Browser.version < 10){

            $$('.flipcard').each(function(flip){
                flip.getElement('.back').fade('hide');
                flip.addEvents({
                    mouseenter:function(e){
                        flip.getElement('.front').fade(0);
                        flip.getElement('.back').fade(1);

                    },
                    mouseleave:function(e){
                        flip.getElement('.front').fade(1);
                        flip.getElement('.back').fade(0);

                    }
                });
            });
        }
    });

}


function page_preloader(){
    window.addEvent('domready', function(){
        //indexOf support for IE8 and below.
        if (!Array.prototype.indexOf){
            Array.prototype.indexOf = function(elt /*, from*/){
                var len = this.length >>> 0;

                var from = Number(arguments[1]) || 0;
                from = (from < 0)
                    ? Math.ceil(from)
                    : Math.floor(from);
                if (from < 0)
                    from += len;

                for (; from < len; from++){
                    if (from in this &&
                        this[from] === elt)
                        return from;
                }
                return -1;
            };
        }

        var bgImg = [];
        var img = [];
        var count = 2;
        var percentage = 0;

        create_preloader();

        $$('*').each(function(item,k){
            //console.log(item.getStyle('background-image'));
            var val = item.getStyle('background-image').replace(/url\(/g,'').replace(/\)/,'').replace(/"/g,'');
            if(item.nodeName != 'SCRIPT')
                var imgVal = item.getProperty('src');
            //console.log(imgVal);

            if(val !== 'none' && val != ''){
                bgImg.push(val)
            }

            if(imgVal !== undefined && img.indexOf(imgVal) === -1){
                img.push(imgVal)
            }
        });
        var imgArray = bgImg.append(img);

        imgArray.each(function(im,k){
            var myImage = Asset.image(im, {
                id: 'myImage',
                title: 'myImage',
                onLoad: load_img
            });
        });

        function create_preloader(){
            var bheight = window.getSize();
            var bheight2 = $('body').getScrollSize().y;
            var preloader_over = new Element('div',{'id':'page_preloader_over'}).inject($('body'));

            if (bheight.x < 1000){
                bheight.x = bheight.x+100;
            }

            var tbg = bheight.y/2-100+'px';
            //console.log(tbg);
            preloader_over.setStyles({
                'position':'absolute',
                'left':0,
                'top':0,
                //'background':'#000',
                'z-index':'1000',
                'width':bheight.x,
                'height':bheight2,
                'background-position':'center '+tbg,
                'opacity':0.9
            });
            var preloader = new Element('div',{'id':'page_preloader'}).inject($('body'));
            preloader.setStyles({
                'position':'absolute',
                'width':'300px',
                'text-align':'center',
                'left':(bheight.x)/2-150,
                'top':bheight.y/2-20,
                'margin':'auto',
                'border':'0px solid red',
                'z-index':'1000'
                //'color':'#fff'
            });
        }
        function destroy_preloader(){
            if ($('page_preloader_over')){
                $('page_preloader_over').fade(0);
                $('page_preloader').fade(0);
            }
            (function(){
                if ($('page_preloader_over')){

                    $('page_preloader_over').dispose();
                    $('page_preloader').dispose();
                }
            }).delay(1000);
        }

        function load_img(){
            //console.log(count / imgArray.length);
            count++;
            percentage = Math.floor(count / imgArray.length * 100);
            if (percentage > 100) percentage = 100;

            if ($('page_preloader'))
                $('page_preloader').set('text',percentage+'/100 %');

            //console.log(percentage);
            if(percentage === 100 || percentage > 100){
                destroy_preloader();
            }
        }
        window.addEvent('load', function(){
            destroy_preloader();

        });
    });
}
function utf8_to_b64( str ) {
    return window.btoa(unescape(encodeURIComponent( str )));
}

function b64_to_utf8( str ) {
    return decodeURIComponent(escape(window.atob( str )));
}

function article_detail(){
    $$('.FstForm').each(function(inner){
        if ($('body').getElement('.contact_form')){
            inner.adopt($('body').getElement('.contact_form'));
        }
    });

}
function preload_links(){

    $('navmenu').getElements('a').addEvent('mousedown',function(e){

        if (!e.control && e.rightClick == false && e.event.button != 1){
            new Event(e).stop();
            if (!this.getProperty('target'))
                window.location = this.href;
            //console.log(e);
        }

    });

}

function placeholder(){
    if(Browser.ie && Browser.version <= 9){
        $$('input').each(function(input){
            if (input.getProperty('placeholder')){
                var holder = input.getProperty('placeholder');

                if (input.value == ''){
                    input.value = holder;
                }
                input.addEvent('click',function(e){
                    if (input.value == holder){
                        input.value = '';
                    }
                });
                input.addEvent('blur',function(e){
                    if (input.value == ''){
                        input.value = holder;
                    }
                });

            }
        });
    }
}


function FstAlert(value){
    new mBox.Notice({
        type: 'ok',
        fadeDuration: 1500,
        position: {
            x: 'center',
            y: 'top'
        },
        content: value
    });
}
function FstError(value){
    new mBox.Notice({
        type: 'error',
        fadeDuration: 1500,
        position: {
            x: 'center',
            y: 'top'
        },
        content: value
    });
}
/** contact form **/
function contact_form(contact_form_id){
    window.addEvent('domready', function(){
        var valid_form = new FormCheck(contact_form_id);
        $('ContactFormSpam').value = 123;
        if ($(contact_form_id+'_send')){

            $('ContactFormSpam').value = 123;
            $(contact_form_id+'_send').removeProperty('disabled');
            $(contact_form_id+'_send').addEvent('click',function(e){

                if (valid_form.isFormValid(contact_form_id) == true){
                    new Event(e).stop();

                    button_preloader($(contact_form_id+'_send'),true);

                    new Request.JSON({
                        url:$(contact_form_id).action,
                        onComplete:function(json){
                            if (!json || json.result == false){
                                FstError('Chyba odeslání formuláře');
                            } else {
                                FstAlert(json.message);
                                $$('.text').each(function(item){
                                    item.value = '';
                                });
                            }
                            button_preloader($(contact_form_id+'_send'));
                        }
                    }).post($(contact_form_id));
                }
            });
        }
    });
}
/** search field */
function search_field(){
    var submitSearch = function(){
        var delka = $('SearchInput').value.length;

        if (delka > 2){
            $('search_formular').submit();
        }
    };

    $('search_btn').addEvent('click', function(e){
        e.stop();
        //$('SearchInput').toggle();
        if($('SearchInput').hasClass('hidden')){
            $('SearchInput').removeClass('hidden').focus();
            $$('body').removeEvents('click');
            $$('body').addEvent('click', function(e){
                if(!e.target.hasClass('search') && e.target.get('id') != 'SearchInput'){
                    $('SearchInput').addClass('hidden');
                }
            });
        }else{
            $('SearchInput').addClass('hidden');
            submitSearch();
        }
    });
    if ($('SearchButton')){
        $('SearchButton').addEvent('click', function(e){
            new Event(e).stop();
            submitSearch();
        });
    }
};

/** ankety */

function polls(poll_id,poll_refresh){
    window.addEvent('domready', function(){
        poll_id.getElements('li').each(function(item){
            var width = item.getElement('.width').get('html');
            var pruh = item.getElement('var');
            pruh.tween('width',width+'%');
            if (item.getElement('a'))
                item.getElement('a').addEvent('click',function(e){
                    new Event(e).stop();
                    new Request.HTML({
                        url:this.href,
                        update:poll_refresh,
                        onComplete:function(){

                        }
                    }).send();
                });
        });
    });
}


function calendar(calendar_id){
    window.addEvent('domready', function(){


        function click_months(e){
            new Event(e).stop();
            $(calendar_id).addClass('preloader');
            new Request.HTML({
                update:calendar_id,
                url:e.target.href+'/'+calendar_id,
                onComplete:function(json){
                    $(calendar_id).removeClass('preloader');

                }
            }).send();
        }

        $(calendar_id).getElements('.month_link').addEvent('click',click_months);

        $(calendar_id).getElements('.point').each(function(point){


            point.addEvent('click',function(e){
                if($('cal_obal')){
                    $('cal_obal').dispose();
                    if ($('body').getElement('.scrollbar')){
                        $('body').getElement('.scrollbar').dispose();
                    }
                }

                if(!$('cal_obal'))
                    var obal = new Element('div',{'id':'cal_obal'}).inject($(calendar_id));
                var obal_in = new Element('div',{'id':'cal_obal2'}).inject(obal);
                var close = new Element('div',{'id':'cal_close'}).set('text','X').inject(obal);

                new Request.JSON({
                    url:'/kalendar-load/'+this.getProperty('data-date'),
                    onComplete:function(json){
                        if (!json){
                            alert('Chyba odeslání formuláře');
                        } else if(json.result == false) {
                            FstError(json.message);
                        } else {
                            var html = '';
                            json.data.each(function(item){
                                var date = item.Calendar.date.split('-');

                                html += '<strong>'+item.Calendar.name+' - '+date[2]+'.'+date[1]+'.'+date[0]+'</strong>';
                                html += '<p>'+item.Calendar.text+'</p>';
                            });
                            obal_in.set('html',html);
                        }
                    }
                }).send();


                if (close){
                    close.addEvent('click',function(e){
                        //console.log(obal);
                        obal.dispose();
                        if ($('body').getElement('.scrollbar')){
                            $('body').getElement('.scrollbar').dispose();
                        }
                    })
                }
                var myScrollable = new Scrollable(obal_in);
            });
        });
    });
}

function tab_slider(params){

    $(params.obal).getElements('.'+params.element_class).each(function(item,k){
        if (k >0){
            if (params.type == 'fade'){
                item.fade('hide');
                item.addClass('close');
            } else {
                item.addClass('none');
            }
        }
    });

    var contents = $(params.obal).getElements('.'+params.element_class);

    if ($(params.obal+'_pagination')){
        $(params.obal+'_pagination').getElement('a').addClass('active');
        $(params.obal+'_pagination').getElements('a').each(function(link,k){
            link.addEvent('click',function(e){
                new Event(e).stop();
                if (!this.hasClass('noclear'))
                    clearInterval(timer);
                else
                    this.removeClass('noclear');
                $(params.obal+'_pagination').getElements('a').removeClass('active');
                this.addClass('active');
                if (params.type == 'fade'){
                    contents.addClass('close');
                    contents.fade(0);
                    if (contents[k]){
                        contents[k].removeClass('close');
                        contents[k].fade(1);
                    }
                } else {
                    contents.addClass('close');
                    contents.addClass('none');
                    if (contents[k]){
                        contents[k].removeClass('close');
                        contents[k].removeClass('none');
                    }
                }
            });
        });

        if (params.next){
            if ($(params.next)){
                $(params.next).addEvent('click',function(e){
                    new Event(e).stop();
                    var list = $(params.obal+'_pagination').getElements('a');
                    var count = $(params.obal+'_pagination').getElements('a').length;
                    var current = $(params.obal+'_pagination').getElement('.active');
                    var active = list.indexOf(current);
                    if(current.getNext('a')){
                        current.getNext('a').addClass('noclear');
                        current.getNext('a').fireEvent('click',current.getNext('a'));
                    } else {
                        list[0].addClass('noclear');
                        list[0].click();
                    }
                });
            }

        }

        if (params.prev){
            if ($(params.prev)){
                $(params.prev).addEvent('click',function(e){
                    new Event(e).stop();
                    var list = $(params.obal+'_pagination').getElements('a');
                    var count = $(params.obal+'_pagination').getElements('a').length;
                    var current = $(params.obal+'_pagination').getElement('.active');
                    var active = list.indexOf(current);
                    if(current.getPrevious('a')){
                        current.getPrevious('a').addClass('noclear');
                        current.getPrevious('a').fireEvent('click',current.getPrevious('a'));
                    } else {
                        list[count-1].addClass('noclear');
                        list[count-1].click();
                    }
                });
            }

        }

        if (params.autorun){
            function autorun(){
                var list = $(params.obal+'_pagination').getElements('a');
                var count = $(params.obal+'_pagination').getElements('a').length;
                var current = $(params.obal+'_pagination').getElement('.active');
                var active = list.indexOf(current);
                if(current.getNext('a')){
                    current.getNext('a').addClass('noclear');
                    current.getNext('a').fireEvent('click',current.getNext('a'));
                } else {
                    list[0].addClass('noclear');
                    list[0].click();
                }
            }
            if (!params.autorun_time) params.autorun_time = 1000;
            var timer = autorun.periodical(params.autorun_time);
        }

    }

}


function getScrollOffsets() {

    // This works for all browsers except IE versions 8 and before
    if ( window.pageXOffset != null )
        return {
            x: window.pageXOffset,
            y: window.pageYOffset
        };

    // For browsers in Standards mode
    var doc = window.document;
    if ( document.compatMode === "CSS1Compat" ) {
        return {
            x: doc.documentElement.scrollLeft,
            y: doc.documentElement.scrollTop
        };
    }

    // For browsers in Quirks mode
    return {
        x: doc.body.scrollLeft,
        y: doc.body.scrollTop
    };
}

function fst_anim(el,options){
	/*
	 var options = {
	 'styles':{
	 'animation-duration':'0.4s',
	 'animation-fill-mode':'both',
	 'animation-name': 'rotateLeft2',
	 'backface-visibility': 'hidden !important',
	 },
	 'keyframes':{
	 'rotateLeft2':{
	 '0%':{'transform-origin': '0 0','transform': 'perspective(800px) rotateY(0deg) translateZ(0px)','transform-origin':'100% 0%'},
	 '100%':{'transform-origin': '50% 0','transform': 'perspective(800px) rotateY(-180deg) translateZ(300px)'}
	 }
	 }
	 }
	 */


    var prefix = ['-webkit-','-moz-','-ms-','-o-',''];
    var styles = {}

    prefix.each(function(pref){
        Object.each(options.styles,function(opt,k){
            //console.log(opt);
            styles[pref+k] = opt;
        });
    });
    el.setStyles(styles);
    //console.log(styles);

    if (options.keyframes){

        var sheet = (function() {
            if (!($('anim-styles'))){
                var style = new Element("style",{'id':'anim-styles'});
                document.head.appendChild(style);
            }
        })();


        function addKeyframe(keyframes){
            Object.each(keyframes,function(opt_frames,frame_name){
                keyframe_all = '';
                prefix.each(function(pref,index){
                    keyframeprefix = pref;
                    keyframe_all += '@' + keyframeprefix + 'keyframes '+frame_name+' { ';

                    Object.each(opt_frames,function(opt,from){
                        keyframe_all += from + ' { ';
                        Object.each(opt,function(opt_value,key){
                            //console.log(k);
                            keyframe_all +=  keyframeprefix + key + ':'+opt_value+';';
                        });
                        keyframe_all +=  ' } ';
                    });

                    //keyframe_all += 'from {' + keyframeprefix + 'transform:rotate( 0deg ) }';
                    //keyframe_all += 'to {' + keyframeprefix + 'transform:rotate( 360deg ) }';

                    keyframe_all += "}\n";
                });
                //console.log(keyframe_all);
                $('anim-styles').set('html',keyframe_all);

            });
        }
        if (options.keyframes)
            addKeyframe(options.keyframes);


    }


}

function resized(run_fce){
    window.addEvent('resize',function(e){
        resized_end();
    });
    var run = true;

    function resized_end(){
        var timer_resize = 0;
        var timer_resize = (function(){
            if (run){
                run = false;
                clearTimeout(timer_resize)
                run_fce();
            }
        }).delay(1000);

    }

    $('body').addEvent('click',function(e){
        window.removeEvents('resize');
        resized(run_fce);
    });


}


function get_inner(cpos,element){
    var pos = element.getPosition();
    var size = element.getSize();
    if ((cpos.x > pos.x && cpos.x < pos.x+size.x) && (cpos.y > pos.y && cpos.y < pos.y+size.y)){
        return true;
    } else {
        return false;
    }
}

var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}


function check_email(email){
    filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (filter.test(email.value)) {
        return true;
    }
    else {
        return false;}
}

function removeClass(el, classNames) {
    if(el && el.className && classNames){
        el.className = el.className.replace(new RegExp("\\b(" + classNames.replace(/\s+/g, "|") + ")\\b", "g"), " ").replace(/\s+/g, " ").replace(/^\s+|\s+$/g, "");
    }
}

function setActiveRow(parentElementId, li, scrollTo, deactiveOnly){
    $$('#'+parentElementId+' li').removeClass('active');
    if(typeof(deactiveOnly) == 'undefined' || deactiveOnly !== true){
        li.addClass('active');
        var additionalInfo = li.getElement('.additional');
        if(additionalInfo){
            additionalInfo.show();
        }
    }
    if(typeof scrollTo != 'undefined' && scrollTo == true){
        var myFx = new Fx.Scroll($(parentElementId)).toElement(li, 'y');
    }
}
function chooseSelectBox(id){
    if(id){
        $$('.selectable-row').set('checked', false);
        $$('.selectable-row').each(function(item){
            // console.log(id == item.value, item.value);
            if(id == item.value){
                item.set('checked', true);
                setActiveRow('seller-list', item.getParent('li'), true);
                return false;
            }
        });
    }
}
function displayInfoWindow(){

}
function initDetailMap(){
    function setMarkers(map, markers) {
        var markerObjects = [];
        for (var i = 0; i < markers.length; i++) {
            var site = markers[i];
            var siteLatLng = new google.maps.LatLng(site[1], site[2]);

            var content = "<h3>" + site[0] + "<br>" + site[5] + "<br><a href='mailto:" + site[4] + "'>"+site[4]+"</a><br>" + (site[7] != '' ? '<a href="http://' +site[7].replace('http://', '') + '">'+site[7].replace('http://', '')+'</a><br>':"") + site[3] + "</h3>";

            var index = markerObjects.push(new google.maps.Marker({
                    position: siteLatLng,
                    map: map,
                    title: site[0],
                    html: content,
                    par_id: site[8],
                    icon: '/css/fastest/icons/slza11.png',
                    animation: google.maps.Animation.DROP
                })) - 1;

            google.maps.event.addListener(markerObjects[index], "click", function () {

                window.infowindow.setContent(this.html);
                window.infowindow.open(map, this);
                chooseSelectBox(this.par_id);
            });
        }
        return markerObjects;
    }

    if($('google-map')){
		/*var lng = 17.3324356;
		 var lat = 48.989267;*/
        var lat = 49.804468;
        var lng = 15.526189;
        var my_canvas = $('google-map');

        var myOptions = {
            zoom: 7,
            center: new google.maps.LatLng(lat ,lng ),
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            disableDefaultUI: false,
            scrollwheel: false,
            styles: [{"featureType":"administrative","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"administrative.province","elementType":"all","stylers":[{"visibility":"off"}]},{"featureType":"landscape","elementType":"all","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","elementType":"all","stylers":[{"saturation":-100},{"lightness":"50"},{"visibility":"simplified"}]},{"featureType":"road","elementType":"all","stylers":[{"saturation":"-100"}]},{"featureType":"road.highway","elementType":"all","stylers":[{"visibility":"simplified"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"lightness":"30"}]},{"featureType":"road.local","elementType":"all","stylers":[{"lightness":"40"}]},{"featureType":"transit","elementType":"all","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]},{"featureType":"water","elementType":"labels","stylers":[{"lightness":-25},{"saturation":-100}]}]

        };

        var markers = $$('.gps-coordinates');


        window.map = new google.maps.Map(my_canvas, myOptions);

        window.infowindow = new google.maps.InfoWindow({
            content: "Loading..."
        });

        if(markers){
            var sites = [];
            markers.each(function(item){
                sites.push(item.value.split('|'));
            });
            window.markerObjects = setMarkers(window.map, sites);
            window.markerCluster = new MarkerClusterer(window.map, window.markerObjects,
                {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m', gridSize: 60, maxZoom: 15});
        }


    }


}



function ImgSlider(wrapperEl){
    if (wrapperEl){
        var timestep = 2000;
        var slider_in = wrapperEl;
        var slider_el = wrapperEl.getElement('div.product-item');
        var slider_count = wrapperEl.getElements('div.product-item').length;
        var slider_over = new Element('div',{'id':'other_imgs_slider'}).inject(wrapperEl,'before');
        var slider_left = new Element('div',{'class':'slider_move fa fa-angle-left'}).inject(slider_over);
        var slider_right = new Element('div',{'class':'slider_move fa fa-angle-right'}).inject(slider_over);

        slider_over.adopt( wrapperEl );

        var max_width = slider_in.getComputedSize().totalWidth;
        //var move_step = slider_in.getElement('a').getStyle('width').toInt();

        var move_step = slider_in.getElement('.product-item').getSize().x;

        slider_in.addClass('mooswipe');

        slider_in.setStyles({
            'position':'absolute',
            'width': move_step * slider_count
        });
        //console.log(slider_in.getCoordinates());

        slider_left.addClass('none');

        if (slider_in.getComputedSize().totalWidth < slider_over.getSize().x){
            slider_right.addClass('none');
        }

        new MooSwipe(slider_in, {
            onSwipeLeft: function() {
                move_right();
            },
            onSwipeRight: function() {
                move_left();
            },
            onSwipe: function(direction) {

            }
        });

        function move_left(move){
            if(typeof move !== 'number'){
                move = (slider_in.getStyle('left') ? slider_in.getStyle('left').toInt() : 0 ) + move_step;
            }

            slider_right.removeClass('none');

            if (move > 0) {
                move = 0;
                slider_left.addClass('none');
            }

            slider_in.tween('left',move);
        }
        function move_right(){
            move = (slider_in.getStyle('left') ? slider_in.getStyle('left').toInt() : 0) - move_step;

            slider_left.removeClass('none');

            if (slider_in.getCoordinates().right < slider_over.getCoordinates().right) {
                slider_right.addClass('none');
                return false;
            }
            slider_in.tween('left',move);
            return true;
        }

        function automove(){
            if(move_right() == false){
                move_left(0);
            }
        }

        if(timestep && timestep > 0){
            var timer = automove.periodical(timestep);
            $$([slider_left, slider_right, slider_in]).addEvent('mouseenter', function(){
                clearInterval(timer);
            });
            $$([slider_left, slider_right, slider_in]).addEvent('mouseleave', function(){
                timer = automove.periodical(timestep);
            });
        }
        slider_left.addEvent('click', move_left);
        slider_right.addEvent('click', move_right);
    }
}




Element.implement({
    setFocus: function(index) {
        this.setAttribute('tabIndex',index || 0);
        this.focus();
    }
});