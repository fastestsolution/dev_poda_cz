<?php

class PagesController extends AppController {

    var $components = array('RequestHandler');
    var $name = 'Pages';
    var $uses = array('Article');

    function clear_cache() {
        clearCache();
        $files = scandir('./app/tmp/cache');
        foreach ($files AS $f) {
            if (strpos($f, 'cake_') !== false) {
                unlink('./app/tmp/cache/' . $f);
            }
        }
        die();
    }
    function sellers(){
        $this->layout = 'nomap';
       
        $this->loadModel('GoogleMap');
        $sellers = $this->GoogleMap->find('all',array('conditions'=>array('status'=>1,'kos'=>0, 'google_map_group_id'=>1),'order'=>'name ASC'));
        $this->set('sellers', $sellers); 
        $this->set('page_caption', 'Prodejci');
        $this->set('fastlinks', array('Prodejci'=>'/prodejci'));
    }
    function hp() {

        $this->loadModel('ShopMenuItem');
        $this->loadModel('ShopProduct');
        $this->loadModel('Actual');
        $aktuality = $this->Actual->find('all', array('conditions' => array('status' => 1, 'kos' => 0), 'orders' => 'id', 'limit' => 2));

        $kategorie = $this->ShopMenuItem->find('all', array('conditions' => array('level' => 0)));
        $tmp = array();
        foreach($kategorie as $item){
            $tmp[$item['ShopMenuItem']['id']] = $item;
        }
        $kategorie = $tmp;

        $this->loadModel('Carousel');
        $carouselTopHp = $this->Carousel->read(null, 1);

        $this->set('carouselTopHp', $carouselTopHp);


        $produkty= array();
        //pr($this);
        if(isset($this->mainpage_categories)){
            foreach ($this->mainpage_categories as $cat_id) {

                $this->ShopProduct->bindModel(array('hasOne' => array(
                    'ShopConnectionMenuProduct' => array('primaryKey' => 'id', 'foreignKey' => 'shop_product_id')
                )));
                $produkty['cat_' . $cat_id] = $this->ShopProduct->find('all', array('conditions' => array(
                    'ShopProduct.status' => 1,
                    'ShopProduct.kos' => 0,
                    'ShopProduct.check_superproduct' => 1,
                    'ShopConnectionMenuProduct.shop_menu_item_id' => $cat_id), 'limit' => 10, 'order' => 'rand()'));
            }
        }

        $this->set('produkty', $produkty);
        $this->set('kategorie', $kategorie);
        $this->set('page_caption', '');
        $this->set('fastlinks', array());
        $this->set('aktuality', $aktuality);
    }

    function isUnique($model, $field, $value = '', $id = null) {
        if (!isset($this->$model)) {
            $this->loadModel($model);
            $this->$model = & new $model();
        }

        $condition = array($model . '.' . $field => $value, $model . '.status' => 1, $model . '.kos' => 0);
        if ($id != null)
            $condition['id'] = '!=' . $id;

        if ($this->$model->findCount($condition))
            die(json_encode(array('return' => false)));
        else
            die(json_encode(array('return' => true)));
    }

    function fst_upload($delete = null) {
        $fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
        //pr($_FILES);
        if (isset($this->params['url']['params'])) {
            $params = json_decode(base64_decode($this->params['url']['params']));
        }

        if ($delete != null) {
            $setting = array(
                'file' => $params->file,
                'upload_path' => $params->upload_path,
            );
            $this->Upload->delete_file($setting);
        } else if (isset($_FILES['file']['type'])) {
            $setting = array(
                'fn' => $fn,
                'files' => $_FILES,
                'filename' => $_FILES['file']['name'][0],
                'tmp_name' => $_FILES['file']['tmp_name'][0],
                'type' => $_FILES['file']['type'][0],
                'file_ext' => $params->file_ext,
                'count_file' => $params->count_file,
                'upload_path' => $params->upload_path,
            );
            $this->Upload->doit($setting);
        } else {
            die(json_encode(array('result' => false, 'message' => 'Chyba nahrání souboru')));
        }
    }
    
    function load_heureka(){
       $this->loadModel('ShopProductVariant');
       $products = $this->ShopProductVariant->find('all', array('fields'=>array('id','shop_product_id','kod'),'conditions'=>array('kos'=>0)));

       $product_list = array();
       foreach($products as $item){
           $product_list[$item['ShopProductVariant']['kod']] = $item['ShopProductVariant']['shop_product_id'];
           /*foreach(explode('|', trim($prodNums, '|')) as $j => $prod_number){
                $product_list[$prod_number] = $id;
           }*/
       }

       $xml = simplexml_load_file("./uploaded/heureka_5.xml") or die("Error: Cannot create object");

       $toSave = array();
       $eans = array();
       foreach($xml->children() as $item){
         $prodNum = (string) $item->PRODUCTNO;

         if(isset($product_list[$prodNum])){
                $id = $product_list[$prodNum];
                if(!isset($toSave[$id])){
                    $toSave[$id] = array();
                }
                if($item->DELIVERY_DATE == 0){
                    $toSave[$id][$prodNum] = 1;
                }else{
                    $toSave[$id][$prodNum] = 2;
                }
                $eans[$id] = (string) $item->EAN;
         }
       }
       
      // pr($eans);
       
       $counter = 0;
       foreach($products as $item){
        
           if(isset($toSave[$item['ShopProductVariant']['shop_product_id']])){
               foreach($toSave[$item['ShopProductVariant']['shop_product_id']] as &$val){
                   if(isset($toSave[$item['ShopProductVariant']['shop_product_id']][$item['ShopProductVariant']['kod']])){
                       $val = $toSave[$item['ShopProductVariant']['shop_product_id']][$item['ShopProductVariant']['kod']];
                   }else{
                       $val = 2;
                   }
               }
               $counter++;
               $this->ShopProductVariant->save(array('id'=>$item['ShopProductVariant']['id'], 'availability'=>$val));
           }
       }
      /* foreach($products as $id => &$prodNums){
           if(isset($toSave[$id])){
               $prodNums = explode('|', trim($prodNums, '|'));
               $prodNums = array_flip($prodNums);
               foreach($prodNums as $prodnum => &$val){
                   if(isset($toSave[$id][$prodnum])){
                       $val = $toSave[$id][$prodnum];
                   }else{
                       $val = 0;
                   }
               }
               $counter++;
               $prodNums = '|'.implode('|', $prodNums).'|';
               $this->ShopProduct->save(array('id'=>$id, 'availabilities'=>$prodNums));
           }
       }*/
       
      
       //pr($toSave);
       die('Nacteno '.$counter.' informaci o dostupnosti z feedu');
    }
    
    function clear_product_description(){
        die(); 
        $this->loadModel('ShopProduct');
        $products = $this->ShopProduct->find('all', array('fields'=>array('id', 'text'),'limit'=>150));
        $pattern= '/<table[^>]*>.*?<\/table>/si';
       
        foreach($products as $prod){
            // echo $prod['ShopProduct']['text'];
            $prod['ShopProduct']['text'] = (preg_replace($pattern,'',$prod['ShopProduct']['text']));
            $this->ShopProduct->save($prod);
        }
        die();
    }
}

?>