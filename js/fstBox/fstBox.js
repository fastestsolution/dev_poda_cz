/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 Fst BOX function
 */
window.debug = true; 
var FstBoxClass = this.FstBoxs = new Class({
	Implements:[Options,Events],
	options: {
		'type':'error', // error, ok
		'fadeDuration':1500,
		'delayClose':2000,
		'noClose':true,
		'positionPadding':{
			x: 0,
			y: 0,
		},
		'position':{
			x:'center',
			y:'top',
		},
		'content':'',
		'elementClassPrefix':'fstBox',
		'event':function(){
			//alert('');
		},
	},
	// init fce
	initialize:function(options){
		if (window.debug) console.log('run fst box');
		this.setOptions(options);
		
		this.createElement();
	},
	
	createElement: function(){
		switch(this.options.type){
			case 'ok':
				type_class = 'ok';
			break;
			case 'error':
				type_class = 'error';
			break;
			default:
				console.log('neni typ');
			break;
		}
		
		this.element = new Element('div',{
			'class':this.options.elementClassPrefix+' '+ type_class + ' alert'
		}).inject($('body'));
		
		this.element_ico = new Element('div',{
			'class':this.options.elementClassPrefix+'_ico'
		}).inject(this.element);
		
		this.element_content = new Element('div',{
			'class':this.options.elementClassPrefix+'_content'
		}).inject(this.element);
		
		this.element_content.set('html',this.options.content);
		
		this.positionElement();
		this.animateElement('show');
		this.closeElement();
		this.elementEvents();
		
	},
	
	positionElement: function(){
		screen = window.getSize();
		elSize = this.element.getSize();
		
		if (this.options.position.x == 'center'){
			posX = screen.x / 2 - elSize.x / 2;
		}
		
		
		
		if (this.options.position.y == 'center'){
			posY = screen.y / 2 - elSize.y / 2;
		}

        if (this.options.position.y == 'top'){
            posY = 0;
        }

        if (this.options.position.x == 'left'){
            posX = 0;
        }

         if (this.options.position.x == 'right'){
             posX = screen.x - elSize.x;
        }

         if (this.options.position.y == 'bottom'){
             posY = screen.y - elSize.y;
        }

		// TODO dodelat pozicovani
		
		
		posY = posY + this.options.positionPadding.y;
		posX = posX + this.options.positionPadding.x;
		
		
		this.element.setStyles({
			top:posY,
			left:posX,
		});
	},
	
	closeElement: function(){
		if (!this.options.noClose){
			(function(){
				this.animateElement('hide');
			}).delay(this.options.delayClose,this);
		}
	},
	
	elementEvents: function(){
		this.element.addEvent('click',function(){
			if (this.options.event){
				this.options.event();
			}
			this.animateElement('hide');
		}.bind(this));
	},
	
	animateElement: function(type){
		if (type == 'show'){
			this.element.fade('hide');
			this.element.set('tween',{
				duration: this.options.fadeDuration
			});
			this.element.fade(1);
		}
		
		if (type == 'hide'){
			this.element.set('tween',{
				duration: this.options.fadeDuration,
				onComplete: function(){
					this.element.destroy();
				}.bind(this)
				
			});
			this.element.fade(0);
			
		}
	},
	

});
/*window.addEvent('domready',function(){
    var FstBox = new FstBoxClass();
    //console.log(this.Fst.test());
});*/
	



