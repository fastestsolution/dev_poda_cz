/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 global pages function
 */
window.debug = true; 
var FstPages = this.FstPages = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		this.init_jquery();
		this.init_delay_scripts();
		this.init_adopt_element();
		this.init_elements_events();
		this.mobile_menu();
		this.header();
		this.thumb_photo();
		this.faq();
		this.sluzby_info();
		this.instructionVideo();
		this.preloaderProgramy();
		this.searchAddressToSession();
		
		if($('arrow_bottom_sipka')) {
			$('arrow_bottom_sipka').addEvent('click', function(e) {
				var myElement = $(document.body);
				var myFx = new Fx.Scroll(myElement).start(0, $(this.getParent('.mainImg')).getSize().y);
			});
		}
		
		$$('.showInfo').addEvent('click', function(e) {
			e.stop();
			
			var wrapper = this.getParent('.wrapper').getElement('.hiddenInfo');
			wrapper.toggleClass('none');
			this.addClass('none');
		});
		
		$('sp22').addEvent('click', function(e) {
			e.stop();
			
			$('searchModal').toggleClass('none');
		});
		
		if($('show-more-text')) {
			$('show-more-text').addEvent('click', function(e) {
				e.stop();
				if(!this.hasClass('active')) {
					this.set('text','Skrýt více informací');
					this.addClass('active');
					$('hidden-text').removeClass('none');
				} else {
					this.set('text','Zobrazit více...');
					this.removeClass('active');
					$('hidden-text').addClass('none');
				}
				
			});
		}
		
		if($('klz')) {
			$('header').toggleClass('active');	
			$('login-section').toggleClass('none');	
			$('drobeckova').toggleClass('hide');
		}
		
		if($('showMoreActuals')) {
			$('showMoreActuals').addEvent('click', function(e) {
				e.stop();
				
				var article = $$('article.none');
				var count = 0;
				
				Object.each(article, function(value,key) {
					count++;
					if(count < 3) {
						value.removeClass('none');
					}
				});
			});
		}

		//if($('formID'))
		//	this.contact_form('ContactForm'+$('formID').get('data-id'));
	},
	
	searchAddressToSession: function(){
		
		if ($$('.searchAddrButton')){
			$$('.clearForm').each(function(item){
				item.value = '';
			});
			$$('.searchAddrButton').addEvent('click',function(e){
				e.stop();
				var value = e.target.getPrevious('.searchAddrType').value;
				var button = e.target;
				var id = $('searchAddr2').get('data-id');
				button_preloader(button);
						
				this.req_form = new Request.JSON({
					url:'/searchAddressSave/'+id+'/'+ value,
					onComplete :(function(json){
						button_preloader(button);
						if (json.result == true){
							window.location = json.url;
							//FstAlert(json.message);
						} else {
							FstError(json.message);
						}						
								
						}).bind(this)
					});
					this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					this.req_form.send();
				//alert($('searchAddr2').value);
			});
		}
	},

    preloaderProgramy: function(){
		if($('wrapper-preloader')){
            window.addEvent('load', function(){
                $('wrapper-preloader').destroy();
            });
		}
    },

    instructionVideo: function(){
        //console.log($('promo_video'));
		if($$('.startVideo')){

			$$('.startVideo').addEvent('click', function(e) {
                e.stop();
                
                new Element('iframe').set('width', '100%').set('height', '100%').set('src', this.get('data-src')).set('id','video').set('frameborder',0).inject($('instructionVideo_frame'));
                
                $('video').set('src', this.get('data-src'));
				$('overlay').addClass('anim');
				$('instructionVideo_frame').addClass('anim');
			});
			
			if($('overlay')) {
				$('overlay').addEvent('click', function() {
					$('instructionVideo_frame').removeClass('anim');
					$('overlay').removeClass('anim');
					$('video').destroy();
				});
				
				if($$('.startVideo .x')){
	                $$('.x').addEvent('click', function() {
	                    $('instructionVideo_frame').removeClass('anim');
	                    $('overlay').removeClass('anim');
	                    $('video').destroy();
	                });
				}
			}
        }
    },
	// init elements events
	init_elements_events: function(){
		// detect screen size
		this.detect_screen_size();
		// form save
		this.form_save_button();
		// contact form
		this.contact_form();
		// google maps
		this.google_maps();
		
		// google charts
		this.google_charts();
		// zoom foto
		this.zoom_foto();
		// load modal
		this.load_modals();
		// hide dorucovaci adresa
		this.hide_dorucovaci(); 
		
		// load google api
		this.load_google_api();

		
		// open nav bar
		this.open_navbar();

        // init autocomplete
        this.init_autocomplete();


	
		// show login element
		this.show_login_element();
	
		// init history
		var opt = {
			complete_fce: function(){
				lazzy_load();
			}
		}
		window.fsthistory = new FstHistory(opt);
		
		
	},
	
	sluzby_info: function() {
		if($('tarify')) {
			$$('.link').addEvent('click', function(e) {
				e.stop();
				
				var tarif = this.getParent('.tarifBox');
				tarif.getElement('.back').removeClass('hide');
			});
			
			$$('.close').addEvent('click', function() {
				var tarif = this.getParent('.tarifBox');
				tarif.getElement('.back').addClass('hide');
			});
		}
		
		if($('scrollTo')) {
			$('scrollTo').addEvent('click', function(e) {
				e.stop();
				
				var myElement = $(document.body);
				var myFx = new Fx.Scroll(myElement).start(0, $('jumpHere').getPosition().y);
			});
		}
		
		if($$('.showMore')) {
			$$('.showMore').addEvent('click', function(e) {
				e.stop();
				
				var parent = this.getParent('li');
				parent.getElement('.back').removeClass('hide');
				
				if(parent.getElement('.back ul').getSize().y > parent.getElement('.back').getSize().y) {
					var s = parent.getElement('.back ul').getSize().y;
					parent.getElement('.back').setStyle('height', s + 80).addClass('dropShadow');
				}
			});
			
			$$('.close').addEvent('click', function() {
				$$('.back').addClass('hide');
			});
		}	
	},
	
	
	faq: function() {
		if($('faq')) {
			$$('.showMoreActual').addEvent('click', function(e) {
				e.stop();
				var parent = this.getParent('.fRow');
				
				parent.toggleClass('active');
				
				if(parent.getElement('.circle').hasClass('color-red')) {
					parent.getElement('.circle').set('text','+');
					parent.getElement('.circle').removeClass('color-red');
				} else {
					parent.getElement('.circle').set('text','-');
					parent.getElement('.circle').addClass('color-red');
				}
				
			});
		}
		
		if($('sluzba-buttons')) {
			$$('#sluzba-buttons li .button').addEvent('click', function(e) {
				e.stop();
				
				$$('#sluzba-buttons li .button').removeClass('active');
				$$('.sluzba-section').addClass('none');
				
				
				
				var section = this.get('data-id');
				$(section).removeClass('none');
				this.addClass('active');
				
				if(section == 'sectionRoaming') {
					google.maps.event.trigger($('map_canvas'), 'resize');
				}
				
				if(section == 'sectionZahranici') {
					google.maps.event.trigger($('map_canvas2'), 'resize');
				}
			});
		}	
	},
	
	thumb_photo: function() {
		$$('.cover_img').each(function(item) {
			var src = item.get('data-src');
			item.setStyles({
				'background-image':'url("'+src+'")',
			});		
		});
	},
	
	header: function() {
		var win = window.getSize().x;
		
		if(win > 800) {
			if($('header_adopt') && $('header_adopt').hasClass('banner')) {
				var height = window.getSize().y - 50;
				
				$('header_adopt').setStyle('height', height);
				
				//$('blur-box').setStyle("height",height);
			}
		}
	},
	
	mobile_menu: function() {
		var bwidth = $('body').getSize().x;
		
		if (bwidth < 1100){	
			var menu_button = new Element('div',{'id':'mobile_menu'}).set('html','&#xf0c9;').inject($('body'));
			var menu = $('navmenu').clone().inject($('body'));
			menu.setProperty('id','mobile_nav');
			//$('navmenu').addClass('none');
			
			menu_button.addEvent('click',function() {
				menu.set('tween', {duration:200});			
				if (!this.hasClass('open')){
					this.addClass('open');
                    this.set('html','&#xf00d;');
					$('mobile_nav').addClass('active');
				} else {
					this.removeClass('open');
                    this.set('html','&#xf0c9;');
					$('mobile_nav').removeClass('active');
				}
			});
		
		} else {
			if ($('mobile_menu'))
				$('mobile_menu').dispose();
			if ($('mobile_nav'))
				$('mobile_nav').dispose();
		}
		
		//window.addEvent('resize:throttle(500)',function(){
		//	mobile_menu(); 
		//});
	},
	
	// zavolani fst Modal
	load_modals: function(){	
		if ($('body').getElement('.fst_modal')){
			var loadModals = Asset.javascript('/js/fstModal/fstModal.js', {
				id: 'script_load_modals',
				onLoad: function(){
				}
			});
		}
	},
	
	// init lazzy load bez scroll
	init_lazy_load: function(){
		$$('.lazy_load').each(function(item){
			//console.log(item);
			lazzy_load(item);
		});
		init_slimbox();
		
	},
		
	// init form helper
	init_form_helper: function(){
		form_helper();
		
	},

	
	// detect screen size
	detect_screen_size: function(){
		window.screen = window.getSize();
	},
	
	// load autocomplete to element
	init_autocomplete: function(){
		//console.log(($('body').getElement('.autocomplete')));
		if ($('body').getElement('.autocomplete')){
			var script_autocomplete = Asset.javascript('/js/fstAutocomplete/FstAutocomplete.js', {

				id: 'script_autocomplete',
				onLoad: function(){
					if (window.debug) console.log('loaded script_autocomplete');
					
					$$('.autocomplete').each(function(el){
						options = {
							'element': el,
							'search_request': el.get('data-url'),
						}
						fstAutocomplete = new FstAutocomplete(options);
						//console.log(el);
					});
				}
			});
		}
		
	},


	
	// hide dorucovaci
	hide_dorucovaci: function(){
		if ($('check-dorucovaci')){
			$('check-dorucovaci').addEvent('click',function(e){
				if (!e.target){
					e.target = e;
				}
				if (e.target.checked){
					$('address_type2').removeClass('none');
					if ($('address_type2').getElement('legend'))
					$('address_type2').getElement('legend').removeClass('none');
					if (!$('address_type2').get('data-height')){
						$('address_type2').set('data-height',$('address_type2').getCoordinates().height + $('address_type2').getStyle('margin-top').toInt() + $('address_type2').getStyle('margin-bottom').toInt());
					}
					$('address_type2').setStyles({
						'height':0,
						'overflow':'hidden',
					});
					$('address_type2').tween('height',$('address_type2').get('data-height'));
					if ($('address_type2').getElement('input').value == $('address_type1').getElement('input').value){
						$('address_type2').getElements('.clear_address').each(function(item){
							item.value = '';
						});
					}
					if ($('user-addresses-1-street'))
					$('user-addresses-1-street').focus();
				} else {
					if (!$('address_type2').get('data-height')){
						$('address_type2').set('data-height',$('address_type2').getCoordinates().height + $('address_type2').getStyle('margin-top').toInt() + $('address_type2').getStyle('margin-bottom').toInt());
					}
					if ($('address_type2').getElement('legend'))
					$('address_type2').getElement('legend').addClass('none');
					$('address_type2').tween('height',0);
					$('address_type2').addClass('none');
				}
			});
			$('check-dorucovaci').fireEvent('click',$('check-dorucovaci'));
		}
	},
	
	
	// open nav bar 
	open_navbar: function(){
		$('body').getElements('nav').each(function(nav){
			if (nav.getElement('.fa')){
				nav.getElements('.fa').addEvent('click',function(e){
					
					if (e.target.hasClass('fa-caret-down')){
						e.target.removeClass('fa-caret-down');
						e.target.addClass('fa-caret-up');
					} else {
						e.target.addClass('fa-caret-down');
						e.target.removeClass('fa-caret-up');
					}
				});
			}
			if (nav.getElement('.active')){
				function show_menu(ul){
					ul.addClass('in');
					if (ul.getPrevious('.fa')){
						ul.getPrevious('.fa').removeClass('fa-caret-down');
						ul.getPrevious('.fa').addClass('fa-caret-up');
					}
				}
				show_menu(nav.getElement('.active').getParent('ul'));
				
				if (nav.getElement('.active').getParent('ul').getParent('ul')){
					show_menu(nav.getElement('.active').getParent('ul').getParent('ul'));
				}
			}
			
			//console.log(nav.getElement('.active'));
		});
		
		// login element show
		if ($('mobile_login')){
			$('mobile_login').addEvent('click',function(e){
				$('login_element').toggleClass('show');
			});
		}
	},
	
	// load zoom foto 
	zoom_foto: function(){
		//console.log($$('.lazy'));
		$$('.lazy').each(function(img){
			//console.log(img);	
			img.removeProperty('src');
			if (img.get('data-nosize') == null){
			
			img.setStyles({
				'width':img.get('data-width')+'px',
				'height':img.get('data-height')+'px'
			});
			
			}
			img.addClass('preloader');
		});
		/*
		if ($('body').getElement('.zoom_foto')){
			
			//var script_slim_box = Asset.javascript('http://scripts.fastesthost.cz/js/slimbox/slimbox.js', {
			var script_slim_box = Asset.javascript('/js/slimbox/slimbox.js', {
				id: 'script_slim_box',
				onLoad: function(){
					console.log('loaded script_slim_box');
				}
			});
		}
		*/
		
		
	},
	
	google_charts: function() {
		if($('chart_firmy_celkem')) {
			var script_googleMap = Asset.javascript('https://www.google.com/uds/?file=visualization&amp;v=1&amp;packages=corechart', {
				id: 'script_googleChart',
				onLoad: function(){
					var script_googleMap = Asset.javascript('https://www.google.com/uds/api/visualization/1.0/40ff64b1d9d6b3213524485974f36cc0/format+en,default+en,ui+en,corechart+en.I.js', {
						id: 'script_googleChartInit',
						onLoad: function(){
							 google.load("visualization", "1", {packages:["corechart"]});
						      google.setOnLoadCallback(drawChart);
						      function drawChart() {
						        var data_firmy_celkem = google.visualization.arrayToDataTable([
						            ['Období', 'Vývoj počtu připojených k internetu – firmy a organizace']
						          ,['1998',  50]
						          ,['1999',  77]
						          ,['2000',  159]
						          ,['2001',  241]
						          ,['2002',  291]
						          ,['2003',  486]
						          ,['2004',  593]
						          ,['2005',  662]
						          ,['2006',  720]
						          ,['2007',  745]
						          ,['2008',  849]
						          ,['2009',  932]
						          ,['2010',  1008]
						          ,['2011',  1074]
						          ,['2012',  1127]
						          ,['2013',  1824]
						          ,['2014',  1911]
						          ,['2015',  1967]
						          ,['2016',  2080]
						          ,['2017',  2218]
						        ]);
						
						        var options_firmy_celkem = {
						          /*title: 'Vývoj počtu připojených k internetu – firmy a organizace'*/
						          backgroundColor: 'none'
						        };
						
						        var chart = new google.visualization.LineChart(document.getElementById('chart_firmy_celkem'));
						        chart.draw(data_firmy_celkem, options_firmy_celkem);
						  
						                var data_domacnosti = google.visualization.arrayToDataTable([
						            ['Období' ,'Celkem','S napojením na optickou síť','Domácnosti s TV službami']
									,['2002',84,0,0]
									,['2003',342,0,0]
									,['2004',1440,0,0]
									,['2005',3209,0,0]
									,['2006',8039,805,0]
									,['2007',10384,2727,0]
									,['2008',11876,6431,403]
									,['2009',13220,9321,1332]
									,['2010',17391,12957,3498]
									,['2011',23901,20118,10365]
									,['2012',28878,24543,15648]
									,['2013',39421,30210,20576]
									,['2014',46068,38348,26490]
									,['2015',52817,44620,31035]
									,['2016',58185,49876,37357]
									,['2017',64335,55990,43000]
									]);
						
						        var options_domacnosti = {
						          /*title: 'Array'*/
						          backgroundColor: 'none'
						        };
						
						        var chart = new google.visualization.LineChart(document.getElementById('chart_domacnosti'));
						        chart.draw(data_domacnosti, options_domacnosti);
						  
						                var data_obrat = google.visualization.arrayToDataTable([
						            ['Období', 'Vývoj obratu za telekomunikační služby (v tis. Kč)']
						          ,['1998',  3445]
						          ,['1999',  4897]
						          ,['2000',  6516]
						          ,['2001',  9809]
						          ,['2002',  13740]
						          ,['2003',  20750]
						          ,['2004',  32076]
						          ,['2005',  42141]
						          ,['2006',  62359]
						          ,['2007',  81805]
						          ,['2008',  91303]
						          ,['2009',  98675]
						          ,['2010',  116882]
						          ,['2011',  147003]
						          ,['2012',  164310]
						          ,['2013',  259587]
						          ,['2014',  276338]
						          ,['2015',  305348]
						          ,['2016',  329976]
						          ,['2017',  352000]
						        ]);
						
						        var options_obrat = {
						          /*title: 'Vývoj obratu za telekomunikační služby (v tis. Kč)'*/
						          backgroundColor: 'none'
						        };
						
						        var chart = new google.visualization.LineChart(document.getElementById('chart_obrat'));
						        chart.draw(data_obrat, options_obrat);
						    }
						}
					});
				}
			});
		}
	},
	
	// load google maps 
	google_maps: function(){
		if ($('load_google_map')){
			var script_googleMap = Asset.javascript('http://maps.googleapis.com/maps/api/js?language=cz&key=AIzaSyBlV8SDwz01_bsreZwbzUOCLJtTuFDrumE', {
				id: 'script_googleMap',
				onLoad: function(){
					function setMarkers(map, markers) {
						for (var i = 0; i < markers.length; i++) {
							var site = markers[i];
							var siteLatLng = new google.maps.LatLng(site[1], site[2]);
							
							var content = "<h3>"+site[0]+"</h3><p>"+site[3]+"</p>"; 
							
							var marker = new google.maps.Marker({
								position: siteLatLng,
								map: map,
								title: site[0],
								//zIndex: site[3],
								html: content,
								icon: '/css/layout/icons/'+site[4],
								animation: google.maps.Animation.DROP
							});
							
							google.maps.event.addListener(marker, "click", function () {
								infowindow.setContent(this.html);
								infowindow.open(map, this);
							});
						}
					}
						
					var map;
					var my_canvas = $('map_canvas');
					
					var lat = 49.802459;//my_canvas.getProperty('data-lat').toFloat();
					var lng = 17.625279;//my_canvas.getProperty('data-lng').toFloat();
					var z = 10;
					
					var myOptions = {
						zoom: 8,
						center: new google.maps.LatLng(lat, lng),
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						disableDefaultUI: false,
						scrollwheel: false
					};
					
					var sites = [
						['Sídlo Ostrava', 49.830749, 18.284919, '28. října 3348/65, Ostrava','blue-marker.png'],
					];
			
					map = new google.maps.Map(my_canvas, myOptions);
					setMarkers(map, sites);
					
					infowindow = new google.maps.InfoWindow({
						content: "Loading..."
					});
					
					if($('map_canvas2')) {
						var map;
						var my_canvas = $('map_canvas2');
						
						var lat = 49.802459;//my_canvas.getProperty('data-lat').toFloat();
						var lng = 17.625279;//my_canvas.getProperty('data-lng').toFloat();
						var z = 10;
						
						var myOptions = {
							zoom: 8,
							center: new google.maps.LatLng(lat, lng),
							mapTypeId: google.maps.MapTypeId.ROADMAP,
							disableDefaultUI: false,
							scrollwheel: false
						};
						
						var sites = [
							['Sídlo Ostrava', 49.830749, 18.284919, '28. října 3348/65, Ostrava','blue-marker.png'],
						];
				
						map = new google.maps.Map(my_canvas, myOptions);
						setMarkers(map, sites);
						
						infowindow = new google.maps.InfoWindow({
							content: "Loading..."
						});
					}
					
					if($('kontakt-google-map')) { 
						var map;
						var my_canvas = $('map_canvas');
						
						var lat = 49.898685;//my_canvas.getProperty('data-lat').toFloat();
						var lng = 15.528496;//my_canvas.getProperty('data-lng').toFloat();
						var z = 6;
						
						var myOptions = {
							zoom: 7,
							center: new google.maps.LatLng(lat, lng),
							mapTypeId: google.maps.MapTypeId.ROADMAP,
							disableDefaultUI: false,
							scrollwheel: false
						};
						
						var sites = [
							['Sídlo společnosti', 49.831362, 18.277610, '28. října 1168/102, 702 00<br />Ostrava - Moravská Ostrava<br /><br /><label>Pondělí, středa:</label> 8.00 - 17.00 hod.<br /><label>Úterý, čtvrtek, pátek:</label> 8.00 - 16.00 hod.','red-marker.png'],
							['Pobočka PODA a. s. (Praha)', 50.050392, 14.436126, 'Budova City Tower<br />Hvězdova 1716/2b<br />140 78 Praha 4','red-marker.png'],
							['Pobočka PODA a. s. (Brno)', 49.200992, 16.644621, 'Gajdošova 4392/7<br />615 00 Brno-Židenice<br /><br /><label>Pondělí, středa:</label> 8.00 - 12.00 hod., 12.30 - 17.00 hod.','red-marker.png'],
							['Pobočka PODA a. s. (Znojmo)', 48.862058, 16.044249, 'Pražská 1653/30<br />669 02 Znojmo<br /><br /><label>Pondělí, středa:</label> 9.00 - 17.00 hod.<br /><label>Úterý, čtvrtek, pátek:</label> 9.00 - 1600 hod.','red-marker.png'],
							['Pobočka PODA a. s. (Třeboň)', 49.004701, 14.754477, 'Boženy Němcové 817/II<br />379 01 Třeboň<br /><br /><label>Pondělí–pátek:</label> 9.00 - 16.00 hod.','red-marker.png'],
							
							//['Klientské centrum Ostrava', 49.831539, 18.277068, 'PODA a.s., 28. října 1168/102<br />702 00 Ostrava-Moravská Ostrava<br /><br /><label>Pondělí, středa:</label> 8.00 - 17.00 hod.<br /><label>Úterý, čtvrtek, pátek:</label> 8.00 - 16.00 hod.','blue-marker.png'],
							['Klientské centrum Ostrava-Jih', 49.785158, 18.257841 , 'PODA a.s., Dr. Martínka 1508/3<br />700 30 Ostrava-Jih – Hrabůvka<br /><br /><label>Pondělí, středa:</label> 9.00 - 12.00 hod., 12.30 - 17.00 hod.<br /><label>Úterý, čtvrtek, pátek:</label> 9.00 - 12.00 hod., 12.30 - 16.00 hod.','blue-marker.png'],
							['Klientské centrum Ostrava-Poruba',  49.832756, 18.177363, 'PODA a.s., Opavská 1140/40<br />708 00 Ostrava-Poruba<br /><br /><label>Pondělí, středa:</label> 9.00 - 12.00 hod., 12.30 - 17.00 hod.<br /><label>Úterý, čtvrtek, pátek:</label> 9.00 - 12.00 hod., 12.30 - 16.00 hod.','blue-marker.png'],
							['Klientské centrum Havířov', 49.781892, 18.426966, 'PODA a.s., Hlavní třída 171/52<br />736 01 Havířov-Město <br /><br /><label>Pondělí, středa:</label> 9.00 - 17.00 hod.<br /><label>Úterý, čtvrtek, pátek:</label> 9.00 - 16.00 hod.','blue-marker.png'],
							['Klientské centrum Bohumín', 49.904503, 18.362821, 'PODA a.s., Studentská 753<br /> <br /><br /><label></label><br /><label></label>','blue-marker.png'],
							['Klientské centrum Karviná', 49.858641, 18.540467, 'PODA a.s., tř. Osvobození 1720/11<br />735 06 Karviná <br /><br /><label>Pondělí, středa:</label> 9.00 - 12.00 hod., 12.30 - 17.00 hod.<br /><label>Čtvrtek:</label> 9.00 - 12.00 hod., 12.30 - 16.00 hod.','blue-marker.png'],
							['Klientské centrum Orlová', 49.870517, 18.423286 , 'PODA a.s., Masarykova třída 1301<br />735 14 Orlová-Lutyně <br /><br /><label>Pondělí:</label> 9.00 - 12.00 hod., 12.30 - 17.00 hod.<br /><label>Čtvrtek:</label> 9.00 - 12.00 hod., 12.30 - 16.00 hod.','blue-marker.png'],
							//['Klientské centrum Brno', 49.200999, 16.644718, 'PODA a.s., Gajdošova 4392/7<br />615 00 Brno-Židenice <br /><br /><label>Pondělí, středa:</label> 8.00 - 12.00 hod., 12.30 - 17.00 hod.','blue-marker.png'],
							['Klientské centrum Praha Jarov', 50.092506, 14.491945 , 'PODA a.s., Koněvova 2496/223,<br />130 00 Praha 3 - Žižkov <br /><br /><label>Pondělí, středa:</label> 9.00 - 12.00 hod., 12.30 - 17.00 hod.','blue-marker.png'],
							['Klientské centrum Praha Ládví', 50.125416, 14.459484, 'PODA a.s., nadzemní část vestibulu metra Ládví, Střelničná,<br />182 00 Praha 8 - Kobylisy <br /><br /><label>Úterý, čtvrtek:</label> 9.00 - 12.00 hod., 12.30 - 17.00 hod.','blue-marker.png'],
							['Klientské centrum Praha Stodůlky', 50.045345, 14.332104 , 'PODA a.s., Zázvorkova 23/1,<br />155 00 Praha 13 - Stodůlky <br /><br /><label>Úterý, čtvrtek:</label> 9.00 - 12.00 hod., 12.30 - 17.00 hod.','blue-marker.png'],
							//['Klientské centrum Znojmo',  48.862072, 16.044228, 'PODA a.s., Pražská 1653/30<br />669 02 Znojmo <br /><br /><label>Pondělí, středa:</label> 9.00 - 17.00 hod.<br /><label>Úterý, čtvrtek, pátek:</label> 9.00 - 1600 hod.','blue-marker.png'],
							//['Klientské centrum Třeboň', 49.004638, 14.754370 , 'PODA a.s., Boženy Němcové 817/II<br />379 01 Třeboň <br /><br /><label>Pondělí–pátek:</label> 9.00 - 16.00 hod.','blue-marker.png'],
						];
				
						map = new google.maps.Map(my_canvas, myOptions);
						setMarkers(map, sites);
						
						infowindow = new google.maps.InfoWindow({
							content: "Loading..."
						});
					}
					
				}
			});
		}
		
	},
	
	// load contact form 
	contact_form: function(){ 
		$$('.load_contact_form').each(function(el_inject){ //alert('');
			el_inject.addClass('preloader');
			this.req_form = new Request.HTML({
				url:'/loadContactForm/'+el_inject.get('data-type'),
				update: el_inject,
				onError: this.req_error = (function(data){
				 
				}).bind(this),

				onComplete :(function(html){
					el_inject.removeClass('preloader');
					this.form_save_button();
					
					if(el_inject.get('data-url')) {
						$('ContactFormText1Value').value = el_inject.get('data-url');
						$('ContactFormText1Value').addClass('none');
					}
					
					if($('technik')) {
						$('technik').addEvent('click', function() {
							if(this.hasClass('blue')) {
								$('ContactFormText5Value').checked = false;
								this.removeClass('blue');
							} else {
								$('ContactFormText5Value').checked = true; 
								this.addClass('blue');
							}
							
						});
					}
					
					if($('PickedTarif')) {
						$('PickedTarif').inject($('tarify')).removeClass('none');
						
						$('ContactFormText10Value').value = $('PickedTarif').get('data-name');
						$('ContactFormText10Value').addClass('none');
					}
					
					if($('continue')) {
						$('continue').addEvent('click', function(e) {
							e.stop();
							
							if($('ContactFormAgree').checked) {
								if($('ContactFormText0Value').value != '' && $('ContactFormText1Value').value != '' && $('ContactFormName').value != '') {
									$('section-1').addClass('none');
									$('section-2').removeClass('none');
								} else {
									var myElement = $(document.body);
									var myFx = new Fx.Scroll(myElement).start(0, $('ContactFormText0Value').getPosition().y - 100);
									
									FstError('Musíte vyplnit jméno, email a telefon');
								}
							} else {
								FstError('Musíte souhlasit s obchodními podmínkami');
							}
						});
					}
						
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
		}.bind(this));
	},
	
	// SaveForm button click
	form_save_button: function(){
			$$('.reset_password').addEvent('click',function(e){
				$$('.reset_password').each(function(item){
					item.value = '';
				});
				$$('.reset_password2').each(function(item){
					item.value = '';
				});
			});
			
			$$('.SaveForm').each(function(item){
				if (item.getParent('form')){
					item.getParent('form').getElements('.float, .integer').inputLimit();
					if (window.screen.x < 800){
						item.getParent('form').getElements('.integer').each(function(item){
							item.set('type','number');
						});
					}
				}
			});
			
			$$('.SaveForm').removeEvents('click');
			$$('.SaveForm').addEvent('click',function(e){
				e.stop();
				
				//if($('ContactFormAgree')) 
				
				if($('ContactFormAgree')) {	
					if($('ContactFormAgree').checked) {
						button = e.target;
						button_preloader(button);
						
						var form = e.target.getParent('form');
						if (!form.getElement('.spam')){
							new Element('input',{'type':'hidden','value':123,'name':'spam','class':'spam'}).inject(form);
						} else {
							$('ContactFormSpam').value = 123;
						}
						
						this.req_form = new Request.JSON({
							url:form.get('action'),
							onError: this.req_error = (function(data){
								button_preloader(button);
							}).bind(this),
		
							onComplete :(function(json){
								button_preloader(button);
								if (json.r == true){
									FstAlert(json.m);
									
									// pokud je json.clear vycisti formular
									if (json.clear){
										form.getElements('.input').each(function(item){
											item.value = '';
										});
									}
									
									// pokud je json.redirect presmeruj
									if (json.redirect){
										(function(){
											if (json.redirect == 'self'){
												window.location = window.location;
											} else {
												window.location = json.redirect;
											}
										}).delay(1000);
									}
									
									// pokud je gopay vloz url do formulare
									if (json.gopay && $('gopay-payment-button')){
										window.FstShop.show_gopay(json.gopay);
									}
									
									
								} else {
									FstError(json.m);
									if (json.invalid){
										$$('.invalid').removeClass('invalid');
										json.invalid.each(function(item){
											//console.log(item);
											item = item.replace('_','-');
											if($(item)){
												$(item).addClass('invalid');	
											}
										});
									}
								}
								
							}).bind(this)
						});
						this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
						this.req_form.post(form);
					} else {
						FstError('Musíte souhlasit s podmínkami');
					}
				} else {
					button = e.target;
					button_preloader(button);
					
					var form = e.target.getParent('form');
					if (!form.getElement('.spam')){
						new Element('input',{'type':'hidden','value':123,'name':'spam','class':'spam'}).inject(form);
					} else {
						$('ContactFormSpam').value = 123;
					}
					
					//alert($('ContactFormSpam').value);
					
					this.req_form = new Request.JSON({
						url:form.get('action'),
						onError: this.req_error = (function(data){
							button_preloader(button);
						}).bind(this),
	
						onComplete :(function(json){
							button_preloader(button);
							if (json.r == true){
								FstAlert(json.m);
								
								// pokud je json.clear vycisti formular
								if (json.clear){
									form.getElements('.input').each(function(item){
										item.value = '';
									});
								}
								
								// pokud je json.redirect presmeruj
								if (json.redirect){
									(function(){
										if (json.redirect == 'self'){
											window.location = window.location;
										} else {
											window.location = json.redirect;
										}
									}).delay(1000);
								}
								
								// pokud je gopay vloz url do formulare
								if (json.gopay && $('gopay-payment-button')){
									window.FstShop.show_gopay(json.gopay);
								}
								
								
							} else {
								FstError(json.m);
								if (json.invalid){
									$$('.invalid').removeClass('invalid');
									json.invalid.each(function(item){
										//console.log(item);
										item = item.replace('_','-');
										if($(item)){
											$(item).addClass('invalid');	
										}
									});
								}
							}
							
						}).bind(this)
					});
					this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					this.req_form.post(form);
				}
			});
	},
	
	// show / hide login element
	show_login_element: function(){
		
		if ($('login_element') && $('login_element').getElement('.show_login') && $('show_login_el')){
			var height = $('show_login_el').getDimensions().height;
			$('show_login_el').set('data-height',height);
			$('show_login_el').addClass('close');
			$('show_login_el').addClass('overflow');
			$('show_login_el').setStyle('height',0);
			
			
			// click show
			$('login_element').getElements('.show_login').addEvent('click',function(e){
				e.stop();
				if ($('show_login_el').hasClass('close')){
					$('show_login_el').removeClass('close');
					$('show_login_el').tween('height',$('show_login_el').get('data-height'));
				} else {
					$('show_login_el').addClass('close');
					$('show_login_el').tween('height',0);

				}
			});
			
		}
	},
	
	// init delay scripts
	init_delay_scripts: function(){
		var txt = 'text/delayscript';
		
		document.getElements("script").each(function(script){
			if (script.get('type').toLowerCase() == txt){
				new Element('script',{'type':'text/javascript','text':script.get('html')}).inject($('addon'));
				script.destroy();
			}
		});
	},
	
	// init adopt element in body by class .el_adopt with data-el
	init_adopt_element:function(){
		// element adopt with class .el_adopt and data-el
		if($('body').getElements('.el_adopt')) {
			$('body').getElements('.el_adopt').each(function (item) {
				$(item.get('data-el')).adopt(item);
			});
		}
		
		// hide template elements TEMPO
		$$('.hide_template').each(function (item) {
			item.setStyle('display','none');
			item.removeClass('hide_template');
		});
		
		// hide specific element
		$$('.hide_element').addEvent('change',function(e){ 
			if ($(e.target.get('data-hide'))){
				if (e.target.value == ''){
					$(e.target.get('data-hide')).addClass('none');	
				} else {
					
					$(e.target.get('data-hide')).removeClass('none');
				}
			}
		});
	},
	
	init_mooswipe: function(fce){
		//if (this.detect_mobile()){
			var script_swipe = Asset.javascript('/js/MooSwipe/MooSwipe.js', {
				id: 'script_jquery',
				onLoad: function(){
					//console.log('load MooSwipe');
					fce();
				}
			});
		//}
		
	},
	
	
	// init jquery 
	init_jquery: function(){
		var script_jquery = Asset.javascript('http://scripts.fastesthost.cz/js/jquery/jquery.js', {
			id: 'script_jquery',
			onLoad: function(){
				if (window.debug) console.log('jquery is loaded!');
				var script_bootstrap = Asset.javascript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', {
					id: 'script_bootstrap',
					properties: {
						integrity: 'Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa',
						crossorigin: 'anonymous',
					},
					onLoad: function(){
						if (window.debug) console.log('script_bootstrap is loaded!');

						var script_jquery = Asset.javascript('/js/jquery_script.js', {
							id: 'script_jquery',
							onLoad: function(){
								
								if (this.debug) console.log('load script_jquery');
								
											
								// init form helper
								this.init_form_helper();
					
					
								// init lazy load to element
								this.init_lazy_load();
								
							}.bind(this)
						});
				
					}.bind(this)
				});
				
				
			}.bind(this)
		});
	},
	
	// detect is mobile
	detect_mobile: function(){
		var platform = [
			'android',
			'ios',
			'webos',
		];
		if (platform.contains(Browser.platform)){
			return true;
		} else {
			return false;
		}
	},
	
	
	// init google api autocomplete	
	init_google_autocomplete: function(){
        //this.sentAdress();
		$$('.google_autocomplete').each(function(item){
			item.addEvent('change',function(e){
				value = e.target.value.split(' ');
				if (value[1]){
					cp_user = value.getLast();
				} else { 
					cp_user = '';
				}
				
			});

			var options = {componentRestrictions: {country: 'cz'}, radius:20};
			var autocomplete = new google.maps.places.Autocomplete(item,options);
			google.maps.event.addListener(autocomplete, 'place_changed', (autocomplete_change = function() {
					var place = autocomplete.getPlace();
					
					if (!place.geometry) {
					  return;
					}
					ulice = '';	
					mesto = '';	
					psc = '';	 
					cp = '';	
					co = '';
					lat = '';
					lng = '';
					ctvrt = '';
					if (place.geometry){
						if (place.geometry.location){
							lat = place.geometry.location.lat();
							lng = place.geometry.location.lng();
						}
					}	
						
					Object.each(place.address_components, function(gitem){
					//console.log(gitem);
						//console.log(item);
						console.log(gitem.types[0]);
						console.log(gitem);

						switch(gitem.types[0]){
							case "street_address":

								ulice = gitem.short_name;
								break;
							case "route":
								ulice = gitem.short_name;
								break;
							case "premise":
								cp = gitem.short_name;
								break;
							case "street_number":
								co = gitem.short_name;
								break;

                            case "administrative_area_level_2":
                            	//console.log('mesto', mesto);
                                if(mesto == ""){
                                    mesto = gitem.short_name;
								}
                                break;
                            case "locality":
                                mesto = gitem.short_name;
                                break;
                            case "sublocality_level_1":
                                ctvrt = gitem.short_name;
                                break;

                            case "lat":
								lat = gitem.short_name;
								break;
                            case "lng":
                                lng = gitem.short_name;
                                break;
                            case "postal_code":
                                psc = gitem.short_name;
                                break;
						}
						if (cp == ''){
							//cp = cp_user;
						}
						if ($(item.get('data-mesto'))) $(item.get('data-mesto')).value = mesto;
						if ($(item.get('data-psc'))) $(item.get('data-psc')).value = psc;
						if ($(item.get('data-cp'))) $(item.get('data-cp')).value = cp+co;
						if ($(item.get('data-ulice'))) $(item.get('data-ulice')).value = ulice;
						if ($(item.get('data-lat'))) $(item.get('data-lat')).value = lat;
						if ($(item.get('data-lng'))) $(item.get('data-lng')).value = lng;
						this.adress_data = {
							'street': ulice,
							'locality': mesto,
                            'sublocality_level_1': ctvrt,
                            'cp': cp,
                            'co': co,
							'postal_code': psc,
							'lat': lat,
							'lng': lng,
						}

					}.bind(this));
					this.sentAdress();
				}.bind(this)
			));
			//console.log(item);
		
		
		}.bind(this)); 

		
	},

	sentAdress: function(){
        this.req_sent = new Request.JSON({
            url:'/searchService/',//routa v controleru
			data: this.adress_data,
            onError: this.req_error = (function(data){
                //button_preloader(button);
            }).bind(this),

            onComplete :(function(json){
			console.log(json);
                //button_preloader(button);
                if (json.result == false){
                    FstError(json.message);
					/*
                    json.data.each(function(item){
						console.log(item);
					});
					*/
				} else {
                    FstAlert('Seznam služeb');
					//console.log(json.message2);

                }

            }).bind(this)
        });
        this.req_sent.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
        this.req_sent.send();

	},
	
	// load google api
	load_google_api: function(){
			function loadScriptMap(){
			  var script = document.createElement("script");
			  script.type = "text/javascript";
			  //script.src = "http://maps.googleapis.com/maps/api/js?&libraries=places&sensor=false&callback=initialize";
			  script.src = "http://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&sensor=false&callback=Fst.init_google_autocomplete&key=AIzaSyA1RBCFRN1GMntXFALQznWGQ5_-EPvcS20";
			  document.body.appendChild(script);
			 
			}
			if ($('body').getElement('.google_autocomplete')){
				loadScriptMap();
			}
			
		
		//});
	}
	
	
	
	
	
});
window.addEvent('domready',function(){
	Fst = new FstPages();
	//console.log(this.Fst.test());
});
	



