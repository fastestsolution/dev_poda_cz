/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 */
 
function fstInit(){
	console.log('a');
}	

function news() {
	window.addEvent('domready', function() {
		$$('.showMoreActual').addEvent('click', function(e) {
			e.stop();
			
			var actual = this.getParent('article');
			
			actual.toggleClass('active');
			actual.getElement('.hidden-section').toggleClass('none');
		});
	});
}

function contact_form(contact_form_id){	
	window.addEvent('domready', function(){
	var valid_form = new FormCheck(contact_form_id);
	$('ContactFormSpam').value = 123;
	if ($(contact_form_id+'_send')){
			
			$('ContactFormSpam').value = 123;
			$(contact_form_id+'_send').removeProperty('disabled');
			$(contact_form_id+'_send').addEvent('click',function(e){
				
				if (valid_form.isFormValid(contact_form_id) == true){
					new Event(e).stop();
				
					button_preloader($(contact_form_id+'_send'),true); 
					
					new Request.JSON({
						url:$(contact_form_id).action,	
						onComplete:function(json){
							if (!json || json.result == false){
								FstError('Chyba odeslání formuláře');
							} else {
								FstAlert(json.message);
								$$('.text').each(function(item){
									item.value = '';
								});
							}	
							button_preloader($(contact_form_id+'_send'));
						}
					}).post($(contact_form_id));		
				} 
			});
	}
	});
}

function header() {
	window.addEvent('domready', function() {
		/*$('sp5').addEvent('click', function(e) {
			e.stop();
			
			$('header').toggleClass('active');	
			$('login-section').toggleClass('none');	
			$('drobeckova').toggleClass('hide');
		});*/
		
		$$('#blur-box .show').addEvent('click', function(e) {
			e.stop();
			
			//$$('.line').removeClass('active');
			//$$('.findServices').addClass('hidden');
			
			this.getParent('.line').getElement('.findServices').toggleClass('hidden');
			this.getParent('.line').toggleClass('active');
		});
		
		$$('.domacnost').addEvent('click', function(e) {
			e.stop();
			
			if(this.hasClass('highway')) {
				this.getParent('.buttons').addClass('none');
				$('highway').removeClass('none');
			} else {
				this.getParent('.buttons').addClass('none');
				this.getParent('.findServices').getElement('.searchPoda').removeClass('hide');
			}
		});
	});
}

// preloader element
function el_preloader(el,class_name){
	//console.log(el);
		if(typeof el.nodeName != 'undefined'){
			elData = [];
			elData.push(el);
		} else {
			elData = el;
		}
		//console.log(elData);
		//console.log(class_name);
		if (!class_name) class_name = 'el_preloader';
		
		elData.each(function(item){
			if (item.hasClass(class_name)){
				item.removeClass(class_name);
			} else {
				
				item.addClass(class_name);
			}
		});
}
 
/** PRICE FORMAT */
function price(price,params){
	if (!params){
	params = {
		'symbol_before':'',
		'kurz':'1',
		'count':'1',
		'decimal': 2,
		'symbol_after':',-'
	}
	}
	price = (price/params.kurz)* params.count;		
	return params.symbol_before + number_format(price, params.decimal, '.', ' ') + params.symbol_after;	
}

/** GET SCROLL OFFSET DATA*/
function getScrollOffsets() {

    // This works for all browsers except IE versions 8 and before
    if ( window.pageXOffset != null ) 
       return {
           x: window.pageXOffset, 
           y: window.pageYOffset
       };

    // For browsers in Standards mode
    var doc = window.document;
    if ( document.compatMode === "CSS1Compat" ) {
        return {
            x: doc.documentElement.scrollLeft, 
            y: doc.documentElement.scrollTop
        };
    }

    // For browsers in Quirks mode
    return { 
        x: doc.body.scrollLeft, 
        y: doc.body.scrollTop 
    }; 
}

/** DATE PICKER */
function date_picker(){
	//window.addEvent('domready', function() {
	if ($$('.date_range').length>0){
	var picker = new Picker.Date.Range($$('.date_range'), {
		timePicker: false,
		columns: 3,
		months: lang.mesice, 
		months_title: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		shortDate: '%Y-%m-%d',
		dateOrder: ['year', 'month','date'],
		shortTime: '%H:%M:%S',
		format:'%Y-%m-%d',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	}
	if ($$('.date').length>0){
	var picker = new Picker.Date($$('.date'), {
		timePicker: false,
		columns: 1,
		//months: lang[CURRENT_LANGUAGE].mesice, 
		//'abbr',{
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		//},
		//shortDate: '%d-%m-%Y',
		//dateOrder: ['date', 'month','year`'],
		//shortTime: '%H:%M:%S',
		format:'%d.%m.%Y',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	
	}
	if ($$('.date_time').length>0){
	$$('.date_time').each(function(item){
		if (item.value == '0000-00-00 00:00:00')
			item.value = '';
	});
	
	var picker = new Picker.Date($$('.date_time'), {
		
		timePicker: true,
		columns: 1,
		months: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
			//months_title: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
			//days_abbr: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
		
		
		shortDate: '%d-%m-%Y',
		dateOrder: ['date', 'month','year'],
		shortTime: '%H:%M:%S',
		format:'%d.%m.%Y %H:%M',
		allowEmpty:true,
		
		//months_abbr: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
		
		positionOffset: {x: 5, y: 0}
	});
	}
	
	//});
}

// check outside element
function outsideElement(element,e){
	 var isClickInside = element.contains(e.target);
	 if (!isClickInside){
		 
		 return false;
	 } else {
		 return true;
	 }
}

/** FASTEST OWN ALERT */
function FstAlert(value,vibrate,modal){
	if (vibrate){
		if (navigator.vibrate)
		navigator.vibrate(50); 
	}
	
	var FstBox = new FstBoxClass({
		'type':'ok',
		'fadeDuration':1500,
		'delayClose':2000,
		'noClose':true,
		'position':{
			x:'center',
			y:'center',
		},
		'content':value,
		
		
	});
	
}

/** FASTEST OWN ERROR */
function FstError(value,modal){
	var FstBox = new FstBoxClass({
		'type':'error',
		'fadeDuration':1500,
		'delayClose':2000,
		'noClose':true,
		'position':{
			x:'center',
			y:'center',
		},
		'content':value,
		
		
	});
}

/** GYROSCOPE **/
function fst_gyro(){
	window.addEvent('domready',function(){
		   var background = $('test');
		   var test_move = $('test_move');
	background.setStyles({
		'background-size':($('body').getSize().x+20)+'px '+($('body').getSize().y+20)+'px' 
		//'background-size':0 
	});
	centerTop = $('body').getSize().y / 2 - test_move.getSize().y/2;
	centerLeft = $('body').getSize().x / 2 - test_move.getSize().x/2;

	maxX = $('body').getSize().x;
	maxY = $('body').getSize().y;

	test_move.setStyles({
		'top':$('body').getSize().y / 2 - test_move.getSize().y/2,
		'left':$('body').getSize().x / 2 - test_move.getSize().x/2,
	});

	move_stepX = window.getSize().x / test_move.getSize().x;
	move_stepY = window.getSize().y / test_move.getSize().y;

	elW = test_move.getSize().x;
	elH = test_move.getSize().y;

	console.log(move_stepX);
	console.log(move_stepY);
	window.addEventListener('deviceorientation', function(eventData) {
	  // Retrieving the front/back tilting of the device and moves the
	  // background in the opposite way of the tilt
	$('gx').set('text',Math.round(eventData.alpha));
	$('gy').set('text',Math.round(eventData.beta));
	$('gz').set('text',Math.round(eventData.gamma));


	moveTop = centerTop - Math.round(eventData.beta)*move_stepX*-1;
	moveLeft = centerLeft - Math.round(eventData.gamma)*move_stepY*-1;

	if (moveTop + elH > maxY){
		moveTop = maxY - elH;
		navigator.vibrate(10);
	}
	if (moveTop < 0){
		moveTop = 0;
		
		navigator.vibrate(10);
	}
	if (moveLeft + elW > maxX){
		moveLeft = maxX - elW;
		navigator.vibrate(10); 
	}
	if (moveLeft < 0){
		moveLeft = 0;
		
		navigator.vibrate(10);
	}
	test_move.setStyles({
		'top':moveTop,
		'left':moveLeft,
	});
	  //var yTilt = Math.round((-eventData.beta + 90) * (40/180) - 40);
	  var yTilt = Math.round((-eventData.beta + 90) * (40/180) - 20);

	  // Retrieve the side to side tilting of the device and move the
	  // background the opposite direction.

	  //var xTilt = Math.round(-eventData.gamma * (10/180) - 10);
	  var xTilt = Math.round(eventData.gamma * (50/180) - 10);

	  // Thi 'if' statement checks if the phone is upside down and corrects
	  // the value that is returned.
	  /*
	  if (xTilt &amp;gt; 0) {
		xTilt = -xTilt;
	  } else if (xTilt &amp;lt; -40) {
		xTilt = -(xTilt + 80);
	  }
	*/
	  var backgroundPositionValue = yTilt + 'px ' + xTilt + 'px';
	$('gp').set('text',backgroundPositionValue);

	  background.style.backgroundPosition = backgroundPositionValue;
	}, false);
	});

}