<?php
class PagesController extends AppController {
    var $components  = array('RequestHandler');
    var $name = 'Pages';
    var $uses = array('Article');


    function parseGoogle($k,$r){
        foreach ($r->address_components AS $data){
            //pr($data);
            switch($data->types[0]){
                case "neighborhood":
                    $this->gresult[$k]['street']= $data->short_name;
                    break;
                case "street_address":
                    $this->gresult[$k]['street']= $data->short_name;
                    break;
                case "route":
                    $this->gresult[$k]['street']= $data->short_name;
                    break;

                case "administrative_area_level_2":
                    if(!isset($this->gresult[$k]['city'])){
                        $this->gresult[$k]['city'] = $data->short_name;
                    }
                    break;
                case "locality":
                    if(!isset($this->gresult[$k]['city'])) {
                        $this->gresult[$k]['city'] = $data->short_name;
                    }
                    break;
                case "premise":
                    if(!isset($this->gresult[$k]['des_num'])) {
                        $this->gresult[$k]['des_num'] = $data->short_name;
                    }
                    break;

                case "street_number":
                    if(!isset($this->gresult[$k]['ori_num'])) {
                        $this->gresult[$k]['ori_num'] = $data->short_name;
                    }
                    break;
                case "postal_code":
                    if(!isset($this->gresult[$k]['code'])) {
                        $this->gresult[$k]['code'] = $data->short_name;
                    }
                    break;
            }
        }

    }
    function searchAddressWhenGoogleFail(){

    }
	
	public function loadContactForm($id=null){
		$this->set("form_id", $id);
		$this->set("title", '');
		$this->set("fastlinks", array());
		$this->loadModel('ContactForm');
		//$contactForm = $this->ContactForms->newEntity();
		$this->autoLayout = false;
		
		//$this->set(compact("contactForm"));
		$this->loadModel('ContactFormGroup');
		$form_data = $this->ContactFormGroup->find('first',array('conditions'=>array('ContactFormGroup.id'=>$id,'kos'=>0,'status'=>1)));
		//pr($form_data);	
		$this->set('form_id',$id);
		if (empty($this->data)){	
			//pr($form_data);
			$this->set('form_data',$form_data);
			$this->set('page_title','');
			$this->set('fastlinks',array());
			$this->render('../contact_forms/index');
		
			
		
		} else {
			if ($this->data['ContactForm']['spam'] != 123) die('Jste spam');
			$admin_email = $this->setting['contact_forms']['email_from'];
			$admin_name = $this->setting['contact_forms']['email_name'];
			$email_podpis = $this->setting['email_podpis'];
			$from_email = $this->data['ContactForm']['name'];
			$from_name = $this->data['ContactForm']['name'];
			
			$email_data = '<br /><table width="60%">';
				$email_data .='<tr><td>Email:</td><td>'.(!empty($this->data['ContactForm']['name'])?$this->data['ContactForm']['name']:'Nevyplněno').'</td></tr>';
				// vypis  variabilnich hodnot
				foreach ($this->data['ContactForm']['text'] AS $key=>$fd){
					if ($fd['type']==4)
						$email_data .='<tr><td>'.$fd['name'].':</td><td>'.(($fd['value']==1)?'Ano':'Ne').'</td></tr>';
					else
						$email_data .='<tr><td>'.$fd['name'].':</td><td>'.(!empty($fd['value'])?$fd['value']:'Nevyplněno').'</td></tr>';
				}
				
			$email_data .= '<table>';
			
			$this->data['ContactForm']['domena'] = '<a href="http://'.$_SERVER['HTTP_HOST'].'/">http://'.$_SERVER['HTTP_HOST'].'/</a>';
			$this->data['ContactForm']['text'] = $email_data;
			$this->data['ContactForm']['podpis'] = $email_podpis;
			
			
			
			$save_form = array(
				'name'=>$this->data['ContactForm']['name'],
				'text'=>$email_data,
				'contact_form_group_id'=>$form_data['ContactFormGroup']['id'],
			);
			$this->ContactForm->save($save_form);
			$this->ContactForm->id = null;
			
			
			if (!$this->Email->send_from_template($form_data['ContactFormGroup']['template'],$from_name,$from_email,null,array($admin_email)))
				die(json_encode(array('result'=>false,'message'=>lang_chyba_odeslani_formulare)));
				
		
		
			
		if (!empty($this->data['ContactForm']['name']) && !empty($form_data['ContactFormGroup']['autoreply'])){
			$email_list = array($this->data['ContactForm']['name']);
			$this->Email->send_from_template($form_data['ContactFormGroup']['autoreply'],$admin_name,$admin_email,null,$email_list);
		}
		 
		
		die(json_encode(array('result'=>true,'message'=>lang_formular_byl_odeslan)));
		}
	}


    function searchAddress3($type, $value = null,$id_city=null, $id_street=null){
        $result = array();

        $this->autoRender = false;


        //pr($value);

        $conditions = array();


        //pr($conditions);die();


        if ($type == 'city'){
            $col = 'id_city';
            $model = 'AddressMapTown';
            $fields = array(
                'id',
                'city',
                'id_city',


            );
            $order = 'city';
            //$city_name = $value;
            $conditions = array(
                'city LIKE' => '%'.$value.'%'
            );

        }

        if ($type == 'street'){
            $model = 'AddressMapStreet';
            $col = 'id_street';
            $fields = array(
                'id',
                'id_city',
                'street',
                'id_street',


            );
            $order = 'street';
            //$city_name = $value;
            $conditions = array(
                'street LIKE' => '%'.$value.'%',
                'id_city' => $id_city,
            );

        }

        if ($type == 'des_num'){
            $col = 'id';
            $model = 'AddressMap';
            $fields = array(
                'id',
                'id_city',
                'id_street',
                'des_num',


            );
            $order = 'des_num';
            //$city_name = $value;
            $conditions = array(
                'id_city' => $id_city,
                'id_street' => $id_street,
                'des_num LIKE' => '%'.$value.'%'
            );

        }
        //pr($conditions);
        $this->loadModel($model);
        $resultLoad = $this->$model->find('all',array(
            'conditions'=>$conditions,
            'fields'=>$fields,
            'order'=>$order.' ASC',
            'limit'=>10,
            //'group'=>$type,
        ));
        //pr($resultLoad);
        if ($resultLoad){
            $result = array();
            foreach($resultLoad AS $r){

            }
            //pr($resultLoad);
            foreach($resultLoad AS $r){

                $result[] = array(
                    'name'=>$r[$model][$type],
                    'id'=>$r[$model][$col],
                );
            }
            die(json_encode(array('r'=>true,'data'=>array('products'=>$result))));
        }
        else{
            die(json_encode(array('r'=>false,)));        }


    }



    function searchAddress2($type,$value=null,$city_id=null,$street_id=null){
        $result = array();
        //$value = $this->diacritic($value);
        //pr($value);
        //die();
        $this->autoRender = false;
        $this->loadModel('AddressMap');
        //$gap = strpos($value, ' ');
        //$comma = strpos($value, ',');

        $value = 'Czechia, '.$value;

        //pr($value);
        $prepAddr = str_replace(' ','+',$value);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyD7hl9urwJrjY3sr0cbkUhgKpPNwkrCURY');
        //pr($geocode);
        $gjson = json_decode($geocode);
        //pr($gjson);
        if(empty($gjson->results)){
            die(json_encode(array('r'=>false)));
        }
        if ($gjson->results){
            //pr($gjson->results);
            //die();
            $conditions = array();
            $conditions ['OR'] = array();//[0]['AND']
            foreach($gjson->results AS $k=>$r){

                $this->parseGoogle($k,$r);

            }
            foreach($this->gresult AS $k=>$gres){


                $conditions ['OR'][$k]=array();
                if (isset($gres['city']) && !empty($gres['city'])){
                    $conditions ['OR'][$k]['city LIKE'] = $gres['city'].'%';
                }
                if (isset($gres['street']) && !empty($gres['street'])){
                    $conditions ['OR'][$k]['street LIKE'] = $gres['street'].'%';
                }
                if (isset($gres['des_num']) && !empty($gres['des_num'])){
                    $conditions ['OR'][$k]['des_num LIKE'] = $gres['des_num'].'%';
                }
                if (isset($gres['ori_num']) && !empty($gres['ori_num'])){
                    $conditions ['OR'][$k]['ori_num LIKE'] = $gres['ori_num'].'%';
                }
                if (isset($gres['code']) && !empty($gres['code'])){

                    $conditions ['OR'][$k]['code'] = strtr($gres['code'],array(' '=>''));
                }


            }
            //pr($conditions);

        }
        $fields = array(
            'id',
            'street',
            'des_num',
            'ori_num',
            'city',
            'city_part',

        );
        if ($type == 'city'){
            $conditions = array();

        }
        if ($type == 'street'){
            $conditions = array();

        }
        if ($type == 'number'){
            $conditions = array();

        }
        //pr($conditions);die();

        $resultLoad = $this->AddressMap->find('all',array(
            'conditions'=>$conditions,
            'fields'=>$fields,
            'limit'=>10,
        ));


        //pr($conditions);
        //pr($resultLoad);
        if ($resultLoad){
            $result = array();
            foreach($resultLoad AS $r){
                //$result[] = $r['AddressMap'][];
                $result[] = array(
                    'name'=>$r['AddressMap']['street'],
                );
            }
            //pr($db);
            //pr($result);
        }
        if (!$result){
            //die(json_encode(array('r'=>false)));
            $conditions = array();
            //pr($conditions);
            // $conditions['OR'] = array('test'=>array('aa'=>array('b')));
            //pr($conditions);
            $conditions['OR'] = array(
                'city LIKE' => '%'.$value.'%',
                'city_part LIKE' => '%'.$value.'%',
                'street LIKE' => '%'.$value.'%',
                'des_num LIKE' => '%'.$value.'%',
                'code LIKE' => $value.'%',
            );
            $fields = array(
                'id',
                'street',
                'des_num',
                'ori_num',
                'city',
                'city_part',

            );
            //pr($conditions);die();

            $resultLoad = $this->AddressMap->find('all',array(
                'conditions'=>$conditions,
                'fields'=>$fields,
                'limit'=>10,
            ));
        } else {
            die(json_encode(array('r'=>true,'data'=>array('products'=>$result))));

        }

    }

    function searchAddress($value=null){
        $result = array();
        $value = $this->diacritic($value);
        //pr($value);
        //die();
        $this->autoRender = false;
        $this->loadModel('AddressMap');
        //$gap = strpos($value, ' ');
        //$comma = strpos($value, ',');
        //pr($value);
        //pr($comma);
        /*if((strlen($value) < 6) || ((strlen($value) < 6) && (($gap === false) || ($comma === false)))){
            pr('Menší než šest a nebo menší než 6 s mezerou nebo čárkou');
            $conditions = array();
            /*pr($conditions);
            $conditions['OR'] = array('test'=>array('aa'=>array('b')));
            pr($conditions);*/
        /* $conditions['OR'] = array(
             'city LIKE' => '%'.$value.'%',
             'city_part LIKE' => '%'.$value.'%',
             'street LIKE' => '%'.$value.'%',
             'des_num LIKE' => '%'.$value.'%',
             'code LIKE' => $value.'%',
         );
     //pr($conditions);

     }*/
        //pr('Google');
        $value = 'Czechia, '.$value;
        //pr($value);
        //$prepAddr = $value;
        $prepAddr = str_replace(' ','+',$value);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyD7hl9urwJrjY3sr0cbkUhgKpPNwkrCURY');
        //pr($geocode);
        $gjson = json_decode($geocode);
        //pr($gjson);
        if(empty($gjson->results)){
            die(json_encode(array('r'=>false)));
        }
        if ($gjson->results){
            //pr($gjson->results);
            //die();
            $conditions = array();
            $conditions ['OR'] = array();//[0]['AND']
            foreach($gjson->results AS $k=>$r){

                $this->parseGoogle($k,$r);

            }
            foreach($this->gresult AS $k=>$gres){


                $conditions ['OR'][$k]=array();
                if (isset($gres['city']) && !empty($gres['city'])){
                    $conditions ['OR'][$k]['city LIKE'] = $gres['city'].'%';
                }
                /*if (isset($gres['street']) && !empty($gres['street'])){
                    $conditions ['OR'][$k]['street LIKE'] = $gres['street'].'%';
                    $conditions ['OR'][$k]['city_part LIKE'] = $gres['street'].'%';
                }
                if (isset($gres['des_num']) && !empty($gres['des_num'])){
                    $conditions ['OR'][$k]['des_num LIKE'] = $gres['des_num'].'%';
                }
                if (isset($gres['ori_num']) && !empty($gres['ori_num'])){
                    $conditions ['OR'][$k]['ori_num LIKE'] = $gres['ori_num'].'%';
                }
                if (isset($gres['code']) && !empty($gres['code'])){

                    $conditions ['OR'][$k]['code'] = strtr($gres['code'],array(' '=>''));
                }*/


            }
            // pr($conditions);

        }

        $fields = array(
            'id',
            'street',
            'des_num',
            'ori_num',
            'city',
            'city_part',

        );
        //pr($conditions);

        $resultLoad = $this->AddressMap->find('all',array(
            'conditions'=>$conditions,
            'fields'=>$fields,
            'limit'=>10,
        ));


        //pr($conditions);
        //die();
        //pr($resultLoad); 
        if ($resultLoad){
            $result = array();
            foreach($resultLoad AS $r){
                //pr($r);
                //$result[] = $r['AddressMap'][];
                $result[] = array(
                    'name'=>$r['AddressMap']['city'],
                    'id'=>$r['AddressMap']['id'],
                );
                //ú . ', ' . $r['AddressMap']['street'],
            }
            //pr($db);
            //pr($result);
        }
        if (!$result){
            die(json_encode(array('r'=>false)));
        } else {
            die(json_encode(array('r'=>true,'data'=>array('products'=>$result))));

        }

    }
    function diacritic($output){
        $replace = array(
            'ě'=>'e',
            'š'=>'s',
            'č'=>'c',
            'ř'=>'r',
            'ž'=>'z',
            'ý'=>'y',
            'á'=>'a',
            'í'=>'i',
            'é'=>'e',
            'ť'=>'t', 
            'ď'=>'d',
            'ň'=>'n',

        );
 
        $output = strtr($output,$replace);
        return $output;
    } 
    function importCities_Filip(){
        $this->autoRender = false;
        $this->loadModel('AddressMap');
        $current_step = file_get_contents("uploaded/log_city.txt");
        if ($current_step == -1){
            die('jiz nacteno');
        }

        //$data =  $this->AddressMap->find()->limit(1000)->page($current_step)->order(['city'=>'ASC']);
        $data = $this->AddressMap->find('all',
            array(
                //'conditions'=>array('Article.spec_type'=>1),
                'fields'=>array(
                    '*',
                ),
                'limit'=>1000,
                'page'=>$current_step,
                'order'=>'city ASC',
            ));
        pr($data);

        $cities = array();
        foreach($data as $item){
            $cities[$item['AddressMap']['city']];
        }

    }
    function importCities($saveData){
        $this->autoRender = false;
        /*
		$this->loadModel('AddressMap');


        $data = $this->AddressMap->find('all',
            array(
                //'conditions'=>array('Article.spec_type'=>1),
                'fields'=>array(
                    'AddressMap.city',
                    'AddressMap.id_city',
                ),
                //'limit'=>10,
                'order'=>'city ASC',
                'group'=>'city',

            ));
        //pr($data);
		*/
        $cities = array(
            'city' => '',
            'id_city' => '',
        );
        //pr($data); die();

        $this->loadModel('AddressMapTown'); //ToDO

        //pr($data); die();
        foreach($data as $item){
            //pr( $cities[$item['AddressMap']['city']]);
            //pr($item);
            //die();

            $already_exist = $this->AddressMapTown->find('first',
                array(
                    'conditions'=>array('AddressMapTown.id_city'=>$item['AddressMap']['id_city']),// todo saveData
                    'fields'=>array(
                        'AddressMapTown.id_city',
                    ),

                ));
            //pr($already_exist);
            $cities['city'] = $item['AddressMap']['city']; //ToDO
            //pr($item);
            $cities['id_city'] = $item['AddressMap']['id_city']; //ToDO

            if(empty($already_exist)){
                $this->AddressMapTown->save($cities); //ToDO
            }


            $cities = array(
                'city' => '',
                'id_city' => '',
            );
        }


    }

    function importStreets(){
        $this->autoRender = false;
        $this->loadModel('AddressMap');

        $current_step = file_get_contents("uploaded/log_street.txt");
        if ($current_step == -1){
            die('jiz nacteno');
        }
        $offset = 1000;

        $data = $this->AddressMap->find('all',
            array(
                'fields'=>array(
                    'AddressMap.street',
                    'AddressMap.id_street',
                    'AddressMap.id_city',
                ),

                'limit'=> $offset,
                'offset'=> $current_step,
                'order'=>'id_poda ASC',
                'group'=>'street',

            ));

        pr($offset);
        pr($current_step);
        $streets = array(
            'street' => '',
            'id_street' => '',
            'id_city' => '',
        );
        //pr($data); die();
        $this->loadModel('AddressMapStreet'); //ToDO

        //pr($data); die();
        foreach($data as $item){
            //pr( $cities[$item['AddressMap']['city']]);
            //pr($item);
            //die();

            $already_exist = $this->AddressMapStreet->find('first',
                array(
                    'conditions'=>array('AddressMapStreet.id_street'=>$item['AddressMap']['id_street']),
                    'fields'=>array(
                        'AddressMapStreet.id_street',
                    ),

                ));

            // K RUZNYM MESTUM MOHOU BYT STEJNE ADRESY - JINE ID
            //pr($already_exist);
            $streets['street'] = $item['AddressMap']['street']; //ToDO

            $streets['id_street'] = $item['AddressMap']['id_street']; //ToDO
            $streets['id_city'] = $item['AddressMap']['id_city']; //ToDO

            if(empty($already_exist)){
                $this->AddressMapStreet->save($streets); //ToDO
                $save_step = file_put_contents("uploaded/log_street.txt",$current_step+1);

            }
            /*else {
                $save_step = file_put_contents("uploaded/log_street.txt",$current_step);

            }*/
            $save_step = file_put_contents("uploaded/log_street.txt",$current_step+1);


            $streets = array(
                'street' => '',
                'id_street' => '',
                'id_city' => '',
            );
        }


    }
    function importCodes(){}
    function importAddress(){
	    
	    die();
        //start number = 324600
        //die();
        $this->autoRender = false;
        $this->loadModel('AddressMap');

        $current_step = file_get_contents("uploaded/log.txt");
        /*if ($current_step == -1){
            die('jiz nacteno');
        }*/
        $offset = 10;
        //$current_step = 5000000;
        //pr($current_step);
        $url = "https://intra.poda.cz/web_api/api.php?adr_list=".$current_step.",".$offset;
        //pr($url);
		$external = file_get_contents($url, "r");
        $csvData = str_getcsv($external, "\n");
        unset($csvData[0]);
        require_once('converter.php');
        $converter = new JTSK_Converter();
		
        if (!empty($csvData)){
            foreach($csvData as &$Row){

                $row = str_getcsv($Row, ",");
                //pr($row);
                $saveData = array(
                    'id_poda'=>$row[0],
                    'district'=>$row[1],
                    'code'=>$row[2],
                    'city'=>$row[3],
                    'city_part'=>$row[4],
                    'city_district'=>$row[5],
                    'street'=>$row[6],
                    'des_num_type'=>$row[7],
                    'des_num'=>$row[8],
                    'ori_num'=>$row[9],
                    'ori_char'=>$row[10],
                    'jtsk_x'=>$row[11],
                    'jtsk_y'=>$row[12],
                    // pripraveno
                    'id_city'=>$row[13],
                    'id_city_part'=>$row[14],
                    'id_city_district'=>$row[15],
                    'id_street'=>$row[16],

                    'lat'=>'',
                    'lng'=>'',
                );


                $con = $converter->JTSKtoWGS84($row[11],$row[12]);
                //pr($con);
                if (!empty($con)){
                    $saveData['lat'] = $con['lat'];
                    $saveData['lng'] = $con['lon'];
                }

                $address = $row[6].' '.$row[8].(!empty($row[9])?'/'.$row[9]:'').' '.$row[3]; // Google HQ
                $prepAddr = str_replace(' ','+',$address);
                $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyD7hl9urwJrjY3sr0cbkUhgKpPNwkrCURY');
                $output = json_decode($geocode);
                //pr($output);
                //die();
                if (isset($output->results[0]) && !empty($output->results[0])){
                    $lat = $output->results[0]->geometry->location->lat;
                    $lng = $output->results[0]->geometry->location->lng;
                    $saveData['glat'] = $lat;
                    $saveData['glng'] = $lng;
                }
                else{
                    die();
                }
                $this->importCities($saveData);
                $this->AddressMap->save($saveData);
                $this->AddressMap->id = null;
                //pr($saveData);die();
                // tady doplnit ulozeni
            }
            $save_step = file_put_contents("uploaded/log.txt",$current_step+$offset);

		
        } else {
            //$save_step = file_put_contents("uploaded/log.txt",-1);

        }
        die('a');
        
		$this->importCities();
        $this->importStreets();

    }
    function clear_cache(){
        clearCache();
        $files = scandir('./app/tmp/cache');
        foreach($files AS $f){
            if (strpos($f,'cake_') !== false){
                unlink('./app/tmp/cache/'.$f);

            }
        }
        die();
    }

    function searchService(){
        $ch = curl_init();
        if (!empty($_POST)){
            $this->data = $_POST;
        }
        //pr($_POST);
        $post = array(
            'obec'=>$this->data['locality'],
            'mcast'=>$this->data['sublocality_level_1'],
            'ulice'=>$this->data['street'],
            'cp'=>$this->data['cp'],
            'co'=>$this->data['co'],
            'lat'=>$this->data['lat'],
            'lng'=>$this->data['lng'],
            'psc'=>$this->data['postal_code'],

        );
        //$post = array();
        $params = http_build_query($post);
        //pr($params);die();
        $apiUrl = 'https://intra.poda.cz/web_api/api.php?';
        $apiUrl .= $params;

        //pr($apiUrl);die('aaa');
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        //curl_setopt($ch, CURLOPT_POST, 1);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $result = curl_exec ($ch);
        $result = json_decode($result);
        //pr($result[1]->reply);
        if($result[1]->reply=="n/a"){
            die(json_encode(array('result'=>false, 'message'=>'V dané oblasti nenabízíme žádné služby.')));
        }
        else{
            $data = explode(',', $result[1]->reply);
            $return = array('result'=>true, 'data' => $data);
            die(json_encode($return));
        }


        curl_close ($ch);


        //pr($post);
        //pr($result);


        die(json_encode(array('result'=>true)));
    }

    function homepage(){
	    //$this->loadModel('Actual');
	    //$actuals = $this->Actual->find('all', array('conditions'=>array('status'=>1,'kos'=>0),'order'=>'created desc','limit'=>2));
	    
	    //$this->set('actuals', $actuals);
        $this->set('page_caption','Unikátní balíčky služeb pro domácnosti - internet, televize, telefon');
        //$this->render('homepage');
    }
    
    function sluzby(){
        $this->set('page_caption','Unikátní balíčky služeb pro domácnosti - internet, televize, telefon');
        $this->set('fastlinks', array('Nabídka TV služeb'=>'#'));
        //$this->set('fstlinksNoBg', true);
    }
    
    function nabidka_tarifu(){
        $this->set('page_caption','Unikátní balíčky služeb pro domácnosti - internet, televize, telefon');
		$this->set('fastlinks', array('Nabídka tarifů'=>'#'));
    }
    
    function internet(){
        $this->set('page_caption','Nabídka internetů');
		$this->set('fastlinks', array('Nabídka internetů'=>'#'));
    }
    
    function mapa(){
        $this->set('page_caption','Unikátní balíčky služeb pro domácnosti - internet, televize, telefon');
    }
    

    function cenik(){
        $this->set('page_caption','Ceník');
        $this->set('fastlinks', array('Ceník'=>'#'));
    }
    
    function podpora(){
        $this->set('page_caption','Podpora');
        $this->set('fastlinks', array('Podpora'=>'#'));
    }
    
    function objednat(){
        $this->set('page_caption','Nezávazná objednávka');
        $this->set('fastlinks', array('Nezávazná objednávka'=>'#'));
    } 

    function dokumenty_ke_stazeni(){
        $this->set('page_caption','Dokumenty ke stažení');
        $this->set('fastlinks', array('Dokumenty ke stažení'=>'#'));
    }

    function jak_prejit(){
        $this->set('page_caption','Jak přejít');
        $this->set('fastlinks', array('Jak přejít'=>'#'));
    }
    
    function kontakty(){
        $this->set('page_caption','Kontakty');
        $this->set('fastlinks', array('Kontakty'=>'#'));
    }

    function seznam_programu() {
	    $this->set('page_caption','Seznam TV programů');
        $this->set('fastlinks', array('TV služby'=>'tv-sluzby/','Seznam TV programů'=>'#'));
    }

    function onas(){
        $this->set('page_caption','O nás');
        $this->set('fastlinks', array('O nás'=>'#'));
    }
    
    function ekonomicke_ukazatele(){
        $this->set('page_caption','Ekonomické údaje');
        $this->set('fastlinks', array('Ekonomické údaje'=>'#'));
    }

    function podanet_tv() {
	    $this->set('page_caption','PODA net.tv');
        $this->set('fastlinks', array('TV služby'=>'tv-sluzby/','PODA net.tv'=>'#'));
    }
    
    function obchodnici(){
        $this->set('page_caption','Obchodníci');
        $this->set('fastlinks', array('Kontakty'=>'kontakty/','Domácí obchodníci'=>'#'));
    }
    
    function chytra_tv(){
        $this->set('page_caption','Chytré funkce TV');
        $this->set('fastlinks', array('TV služby'=>'tv-sluzby/','Chytré funkce TV'=>'#'));
    }

    function mereni_rychlosti_internetu(){
        $this->set('page_caption','Měření rychlosti internetu');
        $this->set('fastlinks', array('Dokumenty ke stažení'=>'dokumenty-ke-stazeni/','Měření rychlosti internetu'=>'#'));
    }
    
    function firemni_sluzby(){
        $this->set('page_caption','Firemní služby');
        $this->set('fastlinks', array('Kontakty'=>'kontakty/','Firemní služby'=>'#'));
    }
    
    function proc_poda(){
        $this->set('page_caption','Proč právě Poda?');
        $this->set('fastlinks', array('Proč právě Poda?'=>'#'));
    }
    
    function chci_podu() {
	    $this->set('page_caption','Poda služby');
        $this->set('fastlinks', array('Poda služby'=>'#'));
        $_SESSION['mesto'] = $_POST['mesto'];
        $_SESSION['ulice'] = $_POST['ulice'];
        $_SESSION['cp'] = $_POST['cp'];
        //pr($_SESSION);
    }
    
    function examples($type){

        $this->set('page_caption','');

        if ($type=="gallery"){
            $this->render('example_gallery');
        }
        else if ($type=='carousel'){

            $this->loadModel('Carousel');

            $carouselData= $this->Carousel->read(null, 1);
            //pr($carouselData); die();
            $this->set('carouselData', $carouselData);

            $this->render('example_carousel');

        }
        else if ($type=='modal'){

            $this->render('example_modal');
        }
    }




    function isUnique($model,$field,$value='',$id=null) {
        if (!isset($this->$model)){
            $this->loadModel($model); $this->$model =& new $model();
        }

        $condition = array($model.'.'.$field=>$value,$model.'.status'=>1,$model.'.kos'=>0);
        if ($id != null) $condition['id'] = '!='.$id;

        if($this->$model->findCount($condition))
            die(json_encode(array('return'=>false)));
        else
            die(json_encode(array('return'=>true)));
    }

    function smallbox($id) {
        $this->loadModel('Smallbox');

        $smallbox_list = $this->Smallbox->find('first',array('conditions'=>array('Smallbox.status'=>1,'Smallbox.kos'=>0,'Smallbox.id'=>$id),'fields'=>array('name','text','id')));


        $this->set('smallbox',$smallbox_list);
    }


    function fst_upload($delete = null){
        $fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
        //pr($_FILES);
        if (isset($this->params['url']['params'])){
            $params = json_decode(base64_decode($this->params['url']['params']));
        }

        if ($delete != null){
            $setting = array(
                'file'=>$params->file,
                'upload_path'=>$params->upload_path,
            );
            $this->Upload->delete_file($setting);


        } else if (isset($_FILES['file']['type'])){
            $setting = array(
                'fn'=>$fn,
                'files'=>$_FILES,
                'filename'=>$_FILES['file']['name'][0],
                'tmp_name'=>$_FILES['file']['tmp_name'][0],
                'type'=>$_FILES['file']['type'][0],
                'file_ext'=>$params->file_ext,
                'count_file'=>$params->count_file,
                'upload_path'=>$params->upload_path,
            );
            $this->Upload->doit($setting);
        } else {
            die(json_encode(array('result'=>false,'message'=>'Chyba nahrání souboru')));

        }

    }

}
?>